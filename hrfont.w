@i format.w
@i types.w

@** Hardly Rendering Fonts. Based closely on the rendering code from
freetype-gl. This module bridges Xft and the FreeType library in a
minimal API.

@!@^freetype-gl@>
@c
#include "hrfont.h"
#include "hrfont-xft.h"
@#
#include <assert.h>
#include <err.h>
@#
#include "log.h"
#undef LOG_PREFIX
#define LOG_PREFIX "font"

@<Preprocessor trickery (\.{hrfont.o})@>@;
@<Special error handling (\.{hrfont.o})@>@;
@<Type definitions (\.{hrfont.o})@>@;
@<Global variables (\.{hrfont.o})@>@;
@<Function declarations (\.{hrfont.o})@>@;

@ FreeType is an Interesting library to say the least. Here is how
its headers are included. For some reason we still care about DOS
and CP/M {\it and\/} in 50 years only MICROS\~{}1 have thought to
overcome the increasingly ridiculous 8.3 filename length limit.

@(hrfont.h@>=
#ifndef HARDLY_FONTS_H
#define HARDLY_FONTS_H

#include <stdbool.h>
#include <stdint.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_LCD_FILTER_H
#include FT_MODULE_H
#include FT_SIZES_H
#include FT_STROKER_H
#include FT_TRUETYPE_TABLES_H

typedef struct hrf_font hrf_font;
typedef struct hrf_stroker hrf_stroker;
typedef struct hrf_metrics hrf_metrics;

@<Public API (\.{hrfont.o})@>@;
#endif /* |HARDLY_FONTS_H| */

@ @(hrfont-xft.h@>=
#ifndef HARDLY_XFT_H
#define HARDLY_XFT_H
#include <X11/Xft/Xft.h>
@<XFT API (\.{hrfont.o})@>@;
#endif /* |HARDLY_XFT_H| */

@ The only useful function currently in this font library wrapper
is this |hrf_stroke| which renders a single glyph to a bitmap.

@c
int
hrf_stroke (hrf_font    *font,
            hrf_stroker *cast)
{
        FT_Bitmap *b;
        FT_Face f;
        FT_Glyph glyph;
        FT_Size old_size;
        FT_Stroker stroker = 0;
        FT_Error e = 0;

        if ((!font && cast != &Builtin) || (font && cast == &Builtin))
                goto fail_args;
        if (!font)
                return _hrf_builtin_stroke();
        assert(HRF_Library_Handle);
        assert(font->face);

        @<Activate the stroker@>@;
        @<Configure the glyph renderer@>@;
        @<Render the glyph@>@;
        @<Save the glyph metrics@>@;

fail: /* And general cleanup. */
        if (stroker)
                FT_Stroker_Done(stroker);
        if (old_size)
                FT_Activate_Size(old_size);
        if (!e)
                return 0;
fail_args:
        hrf_error(e, "hrf_stroke");
        return -1;
}

@ Multiple font sizes may be loaded in FreeType at a time. To use
a particular size it must be activated with |FT_Activate_Size|.
Unfortunately Xft expects to use FreeType exclusively and |XftLockFace|
and |XftUnlockFace| are reference counters to ensure the object
isn't freed prematurely and don't enforce exclusive access.

The freetype documentation implies, obliquely, that locking is no
longer necessary.

When a size is activated the previously active font size is saved
in |old_size| and restored before returning the object to Xft.

@d HRES 64.0f
@<Activate the stroker@>=
f = font->face;
old_size = f->size;
if ((e = FT_Activate_Size(cast->size)))
        goto fail;
if ((e = FT_Stroker_New(HRF_Library_Handle, &stroker)))
        goto fail;
FT_Stroker_Set(stroker, font->outline_thickness * HRES,
        FT_STROKER_LINECAP_ROUND, FT_STROKER_LINEJOIN_ROUND, 0);

@ The different rendering styles features is not used so this just
locates the glyph at the correct size in the font |f| and saves it
in |glyph|.

@d RENDER_NORMAL                0
@d RENDER_OUTLINE_EDGE          1
@d RENDER_OUTLINE_POSITIVE      2
@d RENDER_OUTLINE_NEGATIVE      3
@<Configure the glyph renderer@>=
if ((e = FT_Get_Glyph(f->glyph, &glyph)))
        goto fail;
switch (font->render_mode) {
case RENDER_OUTLINE_EDGE:
        e = FT_Glyph_Stroke(&glyph, stroker, 1);@+
        break;
case RENDER_OUTLINE_POSITIVE:
        e = FT_Glyph_StrokeBorder(&glyph, stroker, 0, 1);@+
        break;
case RENDER_OUTLINE_NEGATIVE:
        e = FT_Glyph_StrokeBorder(&glyph, stroker, 1, 1);@+
        break;
}
if (e)
        goto fail;

@ This code originally supported rendering fonts with support for
LCD-subpixel but this has been possibly-temporarily disabled.

@<Render the glyph@>=
#if 0
if (cast->font->render_depth == 3)
        e = FT_Glyph_To_Bitmap(&glyph, FT_RENDER_MODE_LCD, 0, 1);
else
#endif
        e = FT_Glyph_To_Bitmap(&glyph, FT_RENDER_MODE_NORMAL, 0, 1);
if (e)
        goto fail;

@ If the glyph was rendered successfully the pointer to the result
is saved (as a |FT_BitmapGlyph|, which |FT_Glyph| is synonymous
with). Note that this pointer is to memory owned by FreeType and
may be re-used for other glyphs.

The metrics, which are numbers instructing how to draw the bitmap
in relation to other glyphs, are saved in |metrics|. These are the
width and height of the bitmap, the offset of the bitmap from where
the ``pen'' was left from drawing the previous glyph (ie.~the
baseline) and where to move the pen after this glyph.

Coloured fonts and others with a fixed size draw to a scale of
pixels rather than the ``Scaled Point Integer'' of other fonts. In
practice this means there are now 3 standards: single (albeit
generally tiny) pixels and 26.6 and 16.16 scaled point integer
subpixels.

@<Save the glyph metrics@>=
assert(glyph->format == FT_GLYPH_FORMAT_BITMAP);
cast->glyph = (FT_BitmapGlyph) glyph;
b = &cast->glyph->bitmap;
cast->metrics.width = b->width;
cast->metrics.height = b->rows; /* Plus padding */
cast->metrics.offset_x = cast->glyph->left;
cast->metrics.offset_y = cast->glyph->top;
// why are these from f->glyph (GlyphSlot) not cast->glyph (BitmapGlyph)?
if (FT_HAS_FIXED_SIZES(f)) { /* Colour fonts use actual pixels
                                not subpixels. */
        cast->metrics.advance_rl = f->glyph->advance.x;
        cast->metrics.advance_du = f->glyph->advance.y;
} else {
        cast->metrics.advance_rl = sp6tof(f->glyph->advance.x);
        cast->metrics.advance_du = sp6tof(f->glyph->advance.y);
}

@ That's it for the core of the rendering process but before moving
on to the supporting functions here's a brief interlude to discuss
integers and subpixels.

Drawing operations which demand pixel-perfect accuracy will work
in units smaller than pixels. For example OpenGL uses floating point
values for nearly everything. In the days the font rendering
techniques were developed, and still for some operations which
require perfect or reproducable accuracy or simply if you only have
integer ALUs, floating point hardware was inappropriate.

Subpixel operations worked with scaled point fractional integers,
which are simply a redefinition of the bits in a word to interpret
$n$ low bits as a fraction. In other words the ``bicipal point'',
which is invisibly after the lowest bit like a decimal integer has
an implicit decimal point, is moved up $n$ bits.

The macros here {\it convert\/}, not cast, between scaled point
fractional integers and floating point (\.f: |float| or \.d: |double|)
representation. The number in the macro name is the number of low
bits to treat as fractional. They make no effort to deal with words
larger (or smaller) than 32 bits.

@<Pub...@>=
#define dtosp6(V)  ((long long) ((V) * 64.0))
#define dtosp16(V) ((long long) ((V) * 65536.0))
#define ftosp6(V)  ((long long) ((V) * 64.0f))
#define ftosp16(V) ((long long) ((V) * 65536.0f))
#define sp6tod(V)  (((double) (V)) / 64.0)
#define sp16tod(V) (((double) (V)) / 65536.0)
#define sp6tof(V)  (((float) (V)) / 64.0f)
#define sp16tof(V) (((float) (V)) / 65536.0f)

@ FreeType has a unique way of saving 2 KB (2209 bytes) from the
1\frac1/2 MB (1,517,268 bytes) shared library: put the error message
strings in the application. It does it using macros.

Error symbols are defined by a list of \.{FT\_ERRORDEF\_...} symbols
and one \.{FT\_NOERRORDEF\_...}. These call the macro |FT_ERRORDEF|,
the latter {\it without\/} adding |FT_ERR_BASE| to its second
argument, which prefixes the first (the symbol) with the string
\.{FT\_Err\_}.

Normally the string values in the third argument are discarded. We
want these precious error messages so we do this.

@<Special...@>=
#undef FTERRORS_H_ /* Set the first time |FT_ERRORS_H| was included. */
#define FT_ERROR_START_LIST @t\iII@>    static char *dumberr[FT_Err_Max] = {
#define FT_ERRORDEF(E,V,S)  @t\iIV@>             [E] = (S),
#define FT_ERROR_END_LIST   @t\iIII@>   };
@#
#include FT_ERRORS_H

#define hrf_error(E,M) @[                                     \
        fcomplain("%s @@ %s:%d: %s", (M), __FILE__, __LINE__, \
                ((E) >= 0) ? dumberr[E] : "") @]
#define hrf_abort(E,M) do {                                   \
        fcomplain("%s @@ %s:%d: %s", (M), __FILE__, __LINE__, \
                ((E) >= 0) ? dumberr[E] : "");                \
        abort();                                              \
} while (0)

@ FreeType deals with multi threading and shared data by not dealing
with it. Every FreeType user instantiates a new library object and
nothing can be shared between them. This is especially a problem
when using it with another library that doesn't feel the need to
play nicely with others: Xft.

In case this wrapper library will be used in a threaded application
the FreeType library reference is a thread local variable (if they're
supported) and must be re-initialised in any new thread that uses it.

Although the FreeType documentation suggests that a single library
instance can be supported across multiple threads by protecting
|FT_New_Face| and |FT_Done_Face| (and the code between them?) with
a mutex, further on in the same documentation the optimism recedes
and the recommended way to support multi-threading is with a separate
library instance per thread.

As with many, but not all, freetype type definitions, |FT_Library|
is a {\it pointer\/} type.

@<Preprocessor...@>=
#if defined(__GNUC__) || defined(__clang__)
#define thread_local __thread
#elif defined(_MSC_VER)
#define thread_local __declspec(thread)
#else
#define thread_local
#endif

@ @<Global...@>=
thread_local FT_Library HRF_Library_Handle = NULL;

@ After creating a new thred |HRF_Library_Handle| will have a value
that's cleared prior to calling |FT_Init_FreeType|, which will set
it again.

This wrapper library was written for Turtle which uses Xft to find
fonts, and that has already loaded and initialised FreeType itself,
so |hrf_init_thread| is of no use to this wrapper's only user...

@c
int
hrflib_init_thread (void)
{
        FT_Error e;

        HRF_Library_Handle = NULL; /* Was set by the parent thread. */
        if ((e = FT_Init_FreeType(&HRF_Library_Handle))) {
                hrf_error(e, "FT_Init_FreeType");
                return -1;
        }
        assert(HRF_Library_Handle);

        return 0;
}

@ @c
void
hrflib_destroy_thread (void)
{
        /* assert(no active fonts) */
        FT_Done_FreeType(HRF_Library_Handle);
        HRF_Library_Handle = NULL;
}

@ When not using threads.

@c
int
hrflib_init (FT_Library lib)
{
        assert(HRF_Library_Handle == NULL);
        if (!lib)
                return hrflib_init_thread();
        else
                HRF_Library_Handle = lib;
        return 0;
}

@ @c
void
hrflib_destroy (void)
{
        hrflib_destroy_thread();
}

@ This is what the object representing a font looks like. If a
|FT_Face| has been obtained from Xft then a pointer to Xft's font
is saved and the face is ``locked''.

Hooks to enable disabling [sic] hinting and enabling LCD subpixel
rendering are still here in case I decide to bring them back fully.
The inner struct (union?) would made more sense if there were other
sources than Xft to find the font in.

@d FONT_HINTING   0
@d FONT_LCDFILTER 1
@<Type def...@>=
typedef struct hrf_font {
        struct {
                XftFont *xft;
                Display *xdisplay;
                int      xscreen;
        } src;
        FT_Face  face;
        float    outline_thickness;
        uint16_t render_mode;
        uint16_t flags;
} hrf_font;

@ Initialising this object prepares the memory space but doesn't
assign a font face yet.

@c
hrf_font *
hrf_alloc_font (void)
{
        hrf_font *rval;

        assert(HRF_Library_Handle);
        rval = calloc(1, sizeof (hrf_font));
        if (!rval)
                return NULL;
        rval->render_mode = RENDER_NORMAL;
#if 0
        SET_FLAG(rval->flags, FONT_HINTING);
#endif
        return rval;
}

@ @c
void
hrf_destroy_font (hrf_font *f)
{
        if (!f)
                return;
        assert(f->face);
        if (f->src.xft) {
                XftUnlockFace(f->src.xft);
                f->src.xft = NULL;
        }
        FT_Done_Face(f->face); /* Also frees size objects. */
        f->face = 0;
        free(f);
}

@ After a font has been located by Xft, which is currently the only
supported method, it is searched for a unicode character map.

@c
int
hrf_load_xft (hrf_font *f,
              XftFont  *xf,
              Display  *d,
              int       s)
{
        FT_Error e;
        FT_Face  face;

        assert(!f->face);
        assert(!f->src.xft);

        face = XftLockFace(xf);
        FT_Reference_Face(face);
        assert(HRF_Library_Handle == face->glyph->library);
        if (!face) {
                fcomplain("hrf_load_xft");
                return -1;
        }
        if ((e = FT_Select_Charmap(face, FT_ENCODING_UNICODE))) {
                FT_Done_Face(face);
                XftUnlockFace(xf);
                hrf_error(e, "no unicode character map");
                return -1;
        }
        f->src.xft = xf;
        f->src.xdisplay = d;
        f->src.xscreen = s;
        f->face = face;
        return 0;
}

@ Within the (non-thread-safe) face object are multiple sizes, any
one of which may be activated after creation with |FT_Activate_Size|,
which sounds suspiciously like global state.

@c
hrf_stroker *
hrf_alloc_stroker (hrf_font *font,
                   double    fsize)
{
        FT_Error e;
        FT_Size old_size;
        hrf_stroker *new;

        if (!font)
                return &Builtin;

        assert(HRF_Library_Handle);
        assert(font->face);

        new = calloc(1, sizeof (hrf_stroker));
        if (!new)
                return NULL;
        new->size_pt = fsize;
        new->scale = 1;
        old_size = font->face->size;
        if ((e = FT_New_Size(font->face, &new->size))) {
                hrf_error(e, "FT_New_Size");
                new = NULL;
                goto done;
        }

        if ((e = FT_Activate_Size(new->size))) {
                hrf_error(e, "FT_Activate_Size");
                FT_Done_Size(new->size);
                new = NULL;
                goto done;
        }

        if (FT_HAS_FIXED_SIZES(font->face)) {
                @<Look for a prepared glyph@>
        }

#if 0 /* Seems to be how to render in points. */
        if ((e = FT_Set_Char_Size(font->face, ftosp6(fsize), 0, DPI, DPI)))
                ; /* |DPI| was 72 */
#endif
        if ((e = FT_Set_Pixel_Sizes(font->face, 0, fsize))) {
                hrf_error(e, "FT_Set_Char_Size");
                new = NULL;
                goto done;
        }

done:
        if (old_size)
                FT_Activate_Size(old_size);
        return new;
}

@ Some fonts have pre-drawn glyphs at specific sizes. If this is
one of them then look for one and return it. TODO: Test this.

@.TODO@>
@<Look for a prepared glyph@>=
int match = -1;
float diff = 1e20;
for (int i = 0; i < font->face->num_fixed_sizes; i++) {
        float nsize = sp6tof(font->face->available_sizes[i].size);
        float ndiff = fsize > nsize ? fsize / nsize : nsize / fsize;
        if (ndiff < diff) {
                match = i;
                diff = ndiff;
        }
}
if (match >= 0) { /* and diff is small enough? */
        if ((e = FT_Select_Size(font->face, match))) {
                hrf_error(e, "FT_Select_Size");
                new = NULL;
                goto done;
        }
        new->scale = sp6tof(font->face->available_sizes[match].size);
        new->scale = fsize / new->scale;
        finform("scale %f", new->scale);
        goto done;
}

@ @c
void
hrf_destroy_stroker (hrf_stroker *cast)
{
        if (!cast || cast == &Builtin)
                return;
        if (cast->glyph) {
                FT_Done_Glyph((FT_Glyph) cast->glyph);
                cast->glyph = NULL;
        }
        if (cast->size) {
                FT_Done_Size(cast->size);
                cast->size = NULL;
        }
}

@ A stroker can be used to render many glyphs (of the same size)
from the same font. Each glyph is started with |hrf_start_glyph|
which searches in the font for a representation of a code point.

For |FT_Get_Char_Index| FreeType has swapped the glyph in position
0 with the ``not defined'' glyph, if it wasn't already first, so
that a return value of 0 always means the code point was the
glyph-is-not-present glyph in the character map (TODO: if there is
one?).

This is the first error-returning function which can return a value
other than 0 (success) or -1 (general failure): -2 indicates that
the font didn't contain a glyph for the requested code point. In
this case if |missing| is true then the missing-glyph is returned.

TODO: LCD hinting is forcibly disabled here (as well).

@.TODO@>
@c
int
hrf_start_glyph (hrf_font    *font,
                 hrf_stroker *cast,
                 uint32_t     cp,
                 bool         missing)
{
        FT_Error e;
        FT_UInt index;
        FT_Size old_size;
        FT_Int32 flags;
        int rval = -1;

        if ((!font && cast != &Builtin) || (font && cast == &Builtin))
                goto fail;
        if (!font)
                return _hrf_builtin_glyph(cp, missing);
        assert(HRF_Library_Handle);
        assert(cast->size);
        assert(font->face);
        if (cast->glyph) {
                FT_Done_Glyph((FT_Glyph) cast->glyph);
                cast->glyph = NULL;
        }
        old_size = font->face->size;
        if ((e = FT_Activate_Size(cast->size))) {
                hrf_error(e, "FT_Activate_Size");
                goto fail;
        }

        if ((index = FT_Get_Char_Index(font->face, cp)) || !cp)
                rval = 0;
        else {
                rval = -2;
                if (!missing)
                        goto fail;
        }

        flags = 0;
        if (font->render_mode != RENDER_NORMAL)
                flags |= FT_LOAD_NO_BITMAP;
        else
                flags |= FT_LOAD_RENDER;
#if 0
        if (IS_FLAG(font->flags, FONT_HINTING))
                flags |= FT_LOAD_FORCE_AUTOHINT;
        else
#endif
                flags |= FT_LOAD_NO_HINTING | FT_LOAD_NO_AUTOHINT;
        if (1)
                flags |= FT_LOAD_COLOR;
#if 0
        else { /* TODO: support LCD subpixel filter? */
                static uint8_t lcd_weights[] = { 0x10, 0x40, 0x70, 0x40, 0x10 }; /* |FT_LCD_FILTER_DEFAULT| */
                /* Or |FT_LCD_FILTER_LIGHT| is |{ 0x00, 0x55, 0x56, 0x55, 0x00 }| */

                FT_Library_SetLcdFilter(HRF_Library_Handle,
                        FT_LCD_FILTER_LIGHT);
                flags |= FT_LOAD_TARGET_LCD;
                if (IS_FLAG(font->flags, FONT_LCDFILTER))
                        FT_Library_SetLcdFilterWeights(HRF_Library_Handle,
                                lcd_weights);
        }
#endif

        if ((e = FT_Load_Glyph(font->face, index, flags))) { /* TODO: These leak. */
                hrf_error(e, "FT_Load_Glyph");
                rval = -1;
                goto fail;
        }

fail:
        if (old_size)
                FT_Activate_Size(old_size);
        if (rval && rval != -2)
                fcomplain("hrf_start_glyph(%04x)", cp);
        return rval;
}

@ We can now do this:

\hskip 3em |hrflib_init_thread(); {|\par

\hskip 4em |hrf_font *font = hrf_alloc_font();|\par
\hskip 4em |hrf_load_xft(font, xf, d, s);| /* Where does |xf| come from? */\par
\hskip 4em |hrf_stroker *cast = hrf_alloc_stroker(font, 12);|\par
\hskip 4em |hrf_start_glyph(font, cast, 'H', true);|\par
\hskip 4em |hrf_stroke(font, cast);|\par
\hskip 4em ...\par
\hskip 4em |hrf_start_glyph(font, cast, 'i', true);|\par
\hskip 4em |hrf_stroke(font, cast);|\par
\hskip 4em ...\par
\hskip 4em |hrf_start_glyph(font, cast, '!', true);|\par
\hskip 4em |hrf_stroke(font, cast);|\par
\hskip 4em ...\par
\hskip 4em |hrf_destroy_stroker(cast);|\par
\hskip 4em |hrf_destroy_font(font);|\par
\hskip 4em |free(font);|\par

\hskip 3em |} hrflib_destroy_thread();|\par

To perform the ``...'' |hrf_stroke| has left a |FT_BitmapGlyph|
object in |cast->glyph| within which is a |FT_Bitmap| and glyph
metrics in |cast->metrics|. The glyph object is owned by FreeType
and its contents should be copied before any other call to FreeType.
The metrics likewise will be updated with another glyph is struck.

@ The stroke object passed into the rendering function at the
beginning is this which includes the metrics as its own type that
can be copied by the user.

@<Type def...@>=
typedef struct hrf_metrics {
        int    width, height; /* Size of the rasterised image (pixels). */
        int    offset_x, offset_y; /* Offset of the rasterised image from
                                        the baseline. */
        double advance_rl, advance_du; /* Width of the {\it glyph\/} (pixels). */
} hrf_metrics;

typedef struct hrf_stroker {
        hrf_metrics    metrics;
        double         scale, size_pt; /* For the use of callers. */
        FT_Size        size; /* Contains font's metrics. */
        FT_BitmapGlyph glyph; /* The renderer's output. */
} hrf_stroker;

@ This is as much of the API as has been written about (and written)
so far and is all that's needed to render a glyph from a font (that
has already been located or loaded).

@<Pub...@>=
void hrf_destroy_font (hrf_font *);
hrf_font *hrf_alloc_font (void);
hrf_stroker *hrf_alloc_stroker (hrf_font *, double);
hrf_font *hrf_builtin_font (void);
void hrf_destroy_stroker (hrf_stroker *);
int hrf_start_glyph (hrf_font *, hrf_stroker *, uint32_t, bool);
int hrf_stroke (hrf_font *, hrf_stroker *);
@#
void hrflib_destroy (void);
void hrflib_destroy_thread (void);
int hrflib_init (FT_Library);
int hrflib_init_thread (void);

@ @<XFT...@>=
int hrf_load_xft (hrf_font *, XftFont *, Display *, int);

@* Accessors.

@<Pub...@>=
double hrf_size_get_height (hrf_stroker *);
double hrf_size_get_max_advance (hrf_stroker *);
double hrf_stroker_get_advance_du (hrf_stroker *);
double hrf_stroker_get_advance_rl (hrf_stroker *);
int hrf_stroker_get_height (hrf_stroker *);
int hrf_stroker_get_offset_x (hrf_stroker *);
int hrf_stroker_get_offset_y (hrf_stroker *);
double hrf_stroker_get_scale (hrf_stroker *);
double hrf_stroker_get_size (hrf_stroker *);
int hrf_stroker_get_width (hrf_stroker *);

@ @<XFT...@>=
Display *hrf_get_xdisplay (hrf_font *);
int hrf_get_xscreen (hrf_font *);

@ @c
Display *
hrf_get_xdisplay (hrf_font *f)
{
        return f ? f->src.xdisplay : NULL;
}

@ @c
int
hrf_get_xscreen (hrf_font *f)
{
        return f ? f->src.xscreen : 0;
}

@ @c
double
hrf_stroker_get_size (hrf_stroker *stroker)
{
        return stroker->size_pt;
}

@ @c
double
hrf_stroker_get_scale (hrf_stroker *stroker)
{
        return stroker->scale; // see |@<Look for a prepared glyph@>|
}

@ @c
int
hrf_stroker_get_width (hrf_stroker *stroker)
{
        return stroker->metrics.width; // stroker->glyph->bitmap.metrics.width;
}

@ @c
int
hrf_stroker_get_height (hrf_stroker *stroker)
{
        return stroker->metrics.height; // stroker->glyph->bitmap.metrics.height;
}

@ @c
int
hrf_stroker_get_offset_x (hrf_stroker *stroker)
{
        return stroker->metrics.offset_x; // stroker->glyph->left;
}

@ @c
int
hrf_stroker_get_offset_y (hrf_stroker *stroker)
{
        return stroker->metrics.offset_y; // stroker->glyph->top;
}

@ @c
double
hrf_stroker_get_advance_rl (hrf_stroker *stroker)
{
        return stroker->metrics.advance_rl; // FT\_HAS\_FIXED\_SIZES(FONT) ? FONT->glyph->advance.x : sp6tod()
        // stroker->glyph->root->advance.x
}

@ @c
double
hrf_stroker_get_advance_du (hrf_stroker *stroker)
{
        return stroker->metrics.advance_du; // FT\_HAS\_FIXED\_SIZES(FONT) ? FONT->glyph->advance.y : sp6tod()
}

@ @c
double
hrf_size_get_max_advance (hrf_stroker *stroker)
{
        if (stroker->size)
                return stroker->size->metrics.max_advance;
        else
                return Builtin.metrics.advance_rl * 64;
}

@ @.TODO@> @c
double
hrf_size_get_height (hrf_stroker *stroker)
{
        if (stroker->size)
                return stroker->size->metrics.height;
        else
                return Builtin.metrics.height * 64;
}

@* Locating Fonts. Isn't Xft fun?

@<XFT...@>=
FcPattern *hrf_prepare_xft_pattern (char *, double);
hrf_font *hrf_load_xft_pattern (Display *, int, FcFontSet **, int,
        FcPattern *, bool);

@ @c
FcPattern *
hrf_prepare_xft_pattern (char   *font_name,
                         double  font_size)
{
        FcPattern *pattern;

        if (font_name[0] == '-')
                pattern = XftXlfdParse(font_name, false, false);
        else
                pattern = FcNameParse((const FcChar8 *) font_name);
        if (!pattern) {
                if (font_name[0] == '-')
                        fcomplain("XftXlfdParse(%s)", font_name);
                else
                        fcomplain("FcNameParse(%s)", font_name);
                return NULL;
        }

        if (font_size) {
                FcPatternDel(pattern, FC_PIXEL_SIZE);
                FcPatternDel(pattern, FC_SIZE);
                FcPatternAddDouble(pattern, FC_SIZE, font_size);
        }

        return pattern;
}

@ |*set| can be NULL.

Search for a font using the pattern |find|. Restrict the search to
|set| if defined (with |nset|). Returns a font if successful.

If |keep_pattern| then |find| is not destroyed after a font is
found.

@c
hrf_font *
hrf_load_xft_pattern (Display    *xdisplay,
                      int         xscreen,
                      FcFontSet **set,
                      int         nset,
                      FcPattern  *find,
                      bool        keep_pattern)
{
        FcPattern *found;
        FcResult result;
        XftFont *xmatch;
        hrf_font *ret;

        if (FcConfigSubstitute(NULL, find, FcMatchPattern) == FcFalse)
                goto fail_match;
        XftDefaultSubstitute(xdisplay, xscreen, find);
        if (nset)
                found = FcFontSetMatch(NULL, set, nset, find, &result);
        else
                found = FcFontMatch(NULL, find, &result); /* Saved in |xmatch|. */
        if (!found)
                goto fail_match;
        if (result == FcResultNoMatch)
                goto fail_open;
        xmatch = XftFontOpenPattern(xdisplay, found);
        if (!xmatch)
                goto fail_open;

        if (HRF_Library_Handle == NULL) {
                FT_Face face = XftLockFace(xmatch);
                int rval = hrflib_init(face->glyph->library);
                XftUnlockFace(xmatch);
                if (rval == -1)
                        goto fail_hrflib;
        }

        ret = hrf_alloc_font();
        if (!ret)
                goto fail_alloc;
        if (hrf_load_xft(ret, xmatch, xdisplay, xscreen) == -1)
                goto fail_load;
        if (!keep_pattern)
                FcPatternDestroy(find);
        return ret;

fail_load:
        hrf_destroy_font(ret);
fail_alloc:
fail_hrflib:
        XftUnlockFace(xmatch);
fail_open:
        FcPatternDestroy(found);
fail_match:
        return NULL;
}

@* Saving glyphs.

@<Pub...@>=
void hrf_copy_glyph_out (hrf_font *, hrf_stroker *,
        void (*)(int, void *, int, void *), void *);

@ The region within the atlas must have already been located.

@c
void
hrf_copy_glyph_out (hrf_font     *font,
                    hrf_stroker  *cast,
                    void        (*setline)(int, void *, int, void *),
                    void         *arg)
{
        uint8_t *ibuf, *lbuf, *src, *dst;
        int x, y, realy;
        FT_Bitmap *bmp;

        if ((!font && cast != &Builtin) || (font && cast == &Builtin)) {
                fcomplain("hrf_copy_glyph_out");
                return;
        }
        if (!font) {
                _hrf_builtin_copy(setline, arg);
                return;
        }
        assert(cast->glyph);

        bmp = &cast->glyph->bitmap;
        lbuf = calloc(cast->metrics.width, 4);
        switch (bmp->pixel_mode) {
        case FT_PIXEL_MODE_BGRA:
                for (y = cast->metrics.height - 1; y >= 0; y--) {
                        realy = cast->metrics.height - y - 1; /* Flip y axis */
                        ibuf = &bmp->buffer[y * bmp->pitch];
                        for (x = 0; x < cast->metrics.width; x++) {
                                src = &ibuf[x * 4];
                                dst = &lbuf[x * 4];
                                dst[0] = src[2];
                                dst[1] = src[1];
                                dst[2] = src[0];
                                dst[3] = src[3];
                        }
                        (setline)(realy, lbuf, cast->metrics.width, arg);
                }
                break;
        case FT_PIXEL_MODE_GRAY:
                for (y = cast->metrics.height - 1; y >= 0; y--) {
                        realy = cast->metrics.height - y - 1; /* Flip y axis */
                        ibuf = &bmp->buffer[y * bmp->pitch];
                        for (x = 0; x < cast->metrics.width; x++) {
                                dst = &lbuf[x * 4];
                                dst[0] = dst[1] = dst[2] = ibuf[x] ? 255 : 0;
                                dst[3] = ibuf[x];
                        }
                        (setline)(realy, lbuf, cast->metrics.width, arg);
                }
                break;
        case FT_PIXEL_MODE_MONO:
                for (y = cast->metrics.height - 1; y >= 0; y--) {
                        realy = cast->metrics.height - y - 1; /* Flip y axis */
                        ibuf = &bmp->buffer[(y * cast->metrics.width) / 8];
                        ibuf = &bmp->buffer[y * bmp->pitch];
                        for (x = 0; x < cast->metrics.width; x++) {
                                bool on = ibuf[x / 8] & (1 << (8 - (x % 8)));
                                dst = &lbuf[x * 4];
                                dst[0] = dst[1] = dst[2] = dst[3] = on ? 255 : 0;
                        }
                        (setline)(realy, lbuf, cast->metrics.width, arg);
                }
                break;
        default:
                errx(1, "unsupported pixel mode %u\n", bmp->pixel_mode);
                break;
        }
        free(lbuf);
}

@* Built-in bitmap font.

@c
#include "ql.h"

@ @<Global...@>=
uint32_t Builtin_Glyph = 0;

hrf_stroker Builtin = { { 8, 9, 0, 9, 8, 0 } };

@ @<Fun...@>=
static int _hrf_builtin_glyph (uint32_t, bool);
static int _hrf_builtin_stroke (void);
static void _hrf_builtin_copy (void (*setline)(int, void *, int, void *), void  *arg);

@ @c
hrf_font *
hrf_builtin_font (void)
{
        assert(sizeof (Builtin_Face) == sizeof (*Builtin_Face) * 96);
        return NULL;
}

@ @c
static int
_hrf_builtin_glyph (uint32_t cp,
                    bool     missing)
{
        int rval = 0;
        if ((cp && cp < ' ') || cp > 0x7f) {
                rval = -2;
                if (missing)
                        cp = 0;
        }
        Builtin_Glyph = cp;
        return rval;
}

@ @c
static int
_hrf_builtin_stroke (void)
{
        assert(!Builtin_Glyph || (Builtin_Glyph >= ' ' && Builtin_Glyph <= 0x7f));
        return 0;
}

@ @c
static void
_hrf_builtin_copy (void (*setline)(int, void *, int, void *),
                   void  *arg)
{
        uint8_t space[9] = {0};

        assert(!Builtin_Glyph || (Builtin_Glyph >= ' ' && Builtin_Glyph <= 0x7f));
        uint8_t *glyph = Builtin_Glyph
                ? Builtin_Face[Builtin_Glyph - 32]
                : Builtin_Face[0];
        if (Builtin_Glyph == ' ')
                glyph = space;

        uint8_t lbuf[Builtin.metrics.width * 4];
        for (int y = 0; y < 9; y++) {
                int realy = Builtin.metrics.height - y - 1; /* Flip y axis */
                uint8_t lbuf[8 * 4];
                for (int x = 0; x < 8 * 4; x += 4)
                        lbuf[x] = lbuf[x + 1] = lbuf[x + 2] = lbuf[x + 3]
                                = (glyph[y] & (1 << (8 - (x / 4)))) ? 0xff : 0;
                (setline)(realy, lbuf, 8, arg);
        }
}

@** Hello World.

@(hello.c@>=
#include <assert.h>
#include <err.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>
@#
#include <X11/Xlib.h>
#include <X11/Xft/Xft.h>
#include <epoxy/gl.h>
@#
#include "atlas.h"
#include "font.h"
#include "log.h"
#include "site.h"
#include "tri.h"
#include "ucpdb.h"
#undef LOG_PREFIX
#define LOG_PREFIX "hello"

typedef struct hrf_fonttile {
        uintptr_t cp;
        float     region[4];
} hrf_fonttile;

typedef struct hrf_letter {
        STAILQ_ENTRY(hrf_letter) @, each;
        int x, y;
} hrf_letter;

STAILQ_HEAD(@[@], hrf_letter) @, All_Letters;

char *Message = "Hello, world!";

ucpdb_xt *Tile_Index;
hrf_font *Face;
hrf_stroker *Strike;
atlas *Tiles;

void letter_click (trix_xt *, int, int);
void letter_draw (trix_xt *, struct timespec *, bool);
void letter_draw_atlas (trix_xt *);
bool letter_enlarge_atlas (trix_xt *);
int letter_find_glyph (int32_t, float *);
GLuint letter_init_texture (int);
int letter_render_glyph (int32_t, float *);
void letter_xevent (trix_xt *, XEvent *);

#define FONT_NAME      "Liberation Mono:antialias=true:autohint=true"
#define FONT_SIZE      42
#define ATLAS_SIZE     256
#define ATLAS_MAX_SIZE 4096
#define BORDER_SIZE    3

@ @(hello.c@>=
int
main (int    argc,
      char **argv)
{
        FcPattern *pat;
        trix_xt *tctx;

        if (argc > 1)
                Message = argv[1];

        STAILQ_INIT(&All_Letters);
        do_tri_init(tctx, NULL, letter_xevent, 640, 480);
        if (!tctx)
                errx(1, "tri_init");
        else if (tctx == (trix_xt *) -1)
                return 0;
        tri_set_draw_fun(tctx, letter_draw);

        pat = hrf_prepare_xft_pattern(FONT_NAME, FONT_SIZE);
        if (!pat)
                errx(1, "hrf_prepare_xft_pattern");
        Face = hrf_load_xft_pattern(tri_xdisplay(tctx), tri_xscreen(tctx),
                NULL, 0, pat, true);
        if (!Face)
                errx(1, "hrf_load_xft_pattern");
        Strike = hrf_alloc_stroker(Face, FONT_SIZE);
        if (!Strike)
                errx(1, "hrf_alloc_stroker");
        Tiles = atlas_alloc(ATLAS_SIZE, ATLAS_SIZE, 4, BORDER_SIZE,
                *(uint32_t *) &BLACK, true);
        if (!Tiles)
                errx(1, "atlas_alloc");
        Tile_Index = ucpdb_alloc(sizeof (hrf_fonttile));
        if (!Tile_Index)
                errx(1, "ucpdb_alloc");
        atlas_set_texture(Tiles, letter_init_texture(ATLAS_SIZE));

        tri_set_xevent_mask(tctx, ButtonPressMask);
        XMapWindow(tri_xdisplay(tctx), tri_xwindow(tctx));

        tri_go(tctx);

        finform("Exiting.");

        tri_destroy(tctx);
        return 0;
}

@ @(hello.c@>=
void
letter_xevent (trix_xt *tctx,
               XEvent  *ep)
{
        switch (ep->type) {
        default:
                fcomplain("unhandled event %d", ep->type);
        case ConfigureNotify:
        case Expose:
        case MapNotify:
        case UnmapNotify:
        case ReparentNotify:
                break;
        case ButtonPress:
                letter_click(tctx, ep->xbutton.x, ep->xbutton.y);
                break;
        }
}

@ @(hello.c@>=
void
letter_click (trix_xt *tctx,
              int      x,
              int      y)
{
        hrf_letter *obj;
        int glx, gly;

        tri_glcoord(tctx, x, y, &glx, &gly);
        obj = calloc(1, sizeof (hrf_letter));
        if (!obj)
                errx(1, "letter_click/calloc");
        obj->x = glx;
        obj->y = gly;
        STAILQ_INSERT_TAIL(&All_Letters, obj, each);
        tri_set_dirty(tctx);
}

@ @(hello.c@>=
void
letter_draw (trix_xt *tctx,
             struct timespec *at,
             bool     filthy)
{
        size_t num = 0;
        int w, h, x = 0, y = 0;
        vertex_xt v[] = { @|
                { x - 5, y - 5, 0,  -1,-1,  255,255,255,255 }, @|
                { x + 5, y - 5, 0,  -1,-1,  255,255,255,255 }, @|
                { x - 5, y + 5, 0,  -1,-1,  255,255,255,255 }, @|
                { x + 5, y + 5, 0,  -1,-1,  255,255,255,255 }, @/
        };
        index_xt i[] = { 0, 1, 2, 3, 0xffff };
        float *m, region[4];
        hrf_letter *t;

        if (atlas_get_full(Tiles))
                if (!letter_enlarge_atlas(tctx))
                        fcomplain("failed to enlarge atlas");

        if (1)
                letter_draw_atlas(tctx);
        m = tri_get_projection(tctx);
        STAILQ_FOREACH(t, &All_Letters, each) {
                if (letter_find_glyph(Message[num % strlen(Message)], region) != 0) {
                        fcomplain("failed to render `%c'.", Message[num % strlen(Message)]);
                        continue;
                }
                w = (region[2] - region[0]) * atlas_get_width(Tiles);
                h = (region[3] - region[1]) * atlas_get_height(Tiles);
                v[0].x = t->x - w / 2;
                v[0].y = t->y - h / 2;
                v[1].x = t->x + w / 2;
                v[1].y = t->y - h / 2;
                v[2].x = t->x - w / 2;
                v[2].y = t->y + h / 2;
                v[3].x = t->x + w / 2;
                v[3].y = t->y + h / 2;
                v[0].s = v[2].s = region[0];
                v[0].t = v[1].t = region[1];
                v[1].s = v[3].s = region[2];
                v[2].t = v[3].t = region[3];
                if (!tri_draw_vertices(tctx, GL_TRIANGLE_STRIP, m,
                                atlas_get_texture(Tiles),
                                v, sizeof (v) / sizeof (vertex_xt),
                                i, sizeof (i) / sizeof (index_xt)))
                        fcomplain("failed to draw %zu", num);
                num++;
        }

        if (filthy || atlas_get_dirty(Tiles)) {
                glBindTexture(GL_TEXTURE_2D, atlas_get_texture(Tiles));
                glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0,
                        atlas_get_width(Tiles), atlas_get_height(Tiles),
                        GL_RGBA, GL_UNSIGNED_BYTE, atlas_ref(Tiles, 0, 0));
                atlas_clear_dirty(Tiles);
        }
}

@ @(hello.c@>=
void
letter_draw_atlas (trix_xt *tctx)
{
        int o = 12;
        int w = atlas_get_width(Tiles);
        int h = atlas_get_height(Tiles);
        vertex_xt v[] = { @|
                { o,     o,     0,  0,0,  255,255,255,255 }, @|
                { o + w, o,     0,  1,0,  255,255,255,255 }, @|
                { o,     o + h, 0,  0,1,  255,255,255,255 }, @|
                { o + w, o + h, 0,  1,1,  255,255,255,255 }, @/
        };
        index_xt i[] = { 0, 1, 2, 3, 0xffff };
        if (!tri_draw_vertices(tctx, GL_TRIANGLE_STRIP,
                        tri_get_projection(tctx),
                        atlas_get_texture(Tiles),
                        v, sizeof (v) / sizeof (vertex_xt),
                        i, sizeof (i) / sizeof (index_xt)))
                fcomplain("failed to draw atlas");
}

@ @(hello.c@>=
int
letter_find_glyph (int32_t  cp,
                   float   *region)
{
        hrf_fonttile *ucpp;

        ucpp = (hrf_fonttile *) ucpdbq(Tile_Index, cp);
        if (!ucpp)
                return letter_render_glyph(cp, region);
        memmove(region, ucpp->region, sizeof (float) * 4);
        return 0;
}

@ @(hello.c@>=
int
letter_render_glyph (int32_t  cp,
                     float   *region)
{
        int32_t found[4];
        hrf_fonttile ucp = {0};
        float w, h;
        int rval;

        rval = hrf_start_glyph(Face, Strike, cp, true);
        if (rval == -1)
                goto fail;

        rval = hrf_stroke(Face, Strike);
        if (rval == -1)
                goto fail;

        if (!Strike->metrics.width)
                memset(region, 0, sizeof (float) * 4); /* No need for space in the atlas. */
        else {
                assert(Strike->metrics.height > 0);
                if (!atlas_find_region(Tiles, Strike->metrics.width,
                                Strike->metrics.height, found)) {
                        rval = -3;
                        goto fail_silently;
                }
                w = atlas_get_width(Tiles);
                h = atlas_get_height(Tiles);
                ucp.region[0] = found[0] / w;
                ucp.region[1] = found[1] / h;
                ucp.region[2] = (found[0] + found[2]) / w;
                ucp.region[3] = (found[1] + found[3]) / h;
                if (!ucpdb_save(Tile_Index, cp, (ucpd_xt *) &ucp))
                        goto fail;
                memmove(region, ucp.region, sizeof (float) * 4);
                hrf_copy_glyph_to_atlas(Face, Strike, Tiles, found);
        }
        return 0;

fail:
        fcomplain("failed to render code point %u", cp);
fail_silently:
        return rval;
}

@ @(hello.c@>=
GLuint
letter_init_texture (int size)
{
        GLuint ret;

        glGenTextures(1, &ret);
        glBindTexture(GL_TEXTURE_2D, ret);
        glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, size, size);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);
        return ret;
}

@ @(hello.c@>=
bool
letter_enlarge_atlas (trix_xt *tctx)
{
        atlas *new_atlas;
        GLuint new_texture, old_texture;
        int size;

        size = atlas_get_width(Tiles);
        assert(size == atlas_get_height(Tiles));
        if (size >= ATLAS_MAX_SIZE / 2)
                return false;
        else
                size *= 2;
        new_texture = letter_init_texture(size);
        if (!new_texture)
                return false;
        new_atlas = atlas_alloc(size, size, 4, BORDER_SIZE,
                *(uint32_t *) &BLACK, true);
        old_texture = atlas_get_texture(Tiles);
        if (!new_atlas) {
                glDeleteTextures(1, &new_texture);
                return false;
        }
        glDeleteTextures(1, &old_texture);
        atlas_set_texture(new_atlas, new_texture);
        ucpdb_clear(Tile_Index);
        atlas_destroy(Tiles);
        Tiles = new_atlas;
        tri_set_dirty(tctx);
        return true;
}

@** Index.
