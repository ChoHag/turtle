# There are no build instructions. Run make. Compatible with GNU
# and BSD make, tested on OpenBSD and occasionally Debian.

CTANGLE?=       bin/ctangle.+c
CWEAVE?=        cweave
EMBED?=         bin/bin2c
PDFTEX?=        bin/pdftex.cd
PROVE?=         prove

VISIBLE=-fvisibility=default
HIDDEN=-fvisibility=hidden

CFLAGS+=${DEBUG} -fPIC -std=c11
CFLAGS+=$$(pkg-config --cflags x11 gl glu epoxy xft fontconfig freetype2)
LIBS+=  -lm -lutil -levent
LIBS+=  $$(pkg-config --libs x11 gl glu epoxy xft fontconfig freetype2)

# NIH!
CFLAGS+=$$(pkg-config --exists libbsd-overlay && pkg-config --cflags libbsd-overlay)
CXXFLAGS+=$$(pkg-config --exists libbsd-overlay && pkg-config --cflags libbsd-overlay)
LIBS+=  $$(pkg-config --exists libbsd-overlay && pkg-config --libs libbsd-overlay)

WARNINGS?= -Wall -Wextra -Wpedantic     \
        -Wno-gnu-label-as-value         \
        -Wno-gnu-statement-expression   \
        -Wno-gnu-zero-variadic-macro-arguments \
        -Wno-unused-variable            \
        -Wno-unused-parameter           \
        -Wdisabled-optimization         \
        -Wformat-extra-args             \
        -Wmissing-declarations          \
        -Wmissing-prototypes            \
        -Wpointer-arith                 \
        -Wredundant-decls               \
        -Wsequence-point                \
        -Wshadow                        \
        -Wstrict-prototypes             \
        -Wundef                         \
#NOISY  -Wcast-align                    \
#NOISY  -Wwrite-strings                 \


OBJS=	         \
	atlas.o  \
	buf.o    \
	hrfont.o \
	log.o    \
	panel.o  \
	term.o   \
	tri.o    \
	utf8.o

TESTS=	                      \
	t/CUB.t               \
	t/CUP.t               \
	t/DECALN.t            \
	t/DECCOLM.t           \
	t/ED.t                \
	t/EL.t                \
	t/HTS.t               \
	t/HVP.t               \
	t/SD.t                \
	t/SU.t                \
	t/TBC.t               \
	t/buf-alloc.t         \
	t/tabulation.t        \
	t/tt_panel_blit.t     \
	t/tt_panel_clear_region.t

turtle: ${OBJS} turtle.o user.o tgl.o
	${LINK.c} ${LIBS} ${OBJS} turtle.o user.o tgl.o -o turtle

actor: ${OBJS} actor.o tgl.o
	${LINK.c} ${LIBS} ${OBJS} actor.o tgl.o -o actor

tapdance: tapdance.o
	${LINK.c} ${LIBS} tapdance.o -o tapdance

triangle: tri.o triangle.o log.o
	${LINK.c} ${LIBS} triangle.o tri.o log.o -o triangle

BG_SHADERS=bgvertex.e bgfragment.e bggeometry.e
FG_SHADERS=fgvertex.e fgfragment.e fggeometry.e
CURSOR_SHADERS=cursorvertex.e
SELECT_SHADERS=selectvertex.e selectrectgeometry.e selectlinesgeometry.e

actor.c: panel.c
actor.o: panel.o
atlas.o: buf.o
buf.o:
hrfont.o: log.o ql.h
log.o:
panel.o: atlas.o hrfont.o utf8.o rgbcube.h
tap.o:
tapdance.c: tap.c
term.o: panel.o buf.o utf8.o
tgl.o: tri.o log.o panel.o
tgl.o: ${BG_SHADERS} ${FG_SHADERS} ${CURSOR_SHADERS} ${SELECT_SHADERS}
tri.o: log.o
triangle.c: tri.c
triangle.o: tri.o log.o
turtle.o: tri.o tgl.o log.o user.o term.o panel.o
utf8.o:
user.o: log.o term.o tgl.o
${BG_SHADERS:.e=.glsl}: tgl.c
${FG_SHADERS:.e=.glsl}: tgl.c
${CURSOR_SHADERS:.e=.glsl}: tgl.c
${SELECT_SHADERS:.e=.glsl}: tgl.c

.SUFFIXES: .t

# GNU complains about this even as it parses it to understand it
.c.t: ${OBJS} tap.o test-term.o
	${LINK.c} -I. ${LIBS} ${OBJS} tap.o test-term.o $< -o $@

*.tex t/*.tex: format.w types.w

clean:
	rm -f atlas.[ch] buf.[ch] log.[ch] utf8.[ch] tgl.[ch] user.[ch]
	rm -f hrfont.[ch] hrfont-xft.h hello.c hello
	rm -f panel.c panel-internal.h tpanel.h actor.c actor
	rm -f term.c term-internal.h tterm.h pseudo.c pseudo
	rm -f turtle.[ch] turtle
	rm -f ${BG_SHADERS:.e=.glsl} ${BG_SHADERS}
	rm -f ${FG_SHADERS:.e=.glsl} ${FG_SHADERS}
	rm -f ${CURSOR_SHADERS:.e=.glsl} ${CURSOR_SHADERS}
	rm -f ${SELECT_SHADERS:.e=.glsl} ${SELECT_SHADERS}
	rm -f tap.[ch] tapdance.c tapdance
	rm -f tri.c trix.h triangle.c triangle
	rm -f test-term.[ch]
	rm -f a.out core *.core *.idx *.log *.scn *.toc *.o *.so
	rm -f *.tex *.idx *.log *.scn *.toc *.pdf
	rm -f t/core t/*.core t/*.idx t/*.log t/*.scn t/*.toc t/*.o
	rm -f t/*.[ct] t/*.tex t/*.pdf

test: ${TESTS}
	$(PROVE) -e bin/tehbang $(TESTS)

.PHONY: all clean test

.SUFFIXES: .e .glsl .pdf .t .tex .w

# This is necessary to unbreak more brain damage. GNU make by default
# will delete the files named as targets if the rule is automatically
# generated, in an apparent attempt to be tidy where tidiness is not
# useful and often harmful. The end result is that GNU deletes the
# intermediate files that are named and useful and leaves behind
# all the dross that various compilers also generate.
.SECONDARY:

.c.o:
	${COMPILE.c} ${HIDDEN} $< -o $@

.cpp.o:
	${COMPILE.cc} ${HIDDEN} $< -o $@

.glsl.e:
	name=$$(echo $< | sed 's/.glsl//; s/-/_/g'); \
	${EMBED} GUARD_$$name Embed_$$name $< >$@

.tex.pdf:
	${PDFTEX} $<

.w.c:
	${CTANGLE} $< - $@

.w.tex:
	${CWEAVE} $< - $@
