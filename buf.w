@i format.w
@i types.w

@** Dynamic Buffers. This is a simple implementation of a buffer
object which can grow as it gets added to.

This object makes no use of OpenGL but the container object it
defines has some spare space which doubles as storage for the
identier of an OpenGL binding which represents the buffer.

@c
#include "buf.h"
#include <err.h>
#include <stdlib.h>
#include <string.h>
#include "parsnip/safe-math.h" /* github.com/nemequ/portable-snippets */
@#
@h
@<Global variables (\.{buf.o})@>@;

@ @(buf.h@>=
#ifndef TURTLE_ARRAY_H
#define TURTLE_ARRAY_H
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <sys/queue.h>
@<Exported type definitions (\.{buf.o})@>@;
@<Public API (\.{buf.o})@>@;
#endif /* |TURTLE_ARRAY_H| */

@ For many use cases this can be a convenient layer on top of the
core memory allocator so it includes user-definable wrappers around
malloc.

@<Global...@>=
static void *(*tr_user_calloc) (size_t, size_t) = NULL;
static void *(*tr_user_realloc) (void *, size_t) = NULL;
static void @[(*tr_user_free)@] (void *) = NULL;

@ TODO: deprecate. Use malloc.

@.TODO@>
@<Public...@>=
void trallocator (void *(*)(size_t, size_t), @|
        void *(*)(void *, size_t), @|
        void (*)(void *));
void *trcalloc(size_t, size_t);
void trfree(void *);
void *trrealloc(void *, size_t);

@ @c
void
trallocator (void  *(*user_calloc)(size_t, size_t), @|
             void  *(*user_realloc)(void *, size_t),
             void @[(*user_free)@](void *))
{
        static bool tready = false;

        if (tready)
                return;
        tr_user_calloc = user_calloc ? user_calloc : calloc;
        tr_user_realloc = user_realloc ? user_realloc : realloc;
        tr_user_free = user_free ? user_free : free;
        tready = true;
}

@ @c
void *
trcalloc (size_t nmemb,
          size_t size)
{
        trallocator(NULL, NULL, NULL);
        return tr_user_calloc(nmemb, size);
}

void *
trrealloc (void   *ptr,
           size_t  size)
{
        trallocator(NULL, NULL, NULL);
        return tr_user_realloc(ptr, size);
}

void
trfree (void *ptr)
{
        trallocator(NULL, NULL, NULL);
        tr_user_free(ptr);
}

@ The buffer's attributes are all counted in bytes and so these
macros return results in terms of units of the objects that are
stored within it: the buffer's stride.

The data pointer and size attributes except stride occupy the space
of five pointers. The stride could never need to be so large and
we need space for at least one flag bit so the stride is stored in
16 bits, limiting the size of an individual object in the buffer
to 64KB and freeing up much more space than necessary in the remining
sixth pointer.

An alternative would use 32 bits for stride and flags which is
plenty but on 64 bit architectures, where this is most likely to
be used, using 16 bits for each leaves 32 bits of space which
is exactly the size of an OpenGL buffer identifier.

The metadata container is not implemented behind an opaque pointer
so that users can avoid the extra pointer indirection if necessary.

@.TODO@>
@ @d TRI_MAXBUFSTRIDE  0x10000
@d _array_allocated(O) ((O)->allocated / _array_stride(O))
@d _array_length(O)    ((O)->used / _array_stride(O))
@d _array_stride(O)    (((int32_t) (O)->stride) + 1)
@d _array_used(O)      ((O)->used / _array_stride(O))
@d _array_max(O)       ((size_t) ((O)->max / _array_stride(O)))
@<Export...@>=
typedef struct tp_array {
        void     *data;
        size_t    allocated; /* Bytes of allocated memory. */
        size_t    used;      /* Bytes in used for objects. */
        size_t    max;       /* Maximum size this buffer can grow to. */
        size_t    growth;    /* Growth increment, 0 means to double. */
        uint16_t  stride;    /* $-1$ so 0x10000 is the maximum. */
        uint16_t  flags;
} tp_array;

@ A single flag tracks whether the dynamic buffer container has
been allocated by this library or the caller. Otherwise the remaining
15 flag bits are available to users, except that one more flag is
named ``dirty'' and can be used to know when the buffer has changed.
The dirty flag is not used in here and toggling it is the responsibility
of the user, or the remaining 15 flag bits 0--14 are availble for
any use.

@d ARRAY_MINE 15
@d ARRAY_DIRTY 14 /* 14 user flags 0--13. */
@<Pub...@>=
#define FBIT(O)          (1ull << (O))
#define IS_FLAG(F,O)     !!((F) & FBIT(O)) /* on or off? */
#define CLEAR_FLAG(F,O)  ((F) = ((F) & ~FBIT(O))) /* set it off */
#define SET_FLAG(F,O)    ((F) = ((F) | FBIT(O))) /* set it on */
#define WAVE_FLAG(F,O,B) ((B) ? SET_FLAG((F), (O)) : CLEAR_FLAG((F), (O)))

@ The whole dynamic buffer API is public so that the user and
compiler both have as much information available as possible.

@<Pub...@>=
void *array_alloc_more (tp_array *, size_t);
void *array_base (const tp_array *);
void array_clear (tp_array *);
void array_clear_dirty (tp_array *);
void array_destroy (tp_array *);
void *array_first (const tp_array *);
size_t array_get_allocated (tp_array *);
bool array_get_dirty (tp_array *);
size_t array_get_length (tp_array *);
size_t array_get_max (tp_array *);
size_t array_get_stride (tp_array *);
size_t array_get_used (tp_array *);
tp_array *array_init (tp_array *, size_t, size_t, size_t, size_t);
void *array_insert (tp_array *, size_t, size_t, void *);
void *array_last (const tp_array *);
void *array_pop (tp_array *, size_t, void *);
void *array_push (tp_array *, size_t, void *);
void *array_realloc (tp_array *, size_t);
void *array_ref (const tp_array *, size_t);
void array_remove (tp_array *, size_t, size_t);
void array_set_dirty (tp_array *);
tp_array *new_array (size_t, size_t, size_t, size_t);

void array_clear_flag (tp_array *, int);
bool array_get_flag (tp_array *, int);
uint16_t array_get_flags (tp_array *);
void array_set_flag (tp_array *, int);
void array_set_flags (tp_array *, uint16_t);

@ 15 flags 0--14. Flag 14 is |ARRAY_DIRTY| which this module does
not otherwise use.

@c
void
array_clear_flag (tp_array *buf,
                  int       f)
{
        if (f == ARRAY_MINE) {
                warnx("array_clear_flag(%u)", ARRAY_MINE);
                return;
        }
        CLEAR_FLAG(buf->flags, f);
}

bool
array_get_flag (tp_array *buf,
                int       f)
{
        if (f == ARRAY_MINE) {
                warnx("array_get_flag(%u)", ARRAY_MINE);
                return 0;
        }
        return IS_FLAG(buf->flags, f);
}

void
array_set_flag (tp_array *buf,
                int       f)
{
        if (f == ARRAY_MINE) {
                warnx("array_set_flag(%u)", ARRAY_MINE);
                return;
        }
        SET_FLAG(buf->flags, f);
}

uint16_t
array_get_flags (tp_array *buf)
{
        return buf->flags & ~FBIT(ARRAY_MINE);
}

void
array_set_flags (tp_array *buf,
                 uint16_t  f)
{
        if (IS_FLAG(f, ARRAY_MINE))
                warnx("(array_set_flags(1<<%u)", ARRAY_MINE);
        buf->flags = (f & ~FBIT(ARRAY_MINE)) | (buf->flags & FBIT(ARRAY_MINE));
}

@ The dynamic buffer can be embedded in another object and so may
not need to be allocated itself so the allocation and initialisation
are separate. This interface allocates space for the container and
sets a flag to indicate to the destructor routine that the container
should also be destroyed.

@c
tp_array *
new_array (size_t stride,
           size_t preallocate,
           size_t growth,
           size_t max)
{
        tp_array *real = trcalloc(1, sizeof (tp_array));
        tp_array *ret = array_init(real, stride, preallocate, growth, max);
        if (!ret)
                trfree(real);
        else
                SET_FLAG(ret->flags, ARRAY_MINE);
        return ret;
}

@ This is the main initialisation routine. Some or all of the
underlying storage can be allocated here if |preallocate| is not
zero. The |growth| attribute describes how to enlarge the storage
when necessary: by that amount of bytes or doubling the previous
size if it's zero. The buffer will not grow beyond |max|. All of
these figures are in bytes.

In fact strictly speaking (provided |NULL| is zero) no initialisation
is required for a buffer of byte-sized objects with no size limit
and no preallocation.

@c
tp_array *
array_init (tp_array *buf,
            size_t    stride,
            size_t    preallocate,
            size_t    growth,
            size_t    max)
{
        if (!buf)
                return NULL;
        if (!stride || stride > TRI_MAXBUFSTRIDE)
                return NULL;
        if (max && max < preallocate)
                return NULL;
        if (preallocate) {
                buf->data = trcalloc(1, preallocate); /* May be zero. */
                if (!buf->data)
                        return NULL;
        } else
                buf->data = NULL;
        CLEAR_FLAG(buf->flags, ARRAY_MINE);
        buf->stride = stride - 1;
        buf->allocated = preallocate;
        buf->growth = growth;
        buf->max = max;
        return buf;
}

@ The counterpart destruction after the buffer is completely finished
with. This should probably not call |glDeleteBuffers| here but leave
that to the user (TODO).

@.TODO@>
@c
void
array_destroy (tp_array *buf)
{
        if (!buf)
                return;
        trfree(buf->data);
        if (IS_FLAG(buf->flags, ARRAY_MINE))
                trfree(buf);
}

@ After a dynamic buffer's been used it may have grown to extraordinary
sizes and none of the usual routines make any effort to reduce its
size. This routine reduces the dynamic buffer's underlying allocation
after the contents are finished with.

@c
void *
array_realloc (tp_array *buf,
               size_t    size)
{
        void *new;

        if (buf->used)
                return NULL;
        new = trrealloc(buf->data, size);
        if (new)
                buf->data = new;
        return new;
}

@ These are accessor functions for the dynamic buffer's metadata.

@c
size_t
array_get_allocated (tp_array *buf)
{
        return _array_allocated(buf);
}

size_t
array_get_length (tp_array *buf)
{
        return _array_length(buf);
}

size_t
array_get_stride (tp_array *buf)
{
        return _array_stride(buf);
}

size_t
array_get_used (tp_array *buf)
{
        return _array_used(buf);
}

size_t
array_get_max (tp_array *buf)
{
        return _array_max(buf);
}

@ The first spare flag can, but doesn't have to, function as an
indicator that the buffer's data have changed. These functions and
this flag are not used by any of the routines below.

@c
bool
array_get_dirty (tp_array *buf)
{
        return IS_FLAG(buf->flags, ARRAY_DIRTY);
}

void
array_clear_dirty (tp_array *buf)
{
        CLEAR_FLAG(buf->flags, ARRAY_DIRTY);
}

void
array_set_dirty (tp_array *buf)
{
        SET_FLAG(buf->flags, ARRAY_DIRTY);
}

@ Routines to return a pointer to the |i|th, first, last or next
available element. Note that |array_first| will return |NULL| if no
storage has been used but |array_base| will always return a pointer
to whatever storage has been allocated.

@c
void *
array_ref (const tp_array *buf,
           size_t          index)
{
        if (index > _array_length(buf))
                return NULL;
        return (void *) (((uintptr_t) buf->data) + _array_stride(buf) * index);
}

void *
array_base (const tp_array *buf)
{
        return buf->data;
}

void *
array_first (const tp_array *buf)
{
        if (!_array_length(buf))
                return NULL;
        return array_ref(buf, 0);
}

void *
array_last (const tp_array *buf)
{
        if (!_array_length(buf))
                return NULL;
        return array_ref(buf, _array_length(buf) - 1);
}

@ ``Empty'' the buffer.

@c
void
array_clear (tp_array *buf)
{
        buf->used = 0;
}

@ Ensure the memory allocated is at least $( |length| + |more| )
\times |stride|$ and return a pointer to the beginning of that
space. Does not affect the amount in use.

This is the part of this library that makes the buffer ``dynamic''.

@c
void *
array_alloc_more (tp_array *buf,
                  size_t  more)
{
        void *nbuf;
        size_t grow, val;

        if (!psnip_safe_add(&val, _array_length(buf), more))
                return NULL;
        if (!psnip_safe_mul(&val, val, _array_stride(buf)))
                return NULL;
        if (buf->max && val > buf->max)
                return NULL;
        if (val > buf->allocated) {
                if (buf->allocated)
                        grow = buf->allocated;
                else if (buf->growth)
                        grow = buf->growth;
                else
                        grow = 16;
                if (buf->growth) {
                        while (grow < val)
                                if (!psnip_safe_add(&grow, grow, buf->growth))
                                        return NULL;
                } else {
                        while (grow < val)
                                if (!psnip_safe_mul(&grow, grow, 2))
                                        return NULL;
                }
                nbuf = trrealloc(buf->data, grow);
                if (!nbuf)
                        return NULL;
                memset(((uint8_t *) nbuf) + buf->allocated, 0,
                        grow - buf->allocated);
                buf->data = nbuf;
                buf->allocated = grow;
        }
        return array_ref(buf, _array_length(buf));
}

@ This routine appends |count| items on top of whatever's already
allocated, enlarging the storage if necessary and possible. If |src|
is not |NULL| then the data are also copied into the buffer.

@c
void *
array_push (tp_array *buf,
            size_t    count,
            void     *src)
{
        void *dst;

        dst = array_alloc_more(buf, count);
        if (!dst)
                return NULL;
        buf->used += count * _array_stride(buf); /* Checked in |array_alloc_more|. */
        if (src)
                memmove(dst, src, count * _array_stride(buf));
        return dst;
}

@ This is the reverse of |array_push| which removes |count| items.
If the |dst| pointer is provided then those items are copied into
the location it indicates although this library explicitly doesn't
deallocate that space yet so if the data there are used immediately
after this function returns and before calling any other functions
in here, that operation is safe.

@c
void *
array_pop (tp_array *buf,
           size_t    count,
           void     *dst)
{
        void *src;

        if (!count || count >= _array_length(buf))
                return NULL;
        src = array_ref(buf, _array_length(buf) - count);
        if (dst)
                memmove(dst, src, count * _array_stride(buf));
        buf->used -= count * _array_stride(buf);
        return src;
}

@ Like |array_push| this routine inserts |count| items but at position
|index| rather than the end of the used space. Existing data at
position |index| and above are moved up to make room.

@c
void *
array_insert (tp_array *buf,
              size_t    index,
              size_t    count,
              void     *src)
{
        void *new, *old;
        size_t len, more;

        len = _array_length(buf);
        if (index > len)
                more = index - len;
        else
                more = 0;
        if (!psnip_safe_add(&more, more, count))
                return NULL;
        if (!array_alloc_more(buf, more))
                return NULL;
        buf->used += more * _array_stride(buf);
        old = array_ref(buf, index);
        new = array_ref(buf, index + count);
        if (index < len)
                memmove(new, old, _array_stride(buf) * (len - index));
        if (src)
                memmove(old, src, _array_stride(buf) * count);
        return old;
}

@ Like |array_pop| this is the reverse of |array_insert|. TODO: To
mirror |array_pop| there should probably be a |dst| pointer.

@.TODO@>
@c
void
array_remove (tp_array *buf,
              size_t    index,
              size_t    count)
{
        size_t last, lose, shift;

        if (index >= _array_length(buf))
                return;
        if (!psnip_safe_add(&last, index, count) || last > _array_length(buf))
                last = _array_length(buf);
        lose = last - index;
        shift = _array_length(buf) - last;
        if (last < _array_length(buf))
                memmove(array_ref(buf, index), array_ref(buf, index + lose),
                        _array_stride(buf) * shift);
        buf->used -= lose * _array_stride(buf);
}

@** Index.
