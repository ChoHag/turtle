@i format.w
@i types.w

@** User Input.

@.TODO@>
@c
#include "user.h"
@#
#include <assert.h>
#include <ctype.h>
#include <sys/tree.h>
@#
#include <X11/keysym.h>
#include <X11/Xatom.h>
#include <X11/Xlib.h>
@#
#include "log.h"
#include "tgl.h"
#include "tterm.h"
#undef LOG_PREFIX
#define LOG_PREFIX "user" /* TODO: rename */

@<Type definitions (\.{user.o})@>@;
@<Function declarations (\.{user.o})@>@;
@<Global variables (\.{user.o})@>@;

extern Display    *X_Display;
extern Window      Main_Window;
extern tt_term    *Term;
extern tt_panel   *Panel;

@ @(user.h@>=
#ifndef TT_USER_H
#define TT_USER_H

#include <stdbool.h>
#include <stdint.h>
#include <X11/XKBlib.h>

typedef struct tt_keydef tt_keydef;

@<Public API (\.{user.o})@>@;

#endif /* |TT_USER_H| */

@ No idea how this works. Nicked from ST.

@<Pub...@>=
tt_keydef *user_find_keydef (KeySym, uint16_t);
void user_init_keymap (void);
void user_keypress (XEvent *);
void user_keyrelease (XEvent *);
void ximdestroy (XIM, XPointer, XPointer);
void ximinstantiate (Display *, XPointer, XPointer);
int xicdestroy (XIC, XPointer, XPointer);
int ximopen (Display *);

extern XIC    X_IC;

@ @<Global...@>=
XIC           X_IC;
XIM           X_IM;
XPoint        X_Spot;
XVaNestedList X_Spotlist;

@ @c
int
ximopen(Display *dpy)
{
        assert(dpy == X_Display);
        XIMCallback imdestroy = { .client_data = NULL, .callback = ximdestroy };
        XICCallback icdestroy = { .client_data = NULL, .callback = xicdestroy };

        X_IM = XOpenIM(X_Display, NULL, NULL, NULL);
        if (X_IM == NULL)
                return 0;

        if (XSetIMValues(X_IM, XNDestroyCallback, &imdestroy, NULL))
                fcomplain("XSetIMValues: could not set XNDestroyCallback");

        X_Spotlist = XVaCreateNestedList(0, XNSpotLocation, &X_Spot,
                                              NULL);

        if (X_IC == NULL) {
                X_IC = XCreateIC(X_IM, XNInputStyle,
                                       XIMPreeditNothing | XIMStatusNothing,
                                       XNClientWindow, Main_Window,
                                       XNDestroyCallback, &icdestroy,
                                       NULL);
        }
        if (X_IC == NULL)
                fcomplain("XCreateIC: could not create input context");

        return 1;
}

void
ximinstantiate(Display *dpy, XPointer client, XPointer call)
{
        assert(dpy == X_Display);
        if (ximopen(dpy))
                XUnregisterIMInstantiateCallback(X_Display, NULL, NULL, NULL,
                                                 ximinstantiate, NULL);
}

void
ximdestroy(XIM xim, XPointer client, XPointer call)
{
        X_IM = NULL;
        XRegisterIMInstantiateCallback(X_Display, NULL, NULL, NULL,
                                       ximinstantiate, NULL);
        XFree(X_Spotlist);
}

int
xicdestroy(XIC xim, XPointer client, XPointer call)
{
        X_IC = NULL;
        return 1;
}

@ Holding CTRL and pressing any ASCII from @@ to \_ receives from
X the corresponding C0 code byte in |text|. The keys which would
normally send those codes also do by default and so in this case
|text| is ignored.

@c
void
user_keypress (XEvent *xev)
{
        KeyCode keycode = xev->xkey.keycode;
        KeySym keysym = NoSymbol;
        Status s;
        uint8_t text[64], *textp = text;
        size_t textlen = 0;
        bool handled = false;

        memset(text, 0, sizeof (text));
        if (xev->type == KeyPress) {
                if (X_IC) {
                        textlen = sizeof (text) - 1;
again:
                        textlen = Xutf8LookupString(X_IC, &xev->xkey,
                                textp, textlen, &keysym, &s);
                        switch (s) {
                        case XBufferOverflow: /* unreached? */
                                assert(textp == text);
                                textp = malloc(textlen + 1);
                                goto again;
                        case XLookupNone:
                                assert(textp == text);
                                return; /* probably */
                        case XLookupChars: // textp filled, keysym untouched
                        case XLookupKeySym: // keysym changed, textp untouched
                        case XLookupBoth: // both
                                break;
                        }
                        textp[textlen] = '\0';
                } else
                        XLookupString(&xev->xkey, text, sizeof (text),
                                &keysym, NULL);
        }

#ifdef SDL_USE_IME // IBus or fcitx input method
        if (SDL_GetEventState(SDL_TEXTINPUT) == SDL_ENABLE) {
                handled = SDL_IME_ProcessKeyEvent(keysym, keycode,
                        (xev->type == KeyPress ? SDL_PRESSED : SDL_RELEASED));
        }
#endif
        if (handled || xev->type == KeyRelease)
                goto done;
#ifdef SDL_USE_IME
        /* Don't send the key if it looks like a duplicate of a filtered key sent by an IME */
        if (xev->xkey.keycode != videodata->filter_code || xev->xkey.time != videodata->filter_time) {
                SDL_SendKeyboardKey(SDL_PRESSED, videodata->key_layout[keycode]);
        }
#endif

        if (((keysym >= 0xff00 && keysym <= 0xff1f) || keysym == 0xffff) /* C0 key */
                        || keysym >= 0xff80 && keysym <= 0xffbd) { /* Printable keypad key */
                assert(textp[1] == '\0');
                textp[0] = '\0';
        }

        assert(xev->xkey.state <= UINT16_MAX);
        KeySym search = islower(keysym) ? toupper(keysym) : keysym;
        tt_keydef *act = user_find_keydef(search, xev->xkey.state);
        if (act)
                (act->fun)(act->val, xev, keysym, text);
        else
                term_key(Term, xev->xkey.state & Mod1Mask, textp, textlen);
done:
        if (textp != text)
                free(textp);
}

@ @c
void
user_keyrelease (XEvent *xev)
{
}

@ @<Pub...@>=
typedef union tt_keyarg {
        bool       b;
        char      *s;
        void      *p;
        intptr_t   i;
        uintptr_t  u;
        float      f;
        double     d;
} tt_keyarg;

typedef struct tt_keydef {
        KeySym       sym;
        uint16_t     mod;
        int8_t       pad;
        int8_t       dir;
        void      (*fun)(tt_keyarg, XEvent *, KeySym, char *);
        tt_keyarg    val;
} tt_keydef;

@ @<Type def...@>=
typedef struct tt_keylist {
        SPLAY_ENTRY(tt_keylist) keytree;
        size_t                  len;
        tt_keydef               d[];
} tt_keylist;

@ @<Global...@>=
uint16_t Kbd_Ignore = Mod2Mask; // st also ignores |1<<13| \AM\ |1<<14|
SPLAY_HEAD(tt_keytree, tt_keylist)
                    Keymap = SPLAY_INITIALIZER(&Keymap);
SPLAY_PROTOTYPE(tt_keytree, tt_keylist, keytree, _user_keylist_cmp) @;
SPLAY_GENERATE(tt_keytree, tt_keylist, keytree, _user_keylist_cmp) @;

@ @<Fun...@>=
static int _user_keydef_cmp (tt_keydef *, tt_keydef *);
static int _user_keylist_cmp (tt_keylist *, tt_keylist *);
static KeySym _user_keylist_keysym (tt_keylist *);
static void _user_keymap_sequence (tt_keyarg, XEvent *, KeySym, char *);
static void _user_keymap_ttytrace (tt_keyarg, XEvent *, KeySym, char *);
static void _user_replace_keylist (tt_keylist *);

@ @c
static void
_user_keymap_sequence (tt_keyarg  arg,
                       XEvent    *xev,
                       KeySym     sym,
                       char      *txt)
{
        term_key(Term, false, arg.s, strlen(arg.s));
}

@ @d TT_DEFAULT_TRACE_FILENAME "/tmp/turtle.trace"
@c
static void
_user_keymap_ttytrace (tt_keyarg  arg,
                       XEvent    *xev,
                       KeySym     sym,
                       char      *txt)
{
        if (arg.b)
                term_trace_start(Term, TT_DEFAULT_TRACE_FILENAME);
        else
                term_trace_stop(Term);
}

@ @c

static void _user_init_map (tt_keydef *, size_t);

void
user_init_keymap (void)
{
        _user_init_map(Default_Keymap,
                sizeof (Default_Keymap) / sizeof (*Default_Keymap));
        _user_init_map(Default_Pointmap,
                sizeof (Default_Pointmap) / sizeof (*Default_Pointmap));
}

@ @c
static void
_user_init_map (tt_keydef *map,
                size_t     len)
{
        if (!len)
                return;
        KeySym sym = ~map[0].sym;
        tt_keylist *list = NULL;
        size_t start = 0;
        for (size_t i = 0; i < len; i++) {
#define         _user_prepare_keylist(I) @[ do {                              \
                        size_t n = (I) - start;                               \
                        sym = map[(I)].sym;                                   \
                        if (!n)                                               \
                                continue; /* 'tis the first definition */     \
                        tt_keylist *list = calloc(1,                          \
                                sizeof (*list) + n * sizeof (*list->d));      \
                        list->len = n;                                        \
                        memmove(list->d, &map[start], sizeof (*list->d) * n); \
                        _user_replace_keylist(list);                          \
                        start = (I);                                          \
                } while (0) @]
                if (map[i].sym == sym)
                        continue;
                _user_prepare_keylist(i);
        }
        _user_prepare_keylist(len);
}

@ @c
static void
_user_replace_keylist (tt_keylist *list)
{
        assert(list->len >= 1);
        KeySym sym = _user_keylist_keysym(list);
        for (int i = 0; i < list->len; i++)
                assert(list->d[i].sym == sym);
        tt_keylist *old = SPLAY_REMOVE(tt_keytree, &Keymap, list);
        if (old)
                abort();
        SPLAY_INSERT(tt_keytree, &Keymap, list);
}

@ @c
static KeySym
_user_keylist_keysym (tt_keylist *list)
{
        return list->d[0].sym;
}

@ @c
static int
_user_keydef_cmp (tt_keydef *d1,
                  tt_keydef *d2)
{
        return (d1->sym < d2->sym ? -1 : d1->sym > d2->sym);
}

static int
_user_keylist_cmp (tt_keylist *n1,
                   tt_keylist *n2)
{
        return _user_keydef_cmp(&n1->d[0], &n2->d[0]);
}

@ @c
static tt_keylist *
_user_find_keylist (KeySym sym)
{
        tt_keylist search[2] = {0}; /* extra space for the definition */
        tt_keydef *def = &search[0].d[0];
        search[0].len = 1;
        def->sym = sym;
        return SPLAY_FIND(tt_keytree, &Keymap, search);
}

@ @c
static bool
_user_match_key (KeySym     sym,
                 tt_keydef *def,
                 uint16_t   mod)
{
        if (def->sym != sym)
                return false;
        if (def->mod != (uint16_t) MANY && def->mod != (mod & ~Kbd_Ignore))
                return false;
        if (term_get_keypad_app_mode(Term) ? (def->pad < 0) : (def->pad > 0))
                return false;
#define NUMLOCK true
        if (NUMLOCK && def->pad == KNUM)
                return false;
        if (term_get_cursor_app_mode(Term) ? (def->dir < 0) : (def->dir > 0))
                return false;
        return true;
}

@ @c
static bool
_user_match_pointer (KeySym     sym,
                     tt_keydef *def,
                     uint16_t   mod)
{
        if (def->sym != sym)
                return false;
        if (def->mod != (uint16_t) MANY && def->mod != (mod & ~Kbd_Ignore))
                return false;
                abort();
}

@ @c
tt_keydef *
user_find_keydef (KeySym   sym,
                  uint16_t mod)
{
        tt_keylist *tbl = _user_find_keylist(sym);
        if (!tbl)
                return NULL;
        size_t i = 0;
        tt_keydef *k = &tbl->d[0];
        for (; i < tbl->len; i++, k++)
                if (_user_match_key(sym, k, mod))
                        return k;
        return NULL;
}



@ N - Number of clicks; 1, 2 or 3 (2b)

B - Button ID, 1--5 (3b)

D - Up, down or drag, 0, 1 or 2 (2b)

@<Pub...@>=
#define user_is_keysym_click(K) !!((K) & 0xff000000)
#define user_click_to_keysym(N,B,D) ((uint32_t) ((N) << 4 | (D) << 6 | (B)) << 24)
#define user_keysym_to_click(N,B,D,K) do { \
        (N) = ((K) & 0x30000000) >> 28;    \
        (B) = ((K) & 0x07000000) >> 24;    \
        (D) = ((K) & 0xc0000000) >> 30;    \
} while (0)
@#
void user_pointer_motion (XEvent *);
void user_pointer_press (XEvent *);
void user_pointer_release (XEvent *);

@ @d X_BUTTONS 5
@<Global...@>=
tt_pointer          Pointer = { -1, -1 };
tt_pointer          Drag[X_BUTTONS] = {0};
struct timespec     Click_Timeout[3] = {
        { 0, 150000000 }, { 0, 300000000 }, { 0, 600000000 }
};

@ @<Type def...@>=
typedef struct tt_pointer {
        int             x, y;
        uint16_t        col, row;
        uint8_t         clicks;
        struct timespec at[3];
} tt_pointer;

@ @<Fun...@>=
static void _user_pointer_to_cell (tt_pointer *);
static void _user_select_drag (tt_keyarg, XEvent *, KeySym, char *);
static void _user_select_end (tt_keyarg, XEvent *, KeySym, char *);
static void _user_select_paste (tt_keyarg, XEvent *, KeySym, char *);
static void _user_select_start (tt_keyarg, XEvent *, KeySym, char *);

@ @c
static void
_user_pointer_to_cell (tt_pointer *p)
{
        if (p->x == -1) {
                assert(p->y == -1);
                p->col = p->row = 0;
                return;
        }
        tt_font *dfont = tt_panel_get_font(Panel, 0, 0);
        assert(dfont);
        float x = (float) (p->x - Border_L) / Stretch_Width;
        x /= tt_panel_font_get_cell_width(dfont);
        float y = (float) (p->y - Border_T) / Stretch_Height;
        y /= tt_panel_font_get_cell_height(dfont);
        p->col = x + 1;
        p->row = y + 1;
}

@ @c
void
user_pointer_press (XEvent *xev)
{
        struct timespec now, d;
        clock_gettime(CLOCK_MONOTONIC, &now);
        int b = xev->xbutton.button;
        assert(b && b <= X_BUTTONS);
        Drag[b - 1].x = xev->xbutton.x;
        Drag[b - 1].y = xev->xbutton.y;
        _user_pointer_to_cell(&Drag[b - 1]);
        int c = Drag[b - 1].clicks;
        switch (c) {
        case 1:
        case 2:
                timespecsub(&now, &Drag[b - 1].at[c - 1], &d);
                if (timespeccmp(&d, &Click_Timeout[c - 1], <)) {
                        c = ++Drag[b - 1].clicks;
                        Drag[b - 1].at[c - 1] = now;
                } else {
        case 0:
        case 3:
                        c = Drag[b - 1].clicks = 1;
                        Drag[b - 1].at[0] = now;
                }
        }

        KeySym k = user_click_to_keysym(c, b, 1);
        tt_keydef *act = user_find_keydef(k, xev->xkey.state);
        if (act)
                (act->fun)(act->val, xev, k, NULL);
}

@ @c
void
user_pointer_motion (XEvent *xev)
{
        Pointer.x = xev->xmotion.x;
        Pointer.y = xev->xmotion.y;
        _user_pointer_to_cell(&Pointer);
        for (size_t b = 1; b <= X_BUTTONS; b++) {
                if (!Drag[b - 1].col)
                        assert(!Drag[b - 1].row);
                else {
                        assert(Drag[b - 1].row);
                        assert(Drag[b - 1].clicks);
                        KeySym k = user_click_to_keysym(Drag[b - 1].clicks, b, 2);
                        tt_keydef *act = user_find_keydef(k, xev->xkey.state);
                        if (act)
                                (act->fun)(act->val, xev, k, NULL);
                }
        }
}

@ @c
void
user_pointer_release (XEvent *xev)
{
        int b = xev->xbutton.button;
        assert(b && b <= X_BUTTONS);
        int c = Drag[b - 1].clicks;
        Drag[b - 1].row = Drag[b - 1].col = 0;
        KeySym k = user_click_to_keysym(c, b, 0);
        tt_keydef *act = user_find_keydef(k, xev->xkey.state);
        if (act)
                (act->fun)(act->val, xev, k, NULL);
}

@ @c
static void
_user_select_start (tt_keyarg  arg,
                    XEvent    *xev,
                    KeySym     sym,
                    char      *txt)
{
        int n, b, d;
        user_keysym_to_click(n, b, d, sym);
        tt_panel_select_start(Panel, b, n, Drag[b - 1].col, Drag[b - 1].row);
}

@ @c
static void
_user_select_drag (tt_keyarg  arg,
                   XEvent    *xev,
                   KeySym     sym,
                   char      *txt)
{
        int n, b, d;
        user_keysym_to_click(n, b, d, sym);
        tt_panel_select_move(Panel, b, Pointer.col, Pointer.row);
}

@ @c
static void
_user_select_end (tt_keyarg  arg,
                  XEvent    *xev,
                  KeySym     sym,
                  char      *txt)
{
        int n, b, d;
        user_keysym_to_click(n, b, d, sym);
        tt_panel_select_end(Panel, b);
        XSetSelectionOwner(X_Display, arg.u, Main_Window, xev->xbutton.time);
        if (XGetSelectionOwner(X_Display, arg.u) != Main_Window)
                ;//clear
}

@ @c
static void
_user_select_paste (tt_keyarg  arg,
                    XEvent    *xev,
                    KeySym     sym,
                    char      *txt)
{
        Atom string = XInternAtom(X_Display, "UTF8_STRING", 0);
        XConvertSelection(X_Display, arg.u, string, arg.u, Main_Window,
                CurrentTime);
}

@ @.TODO@> @d KEYSEQ(S,M,K,C,V) {
        .sym = (S),
        .mod = (M),
        .pad = (K),
        .dir = (C),
        .fun = _user_keymap_sequence,
        .val = { .s = (V) }
}
@d MANY ~0
@d MOFF  0
@d MA   Mod1Mask
@d MS   ShiftMask
@d MC   ControlMask
@d M2   Mod2Mask
@d M3   Mod3Mask
@d M4   Mod4Mask
@d MAS  MA | MS
@d MAC  MA | MC
@d MSC  MS | MC
@d MASC MA | MS | MC
@d KANY  0
@d KOFF -1
@d KON  +1
@d KNUM +2
@d CANY  0
@d COFF -1
@d CON  +1
@<Global...@>=
tt_keydef Default_Keymap[] = {
        {
                .sym = XK_T,
                .mod = MAC,
                .fun = _user_keymap_ttytrace,
                .val = { .b = false }
        },
        {
                .sym = XK_T,
                .mod = MASC,
                .fun = _user_keymap_ttytrace,
                .val = { .b = true }
        },

        KEYSEQ(XK_Tab,          MOFF, KANY, CANY, "\011"),

        KEYSEQ(XK_Escape,       MANY, KANY, CANY, "\033"),

        KEYSEQ(XK_KP_Home,      MS,   KANY, COFF, "\2332J"),
        KEYSEQ(XK_KP_Home,      MS,   KANY, CON,  "\2331;2H"),
        KEYSEQ(XK_KP_Home,      MANY, KANY, COFF, "\233H"),
        KEYSEQ(XK_KP_Home,      MANY, KANY, CON,  "\2331~"),
        KEYSEQ(XK_KP_Up,        MANY, KON,  CANY, "\233Ox"),
        KEYSEQ(XK_KP_Up,        MANY, KANY, COFF, "\233[A"),
        KEYSEQ(XK_KP_Up,        MANY, KANY, CON,  "\233OA"),
        KEYSEQ(XK_KP_Down,      MANY, KON,  CANY, "\233Or"),
        KEYSEQ(XK_KP_Down,      MANY, KANY, COFF, "\233[B"),
        KEYSEQ(XK_KP_Down,      MANY, KANY, CON,  "\233OB"),
        KEYSEQ(XK_KP_Left,      MANY, KON,  CANY, "\233Ot"),
        KEYSEQ(XK_KP_Left,      MANY, KANY, COFF, "\233[D"),
        KEYSEQ(XK_KP_Left,      MANY, KANY, CON,  "\233OD"),
        KEYSEQ(XK_KP_Right,     MANY, KON,  CANY, "\233Ov"),
        KEYSEQ(XK_KP_Right,     MANY, KANY, COFF, "\233[C"),
        KEYSEQ(XK_KP_Right,     MANY, KANY, CON,  "\233OC"),
        KEYSEQ(XK_KP_Prior,     MS,   KANY, CANY, "\2335;2~"),
        KEYSEQ(XK_KP_Prior,     MANY, KANY, CANY, "\2335~"),
        KEYSEQ(XK_KP_Begin,     MANY, KANY, CANY, "\233E"),
        KEYSEQ(XK_KP_End,       MC,   KOFF, CANY, "\233J"),
        KEYSEQ(XK_KP_End,       MC,   KON,  CANY, "\2331;5F"),
        KEYSEQ(XK_KP_End,       MS,   KOFF, CANY, "\233K"),
        KEYSEQ(XK_KP_End,       MS,   KON,  CANY, "\2331;2F"),
        KEYSEQ(XK_KP_End,       MANY, KANY, CANY, "\2334~"),
        KEYSEQ(XK_KP_Next,      MS,   KANY, CANY, "\2336;2~"),
        KEYSEQ(XK_KP_Next,      MANY, KANY, CANY, "\2336~"),
        KEYSEQ(XK_KP_Insert,    MS,   KON,  CANY, "\2332;2~"),
        KEYSEQ(XK_KP_Insert,    MS,   KOFF, CANY, "\2334l"),
        KEYSEQ(XK_KP_Insert,    MC,   KOFF, CANY, "\233L"),
        KEYSEQ(XK_KP_Insert,    MC,   KON,  CANY, "\2332;5~"),
        KEYSEQ(XK_KP_Insert,    MANY, KOFF, CANY, "\2334h"),
        KEYSEQ(XK_KP_Insert,    MANY, KON,  CANY, "\2332~"),
        KEYSEQ(XK_KP_Delete,    MC,   KOFF, CANY, "\233M"),
        KEYSEQ(XK_KP_Delete,    MC,   KON,  CANY, "\2333;5~"),
        KEYSEQ(XK_KP_Delete,    MS,   KOFF, CANY, "\2332K"),
        KEYSEQ(XK_KP_Delete,    MS,   KON,  CANY, "\2333;2~"),
        KEYSEQ(XK_KP_Delete,    MANY, KOFF, CANY, "\233P"),
        KEYSEQ(XK_KP_Delete,    MANY, KON,  CANY, "\2333~"),
        KEYSEQ(XK_KP_Multiply,  MANY, KNUM, CANY, "\217j"),
        KEYSEQ(XK_KP_Add,       MANY, KNUM, CANY, "\217k"),
        KEYSEQ(XK_KP_Enter,     MANY, KNUM, CANY, "\217M"),
        KEYSEQ(XK_KP_Enter,     MANY, KOFF, CANY, "\r"),
        KEYSEQ(XK_KP_Subtract,  MANY, KNUM, CANY, "\217m"),
        KEYSEQ(XK_KP_Decimal,   MANY, KNUM, CANY, "\217n"),
        KEYSEQ(XK_KP_Divide,    MANY, KNUM, CANY, "\217o"),
        KEYSEQ(XK_KP_0,         MANY, KNUM, CANY, "\217p"),
        KEYSEQ(XK_KP_1,         MANY, KNUM, CANY, "\217q"),
        KEYSEQ(XK_KP_2,         MANY, KNUM, CANY, "\217r"),
        KEYSEQ(XK_KP_3,         MANY, KNUM, CANY, "\217s"),
        KEYSEQ(XK_KP_4,         MANY, KNUM, CANY, "\217t"),
        KEYSEQ(XK_KP_5,         MANY, KNUM, CANY, "\217u"),
        KEYSEQ(XK_KP_6,         MANY, KNUM, CANY, "\217v"),
        KEYSEQ(XK_KP_7,         MANY, KNUM, CANY, "\217w"),
        KEYSEQ(XK_KP_8,         MANY, KNUM, CANY, "\217x"),
        KEYSEQ(XK_KP_9,         MANY, KNUM, CANY, "\217y"),
        // TODO: source of the modified sequences? DEC says modifiers have no effect on cursor keys
        KEYSEQ(XK_Up,           MS,   KANY, CANY, "\2331;2A"),
        KEYSEQ(XK_Up,           MA,   KANY, CANY, "\2331;3A"),
        KEYSEQ(XK_Up,           MAS,  KANY, CANY, "\2331;4A"),
        KEYSEQ(XK_Up,           MC,   KANY, CANY, "\2331;5A"),
        KEYSEQ(XK_Up,           MSC,  KANY, CANY, "\2331;6A"),
        KEYSEQ(XK_Up,           MAC,  KANY, CANY, "\2331;7A"),
        KEYSEQ(XK_Up,           MASC, KANY, CANY, "\2331;8A"),
        KEYSEQ(XK_Up,           MANY, KANY, COFF, "\233A"),
        KEYSEQ(XK_Up,           MANY, KANY, CON,  "\217A"),
        KEYSEQ(XK_Down,         MS,   KANY, CANY, "\2331;2B"),
        KEYSEQ(XK_Down,         MA,   KANY, CANY, "\2331;3B"),
        KEYSEQ(XK_Down,         MAS,  KANY, CANY, "\2331;4B"),
        KEYSEQ(XK_Down,         MC,   KANY, CANY, "\2331;5B"),
        KEYSEQ(XK_Down,         MSC,  KANY, CANY, "\2331;6B"),
        KEYSEQ(XK_Down,         MAC,  KANY, CANY, "\2331;7B"),
        KEYSEQ(XK_Down,         MASC, KANY, CANY, "\2331;8B"),
        KEYSEQ(XK_Down,         MANY, KANY, COFF, "\233B"),
        KEYSEQ(XK_Down,         MANY, KANY, CON,  "\217B"),
        KEYSEQ(XK_Left,         MS,   KANY, CANY, "\2331;2D"),
        KEYSEQ(XK_Left,         MA,   KANY, CANY, "\2331;3D"),
        KEYSEQ(XK_Left,         MAS,  KANY, CANY, "\2331;4D"),
        KEYSEQ(XK_Left,         MC,   KANY, CANY, "\2331;5D"),
        KEYSEQ(XK_Left,         MSC,  KANY, CANY, "\2331;6D"),
        KEYSEQ(XK_Left,         MAC,  KANY, CANY, "\2331;7D"),
        KEYSEQ(XK_Left,         MASC, KANY, CANY, "\2331;8D"),
        KEYSEQ(XK_Left,         MANY, KANY, COFF, "\233D"),
        KEYSEQ(XK_Left,         MANY, KANY, CON,  "\217D"),
        KEYSEQ(XK_Right,        MS,   KANY, CANY, "\2331;2C"),
        KEYSEQ(XK_Right,        MA,   KANY, CANY, "\2331;3C"),
        KEYSEQ(XK_Right,        MAS,  KANY, CANY, "\2331;4C"),
        KEYSEQ(XK_Right,        MC,   KANY, CANY, "\2331;5C"),
        KEYSEQ(XK_Right,        MSC,  KANY, CANY, "\2331;6C"),
        KEYSEQ(XK_Right,        MAC,  KANY, CANY, "\2331;7C"),
        KEYSEQ(XK_Right,        MASC, KANY, CANY, "\2331;8C"),
        KEYSEQ(XK_Right,        MANY, KANY, COFF, "\233C"),
        KEYSEQ(XK_Right,        MANY, KANY, CON,  "\217C"),
        KEYSEQ(XK_ISO_Left_Tab, MS,   KANY, CANY, "\233Z"),
        KEYSEQ(XK_Return,       MA,   KANY, CANY, "\215"),
        KEYSEQ(XK_Return,       MANY, KANY, CANY, "\015"), // aka.~\.{\\r}

        {
                .sym = XK_Insert,
                .mod = MS,
                .fun = _user_select_paste,
                .val = { .u = XA_PRIMARY }
        },
        KEYSEQ(XK_Insert,       MS,   KOFF, CANY, "\2334l"),
        KEYSEQ(XK_Insert,       MS,   KON,  CANY, "\2332;2~"),
        KEYSEQ(XK_Insert,       MC,   KOFF, CANY, "\233L"),
        KEYSEQ(XK_Insert,       MC,   KON,  CANY, "\2332;5~"),
        KEYSEQ(XK_Insert,       MANY, KOFF, CANY, "\2334h"),
        KEYSEQ(XK_Insert,       MANY, KON,  CANY, "\2332~"),
        KEYSEQ(XK_Delete,       MC,   KOFF, CANY, "\233M"),
        KEYSEQ(XK_Delete,       MC,   KON,  CANY, "\2333;5~"),
        KEYSEQ(XK_Delete,       MS,   KOFF, CANY, "\2332K"),
        KEYSEQ(XK_Delete,       MS,   KON,  CANY, "\2333;2~"),
        KEYSEQ(XK_Delete,       MANY, KOFF, CANY, "\233P"),
        KEYSEQ(XK_Delete,       MANY, KON,  CANY, "\2333~"),
        KEYSEQ(XK_BackSpace,    MA,   KANY, CANY, "\377"),
        KEYSEQ(XK_BackSpace,    MANY, KANY, CANY, "\177"),
        KEYSEQ(XK_Home,         MS,   KANY, COFF, "\2332J"),
        KEYSEQ(XK_Home,         MS,   KANY, CON,  "\2331;2H"),
        KEYSEQ(XK_Home,         MANY, KANY, COFF, "\233H"),
        KEYSEQ(XK_Home,         MANY, KANY, CON,  "\2331~"),
        KEYSEQ(XK_End,          MC,   KOFF, CANY, "\233J"),
        KEYSEQ(XK_End,          MC,   KON,  CANY, "\2331;5F"),
        KEYSEQ(XK_End,          MS,   KOFF, CANY, "\233K"),
        KEYSEQ(XK_End,          MS,   KON,  CANY, "\2331;2F"),
        KEYSEQ(XK_End,          MANY, KANY, CANY, "\2334~"),
        KEYSEQ(XK_Prior,        MC,   KANY, CANY, "\2335;5~"),
        KEYSEQ(XK_Prior,        MS,   KANY, CANY, "\2335;2~"),
        KEYSEQ(XK_Prior,        MANY, KANY, CANY, "\2335~"),
        KEYSEQ(XK_Next,         MC,   KANY, CANY, "\2336;5~"),
        KEYSEQ(XK_Next,         MS,   KANY, CANY, "\2336;2~"),
        KEYSEQ(XK_Next,         MANY, KANY, CANY, "\2336~"),
        KEYSEQ(XK_F1,           0,    KANY, CANY, "\217P" ),
        KEYSEQ(XK_F1,           MS,   KANY, CANY, "\2331;2P"), /* F13 */
        KEYSEQ(XK_F1,           MC,   KANY, CANY, "\2331;5P"), /* F25 */
        KEYSEQ(XK_F1,           M4,   KANY, CANY, "\2331;6P"), /* F37 */
        KEYSEQ(XK_F1,           MA,   KANY, CANY, "\2331;3P"), /* F49 */
        KEYSEQ(XK_F1,           M3,   KANY, CANY, "\2331;4P"), /* F61 */
        KEYSEQ(XK_F2,           0,    KANY, CANY, "\217Q" ),
        KEYSEQ(XK_F2,           MS,   KANY, CANY, "\2331;2Q"), /* F14 */
        KEYSEQ(XK_F2,           MC,   KANY, CANY, "\2331;5Q"), /* F26 */
        KEYSEQ(XK_F2,           M4,   KANY, CANY, "\2331;6Q"), /* F38 */
        KEYSEQ(XK_F2,           MA,   KANY, CANY, "\2331;3Q"), /* F50 */
        KEYSEQ(XK_F2,           M3,   KANY, CANY, "\2331;4Q"), /* F62 */
        KEYSEQ(XK_F3,           0,    KANY, CANY, "\217R" ),
        KEYSEQ(XK_F3,           MS,   KANY, CANY, "\2331;2R"), /* F15 */
        KEYSEQ(XK_F3,           MC,   KANY, CANY, "\2331;5R"), /* F27 */
        KEYSEQ(XK_F3,           M4,   KANY, CANY, "\2331;6R"), /* F39 */
        KEYSEQ(XK_F3,           MA,   KANY, CANY, "\2331;3R"), /* F51 */
        KEYSEQ(XK_F3,           M3,   KANY, CANY, "\2331;4R"), /* F63 */
        KEYSEQ(XK_F4,           0,    KANY, CANY, "\217S" ),
        KEYSEQ(XK_F4,           MS,   KANY, CANY, "\2331;2S"), /* F16 */
        KEYSEQ(XK_F4,           MC,   KANY, CANY, "\2331;5S"), /* F28 */
        KEYSEQ(XK_F4,           M4,   KANY, CANY, "\2331;6S"), /* F40 */
        KEYSEQ(XK_F4,           MA,   KANY, CANY, "\2331;3S"), /* F52 */
        KEYSEQ(XK_F5,           0,    KANY, CANY, "\23315~"),
        KEYSEQ(XK_F5,           MS,   KANY, CANY, "\23315;2~"), /* F17 */
        KEYSEQ(XK_F5,           MC,   KANY, CANY, "\23315;5~"), /* F29 */
        KEYSEQ(XK_F5,           M4,   KANY, CANY, "\23315;6~"), /* F41 */
        KEYSEQ(XK_F5,           MA,   KANY, CANY, "\23315;3~"), /* F53 */
        KEYSEQ(XK_F6,           0,    KANY, CANY, "\23317~"),
        KEYSEQ(XK_F6,           MS,   KANY, CANY, "\23317;2~"), /* F18 */
        KEYSEQ(XK_F6,           MC,   KANY, CANY, "\23317;5~"), /* F30 */
        KEYSEQ(XK_F6,           M4,   KANY, CANY, "\23317;6~"), /* F42 */
        KEYSEQ(XK_F6,           MA,   KANY, CANY, "\23317;3~"), /* F54 */
        KEYSEQ(XK_F7,           0,    KANY, CANY, "\23318~"),
        KEYSEQ(XK_F7,           MS,   KANY, CANY, "\23318;2~"), /* F19 */
        KEYSEQ(XK_F7,           MC,   KANY, CANY, "\23318;5~"), /* F31 */
        KEYSEQ(XK_F7,           M4,   KANY, CANY, "\23318;6~"), /* F43 */
        KEYSEQ(XK_F7,           MA,   KANY, CANY, "\23318;3~"), /* F55 */
        KEYSEQ(XK_F8,           0,    KANY, CANY, "\23319~"),
        KEYSEQ(XK_F8,           MS,   KANY, CANY, "\23319;2~"), /* F20 */
        KEYSEQ(XK_F8,           MC,   KANY, CANY, "\23319;5~"), /* F32 */
        KEYSEQ(XK_F8,           M4,   KANY, CANY, "\23319;6~"), /* F44 */
        KEYSEQ(XK_F8,           MA,   KANY, CANY, "\23319;3~"), /* F56 */
        KEYSEQ(XK_F9,           0,    KANY, CANY, "\23320~"),
        KEYSEQ(XK_F9,           MS,   KANY, CANY, "\23320;2~"), /* F21 */
        KEYSEQ(XK_F9,           MC,   KANY, CANY, "\23320;5~"), /* F33 */
        KEYSEQ(XK_F9,           M4,   KANY, CANY, "\23320;6~"), /* F45 */
        KEYSEQ(XK_F9,           MA,   KANY, CANY, "\23320;3~"), /* F57 */
        KEYSEQ(XK_F10,          0,    KANY, CANY, "\23321~"),
        KEYSEQ(XK_F10,          MS,   KANY, CANY, "\23321;2~"), /* F22 */
        KEYSEQ(XK_F10,          MC,   KANY, CANY, "\23321;5~"), /* F34 */
        KEYSEQ(XK_F10,          M4,   KANY, CANY, "\23321;6~"), /* F46 */
        KEYSEQ(XK_F10,          MA,   KANY, CANY, "\23321;3~"), /* F58 */
        KEYSEQ(XK_F11,          0,    KANY, CANY, "\23323~"),
        KEYSEQ(XK_F11,          MS,   KANY, CANY, "\23323;2~"), /* F23 */
        KEYSEQ(XK_F11,          MC,   KANY, CANY, "\23323;5~"), /* F35 */
        KEYSEQ(XK_F11,          M4,   KANY, CANY, "\23323;6~"), /* F47 */
        KEYSEQ(XK_F11,          MA,   KANY, CANY, "\23323;3~"), /* F59 */
        KEYSEQ(XK_F12,          0,    KANY, CANY, "\23324~"),
        KEYSEQ(XK_F12,          MS,   KANY, CANY, "\23324;2~"), /* F24 */
        KEYSEQ(XK_F12,          MC,   KANY, CANY, "\23324;5~"), /* F36 */
        KEYSEQ(XK_F12,          M4,   KANY, CANY, "\23324;6~"), /* F48 */
        KEYSEQ(XK_F12,          MA,   KANY, CANY, "\23324;3~"), /* F60 */
        KEYSEQ(XK_F13,          0,    KANY, CANY, "\2331;2P"),
        KEYSEQ(XK_F14,          0,    KANY, CANY, "\2331;2Q"),
        KEYSEQ(XK_F15,          0,    KANY, CANY, "\2331;2R"),
        KEYSEQ(XK_F16,          0,    KANY, CANY, "\2331;2S"),
        KEYSEQ(XK_F17,          0,    KANY, CANY, "\23315;2~"),
        KEYSEQ(XK_F18,          0,    KANY, CANY, "\23317;2~"),
        KEYSEQ(XK_F19,          0,    KANY, CANY, "\23318;2~"),
        KEYSEQ(XK_F20,          0,    KANY, CANY, "\23319;2~"),
        KEYSEQ(XK_F21,          0,    KANY, CANY, "\23320;2~"),
        KEYSEQ(XK_F22,          0,    KANY, CANY, "\23321;2~"),
        KEYSEQ(XK_F23,          0,    KANY, CANY, "\23323;2~"),
        KEYSEQ(XK_F24,          0,    KANY, CANY, "\23324;2~"),
        KEYSEQ(XK_F25,          0,    KANY, CANY, "\2331;5P"),
        KEYSEQ(XK_F26,          0,    KANY, CANY, "\2331;5Q"),
        KEYSEQ(XK_F27,          0,    KANY, CANY, "\2331;5R"),
        KEYSEQ(XK_F28,          0,    KANY, CANY, "\2331;5S"),
        KEYSEQ(XK_F29,          0,    KANY, CANY, "\23315;5~"),
        KEYSEQ(XK_F30,          0,    KANY, CANY, "\23317;5~"),
        KEYSEQ(XK_F31,          0,    KANY, CANY, "\23318;5~"),
        KEYSEQ(XK_F32,          0,    KANY, CANY, "\23319;5~"),
        KEYSEQ(XK_F33,          0,    KANY, CANY, "\23320;5~"),
        KEYSEQ(XK_F34,          0,    KANY, CANY, "\23321;5~"),
        KEYSEQ(XK_F35,          0,    KANY, CANY, "\23323;5~"),
};

@ @d POINTSEQ(N,B,D,M,V) {
        .sym = user_click_to_keysym((N), (B), (D)),
        .mod = (M),
        .fun = _user_keymap_sequence,
        .val = { .s = (V) }
}
@d POINTFUN(N,B,D,M,F,V) {
        .sym = user_click_to_keysym((N), (B), (D)),
        .mod = (M),
        .fun = (F),
        .val = V
}
@d UP   0
@d DOWN 1
@d DRAG 2
@<Global...@>=
tt_keydef Default_Pointmap[] = {
        POINTSEQ(1, Button4, DOWN, MS,   "\2335;2~"),
        POINTSEQ(1, Button4, DOWN, MANY, "\031"),
        POINTSEQ(1, Button5, DOWN, MS,   "\2336;2~"),
        POINTSEQ(1, Button5, DOWN, MANY, "\005"),

        POINTSEQ(2, Button4, DOWN, MS,   "\2335;2~"),
        POINTSEQ(2, Button4, DOWN, MANY, "\031"),
        POINTSEQ(2, Button5, DOWN, MS,   "\2336;2~"),
        POINTSEQ(2, Button5, DOWN, MANY, "\005"),

        POINTSEQ(3, Button4, DOWN, MS,   "\2335;2~"),
        POINTSEQ(3, Button4, DOWN, MANY, "\031"),
        POINTSEQ(3, Button5, DOWN, MS,   "\2336;2~"),
        POINTSEQ(3, Button5, DOWN, MANY, "\005"),

        POINTFUN(1, Button1, UP,   MANY, _user_select_end, { .u = XA_PRIMARY }),
        POINTFUN(1, Button1, DOWN, MANY, _user_select_start, { .i = 0 }),
        POINTFUN(1, Button1, DRAG, MANY, _user_select_drag, { .i = 0 }),

        POINTFUN(1, Button2, UP,   MANY, _user_select_paste, { .u = XA_PRIMARY }),
};
