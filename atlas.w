@i format.w
@i types.w
\def\URLfreetype-gl{https://github.com/rougier/freetype-gl}

@** Texture Atlas. A texture atlas is used to pack several 2-dimensional
images into a single buffer. This implementation of one is based
largely on the texture atlas implementation in
freetype-gl\footnote{$^1$}{\pdfURL{\URLfreetype-gl}{\URLfreetype-gl}}.

@c
#include "atlas.h"
@#
#include <arpa/inet.h> /* |htonl| */
#include <assert.h>
#include <stdlib.h>
#include <string.h>
@#
#include "buf.h"
@#
@<Type definitions (\.{atlas.o})@>@;
@<Private functions (\.{atlas.o})@>@;

@ @(atlas.h@>=
#ifndef TURTLE_ATLAS_H
#define TURTLE_ATLAS_H
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
@<Public API (\.{atlas.o})@>@;
#endif /* |TURTLE_ATLAS_H| */

@ A texture atlas is a buffer of pixels representing an area in which
regions can be allocated. Each region can be surrounded by a border.

A node is a region within the atlas which has not been allocated,
denoted by its lower-left co-ordinates and width, extending to the
top of the atlas.

TODO: Replace flags with a byte each.

@.TODO@>
@d ATLAS_DIRTY 0 /* atlas has been modified? */
@d ATLAS_FULL 1 /* something didn't fit */
@<Type def...@>=
typedef struct tp_atlas {
        uint8_t  *data; /* atlas data (pixels) */
        tp_array  nodes; /* available regions */
        int32_t   width, height; /* size of the pixel buffer */

        int8_t    depth; /* depth (in bytes) of a pixel */
        int8_t    border;
        uint16_t  flags;
        uint32_t  spare; /* unused space available to the user */
} tp_atlas;

typedef struct tp_atlas_node {
        int32_t x, y, w;
} tp_atlas_node;

@ This API Should follow dynamic buffers in allowing the container
to be preallocated (TODO) although there is nothing that's useful
to a user (except maybe the spare flag bits).

@.TODO@>
@<Public...@>=
typedef struct tp_atlas tp_atlas;
@#
tp_atlas *atlas_alloc (int32_t, int32_t, int8_t, int8_t, uint32_t, bool);
void atlas_clear_dirty (tp_atlas *);
void atlas_destroy (tp_atlas *);
int atlas_find_region (tp_atlas *, int32_t, int32_t, int32_t[4]);
int8_t atlas_get_depth (tp_atlas *);
bool atlas_get_dirty (tp_atlas *);
bool atlas_get_full (tp_atlas *);
int32_t atlas_get_height (tp_atlas *);
uint32_t atlas_get_spare (tp_atlas *);
uint32_t atlas_get_texture (tp_atlas *);
int32_t atlas_get_width (tp_atlas *);
uint8_t *atlas_ref (tp_atlas *, int32_t, int32_t);
uint8_t *atlas_ref_border (tp_atlas *);
void atlas_set_dirty (tp_atlas *);
void atlas_set_region (tp_atlas *, int32_t[4], uint8_t *, int8_t, size_t,
        size_t);
void atlas_set_spare (tp_atlas *, uint32_t);
void atlas_set_texture (tp_atlas *, uint32_t);

@ @<Private...@>=
static int32_t _atlas_fit (tp_atlas *, size_t, int32_t, int32_t);
static int _atlas_insert_node (tp_atlas *, size_t, tp_atlas_node);
static tp_atlas_node _atlas_load_node (tp_atlas *, size_t);
static void _atlas_merge (tp_atlas *);
static void _atlas_save_node (tp_atlas *, size_t, tp_atlas_node);

@ A new atlas begins empty with a single node covering the available
space. If each allocation within the region will be surrounded by
a border then the lower-left pixel of the atlas is set to its colour
(this pixel will always be a border pixel). The rest of the atlas
can be filled with random noise to assist in debugging.

@d ATLAS_MAX_SIZE 0x10000
@c
tp_atlas *
atlas_alloc (int32_t  width,
             int32_t  height,
             int8_t   depth,
             int8_t   border,
             uint32_t colour,
             bool     scramble)
{
        tp_atlas *new;
        tp_atlas_node whole;
        size_t dim, i;

        assert(SIZE_MAX >= ATLAS_MAX_SIZE * (long) ATLAS_MAX_SIZE);
        if (width <= 0 || width > ATLAS_MAX_SIZE)
                return NULL;
        if (height <= 0 || height > ATLAS_MAX_SIZE)
                return NULL;
        if (border < 0 || border > 32)
                return NULL; /* Doesn't check that anything will actually fit. */
        if (depth != 1 && depth != 2 && depth != 4)
                return NULL;
@#
        new = calloc(1, sizeof (tp_atlas));
        if (!new)
                goto fail_new;
        dim = width * height;
        new->data = calloc(dim, depth);
        if (!new->data)
                goto fail_data;
        if (!array_init(&new->nodes, sizeof (tp_atlas_node), 32, 32, 0))
                goto fail_nodes;
        new->height = height;
        new->width = width;
        new->depth = depth;
        new->border = border;
        if (scramble)
                arc4random_buf(new->data, dim * depth);
@#
        whole.x = whole.y = 0;
        whole.w = width;
        array_push(&new->nodes, 1, &whole);
        if (border) {
                if (depth == 4)
                        *((uint32_t *) atlas_ref(new, 0, 0)) = colour;
                else if (depth == 1)
                        *((uint8_t *) atlas_ref(new, 0, 0)) = colour;
                else
                        *((uint16_t *) atlas_ref(new, 0, 0)) = colour;
        }

        return new;

fail_nodes:
        free(new->data);
fail_data:
        free(new);
fail_new:
        return NULL;
}

@ @c
void
atlas_destroy (tp_atlas *me)
{
        array_destroy(&me->nodes);
        free(me->data);
        free(me);
}

@ Accessor routines. Pixel data.

@c
uint8_t *
atlas_ref (tp_atlas *me,
           int32_t   x,
           int32_t   y)
{
        assert(me->depth == 1 || me->depth == 2 || me->depth == 4);
        return me->data + (y * me->depth * me->width) + (x * me->depth);
}

@ @c
uint8_t *
atlas_ref_border (tp_atlas *me)
{
        if (!me->border)
                return NULL;
        return atlas_ref(me, 0, 0);
}

@ Getting container metadata.

@c
int8_t
atlas_get_depth (tp_atlas *me)
{
        return me->depth;
}

bool
atlas_get_dirty (tp_atlas *me)
{
        return IS_FLAG(me->flags, ATLAS_DIRTY);
}

bool
atlas_get_full (tp_atlas *me)
{
        return IS_FLAG(me->flags, ATLAS_FULL);
}

int32_t
atlas_get_height (tp_atlas *me)
{
        return me->height;
}

uint32_t
atlas_get_spare (tp_atlas *me)
{
        return me->spare;
}

int32_t
atlas_get_width (tp_atlas *me)
{
        return me->width;
}

@ Updating (some) container metadata. The main purpose for the atlas
is an OpenGL texture which the dynamic buffer for the nodes has
unused space for.

@c
void
atlas_clear_dirty (tp_atlas *me)
{
        CLEAR_FLAG(me->flags, ATLAS_DIRTY);
}

void
atlas_set_dirty (tp_atlas *me)
{
        SET_FLAG(me->flags, ATLAS_DIRTY);
}

void
atlas_set_spare (tp_atlas *me,
                 uint32_t  s)
{
        me->spare = s;
}

@ An unallocated region in the atlas is represented by the co-ordinates
of its lower-left corner and its height. These routines save and
retrieve these nodes.

@c
static int
_atlas_insert_node (tp_atlas      *me,
                    size_t         index,
                    tp_atlas_node  node)
{
        if (!array_insert(&me->nodes, index, 1, &node))
                return -1;
        return 0;
}

static void
_atlas_save_node (tp_atlas      *me,
                  size_t         index,
                  tp_atlas_node  node)
{
        assert(index < array_get_length(&me->nodes));
        *((tp_atlas_node *) array_ref(&me->nodes, index)) = node;
}

static tp_atlas_node
_atlas_load_node (tp_atlas *me,
                  size_t    index)
{
        assert(index < array_get_length(&me->nodes));
        return *((tp_atlas_node *) array_ref(&me->nodes, index));
}

@ Finding an unused region in the atlas large enough to fit an image
$|width| \times |height|$ pixels.

@c
int
atlas_find_region (tp_atlas *me,
                   int32_t   width,
                   int32_t   height,
                   int32_t   ret[4])
{
        int_fast32_t best_height, best_index, best_width, shrink, x, y, x0, y0;
        size_t i;
        tp_atlas_node node, prev;

        assert(width > 0 && width < ATLAS_MAX_SIZE - me->border * 2);
        assert(height > 0 && height < ATLAS_MAX_SIZE - me->border * 2);
        width += me->border * 2;
        height += me->border * 2;
        @<Find the best available region@> @;
        @<Remove the space from the available regions list@> @;
        if (me->border) {
                @<Paint the border around the allocated region@>
        }
        return 0;
}

@ To search for available space the list of available region nodes
is scanned in order to find the node (or several adjacent nodes)
with the best fit, which is the node with the largest available
vertical space or which fits the request width in a single region node.

@<Find the best available region@>=
best_index = -1;
best_width = best_height = ATLAS_MAX_SIZE;
x0 = y0 = 0;
for (i = 0; i < array_get_length(&me->nodes); i++) {
        y = _atlas_fit(me, i, width, height);
        if (y == -1)
                continue;
        node = _atlas_load_node(me, i);
        if ((y + height < best_height) || (y + height == best_height
                        && node.w > 0 && node.w < best_width)) {
                /* remember this success */
                best_index = i;
                best_height = y + height;
                best_width = node.w;
                x0 = node.x;
                y0 = y;
        }
}
if (best_index == -1) {
        SET_FLAG(me->flags, ATLAS_FULL);
        return -1;
}

@ A new region node (representing {\it available\/} space) is
inserted immediately before the node chosen above, with the same
horizontal co-ordinate and width as the allocated space but above
it. If this fails (because the node buffer is full) the allocation
fails and the atlas remains unchanged.

Subsequent regions including the selected one are updated by moving
their left edge to the right of this now-used space. They're removed
if this would mean they have negative or zero width.

@<Remove the space from the available regions list@>=
node.x = x0;
node.y = y0 + height;
node.w = width;
if (_atlas_insert_node(me, best_index, node) == -1)
        return -1;

for (i = best_index + 1; i < array_get_length(&me->nodes); i++) {
        prev = _atlas_load_node(me, i - 1); /* May not be |node|. */
        node = _atlas_load_node(me, i);
        if (node.x >= prev.x + prev.w)
                continue;
        shrink = prev.x + prev.w - node.x;
        node.x += shrink;
        node.w -= shrink;
        _atlas_save_node(me, i, node);
        if (node.w > 0)
                break;
        array_remove(&me->nodes, i, 1);
        i--; /* next node was moved to the current location */
}
_atlas_merge(me);

@ The region returned to the caller does not include the border.

@s T int
@d _atlas_clear_border(T) for (i = 0; i < (size_t) me->border; i++) {
        T _ref = *((T *) atlas_ref_border(me));
        for (x = x0; x < x0 + width - me->border; x++) {
                *((T *) atlas_ref(me, x, y0 + i)) = _ref;
                *((T *) atlas_ref(me, x + me->border, y0 + height - i - 1)) = _ref;
        }
        for (y = y0; y < y0 + height - me->border; y++) {
                *((T *) atlas_ref(me, x0 + i, y + me->border)) = _ref;
                *((T *) atlas_ref(me, x0 + i + width - me->border, y)) = _ref;
        }
}
@<Paint the border around the allocated region@>=
ret[0] = x0 + me->border;
ret[1] = y0 + me->border;
ret[2] = width - me->border * 2;
ret[3] = height - me->border * 2;
if (me->depth == 1)
        _atlas_clear_border(uint8_t) @;
else if (me->depth == 2)
        _atlas_clear_border(uint16_t) @;
else
        _atlas_clear_border(uint32_t) @;

@ If a region is not wide enough on its own to fit all of the image
adjacent regions can be used if they're tall enough. The height of
the tallest region node is returned or -1 if the image doesn't fit
here.

@c
static int32_t
_atlas_fit (tp_atlas *me,
            size_t    index,
            int32_t   width,
            int32_t   height)
{
        int_fast32_t y, rem;
        tp_atlas_node node;

        assert(index < array_get_length(&me->nodes));
        assert(width <= ATLAS_MAX_SIZE);

        node = _atlas_load_node(me, index);
        if (node.x + width > me->width - 1) /* give up if there's no
                                                space to the right */
                return -1;
        y = node.y;
        rem = width;
        while (rem > 0) { /* check that regions to the right are tall enough */
                assert(index < array_get_length(&me->nodes)); /* will not
                                occur before |width <= 0| */
                node = _atlas_load_node(me, index);
                if (node.y > y)
                        y = node.y; /* remember this height
                                if the region is taller */
                if (y + height > me->height - 1) /* give up if the
                                vertical space above is too small */
                        return -1;
                rem -= node.w;
                index++;
        }

        return y;
}

@ After allocated space is removed from the list of available region
nodes and adjacent regions may have become the same height which
this routine merges into a single node.

@c
static void
_atlas_merge (tp_atlas *me)
{
        tp_atlas_node next, node;
        size_t i;

        for (i = 0; i < array_get_length(&me->nodes) - 1; i++) {
                node = _atlas_load_node(me, i); /* may not be |next| */
                next = _atlas_load_node(me, i + 1);
                if (node.y == next.y) {
                        node.w += next.w;
                        _atlas_save_node(me, i, node);
                        array_remove(&me->nodes, i + 1, 1);
                        i--; /* next node was moved to the current location */
                }
        }
}

@ The last routine in this library is not related to finding an
unallocated region, but having found one the user will want to fill
it in, which this routine can do line-by-line or all at once. This
is the only non-accessor routine which uses the dirty flag.

The |stride| value is the byte width of a line of pixels in the
source data and |begin| \AM\ |end| the lines to copy from and
(non-inclusively) to.

@c
void
atlas_set_region (tp_atlas *me,
                  int32_t   region[4],
                  uint8_t  *data,
                  int8_t    stride,
                  size_t    begin, @|
                  size_t    end)
{
        uint8_t *dst, *src;
        size_t i, last;

        assert(me && me->data);
        assert(region[0] < me->width - 1);
        assert(region[2] && region[0] + region[2] <= me->width - 1);
        assert(region[1] < me->height - 1);
        assert(region[3] && region[1] + region[3] <= me->height - 1);
        assert(data);
        assert(begin < (size_t) region[3]);
        assert(end == 0 || end > begin);
        assert(end <= (size_t) region[3]);
        assert(me->depth == 1 || me->depth == 2 || me->depth == 4);

        last = end ? end : (size_t) region[3];
        for (i = begin; i < last; i++) {
                src = data;
                src += i * stride;
                dst = me->data;
                dst += ((region[1] + i) * me->width + region[0]) * me->depth;
                memmove(dst, src, region[2] * me->depth);
        }
        SET_FLAG(me->flags, ATLAS_DIRTY);
}

@** Index.
