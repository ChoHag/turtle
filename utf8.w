@i format.w
@i types.w

@** UTF-8 parser.

@(utf8.h@>=
#ifndef TP_UTF8_H
#define TP_UTF8_H
#include <stdint.h>
@h
@<Type definitions (\.{unicode.o})@>@;
@<Function declarations (\.{unicode.o})@>@;
#endif /* |TP_UTF8_H| */

@ @c
#include "utf8.h"
@#
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
@#
@<Global variables (\.{unicode.o})@>@;

@ The |utfio| object converts between (possible) encodings. |value|
is usually the codepoint and |buf| a |NULL|-terminated encoding,
hence the spare byte if the encoding takes all four. In the unlikely
event that this is used to convert UTF-EBCDIC the fifth byte will
be used by encodings of codepoints above |0x3ffff|.

@<Type def...@>=
enum {
        UTFIO_NONE = 0,         /* Doubles duty as a scanner error. */
        UTFIO_INVALID,          /* Encountered an invalid byte/encoding. */
        UTFIO_BAD_CONTINUATION, /* Continuation byte when expecting a
                                        starter byte. */
        UTFIO_BAD_STARTER,      /* Starter byte when expecting a
                                        continuation byte. */
        UTFIO_OVERLONG,         /* Overlong encoding encountered. */
        UTFIO_SURROGATE,        /* Half a surrogate pair was encoded. */
        UTFIO_PROGRESS,         /* Valid but more bytes are required (an
                                        error if an EOF is premature). */
        UTFIO_COMPLETE,         /* A conversion is complete. */
        UTFIO_EOF               /* Sometimes an error. */
};

typedef uint32_t u_cp;

typedef struct {
        u_cp    value;
        uint8_t buf[5];
        int8_t  remaining, length;
        int8_t  status;
} utf8_io;

@ The last two code points of each plane are noncharacters:
\.{U+...FFFE} and \.{U+...FFFF} (recall that the byte-order-mark
has value \.{U+FEFF}) for a total of 34 code points in 17 planes.
In addition to these and the halves of a surrogate pair there is a
contiguous range of another 32 noncharacter code points in the BMP
(plane 0): \.{U+FDD0}--\.{FDEF}.

|UTFIO| is static global metadata used to validate and parse UTF-8,
which is the only encoding currently supported.

@d UCP_NONCHAR_MASK   0xfffe /* The lowest bit doesn't matter. */
@d UCP_NONBMP_MIN     0xfdd0 /* Contained within the ``Arabic Presentation */
@d UCP_NONBMP_MAX     0xfdef /* ... Forms-A block'' by a historic accident. */
@d UCP_SURROGATE_MIN  0xd800 /* Values $\ge2^{16}$ in UTF-16 encoded text. */
@d UCP_SURROGATE_MAX  0xe000
@d UCP_MAX            0x10ffff
@d UCP_REPLACEMENT    0xfffd /* */
@#
@d ucp_isnonchar(C)   (((C) & UCP_NONCHAR_MASK) == UCP_NONCHAR_MASK)
@d ucp_isnonbmp(C)    ((C) >= UCP_NONBMP_MIN && (C) <= UCP_NONBMP_MAX)
@d ucp_isnonrange(C)  ((C) < 0 || (C) > UCP_MAX)
@d ucp_issurrogate(C) ((C) >= UCP_SURROGATE_MIN && (C) <= UCP_SURROGATE_MAX)
@#
@d ucp_isascii(C)     (!(C) || ((C) > 0 && (C) <= 0x7f))
@d ucp_isc0(C)        (!(C) || ((C) > 0 && (C) <= 0x1f) || (C) == 0x7f)
@d ucp_isc1(C)        ((C) >= 0x80 && (C) <= 0x9f)
@d ucp_iscontrol(C)   (ucp_isc0(C) || ucp_isc1(C))
@#
@d uio_isu8cont(C)    (((C) & ~0x3f) == 0x80) /* matches |UTFIO[0]| */
@d uio_isascii(C)     (((C) & ~0x7f) == 0x00) /* matches |UTFIO[1]| */
@d uio_isu8len2(C)    (((C) & ~0x1f) == 0xc0) /* matches |UTFIO[2]| */
@d uio_isu8len3(C)    (((C) & ~0x0f) == 0xe0) /* matches |UTFIO[3]| */
@d uio_isu8len4(C)    (((C) & ~0x07) == 0xf0) /* matches |UTFIO[4]| */
@d uio_isu8start(C)   (uio_isu8len2(C) || uio_isu8len3(C) || uio_isu8len4(C))
@
@d ucp_plane(O)       (((O) & 0x001f0000) >> 16)
@d ucp_plane_code(O)  ((O) &  0x0000ffff) /* Code point within plane. */
@d ucp_stick(O)       (((O) & 0x001ffff0) >> 4)
@<Global...@>=
struct {
        int16_t size; /* How many bits are supplied by this byte (padded). */
        uint8_t data; /* Mask: Encoded bits. */
        uint8_t lead; /* Mask: Leading bits which will be set. */
        int32_t max; /* Maximum code-point value this many bytes can encode. */
} UTFIO[] = {@|
        { 6, 0x3f, 0x80, 0x000000 }, /* Continuation byte. */@t\iII@>
        { 7, 0x7f, 0x00, 0x00007f }, /* Single ASCII byte. */@t\iII@>
        { 5, 0x1f, 0xc0, 0x0007ff }, /* Start of 2-byte encoding. */@t\iII@>
        { 4, 0x0f, 0xe0, 0x00ffff }, /* Start of 3-byte encoding. */@t\iII@>
        { 3, 0x07, 0xf0, 0x10ffff }, /* Start of 4-byte encoding. */@t\iII@>
@t\4\4@>};

@ There's no need to use |error_code| here although the returned
utfio status is also updated in |ctx|.

@<Fun...@>=
char *utfio_errormsg (utf8_io *);
void utfio_init (utf8_io *);
int utfio_read (utf8_io *, uint8_t);
int utfio_reread (utf8_io *, uint8_t);
utf8_io utfio_write (int32_t);
char *utfio_write_cstr (int32_t);

@ @c
void
utfio_init (utf8_io *ctx)
{
        memset(ctx, 0, sizeof (utf8_io));
}

@ @c
int
utfio_read (utf8_io *ctx,
            uint8_t  bval)
{
        int i;

        for (i = 0; i <= 4; i++) {
                if ((bval & ~UTFIO[i].data) != UTFIO[i].lead)
                        continue;
                else if (i == 0) {
                        if (ctx->remaining)
                                ctx->remaining--;
                        else
                                return ctx->status = UTFIO_BAD_CONTINUATION;
                } else {
                        if (ctx->remaining)
                                return ctx->status = UTFIO_BAD_STARTER;
                        else
                                ctx->remaining = i - 1;
                }
                ctx->buf[(int) ctx->length++] = bval;
                ctx->value |= (bval & UTFIO[i].data) << (6 * ctx->remaining);
                if (ctx->remaining)
                        return ctx->status = UTFIO_PROGRESS;
                else if (ucp_isnonrange(ctx->value))
                        return ctx->status = UTFIO_INVALID;
                else if (ucp_isnonchar(ctx->value))
                        return ctx->status = UTFIO_INVALID;
                else if (ucp_issurrogate(ctx->value))
                        return ctx->status = UTFIO_SURROGATE;
                else if (ctx->value <= UTFIO[ctx->length - 1].max)
                        return ctx->status = UTFIO_OVERLONG;
                else
                        return ctx->status = UTFIO_COMPLETE;
        }
        return ctx->status = UTFIO_INVALID;
}

@ The same API but without error checking, for use on known-valid
utf-8.

@c
int
utfio_reread (utf8_io *ctx,
              uint8_t  bval)
{
        int i;

        for (i = 0; i <= 4; i++)
                if ((bval & ~UTFIO[i].data) != UTFIO[i].lead)
                        continue;
                else {
                        if (ctx->remaining)
                                ctx->remaining--;
                        else
                                ctx->remaining = i - 1;
                        ctx->buf[(int) ctx->length++] = bval;
                        ctx->value = (bval & UTFIO[i].data) << (6 * ctx->remaining);
                        ctx->status = ctx->remaining ? UTFIO_PROGRESS : UTFIO_COMPLETE;
                        return ctx->status;
                }
        abort(); /* UNREACHABLE */
}

@ @c
utf8_io
utfio_write (int32_t cp)
{
        int len, mask, next;
        utf8_io ubuf = {0};

        ubuf.value = cp;
        if (ucp_isnonrange(cp) || ucp_isnonchar(cp)
                        || ucp_isnonbmp(cp) || ucp_issurrogate(cp)) {
                cp = UCP_REPLACEMENT;
                if (cp == EOF)
                        ubuf.status = UTFIO_EOF;
                else
                        ubuf.status = UTFIO_INVALID;
        } else
                ubuf.status = UTFIO_COMPLETE;
        if (cp <= UTFIO[1].max)
                ubuf.buf[(int) ubuf.length++] = cp;
        else {
                if (cp <= UTFIO[2].max)
                        ubuf.remaining = len = 2;
                else if (cp <= UTFIO[3].max)
                        ubuf.remaining = len = 3;
                else
                        ubuf.remaining = len = 4;
                while (ubuf.remaining) {
                        ubuf.remaining--;
                        next = ubuf.remaining ? 0 : len;
                        mask = UTFIO[next].data;
                        ubuf.buf[ubuf.remaining] = (cp & mask) | UTFIO[next].lead;
                        cp >>= UTFIO[next].size;
                        ubuf.length++;
                }
                assert(!cp);
        }
        return ubuf;
}

@ @c
char *
utfio_write_cstr (int32_t cp)
{
        static utf8_io ubuf; /* not on the stack */
        char *replacement = "\357\277\275"; /* U+FFFD */
        if (!cp)
                return "";
        ubuf = utfio_write(cp);
        if (ubuf.status != UTFIO_COMPLETE)
                return replacement;
        assert(ubuf.length && ubuf.length <= 4);
        assert(ubuf.buf[ubuf.length] == 0);
        return (char *) ubuf.buf;
}

@ @c
char *
utfio_errormsg (utf8_io *ctx)
{
        switch (ctx->status) {
        case UTFIO_NONE:
                return "no error";
        case UTFIO_INVALID:
                return "invalid byte";
        case UTFIO_BAD_CONTINUATION:
                return "unexpected continuation";
        case UTFIO_BAD_STARTER:
                return "cut-short sequence";
        case UTFIO_OVERLONG:
                return "overlong encoding";
        case UTFIO_SURROGATE:
                return "encoded surrogate half";
        case UTFIO_PROGRESS:
                return "in progress (no error)";
        case UTFIO_COMPLETE:
                return "complete (no error)";
        case UTFIO_EOF:
                return "read past EOF";
        default:
                __builtin_unreachable();
        }
}
