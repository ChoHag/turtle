@i format.w
@i types.w

@* TBD Logging Module.

@c
#include "log.h"
#include <stdio.h>

#ifdef TT_DEBUG
uintmax_t Debug = ~0;
#else
uintmax_t Debug = 0;
#endif
bool Verbose = true;

@ @(log.h@>=
#ifndef TURTLE_LOG_H
#define TURTLE_LOG_H
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>

@h
@<Public API (\.{log.o})@>@;

#endif /* |TURTLE_LOG_H| */

@ The \CEE/ preprocessor is ugly.

@<Pub...@>=
extern uintmax_t Debug;
extern bool Verbose;
void bug_ (const char *, const char *, ...) __attribute__((format(printf, 2, 3)));
void vbug_ (const char *, const char *, va_list);
void inform (const char *, const char *, ...) __attribute__((format(printf, 2, 3)));
void vinform (const char *, const char *, va_list);
void complain (const char *, const char *, ...) __attribute__((format(printf, 2, 3)));
void vcomplain (const char *, const char *, va_list);
@#
#define LOG_PREFIX "log"

#define bug(P,M,...) @[ do { if (Debug) bug_((P), (M) __VA_OPT__(,) __VA_ARGS__); } while (0) @]
#define fbug(M,...) @[ do { if (Debug) bug_(LOG_PREFIX, (M) __VA_OPT__(,) __VA_ARGS__); } while (0) @]
#define finform(M,...) @[ inform(LOG_PREFIX, (M) __VA_OPT__(,) __VA_ARGS__) @]
#define fcomplain(M,...) @[ complain(LOG_PREFIX, (M) __VA_OPT__(,) __VA_ARGS__) @]

#define vbug(P,M,V) @[ do @+ { @+ if (Debug) vbug_((P), (M) (V)); @+ } @+ while (0) @]
#define vfbug(M,V) @[ do @+ { @+ if (Debug) vbug_(LOG_PREFIX, (M), (V)); @+ } @+ while (0) @]
#define vfinform(M,V) @[ vinform(LOG_PREFIX, (M), (V)) @]
#define vfcomplain(M,V) @[ vcomplain(LOG_PREFIX, (M), (V)) @]

@ @c
void
bug_ (const char *prefix,
      const char *fmt, ...)
{
        va_list vl;

        if (!Debug)
                return;
        va_start(vl, fmt);
        vbug_(prefix, fmt, vl);
        va_end(vl);
}

void
vbug_ (const char *prefix,
       const char *fmt,
       va_list     vl)
{
        if (!Debug)
                return;
        fprintf(stderr, "%s BUG: ", prefix);
        vfprintf(stderr, fmt, vl);
        fprintf(stderr, "\n");
}

@ @c
void
inform (const char *prefix,
        const char *fmt, ...)
{
        va_list vl;

        if (!Verbose)
                return;
        va_start(vl, fmt);
        vinform(prefix, fmt, vl);
        va_end(vl);
}

void
vinform (const char *prefix,
         const char *fmt,
         va_list     vl)
{
        if (!Verbose)
                return;
        fprintf(stdout, "%s: ", prefix);
        vfprintf(stdout, fmt, vl);
        fprintf(stdout, "\n");
}

@ @c
void
complain (const char *prefix,
          const char *fmt, ...)
{
        va_list vl;
        va_start(vl, fmt);
        vcomplain(prefix, fmt, vl);
        va_end(vl);
}

void
vcomplain (const char *prefix,
           const char *fmt,
           va_list     vl)
{
        fprintf(stderr, "%s: ", prefix);
        vfprintf(stderr, fmt, vl);
        fprintf(stderr, "\n");
}
