@i format.w
@i types.w

@** Testing the terminal object.

This module sets up |tt_term| and |tt_panel| terminal objects and
prepares them for being tested.

@c #include "test-term.h"

@ @(test-term.h@>=
#include <assert.h>
#include <err.h>
#include <sys/queue.h>
#include "tap.h"
#include "tterm.h"
#include "tpanel.h"

#include <sys/tree.h>
#include "atlas.h"
#include "hrfont.h"
#include "panel-internal.h"
#include "term-internal.h"

typedef struct outbuf {
        STAILQ_ENTRY(outbuf) @, each;
        char   *buf;
        size_t  len;
} outbuf;

typedef struct output {
        STAILQ_HEAD(@[@], outbuf) @, head;
} output;

tt_cell *copy_panel_contents (tt_panel *, int);
tt_term *prepare_test_term (int16_t, int16_t, output **);
void finish_test_term (tt_term *, output *);

void t_cursor_position (char *, tt_panel *, int, int, char *);
void t_default_margins (char *, tt_term *, tt_panel *);
bool t_panel_contents_match (char *, tt_panel *, char *);
void t_the_contents_are_unchanged (char *, tt_cell *, tt_panel *, bool);
void t_the_current_contents_are_all_erased (char *, tt_panel *);
void t_the_current_contents_are_erased_from_to (char *, tt_panel *,
        int, int, int, int);
void t_there_was_no_output (char *, output *);

@ Anything that would be sent to the host is stored in a list instead.

@c
static void
_capture_term_output (uint8_t *buf,
                      size_t   len,
                      void    *arg)
{
        output *out = arg;
        outbuf *next = calloc(1, sizeof (*next));
        next->buf = calloc(1, len + 1);
        memmove(next->buf, buf, len);
        next->len = len;
        STAILQ_INSERT_TAIL(&out->head, next, each);
}

@ Preparing to test a new terminal object.

TODO: Don't capture output if unwanted.

@.TODO@>
@c
tt_term *
prepare_test_term (int16_t   width,
                   int16_t   height,
                   output  **saveout)
{
        if (!width)
                width = 80;
        if (!height)
                height = 24;
        tt_panel *panel = tt_panel_new(width, height);
        tt_font *font = tt_panel_font_new_from_builtin(panel);
        if (!font)
                errx(1, "tt_panel_font_new_from_builtin");
        tt_panel_invoke_font(panel, 0, font, false);
        output *out = calloc(1, sizeof (out));
        if (saveout)
                *saveout = out;
        STAILQ_INIT(&out->head);
        return term_new(panel, _capture_term_output, out);
}

@ @c
void
finish_test_term (tt_term *term,
                  output  *log)
{
}

@ Each of the panel's contents is a list of |tt_cell| objects.

@c
tt_cell *
copy_panel_contents (tt_panel *panel,
                     int       alt)
{
        int16_t w, h;
        tt_panel_get_size(panel, &w, &h, NULL, NULL, NULL, NULL);
        tt_cell *ret = calloc(1 + (int) w * h, sizeof (*ret));
        int *reth = (int *) ret;
        assert((char *) (reth + 2) <= (char *) (ret + 1));
        reth[0] = w;
        reth[1] = h;
        memmove(ret + 1, panel->content[alt], w * h * sizeof (*ret));
        return ret;
}

@ @c
void
t_the_current_contents_are_erased_from_to (char     *tid,
                                           tt_panel *panel,
                                           int       brow,
                                           int       bcol,
                                           int       erow,
                                           int       ecol)
{
        if (!erow)
                erow = panel->height;
        if (!ecol)
                ecol = panel->width;
        int w = panel->width;
        bool empty = true;
        for (int row = brow; empty && row <= erow; row++) {
                for (int col = (row == brow) ? bcol : 1;
                                empty && col <= ((row == erow) ? ecol : w);
                                col++) {
                        tt_cell *c = _panel_refp(panel->content[0], w,
                                col - 1, row - 1);
                        if (c->mask)
                                empty = false; /* ie.~|cp| at el. */
                        if (c->flags != panel->blank.flags)
                                empty = false;
                        if (memcmp(&c->bg, &panel->blank.bg, sizeof (c->bg)))
                                empty = false;
                        if (memcmp(&c->fg, &panel->blank.fg, sizeof (c->fg)))
                                empty = false;
                        assert(!panel->blank.drawn);
                        if (c->drawn)
                                empty = false;
                }
        }
        tap_ok(empty, "%s: the current panel contents are erased from"
                " %u,%u to %u,%u inclusive", tid, brow, bcol, erow, ecol);
}

@ @c
void
t_the_current_contents_are_all_erased (char     *tid,
                                       tt_panel *panel)
{
        t_the_current_contents_are_erased_from_to(tid, panel, 1, 1, 0, 0);
}

@ @c
void
t_the_contents_are_unchanged (char     *tid,
                              tt_cell  *backup,
                              tt_panel *panel,
                              bool      alt)
{
        int *head = (int *) (backup++);
        bool sizeok = head[0] == panel->width && head[1] == panel->height;
        tap_ok(sizeok, "%s: %s panel size is unchanged", tid,
                alt ? "alternate" : "current");
        bool check;
        if (sizeok)
                check = ! memcmp(backup, panel->content[1],
                        sizeof (backup) * head[0] * head[1]);
        else
                check = false;
        tap_ok(check, "%s: %s panel content is unchanged", tid,
                alt ? "alternate" : "current");
}

@ @c
#if 0 @|
@t\iIV@>       " !\"#$%&'()" @|
               "!\"#$%&'()*" @|
               "\"#$%&'()*+" @|
                "#$%&'()*+," @|
                "$%&'()*+,-" @|
                "%&'()*+,-." @|
                "&'()*+,-./" @|
                "'()*+,-./0" @|
                "()*+,-./01" @|
                ")*+,-./012"
#endif

bool
t_panel_contents_match (char     *tid,
                        tt_panel *pp,
                        char     *template)
{
        bool ok = false;
        char *want = template;
        assert(strlen(template) == pp->width * pp->height);
        for (int yi = 0; yi < pp->height; yi++) {
                for (int xi = 0; xi < pp->width; xi++) {
                        if (*want == '~') {
                                if (_panel_ref(pp, 0, xi, yi)->cp != 0)
                                        goto done;
                        } else {
                                if (_panel_ref(pp, 0, xi, yi)->cp != *want)
                                        goto done;
                        }
                        want++;
                }
        }
        ok = true;
done:
        tap_ok(ok, "%s: content matches template", tid);
        return ok;
}

@ @c
void
t_there_was_no_output (char   *tid,
                       output *log)
{
        tap_ok(STAILQ_EMPTY(&log->head),
                "%s: no data was sent to the host", tid);
}

@ @c
void
t_cursor_position (char     *tid,
                   tt_panel *panel,
                   int       col,
                   int       row,
                   char     *movement)
{
        bool ok = panel->cursor_row == row && panel->cursor_col == col;
        tap_ok(ok, "%s: cursor is %s (%u,%u)", tid, movement, row, col);
        if (!ok)
                tap_diag("at %u,%u", panel->cursor_row, panel->cursor_col);
}

@ @c
void
t_default_margins (char     *tid,
                   tt_term  *term,
                   tt_panel *panel)
{
        tap_ok(panel->left == 1
                        && panel->top == 1
                        && panel->right == panel->width
                        && panel->bottom == panel->height,
                "%s: margins are unset", tid);
        tap_ok(IS_FLAG(term->flags, TERM_LEFT_RIGHT_MARGIN),
                "%s: margins are enabled", tid);
}
