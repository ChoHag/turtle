@i format.w
@i types.w

@** Turtle's Graphics Layer.

@c
#include "tgl.h"
@#
#include <assert.h>
#include <err.h>
#include <errno.h>
#include <math.h>
#
#include <epoxy/gl.h>
#include <epoxy/glx.h>
#include <GL/glu.h>
@#
#include "buf.h"
#include "log.h"
#include "tpanel.h"
#include "trix.h"
#undef LOG_PREFIX
#define LOG_PREFIX "tgl"

@<Global variables (\.{tgl.o})@>@;

@ @(tgl.h@>=
#ifndef TT_TGL_H
#define TT_TGL_H

#include <stdint.h>
#include <stdbool.h>
#include <sys/time.h>

@<Type definitions (\.{tgl.o})@>@;

extern float        Stretch_Width, Stretch_Height;
extern float        Border_L, Border_T;

void tgl_initgl (void);

void tgl_drawgl (struct timespec *);

void tgl_panel_to_vertex (void);
void tgl_draw_data (bg_vertex *, fg_vertex *, int, int, select_vertex *);

#endif /* |TT_TGL_H| */

@ @<Type def...@>=
typedef struct bg_vertex {
        uint8_t  r, g, b, a;
} bg_vertex;

typedef struct fg_vertex {
        uint8_t  r, g, b, a;
        struct {
                int16_t s, t;
        } type[2];
        int16_t  x, y, down;
} fg_vertex;

typedef struct cursor_vertex {
        int16_t col, row;
        uint8_t r, g, b, a;
} cursor_vertex;

@ @<Global...@>=
extern Display     *X_Display;
extern Window       Main_Window;
extern int          Window_Width, Window_Height;
extern tt_panel    *Panel;

#define DRAWS 5
#define DRAW_BG 0
#define DRAW_FG 1
#define DRAW_CURSOR 2
#define DRAW_SELECT_RECT 3
#define DRAW_SELECT_LINES 4
GLuint              Shader[DRAWS], VAO[DRAWS], VBO[DRAWS];
GLuint              Type_Case;

bg_vertex          *Draw_BG;
fg_vertex          *Draw_FG;
cursor_vertex       Draw_Cursor;
int                 Draw_Dim = 0, Draw_Panel_Width = 0, Draw_Panel_Height = 0;
float               Draw_Project[16], Draw_Zoom[16];
select_vertex       Draw_Select;

@ @c
static void
_tgl_gl_errors (char *fnname)
{
        for (GLenum e = glGetError(); e != GL_NO_ERROR; e = glGetError())
                fcomplain("%s: %s", fnname, gluErrorString(e));
}

@ @d UNIFORM_PANEL_SIZE 0
@d UNIFORM_TILE_SIZE 1
@d UNIFORM_TYPECASE_SIZE 2
@d UNIFORM_PROJECT 3
@d UNIFORM_ZOOM 4
@d UNIFORM_Z 5
@d UNIFORM_BLINK 6
@<Uniform definitions (\.{tgl.o})@>=
@[layout (location = 0) uniform ivec2@] panel_size;
@[layout (location = 1) uniform ivec2@] tile_size;
@[layout (location = 2) uniform int@] typecase_size;
@[layout (location = 3) uniform mat4@] project;
@[layout (location = 4) uniform mat4@] zoom;
@[layout (location = 5) uniform float@] z;
@[layout (location = 6) uniform vec2@] blink;

@ The shader for the cells' background generates a coloured square.
The vertex shader calculates the cell location from the vertex ID
and passes it along with the colour to the geometry shader.

@(bgvertex.glsl@>=
@<Uniform definitions (\.{tgl.o})@>@;
layout (location = 0) in vec4 colour;
out VS_OUT {
        ivec2 cell;
        vec4  colour;
} vs_out;

void
main (void)
{
        int row = gl_VertexID / panel_size.x;
        int col = gl_VertexID - (row * panel_size.x);
        vs_out.cell = ivec2(col, row);
        vs_out.colour = colour;
}

@ The geometry shader turns the cell location into coordinates of
the cell's corners, emitted as vertices, and passes the colour to
the fragment shader.

@(bggeometry.glsl@>=
@<Uniform definitions (\.{tgl.o})@>@;
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;
in VS_OUT {
        ivec2 cell;
        vec4  colour;
} gs_in[];
out vec4 f_colour;

void
main (void)
{
        ivec2 posbl = gs_in[0].cell * tile_size;
        ivec2 postr = (gs_in[0].cell + ivec2(1, 1)) * tile_size;
        f_colour = gs_in[0].colour;
        gl_Position = project * zoom * vec4(posbl, z, 1);
        EmitVertex();
        gl_Position = project * zoom * vec4(postr.x, posbl.y, z, 1);
        EmitVertex();
        gl_Position = project * zoom * vec4(posbl.x, postr.y, z, 1);
        EmitVertex();
        gl_Position = project * zoom * vec4(postr, z, 1);
        EmitVertex();
        EndPrimitive();
}

@ The fragment shader simply fills in the square with a solid colour.

@(bgfragment.glsl@>=
@<Uniform definitions (\.{tgl.o})@>@;
in vec4 f_colour;
out vec4 frag;

void
main (void)
{
        frag = f_colour;
}

@ The cursor shader shares the geometry and fragment shader. Its
vertex shader expects the cursor's cell location and calculates the
cursor's blink transparency.

@ @(cursorvertex.glsl@>=
@<Uniform definitions (\.{tgl.o})@>@;
layout (location = 0) in vec4 colour;
layout (location = 1) in ivec2 cell;
out VS_OUT {
        ivec2 cell;
        vec4  colour;
} vs_out;

void
main (void)
{
        vs_out.cell = cell;
        vs_out.colour = colour;
        vs_out.colour.a *= blink.x * 0.8;
}

@ Similar to the background, the foreground shader also requires
the glyph visible in the cell in the form of atlas texture coordinates.

@(fgvertex.glsl@>=
@<Uniform definitions (\.{tgl.o})@>@;
layout (location = 0) in vec4 colour;
layout (location = 1) in ivec4 type;
layout (location = 2) in ivec3 inset;
layout (location = 4) in ivec4 px;
out VS_OUT {
        ivec2 cell;
        ivec3 inset;
        ivec4 type;
        vec4  colour;
} vs_out;

void
main (void)
{
        int row = gl_VertexID / panel_size.x;
        int col = gl_VertexID - (row * panel_size.x);
        vs_out.cell = ivec2(col, row);
        vs_out.inset = inset;
        vs_out.type = type;
        vs_out.colour = colour;
}

@ TODO: move inset.z to a uniform.

@.TODO@>
@(fggeometry.glsl@>=
@<Uniform definitions (\.{tgl.o})@>@;
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;
in VS_OUT {
        ivec2 cell;
        ivec3 inset;
        ivec4 type;
        vec4  colour;
} gs_in[];
out vec4 f_colour;
out vec2 f_type;

void
main (void)
{
        vec2 pq = gs_in[0].type.st + gs_in[0].type.pq;
        vec4 tx = vec4(gs_in[0].type.st, pq) / typecase_size;
        ivec2 posbl = gs_in[0].cell * tile_size;
        posbl.x += gs_in[0].inset.x;
        posbl.y += gs_in[0].inset.z;
        posbl.y += gs_in[0].inset.y - gs_in[0].type.q;
        ivec2 postr = posbl + gs_in[0].type.pq;
        f_colour = gs_in[0].colour;
        f_type = vec2(tx.s, tx.t);
        gl_Position = project * zoom * vec4(posbl, z, 1);
        EmitVertex();
        f_type = vec2(tx.p, tx.t);
        gl_Position = project * zoom * vec4(postr.x, posbl.y, z, 1);
        EmitVertex();
        f_type = vec2(tx.s, tx.q);
        gl_Position = project * zoom * vec4(posbl.x, postr.y, z, 1);
        EmitVertex();
        f_type = vec2(tx.p, tx.q);
        gl_Position = project * zoom * vec4(postr, z, 1);
        EmitVertex();
        EndPrimitive();
}

@ @(fgfragment.glsl@>=
@<Uniform definitions (\.{tgl.o})@>@;
layout (binding = 0) uniform sampler2D typecase;
in vec4 f_colour;
in vec2 f_type;
out vec4 frag;

void
main (void)
{
        frag = f_colour * texture(typecase, f_type);
}

@ Buffers |Draw_BG| and |Draw_FG| must be filled in before calling
this. The data for the cursor is tiny and sent in the draw loop.

@c
#include "bgvertex.e"
#include "bggeometry.e"
#include "bgfragment.e"
#include "fgvertex.e"
#include "fggeometry.e"
#include "fgfragment.e"
#include "cursorvertex.e"
#include "selectvertex.e"
#include "selectrectgeometry.e"
#include "selectlinesgeometry.e"
void
tgl_initgl (void)
{
        Shader[DRAW_BG] = trix_link_embedded_program(NULL,
                Embed_bgvertex, Embed_bgvertex_Length,
                Embed_bgfragment, Embed_bgfragment_Length,
                Embed_bggeometry, Embed_bggeometry_Length);
        Shader[DRAW_FG] = trix_link_embedded_program(NULL,
                Embed_fgvertex, Embed_fgvertex_Length,
                Embed_fgfragment, Embed_fgfragment_Length,
                Embed_fggeometry, Embed_fggeometry_Length);
        Shader[DRAW_CURSOR] = trix_link_embedded_program(NULL,
                Embed_cursorvertex, Embed_cursorvertex_Length,
                Embed_bgfragment, Embed_bgfragment_Length,
                Embed_bggeometry, Embed_bggeometry_Length);
        Shader[DRAW_SELECT_RECT] = trix_link_embedded_program(NULL,
                Embed_selectvertex, Embed_selectvertex_Length,
                Embed_bgfragment, Embed_bgfragment_Length,
                Embed_selectrectgeometry, Embed_selectrectgeometry_Length);
        Shader[DRAW_SELECT_LINES] = trix_link_embedded_program(NULL,
                Embed_selectvertex, Embed_selectvertex_Length,
                Embed_bgfragment, Embed_bgfragment_Length,
                Embed_selectlinesgeometry, Embed_selectlinesgeometry_Length);

        glGenVertexArrays(DRAWS, VAO);
        glGenBuffers(DRAWS, VBO);

        /* bg */
        glBindVertexArray(VAO[DRAW_BG]);
        glBindBuffer(GL_ARRAY_BUFFER, VBO[DRAW_BG]);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                sizeof (bg_vertex), (void *) offsetof (bg_vertex, r));

        /* fg */
        glBindVertexArray(VAO[DRAW_FG]);
        glBindBuffer(GL_ARRAY_BUFFER, VBO[DRAW_FG]);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(0, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                sizeof (fg_vertex), (void *) offsetof (fg_vertex, r));
        glVertexAttribIPointer(1, 4, GL_SHORT, sizeof (fg_vertex),
                (void *) offsetof (fg_vertex, type));
        glVertexAttribIPointer(2, 3, GL_SHORT, sizeof (fg_vertex),
                (void *) offsetof (fg_vertex, x));

        /* cursor */
        glBindVertexArray(VAO[DRAW_CURSOR]);
        glBindBuffer(GL_ARRAY_BUFFER, VBO[DRAW_CURSOR]);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(0, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                sizeof (cursor_vertex), (void *) offsetof (cursor_vertex, r));
        glVertexAttribIPointer(1, 2, GL_SHORT, sizeof (cursor_vertex),
                (void *) offsetof (cursor_vertex, col));
        glBufferData(GL_ARRAY_BUFFER, sizeof (cursor_vertex), NULL,
                GL_STATIC_DRAW);

        /* select */
        glBindVertexArray(VAO[DRAW_SELECT_RECT]);
        glBindBuffer(GL_ARRAY_BUFFER, VBO[DRAW_SELECT_RECT]);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(0, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                sizeof (select_vertex), (void *) offsetof (select_vertex, r));
        glVertexAttribIPointer(1, 4, GL_SHORT, sizeof (select_vertex),
                (void *) offsetof (select_vertex, col0));
        glBufferData(GL_ARRAY_BUFFER, sizeof (select_vertex), &Draw_Select,
                GL_STATIC_DRAW);

        /* identical */
        VAO[DRAW_SELECT_LINES] = VAO[DRAW_SELECT_RECT];
        VBO[DRAW_SELECT_LINES] = VBO[DRAW_SELECT_RECT];

        glGenTextures(1, &Type_Case);
        glBindTexture(GL_TEXTURE_2D, Type_Case);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindTexture(GL_TEXTURE_2D, 0);
        glClearColor(0.333f, 0.5f, 0.666f, 1.0f);
        _tgl_gl_errors("tgl_initgl");
}

@ @c
static void
_tgl_project_panel (void)
{
        float l = 0, r = Window_Width, b = 0, t = Window_Height, n = 1, f = 10;
        float tx = -(r + l) / (r - l);
        float ty = -(t + b) / (t - b);
        float tz = -(f + n) / (f - n);
        float ortho[16] = {
                2 / (r - l),
                0,
                0,
                0,
@#
                0,
                2 / (t - b),
                0,
                0,
@#
                0,
                0,
                2 / (f - n),
                0,
@#
                tx, ty, tz, 1
        };
        memmove(Draw_Project, ortho, sizeof (ortho));
}

@ @<Global...@>=
enum {
        TTD_RESIZE_FIXED,
        TTD_RESIZE_STRETCH
}                   Resize_Fixed_Size = TTD_RESIZE_FIXED;
float               Stretch_Width = 1, Stretch_Height = 1;
float               Border_L = 0, Border_T = 0;

@ @c
static void
_tgl_zoom_panel (void)
{
        tt_font *dfont = tt_panel_get_font(Panel, 0, 0);
        assert(dfont);
        int tw = tt_panel_font_get_cell_width(dfont);
        int th = tt_panel_font_get_cell_height(dfont);
        int pw = tt_panel_get_width(Panel);
        int ph = tt_panel_get_height(Panel);
        if (Resize_Fixed_Size == TTD_RESIZE_STRETCH) {
                float w = (float) Window_Width / (pw * tw);
                float h = (float) Window_Height / (ph * th);
                Stretch_Width = w, Stretch_Height = h;
                Border_L = Border_T = 0;
                float m[16] = {
                        w,  0,  0,  0,
                        0,  h,  0,  0,
                        0,  0,  1,  0,
                        0,  0,  0,  1
                };
                memmove(Draw_Zoom, m, sizeof (m));
        } else if (Resize_Fixed_Size == TTD_RESIZE_FIXED) {
                float w = pw * tw;
                float h = ph * th;
                float sw = (float) Window_Width / w;
                float sh = (float) Window_Height / h;
                float tx = 0, ty = 0;
                if (sh < sw)
                        tx = (float) (Window_Width - w * sh) / 2, w = h = sh;
                else
                        ty = (float) (Window_Height - h * sw) / 2, w = h = sw;
                Stretch_Width = w, Stretch_Height = h;
                Border_L = tx, Border_T = ty;
                float m[16] = {
                        w,  0,  0,  0,
                        0,  h,  0,  0,
                        0,  0,  1,  0,
                        tx, ty, 0,  1
                };
                memmove(Draw_Zoom, m, sizeof (m));
        }
}

@ @c
void
tgl_drawgl (struct timespec *pnow)
{
        _tgl_project_panel();
        _tgl_zoom_panel();

        struct timespec now;
        if (!pnow) {
                clock_gettime(CLOCK_MONOTONIC, &now);
                pnow = &now;
        }

        uint32_t size[2] = { Draw_Panel_Width, Draw_Panel_Height };
        tt_font *dfont = tt_panel_get_font(Panel, 0, 0);
        assert(dfont);
        uint32_t cell[2] = {
                tt_panel_font_get_cell_width(dfont),
                tt_panel_font_get_cell_height(dfont)
        };

        double dnow = pnow->tv_sec;
        dnow += (double) pnow->tv_nsec / 1000000000; /* $0\ldots\infty$ */
        double blink, p = tt_panel_get_cursor_period(Panel);
        double fast, f = p / 2;
        fast = (sin(((dnow - (f * (int) (dnow / f))) / p) * 2 * M_PI) + 1) / 2;
        dnow -= p * (int) (dnow / p); /* $0\ldots$|period| */
        dnow /= p; /* $0\ldots1$ */
        dnow *= 2 * M_PI; /* $0\ldots2\pi$ */
        blink = sin(dnow); /* $-1\ldots1$ */
        blink += 1; /* $0\ldots2$ */
        blink /= 2; /* $0\ldots1$ */

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glFrontFace(GL_CCW);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_DEPTH_CLAMP);

        glViewport(0, 0, Window_Width, Window_Height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glBindTexture(GL_TEXTURE_2D, Type_Case);
        if (tt_panel_type_case_get_dirty(Panel)) {
                size_t s = tt_panel_type_case_get_size(Panel);
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, s, s, 0,
                        GL_RGBA, GL_UNSIGNED_BYTE,
                        tt_panel_type_case_get_data(Panel));
                tt_panel_type_case_clear_dirty(Panel);
        }

        glUseProgram(Shader[DRAW_BG]);
        glBindVertexArray(VAO[DRAW_BG]);
        glUniform1f(UNIFORM_Z, 5);
        glUniform2iv(UNIFORM_PANEL_SIZE, 1, size);
        glUniform2iv(UNIFORM_TILE_SIZE, 1, cell);
        glUniformMatrix4fv(UNIFORM_PROJECT, 1, GL_FALSE, Draw_Project);
        glUniformMatrix4fv(UNIFORM_ZOOM, 1, GL_FALSE, Draw_Zoom);
        glDrawArrays(GL_POINTS, 0, Draw_Dim);

        glUseProgram(Shader[DRAW_FG]);
        glBindVertexArray(VAO[DRAW_FG]);
        glUniform1f(UNIFORM_Z, 4);
        glUniform2iv(UNIFORM_PANEL_SIZE, 1, size);
        glUniform2iv(UNIFORM_TILE_SIZE, 1, cell);
        glUniform1i(UNIFORM_TYPECASE_SIZE, tt_panel_type_case_get_size(Panel));
        glUniformMatrix4fv(UNIFORM_PROJECT, 1, GL_FALSE, Draw_Project);
        glUniformMatrix4fv(UNIFORM_ZOOM, 1, GL_FALSE, Draw_Zoom);
        glDrawArrays(GL_POINTS, 0, Draw_Dim);

        glUseProgram(Shader[DRAW_CURSOR]);
        glBindVertexArray(VAO[DRAW_CURSOR]);
        glUniform1f(UNIFORM_Z, 3);
        glUniform2iv(UNIFORM_PANEL_SIZE, 1, size);
        glUniform2iv(UNIFORM_TILE_SIZE, 1, cell);
        glUniformMatrix4fv(UNIFORM_PROJECT, 1, GL_FALSE, Draw_Project);
        glUniformMatrix4fv(UNIFORM_ZOOM, 1, GL_FALSE, Draw_Zoom);
        glUniform2f(UNIFORM_BLINK, blink, fast);
        tt_panel_cursor_get_pos(Panel, &Draw_Cursor.col, &Draw_Cursor.row);
        Draw_Cursor.col--;
        Draw_Cursor.row = Draw_Panel_Height - Draw_Cursor.row;
        (*(tt_rgba *) &Draw_Cursor.r) = tt_panel_cursor_get_colour(Panel);
        glBindBuffer(GL_ARRAY_BUFFER, VBO[DRAW_CURSOR]);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof (Draw_Cursor), &Draw_Cursor);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDrawArrays(GL_POINTS, 0, 1);

        if (Draw_Select.col0) {
                if (Draw_Select.rectangle
                                || Draw_Select.row0 == Draw_Select.row1)
                        glUseProgram(Shader[DRAW_SELECT_RECT]);
                else
                        glUseProgram(Shader[DRAW_SELECT_LINES]);
                assert(VAO[DRAW_SELECT_RECT] == VAO[DRAW_SELECT_LINES]);
                glBindVertexArray(VAO[DRAW_SELECT_RECT]);
                glUniform1f(UNIFORM_Z, 2);
                glUniform2iv(UNIFORM_PANEL_SIZE, 1, size);
                glUniform2iv(UNIFORM_TILE_SIZE, 1, cell);
                glUniformMatrix4fv(UNIFORM_PROJECT, 1, GL_FALSE, Draw_Project);
                glUniformMatrix4fv(UNIFORM_ZOOM, 1, GL_FALSE, Draw_Zoom);
                glDrawArrays(GL_POINTS, 0, 1);
        }

        glBindVertexArray(0);
        glUseProgram(0);
        glBindTexture(GL_TEXTURE_2D, 0);

        glXSwapBuffers(X_Display, Main_Window);
}



@ @<Type def...@>=
typedef struct select_vertex {
        int16_t col0, row0;
        int16_t col1, row1;
        uint8_t r, g, b, a;
        bool rectangle; // not used by the shader
} select_vertex;

@ @(selectvertex.glsl@>=
layout (location = 0) in vec4 colour;
layout (location = 1) in ivec4 area;
out VS_OUT {
        ivec4 area;
        vec4  colour;
} vs_out;

void
main (void)
{
        vs_out.area = area;
        vs_out.colour = colour;
}

@ @(selectrectgeometry.glsl@>=
@<Uniform definitions (\.{tgl.o})@>@;
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;
in VS_OUT {
        ivec4 area;
        vec4  colour;
} gs_in[];
out vec4 f_colour;

void
main (void)
{
        ivec2 posbl = (gs_in[0].area.xy - ivec2(1, 1)) * tile_size;
        ivec2 postr = gs_in[0].area.zw * tile_size;
        f_colour = gs_in[0].colour;
        gl_Position = project * zoom * vec4(posbl, z, 1);
        EmitVertex();
        gl_Position = project * zoom * vec4(postr.x, posbl.y, z, 1);
        EmitVertex();
        gl_Position = project * zoom * vec4(posbl.x, postr.y, z, 1);
        EmitVertex();
        gl_Position = project * zoom * vec4(postr, z, 1);
        EmitVertex();
        EndPrimitive();
}

@ @(selectlinesgeometry.glsl@>=
@<Uniform definitions (\.{tgl.o})@>@;
layout (points) in;
layout (triangle_strip, max_vertices = 12) out;
in VS_OUT {
        ivec4 area;
        vec4  colour;
} gs_in[];
out vec4 f_colour;

void
main (void)
{
        // area.z > area.y
        ivec2 topbl = ivec2(gs_in[0].area.x, gs_in[0].area.y) - ivec2(1, 1);
        ivec2 toptr = ivec2(panel_size.x, gs_in[0].area.y);
        ivec2 midbl = ivec2(1, gs_in[0].area.w + 1) - ivec2(1, 1);
        ivec2 midtr = ivec2(panel_size.x, gs_in[0].area.y - 1);
        ivec2 botbl = ivec2(1, gs_in[0].area.w) - ivec2(1, 1);
        ivec2 bottr = ivec2(gs_in[0].area.z, gs_in[0].area.w);
        topbl *= tile_size;
        toptr *= tile_size;
        midbl *= tile_size;
        midtr *= tile_size;
        botbl *= tile_size;
        bottr *= tile_size;

        f_colour = gs_in[0].colour;
        { /* top line */
                gl_Position = project * zoom * vec4(topbl, z, 1);
                EmitVertex();
                gl_Position = project * zoom * vec4(toptr.x, topbl.y, z, 1);
                EmitVertex();
                gl_Position = project * zoom * vec4(topbl.x, toptr.y, z, 1);
                EmitVertex();
                gl_Position = project * zoom * vec4(toptr, z, 1);
                EmitVertex();
                EndPrimitive();
        }
        { /* bottom line */
                gl_Position = project * zoom * vec4(botbl, z, 1);
                EmitVertex();
                gl_Position = project * zoom * vec4(bottr.x, botbl.y, z, 1);
                EmitVertex();
                gl_Position = project * zoom * vec4(botbl.x, bottr.y, z, 1);
                EmitVertex();
                gl_Position = project * zoom * vec4(bottr, z, 1);
                EmitVertex();
                EndPrimitive();
        }
        if (midbl.y <= midtr.y) {
                gl_Position = project * zoom * vec4(midbl, z, 1);
                EmitVertex();
                gl_Position = project * zoom * vec4(midtr.x, midbl.y, z, 1);
                EmitVertex();
                gl_Position = project * zoom * vec4(midbl.x, midtr.y, z, 1);
                EmitVertex();
                gl_Position = project * zoom * vec4(midtr, z, 1);
                EmitVertex();
                EndPrimitive();
        }
}


























@ |newbg| and |newfg| are saved and later freed by |tgl_draw_data|.

@.TODO@>
@d SWAPC(A,B) do {
        int16_t _tmp = (A);
        (A) = (B);
        (B) = _tmp;
} while (0);
@c
void
tgl_panel_to_vertex (void)
{
        int width = tt_panel_get_width(Panel);
        int height = tt_panel_get_height(Panel);
        int dim = width * height;
        bg_vertex *newbg = calloc(dim, sizeof (*newbg)), *bgp = newbg;
        fg_vertex *newfg = calloc(dim, sizeof (*newfg)), *fgp = newfg;
        for (size_t row = height; row >= 1; row--) {
                for (size_t col = 1; col <= width; col++) {
                        tt_rgba bg, fg;
                        uint32_t flags;
                        tt_panel_cell_get_colour(Panel, col, row, &flags,
                                &bg, &fg, true);
                        memmove(&bgp->r, &bg, sizeof (bg));
                        memmove(&fgp->r, &fg, sizeof (fg));
@#
                        tt_glyph *glyph = tt_panel_cell_get_glyph(Panel,
                                col, row);
                        if (!glyph)
                                fgp->a = 0;
                        else if (IS_FLAG(flags, TTP_FAINT))
                                fgp->a = (fgp->a * 3) / 9;
                        else if (IS_FLAG(flags, TTP_BOLD))
                                fgp->a = 255;
                        else
                                fgp->a = (fgp->a * 9) / 10;
@#
                        int32_t texarea[4];
                        tt_panel_glyph_get_region(glyph, texarea);
                        fgp->type[0].s = texarea[0];
                        fgp->type[0].t = texarea[1];
                        fgp->type[1].s = texarea[2];
                        fgp->type[1].t = texarea[3];

                        if (glyph) {
                                tt_font *f = tt_panel_glyph_get_font(glyph);
                                fgp->x = tt_panel_glyph_get_offset_x(glyph);
                                fgp->y = tt_panel_glyph_get_offset_y(glyph);
                                fgp->down = tt_panel_font_get_cell_inset(f);
                        }
@#
                        bgp++, fgp++;
                }
        }

        select_vertex sel = {0};
        int16_t *area = &sel.col0;
        uint32_t f;
        if (tt_panel_get_selection_range(Panel, 1, area, &f)) {
                area[1] = height - area[1] + 1;
                area[3] = height - area[3] + 1;
                sel.rectangle = false; /* TODO */
                if (sel.rectangle || area[1] == area[3]) {
                        if (area[0] > area[2])
                                SWAPC(area[0], area[2]);
                        if (area[1] > area[3])
                                SWAPC(area[1], area[3]);
                } else {
                        if (area[1] < area[3]) {
                                SWAPC(area[0], area[2]);
                                SWAPC(area[1], area[3]);
                        }
                }
                sel.r = sel.g = sel.b = 200;
                sel.a = 127;
        }

        tgl_draw_data(newbg, newfg, width, height, &sel);
}

@ @c
void
tgl_draw_data (bg_vertex     *bgbuf,
               fg_vertex     *fgbuf,
               int            width,
               int            height,
               select_vertex *select)
{
        free(Draw_BG);
        free(Draw_FG);
        Draw_BG = bgbuf;
        Draw_FG = fgbuf;
        if (width != Draw_Panel_Width || height != Draw_Panel_Height) {
                Draw_Panel_Width = width;
                Draw_Panel_Height = height;
                Draw_Dim = width * height;
                glBindBuffer(GL_ARRAY_BUFFER, VBO[DRAW_BG]);
                glBufferData(GL_ARRAY_BUFFER, sizeof (bg_vertex) * Draw_Dim,
                        bgbuf, GL_STATIC_DRAW);
                glBindBuffer(GL_ARRAY_BUFFER, VBO[DRAW_FG]);
                glBufferData(GL_ARRAY_BUFFER, sizeof (fg_vertex) * Draw_Dim,
                        fgbuf, GL_STATIC_DRAW);
        } else {
                glBindBuffer(GL_ARRAY_BUFFER, VBO[DRAW_BG]);
                glBufferSubData(GL_ARRAY_BUFFER, 0,
                        sizeof (bg_vertex) * Draw_Dim, Draw_BG);
                glBindBuffer(GL_ARRAY_BUFFER, VBO[DRAW_FG]);
                glBufferSubData(GL_ARRAY_BUFFER, 0,
                        sizeof (fg_vertex) * Draw_Dim, Draw_FG);
        }
        memmove(&Draw_Select, select, sizeof (*select));
        assert(VBO[DRAW_SELECT_RECT] == VBO[DRAW_SELECT_LINES]);
        glBindBuffer(GL_ARRAY_BUFFER, VBO[DRAW_SELECT_RECT]);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof (select_vertex),
                &Draw_Select);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
}
