@i format.w
@i types.w
% TODO: The section numbers in this chapter should be hidden.
\def\URLlibevent{http://libevent.org/}
\def\URLlibepoxy{https://github.com/anholt/libepoxy}
\def\URLportable{https://github.com/nemequ/portable-snippets}
\def\URLCWEB{https://www-cs-faculty.stanford.edu/~knuth/cweb.html}

@** Turtle. A computer terminal is a device for communicating with
a computer. When the term first arose a computer was a person who
would calculate, or compute, the answer to mathematical problems.
In the middle of the 20th century automated computing machines were
invented which could do this electronically and a terminal was a
device with which somebody could interact with one of these machines.

Computer terminals received intense development in the late 20th
century with features galore but eventually settled on a 2-dimensional
grid of cells that can contain, for the most part, alphabetic
characters.

Turtle emulates these terminals in the same vein as XTerm.

@ These are the standards, including some pseudo or de-facto
standards, which govern a terminal's communication with its host:

ECMA-6, known as ISO-646, ANSI-X3.4 or ASCII. 7-Bit Coded Character
Set.

ECMA-35, ISO-2022 or ANSI-X3.41: Character Code Structure and
Extension Techniques.

ECMA-43, or ISO-4873: 8-Bit Coded Character Set Structure and Rules.

ECMA-48, ISO-6429 or ANSI-X3.64: Control Functions for Coded Character
Sets.

Turtle also supports Unicode, also known as ISO-10646.

Digital Equipment Corporation made a series of standard documents
describing the operation of their terminals and terminal emulators
which Turtle mostly emulates. These are known as DEC-STD-070.

The development of personal computers led Xterm to become the de
facto standard terminal emulator and it includes a document describing
the control sequences it understands known as ctlseqs.

The contents of a terminal description file (terminfo) are described
by ncurses in the file Caps.

@ Turtle is a unix application with few dependencies however it has
been written with an eye towards portability:

% Footnotes will not show up if this is the last text/code page.

OpenGL graphics on X (GLX). Turtle uses
libepoxy\footnote{$^1$}{\pdfURL{\URLlibepoxy}{\URLlibepoxy}} to
find extensions and resolve symbols.

libevent\footnote{$^2$}{\pdfURL{\URLlibevent}{\URLlibevent}} to
multiplex I/O.

Portable Snippets\footnote{$^3$}{\pdfURL{\URLportable}{\URLportable}}.
A collection of \CEE/ preprocessor macros that aid portability,
included in the \.{parsnip} directory.

And of course \.{CWEB}\footnote{$^4$}{\pdfURL{\URLCWEB}{\URLCWEB}}
is required to build the sources.
