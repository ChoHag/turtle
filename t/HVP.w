@i format.w
@i types.w

@** Testing the HVP Horizontal/Vertical Position control.

Identical to and implemented in terms of \.{CUP}.

DEC notes that this control is provided primarily for compatibility,
should not be used and may be redefined in the future.

Described in \ECMA{48}{8.3.63}, \DEC{5.4.4} (p.~5-51) and \DEC{D.6}.

The ECMA standards and DEC specifications are the same except that
the latter also includes the note mentioned above.

@ @c
#include "tap.h"

int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(1);
        tap_ok(true, "HVP: implemented and tested by CUP");
        return tap_exit();
}
