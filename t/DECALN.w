@i format.w
@i types.w

@** Testing the DECALN Screen Alignmnent control.

Originally used to calibrate or otherwise test a hardware terminal
and not intended for use by applications, now only really useful
to keep \.{vttest} happy.

Described in \DEC{D.8}.

Takes no arguments.

From the DEC spec:

Width is unchanged.

Resets origin mode

Disables rendition (bold/underscore/blink/reverse)

Lines to single-width

Cells set to ``the empty character''

Cursor to 1,1

Top/bottom margin cleared.

Last-column cleared.

But:

The empty character is `\.E'.

Xterm probably does some more:

...

Turtle has features which require more:

...

Missing tests:

Dynamic size

Margins

Last-column flag

Origin mode

Disables rendition (bold/underscore/blink/reverse)

Lines to single-width

Find where these came from:

The rendition is cleared à la \.{SGR(0)} only while writing E's.

@.TODO@>
@c
#include "tap.h"
#include "test-term.h"

@ @<|main...@>=
int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(10 + 10);
        test_DECALN_80();
        test_DECALN_132();
        return tap_exit();
}

@ @c
static void
tDECALN_test_state (char    *tid,
                    tt_term *t,
                    output  *log,
                    tt_cell *altcopy,
                    int      width)
{
        t_there_was_no_output(tid, log);
        t_the_contents_are_unchanged(tid, altcopy, t->panel, true);
        tap_ok(!IS_FLAG(t->flags, TERM_LAST_COLUMN),
                "%s: last-column flag is cleared", tid);
        t_cursor_position(tid, t->panel, 1, 1, "moved to");
        tap_ok(t->panel->width == width, "%s: width is not changed", tid);
        bool clean = true;
        for (int row = 1; clean && row <= t->panel->height; row++) {
                for (int col = 1; clean && col <= t->panel->width; col++) {
                        tt_cell *c = _panel_refp(t->panel->content[0],
                                t->panel->width, col - 1, row - 1);
                        if (c->cp != 'E')
                                clean = false;
                        if (c->invoked || c->alt)
                                clean = false;
                        if (!c->drawn || c->drawn->font
                                        != t->panel->invoked_font[0][0])
                                clean = false;
                        if (c->flags != t->panel->blank.flags)
                                clean = false;
                        if (memcmp(&c->bg, &t->panel->blank.bg, sizeof (c->bg)))
                                clean = false;
                        if (memcmp(&c->fg, &t->panel->blank.fg, sizeof (c->fg)))
                                clean = false;
                }
        }
        tap_ok(clean, "%s: contents are cleared to E", tid);
        tap_ok(!IS_FLAG(t->flags, TERM_ORIGIN_MOTION),
                "%s: origin motion is disabled", tid);
        tap_ok(t->panel->top == 1, "%s: top margin is reset", tid);
        tap_ok(t->panel->bottom == t->panel->height,
                "%s: bottom margin is reset", tid);
}

@ @c
void
test_DECALN_80 (void)
{
        output *log;
        tt_term *t = prepare_test_term(80, 0, &log);
        assert(t->panel->width == 80);
        tt_panel_garble(t->panel);
        tt_cell *copy1 = copy_panel_contents(t->panel, 1);
        tt_panel_cursor_set_pos(t->panel, 42, 7);
        term_exec_DECALN(t, NULL);
        tDECALN_test_state("DECALN (narrow)", t, log, copy1, 80);
        finish_test_term(t, log);
}

@ @c
void
test_DECALN_132 (void)
{
        output *log;
        tt_term *t = prepare_test_term(132, 0, &log);
        assert(t->panel->width == 132);
        tt_panel_garble(t->panel);
        tt_cell *copy1 = copy_panel_contents(t->panel, 1);
        tt_panel_cursor_set_pos(t->panel, 42, 7);
        term_exec_DECALN(t, NULL);
        tDECALN_test_state("DECALN (wide)", t, log, copy1, 132);
        finish_test_term(t, log);
}

@ @c @<|main| function@> @;
