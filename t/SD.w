@i format.w
@i types.w

@** Testing the SD Cursor Position control.

Move the presentation ``down'' even if rotated. Does not move the cursor.

From the DEC spec:

DEC paging/windowing extension allows the logical terminal to extend
beyond the borders of the screen, where the new line(s) of text
would come from otherwise DEC does not expand upon ECMA-48.

Not listed in D.6 AWM.

Missing tests:

...

@ @c
#include "tap.h"
#include "test-term.h"

@ @<|main...@>=
int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(4 * 3);
        test_SD();
        test_SD_1();
        test_SD_2();
        test_SD_5();
        return tap_exit();
}

@ @c
void
test_SD (void)
{
        output *log;
        tt_term *tt = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(tt->panel, false);
        tt_panel_cursor_set_pos(tt->panel, 3, 4);
        tt_action arg = {0};
        term_exec_SD(tt, &arg);
        t_there_was_no_output("SD()", log);
        t_cursor_position("SD()", tt->panel, 3, 4, "unmoved");
        t_panel_contents_match("SD()", tt->panel, @|
                "~~~~~~~~~~" @|
               " !\"#$%&'()" @|
               "!\"#$%&'()*" @|
               "\"#$%&'()*+" @|
                "#$%&'()*+," @|
                "$%&'()*+,-" @|
                "%&'()*+,-." @|
                "&'()*+,-./" @|
                "'()*+,-./0" @|
                "()*+,-./01");
}

@ @c
void
test_SD_0 (void)
{
        output *log;
        tt_term *tt = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(tt->panel, false);
        tt_panel_cursor_set_pos(tt->panel, 3, 4);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 0;
        term_exec_SD(tt, &arg);
        t_there_was_no_output("SD(0)", log);
        t_cursor_position("SD(0)", tt->panel, 3, 4, "unmoved");
        t_panel_contents_match("SD(0)", tt->panel, @|
                "~~~~~~~~~~" @|
               " !\"#$%&'()" @|
               "!\"#$%&'()*" @|
               "\"#$%&'()*+" @|
                "#$%&'()*+," @|
                "$%&'()*+,-" @|
                "%&'()*+,-." @|
                "&'()*+,-./" @|
                "'()*+,-./0" @|
                "()*+,-./01");
}

@ @c
void
test_SD_1 (void)
{
        output *log;
        tt_term *tt = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(tt->panel, false);
        tt_panel_cursor_set_pos(tt->panel, 3, 4);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 1;
        term_exec_SD(tt, &arg);
        t_there_was_no_output("SD(1)", log);
        t_cursor_position("SD(1)", tt->panel, 3, 4, "unmoved");
        t_panel_contents_match("SD(1)", tt->panel, @|
                "~~~~~~~~~~" @|
               " !\"#$%&'()" @|
               "!\"#$%&'()*" @|
               "\"#$%&'()*+" @|
                "#$%&'()*+," @|
                "$%&'()*+,-" @|
                "%&'()*+,-." @|
                "&'()*+,-./" @|
                "'()*+,-./0" @|
                "()*+,-./01");
}

@ @c
void
test_SD_2 (void)
{
        output *log;
        tt_term *tt = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(tt->panel, false);
        tt_panel_cursor_set_pos(tt->panel, 3, 4);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 2;
        term_exec_SD(tt, &arg);
        t_there_was_no_output("SD(2)", log);
        t_cursor_position("SD(2)", tt->panel, 3, 4, "unmoved");
        t_panel_contents_match("SD(2)", tt->panel, @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
               " !\"#$%&'()" @|
               "!\"#$%&'()*" @|
               "\"#$%&'()*+" @|
                "#$%&'()*+," @|
                "$%&'()*+,-" @|
                "%&'()*+,-." @|
                "&'()*+,-./" @|
                "'()*+,-./0");
}

@ @c
void
test_SD_5 (void)
{
        output *log;
        tt_term *tt = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(tt->panel, false);
        tt_panel_cursor_set_pos(tt->panel, 3, 4);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 5;
        term_exec_SD(tt, &arg);
        t_there_was_no_output("SD(5)", log);
        t_cursor_position("SD(5)", tt->panel, 3, 4, "unmoved");
        t_panel_contents_match("SD(5)", tt->panel, @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
               " !\"#$%&'()" @|
               "!\"#$%&'()*" @|
               "\"#$%&'()*+" @|
                "#$%&'()*+," @|
                "$%&'()*+,-");
}

@ @c @<|main| function@> @;
