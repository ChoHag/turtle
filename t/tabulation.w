@i format.w
@i types.w

@** Testing panel tabulation.

@c
#include "tap.h"
#include "tpanel.h"

static void test_tab (void);

@ @c
int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(3);
        test_tab();
        return tap_exit();
}

@ @c
static tt_panel *
_prepare_panel (int16_t width,
                int16_t height)
{
        return tt_panel_new(width, height);
}

@ @c
static void
test_tab (void)
{
        tt_panel *p = _prepare_panel(80, 24);
        int t = tt_panel_find_tabstop(p, 1);
        tap_ok(t == 9, "first tab stop is at 9");
        t = tt_panel_find_tabstop(p, 8);
        tap_ok(t == 9, "tab stop from 8 is at 9");
        t = tt_panel_find_tabstop(p, 9);
        tap_ok(t == 17, "tab stop from 9 is at 17");
}
