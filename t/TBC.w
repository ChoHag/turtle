@i format.w
@i types.w

@** Testing the TBC Tabulation Clear control.

Clear the current or all horizontal tab stops.

Described in \ECMA{48}{8.3.154} and \DEC{5.4.5} (p.~5-66).

From the spec:

Argument values 0 and 3 are described by DEC as clearing the current
cursor position or all tab stops respectively.

But:

ECMA also specifies arguments for clearing line (vertical?) tab
stops and tab stops which differ per line however argument values
2 (possibly) and 5 should also act like parameter 3.

Xterm probably does some more:

...

Turtle has features which require more:

Tab strips for the alternate display is independent.

Missing tests:

Multiple arguments.

Alternate panel.

...

@ @c
#include "tap.h"
#include "test-term.h"

@ @<|main...@>=
int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(5 + 3);
        test_TBC0();
        test_TBC3();
        return tap_exit();
}

@ @c
void
test_TBC0 (void)
{
        output *log;
        tt_term *t = prepare_test_term(0, 0, &log);
        tt_panel_cursor_set_pos(t->panel, 41, 7);
        assert(_tt_panel_tabstrip_stop_at(t->panel->htab[0], 41 - 1));
        tt_action arg = {0};
        term_exec_TBC(t, &arg);
        t_there_was_no_output("TBC(0)", log);
        t_cursor_position("TBC(0)", t->panel, 41, 7, "unmoved");
        tap_ok(!_tt_panel_tabstrip_stop_at(t->panel->htab[0], 41 - 1),
                "TBC(0): current tab stop was removed");
        tap_ok(_tt_panel_tabstrip_stop_at(t->panel->htab[0], 41 - 8 - 1),
                "TBC(0): previous tab stop was not removed");
        tap_ok(_tt_panel_tabstrip_stop_at(t->panel->htab[0], 41 + 8 - 1),
                "TBC(0): next tab stop was not removed");
        finish_test_term(t, log);
}

@ @c
void
test_TBC3 (void)
{
        output *log;
        uint8_t zero[20] = {0};
        tt_term *t = prepare_test_term(0, 0, &log);
        tt_panel_cursor_set_pos(t->panel, 42, 7);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 3;
        term_exec_TBC(t, &arg);
        t_there_was_no_output("TBC(3)", log);
        t_cursor_position("TBC(3)", t->panel, 42, 7, "unmoved");
        int tablen = _tabstrip_bytes(t->panel->width);
        tap_ok(!memcmp(t->panel->htab[0], zero, tablen),
                "TBC(3): all tab stops are removed");
        if (memcmp(t->panel->htab[0], zero, tablen)) {
                char *blob = calloc(tablen + 1, 2);
                for (int i = 0; i < tablen; i++)
                        sprintf(blob + 2 * i, "%02x", t->panel->htab[0][i]);
                tap_diag("tabs: %s", blob);
                free(blob);
        }
        finish_test_term(t, log);
}

@ @c @<|main| function@> @;
