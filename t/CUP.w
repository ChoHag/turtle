@i format.w
@i types.w

@** Testing the CUP Cursor Position control.

Move the cursor to the given position.

Described in \ECMA{48}{8.3.21}, \DEC{5.4.4} (p.~5-49) and \DEC{D.6}.

The main thing to test here is the exceptions, ie.~how turtle reacts
to attempts to move the cursor somewhere out of bounds.

From the DEC spec:

Moves the cursor to r,c within the screen or margins if origin-mode.

Clears last-column flag.

Omitted or 0 is 1.

If origin-mode is set r,c are relative to the top/left margin.

Xterm probably does some more:

...

Missing tests:

Moved to current location on the right margin with or w/o last-column
set (literal edge case).

Moved to margins.

Moved beyond margins.

Moved off screen.

Previous 2 in one direction only.

Moved into a double width or height cell.

@ @c
#include "tap.h"
#include "test-term.h"

@ @<|main...@>=
int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(3 + 3);
        test_move_elsewhere();
        test_move_nowhere();
        return tap_exit();
}

@ @c
void
test_move_nowhere (void)
{
        output *log;
        tt_term *t = prepare_test_term(0, 0, &log);
        tt_panel_cursor_set_pos(t->panel, 42, 7);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 7;
        arg.arg[arg.narg++] = 42;
        term_exec_CUP(t, &arg);
        t_there_was_no_output("CUP no motion", log);
        tap_ok(!IS_FLAG(t->flags, TERM_LAST_COLUMN),
                "CUP no motion: last-column flag is clear");
        t_cursor_position("CUP no motion", t->panel, 42, 7, "unmoved");
        finish_test_term(t, log);
}

@ @c
void
test_move_elsewhere (void)
{
        output *log;
        tt_term *t = prepare_test_term(0, 0, &log);
        tt_panel_cursor_set_pos(t->panel, 42, 7);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 21;
        arg.arg[arg.narg++] = 7;
        term_exec_CUP(t, &arg);
        t_there_was_no_output("CUP motion", log);
        tap_ok(!IS_FLAG(t->flags, TERM_LAST_COLUMN),
                "CUP motion: last-column flag is clear");
        t_cursor_position("CUP motion", t->panel, 7, 21, "moved to");
        finish_test_term(t, log);
}

@ @c @<|main| function@> @;
