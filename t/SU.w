@i format.w
@i types.w

@** Testing the SU Cursor Position control.

Move the presentation ``up'' even if rotated. Does not move the cursor.

From the DEC spec:

DEC paging/windowing extension allows the logical terminal to extend
beyond the borders of the screen, where the new line(s) of text
would come from otherwise DEC does not expand upon ECMA-48.

Not listed in D.6 AWM.

Missing tests:

...

@ @c
#include "tap.h"
#include "test-term.h"

@ @<|main...@>=
int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(4 * 3);
        test_SU();
        test_SU_1();
        test_SU_2();
        test_SU_5();
        return tap_exit();
}

@ @c
void
test_SU (void)
{
        output *log;
        tt_term *tt = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(tt->panel, false);
        tt_panel_cursor_set_pos(tt->panel, 3, 4);
        tt_action arg = {0};
        term_exec_SU(tt, &arg);
        t_there_was_no_output("SU()", log);
        t_cursor_position("SU()", tt->panel, 3, 4, "unmoved");
        t_panel_contents_match("SU()", tt->panel, @|
               "!\"#$%&'()*" @|
               "\"#$%&'()*+" @|
                "#$%&'()*+," @|
                "$%&'()*+,-" @|
                "%&'()*+,-." @|
                "&'()*+,-./" @|
                "'()*+,-./0" @|
                "()*+,-./01" @|
                ")*+,-./012" @|
                "~~~~~~~~~~");
}

@ @c
void
test_SU_0 (void)
{
        output *log;
        tt_term *tt = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(tt->panel, false);
        tt_panel_cursor_set_pos(tt->panel, 3, 4);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 0;
        term_exec_SU(tt, &arg);
        t_there_was_no_output("SU(0)", log);
        t_cursor_position("SU(0)", tt->panel, 3, 4, "unmoved");
        t_panel_contents_match("SU(0)", tt->panel, @|
               "!\"#$%&'()*" @|
               "\"#$%&'()*+" @|
                "#$%&'()*+," @|
                "$%&'()*+,-" @|
                "%&'()*+,-." @|
                "&'()*+,-./" @|
                "'()*+,-./0" @|
                "()*+,-./01" @|
                ")*+,-./012" @|
                "~~~~~~~~~~");
}

@ @c
void
test_SU_1 (void)
{
        output *log;
        tt_term *tt = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(tt->panel, false);
        tt_panel_cursor_set_pos(tt->panel, 3, 4);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 1;
        term_exec_SU(tt, &arg);
        t_there_was_no_output("SU(1)", log);
        t_cursor_position("SU(1)", tt->panel, 3, 4, "unmoved");
        t_panel_contents_match("SU(1)", tt->panel, @|
               "!\"#$%&'()*" @|
               "\"#$%&'()*+" @|
                "#$%&'()*+," @|
                "$%&'()*+,-" @|
                "%&'()*+,-." @|
                "&'()*+,-./" @|
                "'()*+,-./0" @|
                "()*+,-./01" @|
                ")*+,-./012" @|
                "~~~~~~~~~~");
}

@ @c
void
test_SU_2 (void)
{
        output *log;
        tt_term *tt = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(tt->panel, false);
        tt_panel_cursor_set_pos(tt->panel, 3, 4);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 2;
        term_exec_SU(tt, &arg);
        t_there_was_no_output("SU(2)", log);
        t_cursor_position("SU(2)", tt->panel, 3, 4, "unmoved");
        t_panel_contents_match("SU(2)", tt->panel, @|
               "\"#$%&'()*+" @|
                "#$%&'()*+," @|
                "$%&'()*+,-" @|
                "%&'()*+,-." @|
                "&'()*+,-./" @|
                "'()*+,-./0" @|
                "()*+,-./01" @|
                ")*+,-./012" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~");
}

@ @c
void
test_SU_5 (void)
{
        output *log;
        tt_term *tt = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(tt->panel, false);
        tt_panel_cursor_set_pos(tt->panel, 3, 4);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 5;
        term_exec_SU(tt, &arg);
        t_there_was_no_output("SU(5)", log);
        t_cursor_position("SU(5)", tt->panel, 3, 4, "unmoved");
        t_panel_contents_match("SU(5)", tt->panel, @|
                "%&'()*+,-." @|
                "&'()*+,-./" @|
                "'()*+,-./0" @|
                "()*+,-./01" @|
                ")*+,-./012" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~");
}

@ @c @<|main| function@> @;
