@i format.w
@i types.w

@** Testing buf.

@c
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>
#include "tap.h"
@#
#include "buf.h"

void test_atlas_alloc (void);

@ @c
int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(1);
        tap_ok(false, "there are tests");
        return tap_exit();
}

