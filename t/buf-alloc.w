@i format.w
@i types.w
@s allocations int

@** Testing buf.

@c
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>
#include "tap.h"
@#
#include "buf.h"

void test_buf_alloc (void);
void test_buf_ref (void);
void test_buf_alloc_more (void);
void test_buf_push (void);
void test_buf_pop (void);
void test_buf_insert (void);
void test_buf_remove (void);

@<Testing utilities (\.{buf-alloc})@>@;

@ @c
int
main (int    argc,
      char **argv)
{
        trallocator(_test_calloc, _test_realloc, _test_free);

        tap_getargs(argc, argv);
        tap_plan(
                TESTS_IN_buf_alloc
                + TESTS_IN_buf_ref
                + TESTS_IN_buf_alloc_more
                + TESTS_IN_buf_push
                + TESTS_IN_buf_pop
                + TESTS_IN_buf_insert
                + TESTS_IN_buf_remove
        );

        test_buf_alloc();
        test_buf_ref();
        test_buf_alloc_more();
        test_buf_push();
        test_buf_pop();
        test_buf_insert();
        test_buf_remove();

        return tap_exit();
}

@ |new_array| allocates memory and fills it in with its arguments.

@d TESTS_IN_buf_alloc (TESTS_IN_buf_alloc_minima
        + TESTS_IN_buf_alloc_preallocation)
@d was_calloc(A,C,S) ((A)->function == fn_calloc && (A)->count == (C)
        && (A)->size == (S))
@d was_free(A,P) ((A)->function == fn_free && (A)->ptr == (P))
@c
void
test_buf_alloc (void)
{
        allocations *a;
        tp_array *but;
        void *bufp, *datp;
        long count;

        @<Test |new_array|'s minima@>@;
        @<Test |new_array| with preallocation@>@;
        tap_prefix(NULL);
}

@ Test that allocation and destruction works.

@d TESTS_IN_buf_alloc_minima 8 + 3
@<Test |new_array|'s minima@>=
tap_prefix("new_array simply;");
_trace_alloc_start();
count = 0;
bufp = NULL;
but = new_array(2, 0, 0, 0);
TAILQ_FOREACH(a, &Allocations, each) {
#ifdef DEBUG
        _dump_alloc(count, a);
#endif
        switch (count) {
        case 0:
                tap_ok(was_calloc(a, 1, sizeof (tp_array)),
                        "allocation for tp_array object");
                bufp = a->result;
                break;
        default:
                tap_ok(false, "too many allocations: %ld", count);
                break;
        }
        count++;
}
tap_ok(count == 1, "one allocation");
tap_ok(bufp && but == bufp, "new_array returns a tp_array pointer");
tap_ok(but->data == NULL, "data buffer is not allocated");
tap_ok(but->allocated == 0, "0 byte allocated is recorded");
tap_ok(but->used == 0, "0 bytes used");
tap_ok(but->max == 0, "max is 0");
tap_ok(but->stride == 1, "stride is set correctly");
@#
tap_prefix("array_destroy;");
_reset_alloc();
count = 0;
array_destroy(but);
TAILQ_FOREACH(a, &Allocations, each) {
#ifdef DEBUG
        _dump_alloc(count, a);
#endif
        switch (count) {
        case 0:
                tap_ok(was_free(a, NULL), "data buffer (NULL) is freed");
                break;
        case 1:
                tap_ok(was_free(a, bufp), "buffer object freed");
                break;
        default:
                tap_ok(false, "too many allocations: %ld", count);
                break;
        }
        count++;
}
tap_ok(count == 2, "two frees");
_trace_alloc_stop();

@ Test allocation of a new buffer object with preallocated storage.

@d TESTS_IN_buf_alloc_preallocation 9
@<Test |new_array| with preallocation@>=
tap_prefix("new_array with preallocation;");
_trace_alloc_start();
count = 0;
bufp = datp = NULL;
but = new_array(3, 42, 0, 1024);
TAILQ_FOREACH(a, &Allocations, each) {
#ifdef DEBUG
        _dump_alloc(count, a);
#endif
        switch (count) {
        case 0:
                tap_ok(was_calloc(a, 1, sizeof (tp_array)),
                        "allocation for tp_array object");
                bufp = a->result;
                break;
        case 1:
                tap_ok(was_calloc(a, 1, 42),
                        "allocation for data buffer");
                datp = a->result;
                break;
        default:
                tap_ok(false, "too many allocations: %ld", count);
                break;
        }
        count++;
}
tap_ok(count == 2, "two allocations");
tap_ok(bufp && but == bufp, "new_array returns a tp_array pointer");
tap_ok(datp && but->data == datp, "data buffer is allocated");
tap_ok(but->allocated == 42, "allocated size is recorded");
tap_ok(but->max == 1024, "max is set");
tap_ok(but->used == 0, "0 bytes used");
tap_ok(but->stride == 2, "stride is set correctly");
array_destroy(but);
_trace_alloc_stop();

@ @d TESTS_IN_buf_ref 12
@c
void
test_buf_ref (void)
{
        tp_array *but;
        uintptr_t base, ref;

        tap_prefix("array_ref without backing store;");
        {
                but = new_array(2, 0, 0, 0);
                base = (uintptr_t) but->data;
                if (base)
                        tap_diag("allocation of zero bytes");
                tap_ok(array_ref(but, 0) == NULL, "ref 0 is NULL");
                tap_ok(array_ref(but, 1) == NULL, "ref past the end is NULL");
                array_destroy(but);
        }

        tap_prefix("array_ref with backing store;");
        {
                but = new_array(2, 2, 0, 0);
                base = (uintptr_t) but->data;
                tap_ok(base && array_ref(but, 0) == (void *) base,
                        "ref 0 is base");
                tap_ok(array_ref(but, 1) == NULL, "ref past the end is NULL");
                array_destroy(but);
        }

        tap_prefix("array_ref with contents;");
        {
                but = new_array(2, 0, 0, 0);
                ref = (uintptr_t) array_push(but, 2, NULL);
                base = (uintptr_t) but->data;
                if (base != ref)
                        tap_diag("array_push returned some other pointer");
                tap_ok(array_ref(but, 0) == (void *) base, "ref 0 is base");
                tap_ok(array_ref(but, 1) == (void *) (base + 2),
                        "ref 1 is now valid");
                tap_ok(array_ref(but, 2) == (void *) (base + 4),
                        "ref 2 is now valid");
                tap_ok(array_ref(but, 3) == NULL,
                        "ref 3 is past the end");
                array_destroy(but);
        }

        tap_prefix("array_ref with a prime stride;");
        {
                but = new_array(7, 0, 0, 0);
                ref = (uintptr_t) array_push(but, 2, NULL);
                base = (uintptr_t) but->data;
                if (base != ref)
                        tap_diag("array_push returned some other pointer");
                tap_ok(array_ref(but, 0) == (void *) base, "ref 0 is base");
                tap_ok(array_ref(but, 1) == (void *) (base + 7),
                        "ref 1 is now valid");
                tap_ok(array_ref(but, 2) == (void *) (base + 14),
                        "ref 2 is now valid");
                tap_ok(array_ref(but, 3) == NULL,
                        "ref 3 is past the end");
                array_destroy(but);
        }

        tap_prefix(NULL);
}

@ Affected by the length and stride values. Fails (returns |NULL|)
with the buffer unchanged if:

\yitem\dot The new size would cause the variables holding the value
to overflow,

\yitem\dot the new size is greater than the maximum, if there is
one,

\yitem\dot |realloc| fails.

Otherwise the data (possibly) and allocated attributes are updated.

Don't test overflow maths, just max, growth rate \AM\ realloc.

Returns a pointer to the first available (but unused) slot.

@d TESTS_IN_buf_alloc_more 19
@c
void
test_buf_alloc_more (void)
{
        tp_array *but;
        void *ref;
        uintptr_t refi;

        tap_prefix("array_alloc_more from empty;");
        {
                but = new_array(2, 0, 0, 0);
                tap_ok(!but->data, "data is not allocated");
                ref = array_alloc_more(but, 32);
                tap_ok(but->data, "data is allocated for expansion");
                tap_ok(but->allocated >= 32, "allocation is large enough");
                tap_ok(!but->used, "length is not increased");
                tap_ok(ref == but->data, "reference to the next position");
                array_destroy(but);
        }

        tap_prefix("array_alloc_more with preallocation;");
        {
                but = new_array(2, 16, 0, 0);
                tap_ok(but->allocated == 16, "data is allocated");
                ref = array_alloc_more(but, 32);
                tap_ok(but->allocated >= 32, "allocation is large enough");
                tap_ok(!but->used, "length is not increased");
                tap_ok(ref == but->data, "reference to the next position");
                array_destroy(but);
        }

        tap_prefix("array_alloc_more with content;");
        {
                uint32_t noise = arc4random();
                but = new_array(2, 16, 0, 0);
                array_push(but, 2, &noise);
                tap_ok(but->used == 4, "data is allocated");
                tap_ok(!memcmp(&noise, but->data, 4), "content is copied");
                ref = array_alloc_more(but, 32);
                tap_ok(but->allocated >= 32, "allocation is large enough");
                tap_ok(but->used == 4, "length is not increased");
                tap_ok(!memcmp(&noise, but->data, 4), "content is unchanged");
                refi = (uintptr_t) but->data + 4;
                tap_ok(ref == (void *) refi, "reference to the next position");
                array_destroy(but);
        }

        tap_prefix("array_alloc_more beyond max;");
        {
                but = new_array(2, 6, 0, 6);
                array_push(but, 3, NULL);
                tap_ok(but->allocated == 6, "data is allocated");
                tap_ok(but->max == 6, "max size is set");
                ref = array_alloc_more(but, 1);
                tap_ok(ref == NULL, "enlarge over max fails");
                tap_ok(but->allocated == 6, "size is unchanged");
                array_destroy(but);
        }

        tap_prefix(NULL);
}

@ Enlarge the allocation if necessary, increment the used count and
optionally copy data.

Fails under the circumstances |array_alloc_more| fails.

@d TESTS_IN_buf_push 5
@c
void
test_buf_push (void)
{
        tp_array *but;
        void *ref;
        uint8_t to_save[6], zero[8] = {0};

        tap_prefix("array_push with NULL;");
        {
                but = new_array(2, 0, 0, 0);
                ref = array_push(but, 1, NULL);
                tap_ok(ref && ref == but->data, "returns pointer to item");
                tap_ok(but->used == 1 * 2, "single item is used");
                tap_ok(!memcmp(ref, &zero, 1 * 2), "memory is unchanged");
                array_destroy(but);
        }

        tap_prefix("array_push with data;");
        {
                arc4random_buf(to_save, 6);
                but = new_array(3, 0, 0, 0);
                array_push(but, 2, to_save);
                tap_ok(!memcmp(but->data, to_save, 6), "contents are saved");
                tap_ok(but->used == 2 * 3, "2 elements are used");
                array_destroy(but);
        }

        tap_prefix(NULL);
}

@ Only fails if count is zero or larger than length.

@d TESTS_IN_buf_pop 0
@c
void
test_buf_pop (void)
{
        /* Boring. */
}

@ |array_insert(buffer, index, count, source)|.

@d TESTS_IN_buf_insert (8 * 3)
@c
tp_array *
_prepare (size_t stride,
          size_t preallocate,
          size_t fill,
          size_t use)
{
        tp_array *but = new_array(stride, preallocate, stride, 0);
        memmove(but->data, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", fill);
        but->used = use;
        return but;
}

void
_test_buf_insert (tp_array *but,
                  void   *ref,
                  size_t  off,
                  size_t  use,
                  char   *pat,
                  size_t  len)
{
        uintptr_t pv = (uintptr_t) but->data;

        tap_ok(ref && (uintptr_t) ref == pv + off, "returns pointer to item");
        tap_ok(but->used / (but->stride + 1) == use, "the used count is increased");
        tap_ok(!memcmp(but->data, pat, len), "the data are moved");
}
 
void
test_buf_insert (void)
{
        tp_array *but;
        void *ref;

        tap_prefix("array_insert 2-wide at 0 preallocated;");
        {
                but = _prepare(2, 16, 4, 4);
                ref = array_insert(but, 0, 1, "XY");
                _test_buf_insert(but, ref, 0, 3, "XYABCD", 6);
        }

        tap_prefix("array_insert 2-wide at 1 preallocated;");
        {
                but = _prepare(2, 16, 6, 6);
                ref = array_insert(but, 1, 1, "XY");
                _test_buf_insert(but, ref, 2, 4, "ABXYCDEF", 8);
        }

        tap_prefix("array_insert 2-wide at 2 preallocated;");
        {
                but = _prepare(2, 16, 6, 6);
                ref = array_insert(but, 2, 1, "XY");
                _test_buf_insert(but, ref, 4, 4, "ABCDXYEF", 8);
        }

        tap_prefix("array_insert 2-wide at 4 preallocated;");
        {
                but = _prepare(2, 16, 6, 6);
                ref = array_insert(but, 4, 1, "XY");
                _test_buf_insert(but, ref, 8, 5, "ABCDEF\000\000XY", 10);
        }


        tap_prefix("array_insert 3-wide at 0 preallocated;");
        {
                but = _prepare(3, 16, 6, 6);
                ref = array_insert(but, 0, 1, "XYZ");
                _test_buf_insert(but, ref, 0, 3, "XYZABCDEF", 9);
        }

        tap_prefix("array_insert 3-wide at 1 preallocated;");
        {
                but = _prepare(3, 16, 9, 9);
                ref = array_insert(but, 1, 1, "XYZ");
                _test_buf_insert(but, ref, 3, 4, "ABCXYZDEFGHI", 12);
        }

        tap_prefix("array_insert 3-wide at 2 preallocated;");
        {
                but = _prepare(3, 16, 9, 9);
                ref = array_insert(but, 2, 1, "XYZ");
                _test_buf_insert(but, ref, 6, 4, "ABCDEFXYZGHI", 12);
        }

        tap_prefix("array_insert 3-wide at 4 preallocated;");
        {
                but = _prepare(3, 16, 9, 9);
                ref = array_insert(but, 4, 1, "XYZ");
                _test_buf_insert(but, ref, 12, 5, "ABCDEFGHI\000\000\000XYZ", 15);
        }

}
 
@ @c
void
_test_buf_remove (tp_array *but,
                  size_t  use,
                  char   *pat,
                  size_t  len)
{
        tap_ok(but->used / (but->stride + 1) == use, "the used count is decreased");
        tap_ok(!memcmp(but->data, pat, len), "the data are moved");
}

@ @d TESTS_IN_buf_remove (5 * 2)
@c
void
test_buf_remove (void)
{
        tp_array *but;

        tap_prefix("array_remove 2-wide from 0;");
        {
                but = _prepare(2, 16, 8, 6);
                array_remove(but, 0, 1);
                _test_buf_remove(but, 2, "CDEFEFGH", 8);
        }

        tap_prefix("array_remove 2-wide from 1;");
        {
                but = _prepare(2, 16, 8, 6);
                array_remove(but, 1, 1);
                _test_buf_remove(but, 2, "ABEFEFGH", 8);
        }

        tap_prefix("array_remove 2-wide from 2;");
        {
                but = _prepare(2, 16, 8, 6);
                array_remove(but, 2, 1);
                _test_buf_remove(but, 2, "ABCDEFGH", 8);
        }

        tap_prefix("array_remove 2-wide from beyond;");
        {
                but = _prepare(2, 16, 8, 6);
                array_remove(but, 4, 1);
                _test_buf_remove(but, 3, "ABCDEFGH", 8);
        }

        tap_prefix("array_remove 2-wide from 2 goes beyond;");
        {
                but = _prepare(2, 16, 8, 6);
                array_remove(but, 2, 2);
                _test_buf_remove(but, 2, "ABCDEFGH", 8);
        }

}

@* Auxiliary functions.

Whenever one of |trcalloc|, |trrealloc| or |trfree| is called the
arguments and return value are saved for examination.

The list would grow unchecked if this were not a one-shot test script.

@<Testing...@>=
typedef struct allocations {
        TAILQ_ENTRY(allocations) @, each;
        enum { fn_malloc, fn_calloc, fn_realloc, fn_free }
                function;
        size_t  count;
        size_t  size;
        void   *ptr;
        void   *result;
} allocations;

static TAILQ_HEAD(@[@], allocations) Allocations
        = TAILQ_HEAD_INITIALIZER(Allocations);

bool Tracing = false;

void _dump_alloc (size_t, allocations *);
void _remember_alloc (int, size_t, size_t, void *, void *);
void _reset_alloc (void);
void _trace_alloc_start (void);
void _trace_alloc_stop (void);
void *_test_calloc (size_t, size_t);
void _test_free (void *);
void *_test_realloc (void *, size_t);

@ Functions to add records to the list and remove them all.

@c
void
_remember_alloc (int     function,
                 size_t  count,
                 size_t  size,
                 void   *ptr,
                 void   *result)
{
        allocations *a = malloc(sizeof (allocations));
        a->function = function;
        a->result = result;
        a->count = count;
        a->size = size;
        a->ptr = ptr;
        TAILQ_INSERT_TAIL(&Allocations, a, each);
}

void
_reset_alloc (void)
{
        allocations *a;
        while ((a = TAILQ_FIRST(&Allocations))) {
                TAILQ_REMOVE(&Allocations, a, each);
                free(a);
        }
}

void
_trace_alloc_start (void)
{
        _reset_alloc();
        Tracing = true;
}

void
_trace_alloc_stop (void)
{
        Tracing = false;
}

@ This may come in handy.

@c
void
_dump_alloc (size_t       count,
             allocations *a)
{
        tap_diag("alloc %zu", count);
        tap_diag("  called %s",
                a->function == fn_malloc ? "malloc" @|
                : a->function == fn_calloc ? "calloc" @|
                : a->function == fn_realloc ? "realloc" @|
                : a->function == fn_free ? "free" @|
                : "unexpected");
        tap_diag("  count: %zu", a->count);
        tap_diag("  size: %zu", a->size);
        tap_diag("  pointer: %p", a->ptr);
        tap_diag("=>result: %p", a->result);
}

@ The wrappers. Because these ``use'' the pointer after it's been
freed or reallocated the compiler's \.{-Wuse-after-free} option
will emit a warning.

@c
void *
_test_calloc (size_t count,
              size_t size)
{
        void *r = calloc(count, size);
        if (Tracing)
                _remember_alloc(fn_calloc, count, size, NULL, r);
        return r;
}

void *
_test_realloc (void   *ptr,
               size_t  size)
{
        void *r = realloc(ptr, size);
        if (Tracing)
                _remember_alloc(fn_realloc, 0, size, ptr, r);
        return r;
}

void
_test_free (void *ptr)
{
        free(ptr);
        if (Tracing)
                _remember_alloc(fn_free, 0, 0, ptr, NULL);
}

@** Index.
