@i format.w
@i types.w

@** Testing the CUP Cursor Position control.

...

From the DEC spec:

...

But:

...

Xterm probably does some more:

...

Turtle has features which require more:

...

Missing tests:

...

Find where these came from:

...

@ @c
#include "tap.h"
#include "test-term.h"

@ @<|main...@>=
int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(5 + 4 + 4);
        test_DECALN();
        return tap_exit();
}

@ @c
void
test_ED (void)
{
        output *log;
        tt_term *tt = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(tt->panel, false);
        tt_panel_cursor_set_pos(tt->panel, 1, 1);
        tt_action arg = {0};
        term_exec_ED(tt, &arg);
        t_there_was_no_output("ED() @@ 1,1", log);
        t_cursor_position("ED() @@ 1,1", tt->panel, 1, 1, "unmoved");
        t_panel_contents_match("ED() @@ 1,1", tt->panel, @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~");
}

@ @c @<|main| function@> @;
