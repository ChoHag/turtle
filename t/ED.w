@i format.w
@i types.w

@** Testing the ED Erase in Display control.

Erases adjacent cells within the display across line boundaries.

Described in \ECMA{48}{8.3.39}, \DEC{5.11} (p.~5-156) and \DEC{D.6}.

From the spec:

Empties character (space), rentition and attributes.

        Attributes: selective-erase

        Rendition: bold, blink, underscore, reverse

Argument:

        0: clear from cursor to last position inclusive

        1: clear from first position to cursor inclusive

        2: clear the whole display

Clears the last-column flag.

Not affected by margins (scrolling region) or origin mode.

Line rendition of completely-erased lines is set to single-width
(DEC says ``all character positions in the line set to the erased
state'' covering the edge case where the cursor is on the edge).

ECMA adapts the control depending on DEVICE COMPONENT SELECT MODE
(DCSM) and ERASURE MODE (ERM) which are unimplemented in Turtle.

But:

Modern rendition includes more states and also colour.

Xterm additions:

Argument 3: clear scrollback history

Missing tests:

Xterm extension.

Multiple arguments.

TODO: Is there a better reference for applying all parameters to a
control than the pseudo code in the DEC manual? ISTR seeing something
in one of the specs or descriptions of control sequences.

@.TODO@>
@c
#include "tap.h"
#include "test-term.h"

@ @<|main...@>=
int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(6 + 6 + 6 + 3);
        test_ED0();
        test_ED1();
        test_ED2();
        test_ED_1_1();
        return tap_exit();
}


@ \.{ED(0)} erases from the Active Position line to the end of the
last line and clear the last-column flag.

@c
void
test_ED0 (void)
{
        output *log;
        tt_term *t = prepare_test_term(0, 0, &log);
        tt_panel_garble(t->panel);
        tt_cell *copy1 = copy_panel_contents(t->panel, 1);
        tt_panel_cursor_set_pos(t->panel, 42, 7);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 0;
        term_exec_ED(t, &arg);
        tap_ok(!IS_FLAG(t->flags, TERM_LAST_COLUMN),
                "ED(0): last-column flag is cleared");
        t_there_was_no_output("ED(0)", log);
        t_the_contents_are_unchanged("ED(0)", copy1, t->panel, true);
        t_the_current_contents_are_erased_from_to("ED(0)", t->panel,
                7, 42, 0, 0);
        t_cursor_position("ED(0)", t->panel, 42, 7, "unmoved");
        finish_test_term(t, log);
}

@ \.{ED(1)} erases from the beginning of the first line to the
Active Position.

@c
void
test_ED1 (void)
{
        output *log;
        tt_term *t = prepare_test_term(0, 0, &log);
        tt_panel_garble(t->panel);
        tt_cell *copy1 = copy_panel_contents(t->panel, 1);
        tt_panel_cursor_set_pos(t->panel, 42, 7);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 1;
        term_exec_ED(t, &arg);
        tap_ok(!IS_FLAG(t->flags, TERM_LAST_COLUMN),
                "ED(1): last-column flag is cleared");
        t_there_was_no_output("ED(1)", log);
        t_the_contents_are_unchanged("ED(1)", copy1, t->panel, true);
        t_the_current_contents_are_erased_from_to("ED(1)", t->panel,
                1, 1, 7, 42);
        t_cursor_position("ED(1)", t->panel, 42, 7, "unmoved");
        finish_test_term(t, log);
}

@ \.{ED(2)} erases the entire display.

@ @c
void
test_ED2 (void)
{
        output *log;
        tt_term *t = prepare_test_term(0, 0, &log);
        tt_panel_garble(t->panel);
        tt_cell *copy1 = copy_panel_contents(t->panel, 1);
        tt_panel_cursor_set_pos(t->panel, 42, 7);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 2;
        term_exec_ED(t, &arg);
        tap_ok(!IS_FLAG(t->flags, TERM_LAST_COLUMN),
                "ED(2): last-column flag is cleared");
        t_there_was_no_output("ED(2)", log);
        t_the_contents_are_unchanged("ED(2)", copy1, t->panel, true);
        t_the_current_contents_are_all_erased("ED(2)", t->panel);
        t_cursor_position("ED(2)", t->panel, 42, 7, "unmoved");
        finish_test_term(t, log);
}

@ @c
void
test_ED_1_1 (void)
{
        output *log;
        tt_term *tt = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(tt->panel, false);
        tt_panel_cursor_set_pos(tt->panel, 1, 1);
        tt_action arg = {0};
        term_exec_ED(tt, &arg);
        t_there_was_no_output("ED() @@ 1,1", log);
        t_cursor_position("ED() @@ 1,1", tt->panel, 1, 1, "unmoved");
        t_panel_contents_match("ED() @@ 1,1", tt->panel, @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~" @|
                "~~~~~~~~~~");
}

@ @c @<|main| function@> @;
