@i format.w
@i types.w

@** Testing the DECCOLM Column Mode control.

Implementation of DECSET(3)/SM(3)/RM(3). Toggle which resizes the
display to 80x24 or 132x24.

Moves the cursor to 1,1.

Clears last-column flag.

Clears the display.

Scrolling region (margins) eliminated.

Margin-mode is enabled.

Missing tests:

Resizing when the cursor is outside of the new region

Alternate display

Terminal is dynamically size

Host-resize is blocked

@ @c
#include "tap.h"
#include "test-term.h"

@ @<|main...@>=
int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(4 * 7);
        test_80_to_132();
        test_132_to_80();
        test_80_to_80();
        test_132_to_132();
        return tap_exit();
}

@ \.{DECCOLM(true)} when at 80 columns.

@c
void
test_80_to_132 (void)
{
        output *log;
        tt_term *tt = prepare_test_term(80, 0, &log);
        SET_FLAG(tt->flags, TERM_HOST_CAN_RESIZE);
        tt_panel_garble(tt->panel);
        tt_cell *copy1 = copy_panel_contents(tt->panel, 1);
        tt_panel_cursor_set_pos(tt->panel, 42, 7);
        tt_action arg = {0};
        arg.mode = true;
        term_exec_DECCOLM(tt, &arg);
        t_there_was_no_output("DECCOLM(true)@@80", log);
        tap_ok(tt->panel->width == 132,
                "DECCOLM(true)@@80: panel is 132 columns wide");
        tap_ok(!IS_FLAG(tt->flags, TERM_LAST_COLUMN),
                "DECCOLM(true)@@80: last-column flag is cleared");
        t_the_current_contents_are_all_erased("DECCOLM(true)@@80", tt->panel);
        t_default_margins("DECCOLM(true)@@80", tt, tt->panel);
        t_cursor_position("DECCOLM(true)@@80", tt->panel, 1, 1, "moved to");
        finish_test_term(tt, log);
}

@ \.{DECCOLM(false)} when at 132 columns.

@c
void
test_132_to_80 (void)
{
        output *log;
        tt_term *tt = prepare_test_term(132, 0, &log);
        SET_FLAG(tt->flags, TERM_HOST_CAN_RESIZE);
        tt_panel_garble(tt->panel);
        tt_cell *copy1 = copy_panel_contents(tt->panel, 1);
        tt_panel_cursor_set_pos(tt->panel, 42, 7);
        tt_action arg = {0};
        arg.mode = false;
        term_exec_DECCOLM(tt, &arg);
        t_there_was_no_output("DECCOLM(false)@@132", log);
        tap_ok(tt->panel->width == 80,
                "DECCOLM(false)@@132: panel is 80 columns wide");
        tap_ok(!IS_FLAG(tt->flags, TERM_LAST_COLUMN),
                "DECCOLM(false)@@132: last-column flag is cleared");
        t_the_current_contents_are_all_erased("DECCOLM(false)@@132", tt->panel);
        t_default_margins("DECCOLM(false)@@132", tt, tt->panel);
        t_cursor_position("DECCOLM(false)@@132", tt->panel, 1, 1, "moved to");
        finish_test_term(tt, log);
}

@ \.{DECCOLM(false)} when at 80 columns.

@c
void
test_80_to_80 (void)
{
        output *log;
        tt_term *tt = prepare_test_term(80, 0, &log);
        SET_FLAG(tt->flags, TERM_HOST_CAN_RESIZE);
        tt_panel_garble(tt->panel);
        tt_cell *copy1 = copy_panel_contents(tt->panel, 1);
        tt_panel_cursor_set_pos(tt->panel, 42, 7);
        tt_action arg = {0};
        arg.mode = false;
        term_exec_DECCOLM(tt, &arg);
        t_there_was_no_output("DECCOLM(false)@@80", log);
        tap_ok(tt->panel->width == 80,
                "DECCOLM(false)@@80: panel is 80 columns wide");
        tap_ok(!IS_FLAG(tt->flags, TERM_LAST_COLUMN),
                "DECCOLM(false)@@80: last-column flag is cleared");
        t_the_current_contents_are_all_erased("DECCOLM(false)@@80", tt->panel);
        t_default_margins("DECCOLM(false)@@80", tt, tt->panel);
        t_cursor_position("DECCOLM(false)@@80", tt->panel, 1, 1, "moved to");
        finish_test_term(tt, log);
}

@ \.{DECCOLM(true)} when at 132 columns.

@c
void
test_132_to_132 (void)
{
        output *log;
        tt_term *tt = prepare_test_term(132, 0, &log);
        SET_FLAG(tt->flags, TERM_HOST_CAN_RESIZE);
        tt_panel_garble(tt->panel);
        tt_cell *copy1 = copy_panel_contents(tt->panel, 1);
        tt_panel_cursor_set_pos(tt->panel, 42, 7);
        tt_action arg = {0};
        arg.mode = true;
        term_exec_DECCOLM(tt, &arg);
        t_there_was_no_output("DECCOLM(true)@@132", log);
        tap_ok(tt->panel->width == 132,
                "DECCOLM(true)@@132: panel is 132 columns wide");
        tap_ok(!IS_FLAG(tt->flags, TERM_LAST_COLUMN),
                "DECCOLM(true)@@132: last-column flag is cleared");
        t_the_current_contents_are_all_erased("DECCOLM(true)@@132", tt->panel);
        t_default_margins("DECCOLM(true)@@132", tt, tt->panel);
        t_cursor_position("DECCOLM(true)@@132", tt->panel, 1, 1, "moved to");
        finish_test_term(tt, log);
}

@ @c @<|main| function@> @;
