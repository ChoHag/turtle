@i format.w
@i types.w

@** Testing the CUB Cursor Backward control.

Moves the cursor backwards by the given number of columns.

Described in \ECMA{48}{8.3.18}, \DEC{5.4.4} (p.~5-47) and \DEC{D.6}.

From the DEC spec:

If the cursor is within the margins it is not moved beyond them.
If the cursor is between the edge and left margin it is moved up
to the first column.

The display will not scroll.

Missing tests:

Margins.

\.{CUF}/\.{CUU}/\.{CUD}.

@ @c
#include "tap.h"
#include "test-term.h"
#include <stdarg.h>

@ @<|main...@>=
int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(5 * 5);
        test_CUB_central_default();
        test_CUB_central_1();
        test_CUB_central_7();
        test_CUB_left();
        test_CUB_right();
        return tap_exit();
}

@ @c
void
_motion_test (char  *tid,
              void (*fun)(tt_term *, tt_action *),
              int    srow,
              int    scol,
              int    erow,
              int    ecol,
              int    narg, ...)
{
        output *log;
        tt_term *t = prepare_test_term(0, 0, &log);
        if (scol == 81)
                scol = 80, SET_FLAG(t->flags, TERM_LAST_COLUMN);
        if (srow || scol)
                tt_panel_cursor_set_pos(t->panel, scol, srow);
        tt_cell *copy0 = copy_panel_contents(t->panel, 0);
        tt_action arg = {0};
        va_list vl;
        va_start(vl, narg);
        for (int i = 0; i < narg; i++)
                arg.arg[arg.narg++] = va_arg(vl, int);
        va_end(vl);
        (fun)(t, &arg);
        t_there_was_no_output(tid, log);
        tap_ok(!IS_FLAG(t->flags, TERM_LAST_COLUMN),
                "%s: last-column flag is cleared", tid);
        t_cursor_position(tid, t->panel, ecol, erow,
                ((srow == erow && scol == ecol) ? "unmoved" : "moved to"));
        t_the_contents_are_unchanged(tid, copy0, t->panel, false);
        finish_test_term(t, log);
}

@ @c
void
test_CUB_central_default (void)
{
        _motion_test("CUB()", term_exec_CUB, 7, 42, 7, 41, 0);
}

@ @c
void
test_CUB_central_1 (void)
{
        _motion_test("CUB(1)", term_exec_CUB, 7, 42, 7, 41, 1, 1);
}

@ @c
void
test_CUB_central_7 (void)
{
        _motion_test("CUB(7)", term_exec_CUB, 7, 42, 7, 35, 1, 7);
}

@ @c
void
test_CUB_left (void)
{
        _motion_test("CUB() left edge", term_exec_CUB, 7, 1, 7, 1, 1, 7);
}

@ @c
void
test_CUB_right (void)
{
        _motion_test("CUB() right edge", term_exec_CUB, 7, 81, 7, 79, 1, 1);
}

@ @c @<|main| function@> @;
