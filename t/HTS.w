@i format.w
@i types.w

@** Testing the HTS Horizontal Tabulation Set control.

Set the current column as a tab stop. DEC notes that no other
horizontal tab settings are affected by this control while ECMA
allows for the possibility of limiting the lines affected.

Described in \ECMA{48}{8.3.62} and \DEC{5.4.5} (p.~5-69).

@c
#include "tap.h"
#include "test-term.h"

@ @<|main...@>=
int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(3);
        test_HTS();
        return tap_exit();
}

@ @c
void
test_HTS (void)
{
        output *log;
        tt_term *t = prepare_test_term(0, 0, &log);
        tt_panel_cursor_set_pos(t->panel, 42, 7);
        term_exec_HTS(t, NULL);
        t_there_was_no_output("HTS", log);
        t_cursor_position("HTS", t->panel, 42, 7, "unmoved");
        tap_ok(tt_panel_find_tabstop(t->panel, 41) == 42,
                "HTS: the tab stop is inserted");
        finish_test_term(t, log);
}

@ @c @<|main| function@> @;
