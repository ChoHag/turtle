@i format.w
@i types.w

@** Testing the EL Erase In Line control.

Erases character positions within the active line.

Identical to \.{DECSEL}.

Described in \ECMA{48}{8.3.41}, \DEC{5.11} (p.~5-154) and \DEC{D.6}.

From the spec:

Empties character (space), rentition and attributes.

        Attributes: selective-erase

        Rendition: bold, blink, underscore, reverse

Argument:

        0: clear from cursor to last column inclusive

        1: clear from first column to cursor inclusive

        2: clear the whole line

Clears the last-column flag.

Not affected by margins (scrolling region).

Line rendition is not affected.

ECMA adapts the control depending on DEVICE COMPONENT SELECT MODE
(DCSM) and ERASURE MODE (ERM) which are unimplemented in Turtle.

But:

Modern rendition includes more states and also colour.

Missing tests:

Margins.

Line rendition.

Multiple arguments.

@ @c
#include "tap.h"
#include "test-term.h"

@ @<|main...@>=
int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(4 + 4 + 4 + 3 + 3);
        test_EL0();
        test_EL1();
        test_EL2();
        test_EL_top();
        test_EL_bottom();
        return tap_exit();
}

@ @c
void
test_EL0 (void)
{
        output *log;
        tt_term *t = prepare_test_term(0, 0, &log);
        tt_panel_garble(t->panel);
        tt_panel_cursor_set_pos(t->panel, 42, 7);
        tt_action arg = {0};
        term_exec_EL(t, &arg);
        t_there_was_no_output("EL(0)", log);
        tap_ok(!IS_FLAG(t->flags, TERM_LAST_COLUMN),
                "EL(0): last-column flag is cleared");
        t_the_current_contents_are_erased_from_to("EL(0)", t->panel, 7, 42, 7, 0);
        t_cursor_position("EL(0)", t->panel, 42, 7, "unmoved");
        finish_test_term(t, log);
}

@ @c
void
test_EL1 (void)
{
        output *log;
        tt_term *t = prepare_test_term(0, 0, &log);
        tt_panel_garble(t->panel);
        tt_panel_cursor_set_pos(t->panel, 42, 7);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 1;
        term_exec_EL(t, &arg);
        t_there_was_no_output("EL(1)", log);
        tap_ok(!IS_FLAG(t->flags, TERM_LAST_COLUMN),
                "EL(1): last-column flag is cleared");
        t_the_current_contents_are_erased_from_to("EL(1)", t->panel, 7, 1, 7, 42);
        t_cursor_position("EL(1)", t->panel, 42, 7, "unmoved");
        finish_test_term(t, log);
}

@ @c
void
test_EL2 (void)
{
        output *log;
        tt_term *t = prepare_test_term(0, 0, &log);
        tt_panel_garble(t->panel);
        tt_panel_cursor_set_pos(t->panel, 42, 7);
        tt_action arg = {0};
        arg.arg[arg.narg++] = 2;
        term_exec_EL(t, &arg);
        t_there_was_no_output("EL(2)", log);
        tap_ok(!IS_FLAG(t->flags, TERM_LAST_COLUMN),
                "EL(2): last-column flag is cleared");
        t_the_current_contents_are_erased_from_to("EL(2)", t->panel, 7, 1, 7, 0);
        t_cursor_position("EL(2)", t->panel, 42, 7, "unmoved");
        finish_test_term(t, log);
}

@ @c
void
test_EL_bottom (void)
{
        output *log;
        tt_term *t = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(t->panel, false);
        tt_panel_cursor_set_pos(t->panel, 3, 10);
        tt_action arg = {0};
        term_exec_EL(t, &arg);
        t_panel_contents_match("EL() bottom row", t->panel,
               " !\"#$%&'()"
               "!\"#$%&'()*"
               "\"#$%&'()*+"
                "#$%&'()*+,"
                "$%&'()*+,-"
                "%&'()*+,-."
                "&'()*+,-./"
                "'()*+,-./0"
                "()*+,-./01"
                ")*~~~~~~~~");
        t_there_was_no_output("EL() bottom row", log);
        t_cursor_position("EL() bottom row", t->panel, 3, 10, "unmoved");
        finish_test_term(t, log);
}

@ @c
void
test_EL_top (void)
{
        output *log;
        tt_term *t = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(t->panel, false);
        tt_panel_cursor_set_pos(t->panel, 3, 1);
        tt_action arg = {0};
        term_exec_EL(t, &arg);
        t_panel_contents_match("EL() top row", t->panel,
                " !~~~~~~~~"
               "!\"#$%&'()*"
               "\"#$%&'()*+"
                "#$%&'()*+,"
                "$%&'()*+,-"
                "%&'()*+,-."
                "&'()*+,-./"
                "'()*+,-./0"
                "()*+,-./01"
                ")*+,-./012");
        t_there_was_no_output("EL() top row", log);
        t_cursor_position("EL() top row", t->panel, 3, 1, "unmoved");
        finish_test_term(t, log);
}

@ @c @<|main| function@> @;
