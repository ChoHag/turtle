@i format.w
@i types.w

@** Testing the |tt_panel_blit| function.

Copies a rectangle to another location by its top-left corner.

src(x,y),dst(x,y),w,h

@ @c
#include "tap.h"
#include "test-term.h"

@ @<|main...@>=
int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(4 * 1);
        test_blit_right_down();
        test_blit_right_up();
        test_blit_left_down();
        test_blit_left_up();
        return tap_exit();
}

@ @c
void
test_blit_right_down (void)
{
        output *log;
        tt_term *t = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(t->panel, false);
        tt_panel_blit(t->panel, 2, 3, 4, 5, 7, 6);
        t_panel_contents_match("tt_panel_blit right+down", t->panel, @|
               " !\"#$%&'()" @|
               "!\"#$%&'()*" @|
               "\"#$%&'()*+" @|
                "#$%&'()*+," @|
                "$%&#$%&'()" @|
                "%&'$%&'()*" @|
                "&'(%&'()*+" @|
                "'()&'()*+," @|
                "()*'()*+,-" @|
                ")*+()*+,-.");
        finish_test_term(t, log);
}

@ @c
void
test_blit_right_up (void)
{
        output *log;
        tt_term *t = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(t->panel, false);
        tt_panel_blit(t->panel, 3, 4, 6, 2, 5, 6);
        t_panel_contents_match("tt_panel_blit right+up", t->panel, @|
               " !\"#$%&'()" @|
               "!\"#$%%&'()" @|
               "\"#$%&&'()*" @|
                "#$%&''()*+" @|
                "$%&'(()*+," @|
                "%&'())*+,-" @|
                "&'()**+,-." @|
                "'()*+,-./0" @|
                "()*+,-./01" @|
                ")*+,-./012");
        finish_test_term(t, log);
}

@ @c
void
test_blit_left_down (void)
{
        output *log;
        tt_term *t = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(t->panel, false);
        tt_panel_blit(t->panel, 7, 2, 4, 3, 4, 6);
        t_panel_contents_match("tt_panel_blit left+down", t->panel, @|
               " !\"#$%&'()" @|
               "!\"#$%&'()*" @|
               "\"#$'()*)*+" @|
                "#$%()*+*+," @|
                "$%&)*+,+,-" @|
                "%&'*+,-,-." @|
                "&'(+,-.-./" @|
                "'(),-././0" @|
                "()*+,-./01" @|
                ")*+,-./012");
        finish_test_term(t, log);
}

@ @c
void
test_blit_left_up (void)
{
        output *log;
        tt_term *t = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(t->panel, false);
        tt_panel_blit(t->panel, 5, 3, 2, 1, 5, 7);
        t_panel_contents_match("tt_panel_blit left+up", t->panel, @|
                " &'()*&'()" @|
                "!'()*+'()*" @|
               "\"()*+,()*+" @|
                "#)*+,-)*+," @|
                "$*+,-.*+,-" @|
                "%+,-./+,-." @|
                "&,-./0,-./" @|
                "'()*+,-./0" @|
                "()*+,-./01" @|
                ")*+,-./012");
        finish_test_term(t, log);
}

@ @c
#if 0 @|
               " !\"#$%&'()" @|
               "!\"#$%&'()*" @|
               "\"#$%&'()*+" @|
                "#$%&'()*+," @|
                "$%&'()*+,-" @|
                "%&'()*+,-." @|
                "&'()*+,-./" @|
                "'()*+,-./0" @|
                "()*+,-./01" @|
                ")*+,-./012"
#endif

@ @c @<|main| function@> @;
