@i format.w
@i types.w

@** Testing the |tt_panel_clear_region| function.

Reset cells' content to the pen rendition and the givem character (or 0).

@ @c
#include "tap.h"
#include "test-term.h"
#include "log.h"

@ @<|main...@>=
int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv);
        tap_plan(1);
        test_clear_rectangle();
        return tap_exit();
}

@ @c
void
test_clear_rectangle (void)
{
        output *log;
        tt_term *t = prepare_test_term(10, 10, &log);
        tt_panel_test_pattern(t->panel, false);
        tt_panel_clear_region(t->panel, 3, 4, 5, 6, 0);
        t_panel_contents_match("tt_panel_clear_region rectangle", t->panel,
               " !\"#$%&'()"
               "!\"#$%&'()*"
               "\"#$%&'()*+"
                "#$~~~~~*+,"
                "$%~~~~~+,-"
                "%&~~~~~,-."
                "&'~~~~~-./"
                "'(~~~~~./0"
                "()~~~~~/01"
                ")*+,-./012");
        finish_test_term(t, log);
}

@ @c @<|main| function@> @;
