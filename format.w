\pdfpagewidth=210mm
\pdfpageheight=297mm
\pagewidth=159.2mm
\pageheight=238.58mm
\fullpageheight=246.2mm
\setpage % A4
%
\font\sc=cmcsc10
\def\nullptr{\NULL}
\def\<{$\langle$}
\def\>{$\rangle$}
\def\ditto{--- \lower1ex\hbox{''} ---}
\def\dot{\vrule width3pt height4pt depth-1pt}
\def\epdf#1{\pdfximage{#1}\pdfrefximage\pdflastximage}% Why isn't this provided?
\def\ft{{\tt\char13}}
\def\hex{\hbox{${\scriptstyle 0x}$\tt\aftergroup}}
\def\iIV{\hskip4em}
\def\iIII{\hskip3em}
\def\iII{\hskip2em}
\def\iI{\hskip1em}
\def\J{}
\let\K=\leftarrow
\def\Lt{{\char124}}
\def\L{{$\char124$}}
\def\man#1#2{\pdfURL{\.{#2}(#1)}{http://man.openbsd.org/#2.#1}}
\def\pipe/{|}
\def\qc{$\rangle\!\rangle$}
\def\qo{$\langle\!\langle$}
\def\to{{$\rightarrow$}}
\def\yhang{\yskip\hang}
\def\yitem#1{\yskip\item{#1}}
%
% From the TeX book:
\def\frac#1/#2{\leavevmode\kern.1em
  \raise.5ex\hbox{\the\scriptfont0 #1}\kern-.1em
  /\kern-.15em\lower.25ex\hbox{\the\scriptfont0 #2}}
%
\def\DECp#1{DEC-STD-070 p.~#1}
\def\DEC#1{DEC-STD-070 ss.~#1}
\def\ECMA#1#2{ECMA-#1 ss.~#2}
