@i format.w
@i types.w

@** Triangles in X.

Much of the text in this documentation is now entirely incorrect.

@(trix.h@>=
#ifndef TRIX_H
#define TRIX_H
#include <err.h>
#include <setjmp.h>
#include <stdbool.h>
#include <event.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <GL/gl.h>
@<Public interface (\.{tri.o})@>@;
#endif /* |TRIX_H| */

@ @c
#include <epoxy/gl.h>
#include "trix.h"
@#
#include <assert.h>
#include <err.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
@#
#include <epoxy/glx.h>
@#
#include "log.h"
#undef LOG_PREFIX
#define LOG_PREFIX "tri"

@<Function declarations (\.{tri.o})@> @;

@ @c
typedef struct trix {
        uint32_t           flags;
        jmp_buf            iofail;
        struct event_base *loop; /* Event loop this library will control. */
        struct event       evxio; /* Handler for I/O events. */
        Display           *xdisplay; /* X display. */
        XVisualInfo       *xctx; /* X drawing context. */
        Colormap           cmap; /* Colour map for ancient terminals. */
        Window             xwin;
        GLXContext         glctx; /* OpenGL drawing context. */
        GLXFBConfig        fbconfig; /* Link between the two. */
        void             (*xevent_fun)(trix *, XEvent *);
} trix;

@ @<Pub...@>=
typedef struct trix trix;

#define TRIX_API

TRIX_API trix *do_trix_init_full (jmp_buf *, struct event_base *,
        void (*)(trix *, XEvent *), const char *, XSetWindowAttributes *,
        XSizeHints *);
TRIX_API GLuint trix_compile_embedded_shader (GLenum, const char *,
        const char *, long);
TRIX_API void trix_destroy (trix *);
TRIX_API Display *trix_get_xdisplay (trix *);
TRIX_API int trix_get_xscreen (trix *);
TRIX_API Window trix_get_xwindow (trix *);
TRIX_API GLuint trix_link_embedded_program (const char *, const char *,
        long, const char *, long, const char *, long);
TRIX_API void trix_set_iofail (trix *, jmp_buf *);

#define trix_init_full(T,L,F,R,A,N) do {                             \
        jmp_buf _failure;                                            \
        if (setjmp(_failure))                                        \
                err(1, "trix_init_full");                            \
        (T) = do_trix_init_full(&_failure, (L), (F), (R), (A), (N)); \
        if (setjmp(_failure))                                        \
                err(1, "after init");                                \
        trix_set_iofail((T), &_failure);                             \
} while (0)

@ @c
void
trix_set_iofail (trix    *tctx,
                 jmp_buf *failure)
{
        memmove(tctx->iofail, failure, sizeof (*failure));
}

@ This is the first routine called when any Xlib routine suffers
an I/O failure. It's called with the |Display| pointer associated
with the failing connection.

It is necessary to briefly describe how XOrg handles I/O errors.
Anywhere an I/O error is detected first calls |_XIOError|. After
handling internal state this simply calls the global Xlib I/O error
handler (which can be updated by |XSetIOErrorHandler|) then the
display exit handler (|XSetIOErrorExitHandler|).

The default error handler is |_XDefaultIOError| which prints a
message depending on the type of I/O error then {\it unconditionally
calls |exit|!}

Because by default the per-display I/O error will never be called
it doesn't matter that it also calls |exit| from a library routine.
That is all it does so exiting from the default I/O error routine
is doubly stupid.

This function replicates |_XDefaultIOError| from XOrg without the
unconditional |exit|. The return value so carefully prepared is
ignored by XOrg.

@c
static int
_tri_xio_error (Display *dpy)
{
        int bytes = 0, last_error;

        last_error = errno;
        ioctl(ConnectionNumber(dpy), FIONREAD, &bytes);
        errno = last_error;

        if (errno == EPIPE || (errno == EAGAIN && bytes <= 0)) {
                fprintf (stderr,
                        "X connection to %s broken (explicit kill or server shutdown).\r\n",
                        DisplayString(dpy));
                return 1;
        } else {
                fprintf (stderr,
                        "XIO:  fatal IO error %d (%s) on X server \"%s\"\r\n",
#ifdef WIN32
                        WSAGetLastError(), strerror(WSAGetLastError()),
#else
                        errno, strerror (errno),
#endif
                        DisplayString(dpy));
                fprintf (stderr,
                        "      after %lu requests (%lu known processed) with %d events remaining.\r\n",
                        NextRequest(dpy) - 1, LastKnownRequestProcessed(dpy),
                        QLength(dpy));
                return 2;
        }
}

@ Install the global error handler by first resetting the current
handler to the default which also returns it, then putting the
handler back which now returns the default handler.

If the current handler is not already |_tri_xio_error| but has been
changed from the default, it's not replaced and this function fails.

@c
static void *
_tri_xio_handler (void)
{
        XIOErrorHandler hdefault, hold;

        hold = XSetIOErrorHandler(NULL); /* What it was */
        hdefault = XSetIOErrorHandler(hold); /* Put hold back */
        if (hold != _tri_xio_error && hold != hdefault)
                return NULL; /* Already changed. */
        return XSetIOErrorHandler(_tri_xio_error);
}

@ Rather than exiting the display error handler, now that it can
be reached, long-jumps to the beginning of the event loop (or
initialisation; see |tri_init|).

@c
static void
_tri_fatal_x_error (Display *d,
                    void    *ctx)
{
        trix *tctx = ctx;
        longjmp(tctx->iofail, 42);
}

@ @<Fun...@>=
static void _tri_init (trix *, const char *, XSetWindowAttributes *,
        XSizeHints *);
static void _tri_xevent (int, short, void *);

@ @c
Display *
trix_get_xdisplay (trix *tctx)
{
        return tctx->xdisplay;
}

int
trix_get_xscreen (trix *tctx)
{
        return tctx->xctx->screen;
}

Window
trix_get_xwindow (trix *tctx)
{
        return tctx->xwin;
}

@ Uses obsolete width and height members of |size| to specify
corresponding arguments to |XCreateWindow|. The |PSize| flag is not
set.

@c
trix *
do_trix_init_full (jmp_buf               *comefrom,
                   struct event_base     *loop,
                   void                 (*xevent_fun)(trix *, XEvent *),
                   const char            *xremote,
                   XSetWindowAttributes  *attr,
                   XSizeHints            *size)
{
        trix *tctx = calloc(1, sizeof (*tctx));
        memmove(&tctx->iofail, comefrom, sizeof (*comefrom));
        tctx->loop = loop;
        tctx->xevent_fun = xevent_fun;
        _tri_init(tctx, xremote, attr, size);
        return tctx;
}

@ @c
static void
_tri_init (trix                 *tctx,
           const char           *xremote,
           XSetWindowAttributes *attr,
           XSizeHints           *size)
{
        if (_tri_xio_handler() == NULL)
                errx(1, "_tri_xio_handler: handler conflict");

        tctx->xdisplay = XOpenDisplay(xremote);
        if (tctx->xdisplay == NULL)
                err(1, "XOpenDisplay");
        XSetIOErrorExitHandler(tctx->xdisplay, _tri_fatal_x_error,
                (void *) tctx);

        finform("X connection fd %u", XConnectionNumber(tctx->xdisplay));
        event_set(&tctx->evxio, XConnectionNumber(tctx->xdisplay),
                EV_READ | EV_PERSIST, _tri_xevent, tctx);
        event_base_set(tctx->loop, &tctx->evxio);
        event_add(&tctx->evxio, NULL);

        if (glXQueryExtension(tctx->xdisplay, NULL, NULL) == 0)
                err(1, "glXQueryExtension");

        int major, minor;
        if (glXQueryVersion(tctx->xdisplay, &major, &minor) == 0)
                err(1, "glXQueryVersion");
        finform("found GLX version %u.%u", major, minor);

        int fbn, visattr[] = { @|
                GLX_DOUBLEBUFFER, True, @|
                GLX_X_RENDERABLE, True, @|
                GLX_RENDER_TYPE, GLX_RGBA_BIT, @|
                GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, @|
                GLX_RED_SIZE, 8, @|
                GLX_GREEN_SIZE, 8, @|
                GLX_BLUE_SIZE, 8, @|
                GLX_ALPHA_SIZE, 8, @|
                GLX_DEPTH_SIZE, 24, @|
                0 @/
        };
        GLXFBConfig *fbc = glXChooseFBConfig(tctx->xdisplay,
                DefaultScreen(tctx->xdisplay), visattr, &fbn);
        if (fbc == NULL || fbn == 0) {
                XFree(fbc);
                err(1, "glXChooseFBConfig");
        }
        tctx->fbconfig = fbc[0];
        XFree(fbc);
        fbc = NULL;

        tctx->xctx = glXGetVisualFromFBConfig(tctx->xdisplay, tctx->fbconfig);
        if (tctx->xctx == NULL)
                err(1, "glXChooseVisual");

        tctx->cmap = XCreateColormap(tctx->xdisplay,
                RootWindow(tctx->xdisplay, tctx->xctx->screen),
                tctx->xctx->visual, AllocNone);
        if (tctx->cmap == 0)
                err(1, "XCreateColormap");

        XSizeHints sh = {0};
        if (size)
                memmove(&sh, size, sizeof (sh));
        else {
                sh.width = 640;
                sh.height = 480;
        }
        XSetWindowAttributes swa = {0};
        if (attr)
                memmove(&swa, attr, sizeof (swa));
        swa.colormap = tctx->cmap;
        tctx->xwin = XCreateWindow(tctx->xdisplay,
                RootWindow(tctx->xdisplay, tctx->xctx->screen),
                0, 0, sh.width, sh.height, 0,
                tctx->xctx->depth, InputOutput, tctx->xctx->visual,
                CWColormap, &swa);
        if (tctx->xwin == 0)
                err(1, "XCreateWindow");
        XSetWMNormalHints(tctx->xdisplay, tctx->xwin, &sh);

        if (!epoxy_has_glx_extension(tctx->xdisplay, tctx->xctx->screen,
                        "GLX_ARB_create_context"))
                err(1, "GLX_ARB_create_context");

        static GLint glxattr[] = { @|
                GLX_CONTEXT_MAJOR_VERSION_ARB, 4, @|
                GLX_CONTEXT_MINOR_VERSION_ARB, 5, @|
                0 @/
        };
        tctx->glctx = glXCreateContextAttribsARB(tctx->xdisplay,
                tctx->fbconfig, 0, GL_TRUE, glxattr);
        if (!tctx->glctx)
                err(1, "glXCreateContext");

        if (!glXMakeCurrent(tctx->xdisplay, tctx->xwin, tctx->glctx))
                err(1, "glXMakeCurrent");

        major = minor = 0;
        glGetIntegerv(GL_MAJOR_VERSION, &major);
        glGetIntegerv(GL_MINOR_VERSION, &minor);
        finform("found OpenGL version %u.%u (%s)", major, minor,
                glXIsDirect(tctx->xdisplay, tctx->glctx) ? "direct" : "indirect");
}

@ @c
void
trix_destroy (trix *tctx)
{
        event_del(&tctx->evxio);
}

@ @c
static void
_tri_xevent (int    fd,
             short  act,
             void  *arg)
{
        jmp_buf ioback = {0};
        trix *tctx = (trix *) arg;
        Display *d;
        XEvent e;
        int qmode;

        memmove(&ioback, &tctx->iofail, sizeof (ioback));
        if (setjmp(tctx->iofail)) {
                /* Happens in the event of an IO error. Abort all the things (in tctx). */
                event_del(&tctx->evxio);
                memset(&e, 0, sizeof (e));
                (tctx->xevent_fun)(tctx, &e);
                goto finish;
        }

        qmode = QueuedAfterFlush;
        while (XEventsQueued(tctx->xdisplay, qmode)) {
                qmode = QueuedAlready;
                XNextEvent(tctx->xdisplay, &e);
                if (XFilterEvent(&e, None))
                        continue;
                (tctx->xevent_fun)(tctx, &e);
        }
finish:
        memmove(&tctx->iofail, &ioback, sizeof (ioback));
}

@ I don't like this. @<Pub...@>=
TRIX_API void trix_trigger (trix *tcxt);
@ @c
void trix_trigger (trix *tctx)
{
        _tri_xevent(ConnectionNumber(tctx->xdisplay), EV_READ, (void *) tctx);
}

@ Less magical than it looks. The |GL_ARB_shading_language_include|
extension instructs the GLSL compiler to honour preprocessor
directives such as the \.{\#line}s inserted by \.{CWEB} when embedding
the shader source.

@d TRIX_SHADER_HEADER "#extension GL_ARB_shading_language_include : require\n"
@.GL\_ARB\_shading\_language\_include@>
@c
GLuint
trix_compile_embedded_shader (GLenum      type,
                              const char *version,
                              const char *source, @|
                              long        length)
{
        const GLchar *cat[5] = {
                "#version ", version, "\n",
                TRIX_SHADER_HEADER,
                source
        };
        GLint clen[5] = { 9, 0, 1, strlen(TRIX_SHADER_HEADER), length };

        if (!version)
                cat[1] = version = "450 core";
        clen[1] = strlen(cat[1]);
        /* Should ask GL for availability... */
        if (version[0] < '3' || (version[0] == '3' && version[1] <= '2')) {
                cat[3] = cat[4];
                clen[3] = clen[4];
                cat[4] = "";
                clen[4] = 0;
        }

        GLuint ret = glCreateShader(type);
        glShaderSource(ret, clen[4] ? 5 : 4, cat, clen);
        glCompileShader(ret);

        GLint status;
        glGetShaderiv(ret, GL_COMPILE_STATUS, &status);
        if (status == GL_FALSE) {
                GLchar msg[1024];
                glGetShaderInfoLog(ret, sizeof (msg), 0, msg);
                fcomplain("trix_compile_embedded_shader: %s\n```%s%s%s%s%s'''",
                        msg, cat[0], cat[1], cat[2], cat[3], cat[4]);
                return 0;
        }

        return ret;
}

@ @c
GLuint
trix_link_embedded_program (const char *version,
                            const char *vertex_source, @|
                            long        vertex_length,
                            const char *fragment_source,
                            long        fragment_length, @|
                            const char *geometry_source,
                            long        geometry_length)
{
        int success;
        char msg[512];

        if (!vertex_length)
                vertex_length = strlen(vertex_source);
        if (!fragment_length)
                fragment_length = strlen(fragment_source);
        GLuint v = trix_compile_embedded_shader(GL_VERTEX_SHADER, version,
                vertex_source, vertex_length);
        GLuint f = trix_compile_embedded_shader(GL_FRAGMENT_SHADER, version,
                fragment_source, fragment_length);
        GLuint g = 0;
        if (geometry_source)
                g = trix_compile_embedded_shader(GL_GEOMETRY_SHADER, version,
                        geometry_source, geometry_length);
        GLuint p = glCreateProgram();
        glAttachShader(p, v);
        if (geometry_source)
                glAttachShader(p, g);
        glAttachShader(p, f);
        glLinkProgram(p);
        glGetProgramiv(p, GL_LINK_STATUS, &success);
        if (!success) {
                glGetProgramInfoLog(p, sizeof (msg), 0, msg);
                fcomplain("failed to compile shaders: %s", msg);
                glDeleteProgram(p);
                p = 0;
        }
        glDeleteShader(v);
        glDeleteShader(f);
        return p;
}

@** Example Triangle.

@(triangle.c@>=
#include <stdlib.h>
#include <epoxy/glx.h>
#include "trix.h"

struct event_base *Main_Loop;

Display           *X_Display;
int                X_Screen;
Window             Main_Window;

int                Width = 0, Height = 0;

GLuint             Shader;
GLuint             VAO, VBO, VIO;

#define VERTEX_SHADER ""                             \
        "layout(location = 0) in vec3 position;\n"   \
        "layout(location = 1) in vec4 colour;\n"     \
        "out vec4 vx_colour;\n"                      \
        "void main (void)\n"                         \
        "{\n"                                        \
        "        vx_colour = colour;\n"              \
        "        gl_Position = vec4(position, 1);\n" \
        "}\n"

#define FRAGMENT_SHADER ""                           \
        "in vec4 vx_colour;\n"                       \
        "out vec4 frag;\n"                           \
        "void main (void)\n"                         \
        "{\n"                                        \
        "        frag = vx_colour;\n"                \
        "}\n"

typedef struct vertex {
        float   x, y, z;
        uint8_t r, g, b, a;
} vertex;

static void
_triangle (float s)
{
        vertex v[] = { @|
                { -s, -s, 0, 255,0,0,255 }, @|
                {  s, -s, 0, 255,0,0,255 }, @|
                {  0,  s, 0, 255,0,0,255 }, @/
        };
        uint8_t i[] = { 0, 1, 2 };
        glBufferData(GL_ARRAY_BUFFER, sizeof (v), v, GL_STATIC_DRAW);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof (i), i, GL_STATIC_DRAW);
}

static void
_init_gl (void)
{
        Shader = trix_link_embedded_program(NULL, VERTEX_SHADER, 0,
                FRAGMENT_SHADER, 0, NULL, 0);

        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);
        glGenBuffers(1, &VIO);
        glBindVertexArray(VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VIO);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof (vertex),
                (void *) offsetof (vertex, x));
        glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                sizeof (vertex), (void *) offsetof (vertex, r));
        _triangle(0.5);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

        glClearColor(0.333f, 0.5f, 0.666f, 1.0f);
}

static void
_draw_gl (void)
{
        glViewport(0, 0, Width, Height);
        glClear(GL_COLOR_BUFFER_BIT);
        glUseProgram(Shader);
        glBindVertexArray(VAO);
        glDrawElementsBaseVertex(GL_TRIANGLES, 3, GL_UNSIGNED_BYTE, 0, 0);
        glBindVertexArray(0);
        glUseProgram(0);
        glXSwapBuffers(X_Display, Main_Window);
}

static void
_xevent (trix   *tctx,
         XEvent *xev)
{
        switch (xev->type) {
        case ConfigureNotify:
                Width = xev->xconfigure.width;
                Height = xev->xconfigure.height;
                /* fallthrough */
        case Expose:
                _draw_gl();
                break;
        }
}

int
main (int    argc,
      char **argv)
{
        trix *tctx;
        Main_Loop = event_init();
        trix_init_full(tctx, Main_Loop, _xevent, NULL, NULL, NULL);
        X_Display = trix_get_xdisplay(tctx);
        X_Screen = trix_get_xscreen(tctx);
        Main_Window = trix_get_xwindow(tctx);
        _init_gl();
        XMapWindow(X_Display, Main_Window);
        XSelectInput(X_Display, Main_Window, StructureNotifyMask | ExposureMask);
        XFlush(X_Display);
        return event_base_dispatch(Main_Loop);
}
