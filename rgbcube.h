#ifndef TP_RGBCUBE_H
#define TP_RGBCUBE_H
tt_rgba RGBCube[240] = {
        [0] = { 0x00, 0x00, 0x00, 0xff },
        [1] = { 0x00, 0x00, 0x5f, 0xff },
        [2] = { 0x00, 0x00, 0x87, 0xff },
        [3] = { 0x00, 0x00, 0xaf, 0xff },
        [4] = { 0x00, 0x00, 0xd7, 0xff },
        [5] = { 0x00, 0x00, 0xff, 0xff },
        [6] = { 0x00, 0x5f, 0x00, 0xff },
        [7] = { 0x00, 0x5f, 0x5f, 0xff },
        [8] = { 0x00, 0x5f, 0x87, 0xff },
        [9] = { 0x00, 0x5f, 0xaf, 0xff },
        [10] = { 0x00, 0x5f, 0xd7, 0xff },
        [11] = { 0x00, 0x5f, 0xff, 0xff },
        [12] = { 0x00, 0x87, 0x00, 0xff },
        [13] = { 0x00, 0x87, 0x5f, 0xff },
        [14] = { 0x00, 0x87, 0x87, 0xff },
        [15] = { 0x00, 0x87, 0xaf, 0xff },
        [16] = { 0x00, 0x87, 0xd7, 0xff },
        [17] = { 0x00, 0x87, 0xff, 0xff },
        [18] = { 0x00, 0xaf, 0x00, 0xff },
        [19] = { 0x00, 0xaf, 0x5f, 0xff },
        [20] = { 0x00, 0xaf, 0x87, 0xff },
        [21] = { 0x00, 0xaf, 0xaf, 0xff },
        [22] = { 0x00, 0xaf, 0xd7, 0xff },
        [23] = { 0x00, 0xaf, 0xff, 0xff },
        [24] = { 0x00, 0xd7, 0x00, 0xff },
        [25] = { 0x00, 0xd7, 0x5f, 0xff },
        [26] = { 0x00, 0xd7, 0x87, 0xff },
        [27] = { 0x00, 0xd7, 0xaf, 0xff },
        [28] = { 0x00, 0xd7, 0xd7, 0xff },
        [29] = { 0x00, 0xd7, 0xff, 0xff },
        [30] = { 0x00, 0xff, 0x00, 0xff },
        [31] = { 0x00, 0xff, 0x5f, 0xff },
        [32] = { 0x00, 0xff, 0x87, 0xff },
        [33] = { 0x00, 0xff, 0xaf, 0xff },
        [34] = { 0x00, 0xff, 0xd7, 0xff },
        [35] = { 0x00, 0xff, 0xff, 0xff },
        [36] = { 0x5f, 0x00, 0x00, 0xff },
        [37] = { 0x5f, 0x00, 0x5f, 0xff },
        [38] = { 0x5f, 0x00, 0x87, 0xff },
        [39] = { 0x5f, 0x00, 0xaf, 0xff },
        [40] = { 0x5f, 0x00, 0xd7, 0xff },
        [41] = { 0x5f, 0x00, 0xff, 0xff },
        [42] = { 0x5f, 0x5f, 0x00, 0xff },
        [43] = { 0x5f, 0x5f, 0x5f, 0xff },
        [44] = { 0x5f, 0x5f, 0x87, 0xff },
        [45] = { 0x5f, 0x5f, 0xaf, 0xff },
        [46] = { 0x5f, 0x5f, 0xd7, 0xff },
        [47] = { 0x5f, 0x5f, 0xff, 0xff },
        [48] = { 0x5f, 0x87, 0x00, 0xff },
        [49] = { 0x5f, 0x87, 0x5f, 0xff },
        [50] = { 0x5f, 0x87, 0x87, 0xff },
        [51] = { 0x5f, 0x87, 0xaf, 0xff },
        [52] = { 0x5f, 0x87, 0xd7, 0xff },
        [53] = { 0x5f, 0x87, 0xff, 0xff },
        [54] = { 0x5f, 0xaf, 0x00, 0xff },
        [55] = { 0x5f, 0xaf, 0x5f, 0xff },
        [56] = { 0x5f, 0xaf, 0x87, 0xff },
        [57] = { 0x5f, 0xaf, 0xaf, 0xff },
        [58] = { 0x5f, 0xaf, 0xd7, 0xff },
        [59] = { 0x5f, 0xaf, 0xff, 0xff },
        [60] = { 0x5f, 0xd7, 0x00, 0xff },
        [61] = { 0x5f, 0xd7, 0x5f, 0xff },
        [62] = { 0x5f, 0xd7, 0x87, 0xff },
        [63] = { 0x5f, 0xd7, 0xaf, 0xff },
        [64] = { 0x5f, 0xd7, 0xd7, 0xff },
        [65] = { 0x5f, 0xd7, 0xff, 0xff },
        [66] = { 0x5f, 0xff, 0x00, 0xff },
        [67] = { 0x5f, 0xff, 0x5f, 0xff },
        [68] = { 0x5f, 0xff, 0x87, 0xff },
        [69] = { 0x5f, 0xff, 0xaf, 0xff },
        [70] = { 0x5f, 0xff, 0xd7, 0xff },
        [71] = { 0x5f, 0xff, 0xff, 0xff },
        [72] = { 0x87, 0x00, 0x00, 0xff },
        [73] = { 0x87, 0x00, 0x5f, 0xff },
        [74] = { 0x87, 0x00, 0x87, 0xff },
        [75] = { 0x87, 0x00, 0xaf, 0xff },
        [76] = { 0x87, 0x00, 0xd7, 0xff },
        [77] = { 0x87, 0x00, 0xff, 0xff },
        [78] = { 0x87, 0x5f, 0x00, 0xff },
        [79] = { 0x87, 0x5f, 0x5f, 0xff },
        [80] = { 0x87, 0x5f, 0x87, 0xff },
        [81] = { 0x87, 0x5f, 0xaf, 0xff },
        [82] = { 0x87, 0x5f, 0xd7, 0xff },
        [83] = { 0x87, 0x5f, 0xff, 0xff },
        [84] = { 0x87, 0x87, 0x00, 0xff },
        [85] = { 0x87, 0x87, 0x5f, 0xff },
        [86] = { 0x87, 0x87, 0x87, 0xff },
        [87] = { 0x87, 0x87, 0xaf, 0xff },
        [88] = { 0x87, 0x87, 0xd7, 0xff },
        [89] = { 0x87, 0x87, 0xff, 0xff },
        [90] = { 0x87, 0xaf, 0x00, 0xff },
        [91] = { 0x87, 0xaf, 0x5f, 0xff },
        [92] = { 0x87, 0xaf, 0x87, 0xff },
        [93] = { 0x87, 0xaf, 0xaf, 0xff },
        [94] = { 0x87, 0xaf, 0xd7, 0xff },
        [95] = { 0x87, 0xaf, 0xff, 0xff },
        [96] = { 0x87, 0xd7, 0x00, 0xff },
        [97] = { 0x87, 0xd7, 0x5f, 0xff },
        [98] = { 0x87, 0xd7, 0x87, 0xff },
        [99] = { 0x87, 0xd7, 0xaf, 0xff },
        [100] = { 0x87, 0xd7, 0xd7, 0xff },
        [101] = { 0x87, 0xd7, 0xff, 0xff },
        [102] = { 0x87, 0xff, 0x00, 0xff },
        [103] = { 0x87, 0xff, 0x5f, 0xff },
        [104] = { 0x87, 0xff, 0x87, 0xff },
        [105] = { 0x87, 0xff, 0xaf, 0xff },
        [106] = { 0x87, 0xff, 0xd7, 0xff },
        [107] = { 0x87, 0xff, 0xff, 0xff },
        [108] = { 0xaf, 0x00, 0x00, 0xff },
        [109] = { 0xaf, 0x00, 0x5f, 0xff },
        [110] = { 0xaf, 0x00, 0x87, 0xff },
        [111] = { 0xaf, 0x00, 0xaf, 0xff },
        [112] = { 0xaf, 0x00, 0xd7, 0xff },
        [113] = { 0xaf, 0x00, 0xff, 0xff },
        [114] = { 0xaf, 0x5f, 0x00, 0xff },
        [115] = { 0xaf, 0x5f, 0x5f, 0xff },
        [116] = { 0xaf, 0x5f, 0x87, 0xff },
        [117] = { 0xaf, 0x5f, 0xaf, 0xff },
        [118] = { 0xaf, 0x5f, 0xd7, 0xff },
        [119] = { 0xaf, 0x5f, 0xff, 0xff },
        [120] = { 0xaf, 0x87, 0x00, 0xff },
        [121] = { 0xaf, 0x87, 0x5f, 0xff },
        [122] = { 0xaf, 0x87, 0x87, 0xff },
        [123] = { 0xaf, 0x87, 0xaf, 0xff },
        [124] = { 0xaf, 0x87, 0xd7, 0xff },
        [125] = { 0xaf, 0x87, 0xff, 0xff },
        [126] = { 0xaf, 0xaf, 0x00, 0xff },
        [127] = { 0xaf, 0xaf, 0x5f, 0xff },
        [128] = { 0xaf, 0xaf, 0x87, 0xff },
        [129] = { 0xaf, 0xaf, 0xaf, 0xff },
        [130] = { 0xaf, 0xaf, 0xd7, 0xff },
        [131] = { 0xaf, 0xaf, 0xff, 0xff },
        [132] = { 0xaf, 0xd7, 0x00, 0xff },
        [133] = { 0xaf, 0xd7, 0x5f, 0xff },
        [134] = { 0xaf, 0xd7, 0x87, 0xff },
        [135] = { 0xaf, 0xd7, 0xaf, 0xff },
        [136] = { 0xaf, 0xd7, 0xd7, 0xff },
        [137] = { 0xaf, 0xd7, 0xff, 0xff },
        [138] = { 0xaf, 0xff, 0x00, 0xff },
        [139] = { 0xaf, 0xff, 0x5f, 0xff },
        [140] = { 0xaf, 0xff, 0x87, 0xff },
        [141] = { 0xaf, 0xff, 0xaf, 0xff },
        [142] = { 0xaf, 0xff, 0xd7, 0xff },
        [143] = { 0xaf, 0xff, 0xff, 0xff },
        [144] = { 0xd7, 0x00, 0x00, 0xff },
        [145] = { 0xd7, 0x00, 0x5f, 0xff },
        [146] = { 0xd7, 0x00, 0x87, 0xff },
        [147] = { 0xd7, 0x00, 0xaf, 0xff },
        [148] = { 0xd7, 0x00, 0xd7, 0xff },
        [149] = { 0xd7, 0x00, 0xff, 0xff },
        [150] = { 0xd7, 0x5f, 0x00, 0xff },
        [151] = { 0xd7, 0x5f, 0x5f, 0xff },
        [152] = { 0xd7, 0x5f, 0x87, 0xff },
        [153] = { 0xd7, 0x5f, 0xaf, 0xff },
        [154] = { 0xd7, 0x5f, 0xd7, 0xff },
        [155] = { 0xd7, 0x5f, 0xff, 0xff },
        [156] = { 0xd7, 0x87, 0x00, 0xff },
        [157] = { 0xd7, 0x87, 0x5f, 0xff },
        [158] = { 0xd7, 0x87, 0x87, 0xff },
        [159] = { 0xd7, 0x87, 0xaf, 0xff },
        [160] = { 0xd7, 0x87, 0xd7, 0xff },
        [161] = { 0xd7, 0x87, 0xff, 0xff },
        [162] = { 0xd7, 0xaf, 0x00, 0xff },
        [163] = { 0xd7, 0xaf, 0x5f, 0xff },
        [164] = { 0xd7, 0xaf, 0x87, 0xff },
        [165] = { 0xd7, 0xaf, 0xaf, 0xff },
        [166] = { 0xd7, 0xaf, 0xd7, 0xff },
        [167] = { 0xd7, 0xaf, 0xff, 0xff },
        [168] = { 0xd7, 0xd7, 0x00, 0xff },
        [169] = { 0xd7, 0xd7, 0x5f, 0xff },
        [170] = { 0xd7, 0xd7, 0x87, 0xff },
        [171] = { 0xd7, 0xd7, 0xaf, 0xff },
        [172] = { 0xd7, 0xd7, 0xd7, 0xff },
        [173] = { 0xd7, 0xd7, 0xff, 0xff },
        [174] = { 0xd7, 0xff, 0x00, 0xff },
        [175] = { 0xd7, 0xff, 0x5f, 0xff },
        [176] = { 0xd7, 0xff, 0x87, 0xff },
        [177] = { 0xd7, 0xff, 0xaf, 0xff },
        [178] = { 0xd7, 0xff, 0xd7, 0xff },
        [179] = { 0xd7, 0xff, 0xff, 0xff },
        [180] = { 0xff, 0x00, 0x00, 0xff },
        [181] = { 0xff, 0x00, 0x5f, 0xff },
        [182] = { 0xff, 0x00, 0x87, 0xff },
        [183] = { 0xff, 0x00, 0xaf, 0xff },
        [184] = { 0xff, 0x00, 0xd7, 0xff },
        [185] = { 0xff, 0x00, 0xff, 0xff },
        [186] = { 0xff, 0x5f, 0x00, 0xff },
        [187] = { 0xff, 0x5f, 0x5f, 0xff },
        [188] = { 0xff, 0x5f, 0x87, 0xff },
        [189] = { 0xff, 0x5f, 0xaf, 0xff },
        [190] = { 0xff, 0x5f, 0xd7, 0xff },
        [191] = { 0xff, 0x5f, 0xff, 0xff },
        [192] = { 0xff, 0x87, 0x00, 0xff },
        [193] = { 0xff, 0x87, 0x5f, 0xff },
        [194] = { 0xff, 0x87, 0x87, 0xff },
        [195] = { 0xff, 0x87, 0xaf, 0xff },
        [196] = { 0xff, 0x87, 0xd7, 0xff },
        [197] = { 0xff, 0x87, 0xff, 0xff },
        [198] = { 0xff, 0xaf, 0x00, 0xff },
        [199] = { 0xff, 0xaf, 0x5f, 0xff },
        [200] = { 0xff, 0xaf, 0x87, 0xff },
        [201] = { 0xff, 0xaf, 0xaf, 0xff },
        [202] = { 0xff, 0xaf, 0xd7, 0xff },
        [203] = { 0xff, 0xaf, 0xff, 0xff },
        [204] = { 0xff, 0xd7, 0x00, 0xff },
        [205] = { 0xff, 0xd7, 0x5f, 0xff },
        [206] = { 0xff, 0xd7, 0x87, 0xff },
        [207] = { 0xff, 0xd7, 0xaf, 0xff },
        [208] = { 0xff, 0xd7, 0xd7, 0xff },
        [209] = { 0xff, 0xd7, 0xff, 0xff },
        [210] = { 0xff, 0xff, 0x00, 0xff },
        [211] = { 0xff, 0xff, 0x5f, 0xff },
        [212] = { 0xff, 0xff, 0x87, 0xff },
        [213] = { 0xff, 0xff, 0xaf, 0xff },
        [214] = { 0xff, 0xff, 0xd7, 0xff },
        [215] = { 0xff, 0xff, 0xff, 0xff },
        [216] = { 0x08, 0x08, 0x08, 0xff },
        [217] = { 0x12, 0x12, 0x12, 0xff },
        [218] = { 0x1c, 0x1c, 0x1c, 0xff },
        [219] = { 0x26, 0x26, 0x26, 0xff },
        [220] = { 0x30, 0x30, 0x30, 0xff },
        [221] = { 0x3a, 0x3a, 0x3a, 0xff },
        [222] = { 0x44, 0x44, 0x44, 0xff },
        [223] = { 0x4e, 0x4e, 0x4e, 0xff },
        [224] = { 0x58, 0x58, 0x58, 0xff },
        [225] = { 0x62, 0x62, 0x62, 0xff },
        [226] = { 0x6c, 0x6c, 0x6c, 0xff },
        [227] = { 0x76, 0x76, 0x76, 0xff },
        [228] = { 0x80, 0x80, 0x80, 0xff },
        [229] = { 0x8a, 0x8a, 0x8a, 0xff },
        [230] = { 0x94, 0x94, 0x94, 0xff },
        [231] = { 0x9e, 0x9e, 0x9e, 0xff },
        [232] = { 0xa8, 0xa8, 0xa8, 0xff },
        [233] = { 0xb2, 0xb2, 0xb2, 0xff },
        [234] = { 0xbc, 0xbc, 0xbc, 0xff },
        [235] = { 0xc6, 0xc6, 0xc6, 0xff },
        [236] = { 0xd0, 0xd0, 0xd0, 0xff },
        [237] = { 0xda, 0xda, 0xda, 0xff },
        [238] = { 0xe4, 0xe4, 0xe4, 0xff },
        [239] = { 0xee, 0xee, 0xee, 0xff },
};
#endif /* TP_RGBCUBE_H */
