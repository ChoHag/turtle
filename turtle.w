@i format.w
@i types.w

@* Turtle Terminal Emulator.

This file is still a mess but not so much now.

@c
#include <assert.h>
#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <math.h>
#include <pwd.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <termios.h>
#include <unistd.h>
#ifdef __GLIBC__
#include <pty.h>
#elif __FreeBSD__
#include <libutil.h>
#else
#include <util.h>
#endif
@#
#include <epoxy/gl.h>
#include <epoxy/glx.h>
#include <event.h>
#include <GL/glu.h>
#include <X11/X.h>
#include <X11/Xatom.h>
#include <X11/Xlib.h>
@#
#include "user.h"
#include "log.h"
#include "tgl.h"
#include "tterm.h"
#include "tpanel.h"
#include "trix.h"
#undef LOG_PREFIX
#define LOG_PREFIX "turtle"

@<Function declarations (\.{turtle.o})@>@;
static void _turtle_ioerr (struct bufferevent *, short, void *);
static void _turtle_pty_exec (char *, char **, pid_t *);
static void _turtle_read (int, short, void *);
static void _turtle_reap (int, short, void *);
static void _turtle_resize_pty (int16_t, int16_t, void *);
static void _turtle_write (uint8_t *, size_t, void *);
static void _turtle_xevent (trix *, XEvent *);

static void _turtle_selection_request (XEvent *);
static void _turtle_selection_notify (XEvent *);

#define FONT_NAME "Liberation Mono:antialias=true:autohint=true"
#define FONT_SIZE 21 /* dog food */

@<Global variables (\.{turtle.o})@>@;
struct event_base  *Main_Loop;
struct event        Ev_TTY_Read, Ev_SIGCHLD;
struct bufferevent *Ev_TTY_Write;

int                 PTY_FD = 0;

tt_panel           *Panel;
tt_term            *Term;

trix               *Tctx;
Display            *X_Display;
int                 X_Screen;
long                X_Events;
Window              Main_Window;
int                 Window_Width = 0, Window_Height = 0;

@ @c
int
main (int    argc,
      char **argv)
{
        pledge("stdio rpath wpath cpath ps sendfd recvfd drm dns unix"
                " prot_exec flock tty proc exec", NULL);

        pid_t cpid = 0;
        _turtle_pty_exec(NULL, NULL, &cpid);
        assert(cpid);
        assert(PTY_FD);

        pledge("stdio rpath wpath cpath ps sendfd recvfd drm dns unix"
                " prot_exec flock tty", "");

        Main_Loop = event_init();

        signal_set(&Ev_SIGCHLD, SIGCHLD, _turtle_reap, (void *) (uintptr_t) cpid);
        event_base_set(Main_Loop, &Ev_SIGCHLD);
        signal_add(&Ev_SIGCHLD, NULL);

        event_set(&Ev_TTY_Read, PTY_FD, EV_READ | EV_PERSIST, _turtle_read, NULL);
        event_base_set(Main_Loop, &Ev_TTY_Read);
        event_add(&Ev_TTY_Read, NULL);

        Ev_TTY_Write = bufferevent_new(PTY_FD, NULL, NULL, _turtle_ioerr, NULL);
        bufferevent_base_set(Main_Loop, Ev_TTY_Write);
        bufferevent_enable(Ev_TTY_Write, EV_WRITE);

        trix_init_full(Tctx, Main_Loop, _turtle_xevent, NULL, NULL, NULL);
        X_Display = trix_get_xdisplay(Tctx);
        X_Screen = trix_get_xscreen(Tctx);
        Main_Window = trix_get_xwindow(Tctx);
        XMapWindow(X_Display, Main_Window);
        X_Events = StructureNotifyMask | ExposureMask
                | KeyPressMask
                | PointerMotionMask | ButtonPressMask | ButtonReleaseMask;
        XSelectInput(X_Display, Main_Window, X_Events);
        XFlush(X_Display);

        if (!ximopen(X_Display)) {
                XRegisterIMInstantiateCallback(X_Display, NULL, NULL, NULL,
                                               ximinstantiate, NULL);
        }

        Panel = tt_panel_new(80, 24);
        if (!Panel)
                errx(1, "panel_init");

        Term = term_new(Panel, _turtle_write, NULL);
        if (!Term)
                errx(1, "term_new");
        tt_panel_set_resize_fun(Panel, _turtle_resize_pty, NULL);

        tgl_initgl();
        tgl_panel_to_vertex();
        user_init_keymap();
        _turtle_init_clock();

        tt_font *font = tt_panel_font_new_from_xft_name(Panel, X_Display,
                X_Screen, FONT_NAME, FONT_SIZE);
        if (!font)
                errx(1, "tt_panel_font_new_from_xft_name");
        tt_panel_invoke_font(Panel, 0, font, false);

        trix_trigger(Tctx); /* font init swallows filedes readiness indicator */
        return event_base_dispatch(Main_Loop);
}

@ @c
static void
_turtle_pty_exec (char   *cmd,
                  char  **args,
                  pid_t  *child)
{
        char *no_args[] = { NULL, NULL };
        if (!args)
                args = no_args;
        if (!cmd)
                cmd = args[0];
        if (!cmd) {
                struct passwd *pw = getpwuid(getuid());
                if (pw && *pw->pw_shell)
                        cmd = pw->pw_shell;
                else {
                        warn("getpwuid");
                        cmd = "/bin/sh";
                }
        }
        if (!args[0])
                args[0] = cmd;

        int m, s;
        if (openpty(&m, &s, NULL, NULL, NULL) == -1)
                err(1, "openpty");

        pid_t p = fork();
        switch (p) {
        case -1:
                err(1, "fork");
        case 0: /* New process. */
                if (close(m) == -1) /* Master side of pty */
                        err(1, "master close");
                if (setsid() == -1) /* Become new session group leader. */
                        err(1, "setsid");
                if (ioctl(s, TIOCSCTTY, NULL) < 0) /* Controlling terminal. */
                        err(1, "ioctl/TIOCSCTTY");
                if (unsetenv("COLUMNS") == -1
                                || unsetenv("LINES") == -1
                                || unsetenv("TERMCAP") == -1)
                        warn("unsetenv");
                if (setenv("TERM", "turtle", 1) == -1)
                        warn("setenv");
                if (dup2(s, 0) == -1 || dup2(s, 1) == -1 || dup2(s, 2) == -1)
                        err(1, "dup2"); /* Replace the child's stdio
                                                with the pty slave last. */
                execvp(cmd, args);
                abort(); /* no stdio */
        default: /* Turtle. */
                if (close(s) == -1) /* Slave side of pty */
                        err(1, "slave close");
                *child = p;
                PTY_FD = m;
        }
}

@ @.TODO@> @c
static void
_turtle_reap (int    fd,
              short  e,
              void  *arg)
{
        assert(fd == SIGCHLD);
        assert(e & EV_SIGNAL);
        assert(arg);
        siginfo_t status = {0};
        if (waitid(P_PID, (uintptr_t) arg, &status, WEXITED) == -1)
                err(1, "waitid");

        finform("signal %u", status.si_signo);
        char *msg;
        switch (status.si_code) {
        case CLD_CONTINUED:
                msg = "continued";
                break;
        case CLD_DUMPED:
                msg = "dumped";
                break;
        case CLD_EXITED:
                msg = "exited";
                break;
        case CLD_KILLED:
                msg = "killed";
                break;
        case CLD_STOPPED:
                msg = "stopped";
                break;
        case CLD_TRAPPED:
                msg = "trapped";
                break;
        default:
                msg = "?";
                break;
        }
        finform("exit code: %s", msg);
        signal_del(&Ev_SIGCHLD);
        _turtle_stop_clock();
        trix_destroy(Tctx);
}

@ @c
static void
_turtle_resize_pty (int16_t  width,
                    int16_t  height,
                    void    *fdp)
{
        struct winsize wsz;
        wsz.ws_row = height;
        wsz.ws_col = width;
#if 0
        wsz.ws_xpixel = ...;
        wsz.ws_ypixel = ...;
#endif
        if (ioctl(PTY_FD, TIOCSWINSZ, &wsz))
                warn("ioctl/TIOCSWINSZ");
}

@ @c
static void
_turtle_read (int    fd,
              short  e,
              void  *arg)
{
        assert(e & EV_READ);
        assert(!arg);
        assert(Term);

        size_t readlen = 1024;
        uint8_t buf[1024] = {0};
        ssize_t r = read(fd, buf, readlen);

        switch (r) {
        case -1:
                if (errno != EINTR)
                        warn("read");
                return;
        case 0:
                event_del(&Ev_TTY_Read);
                tt_panel_reset_rendition(Panel);
                term_exec_CR(Term, NULL);
                term_consume_bytes(Term, "\r\nEOF\r\n", 7);
                break;
        default:
                term_consume_bytes(Term, buf, r);
                break;
        }
}

@ @.TODO@>
@c
static void
_turtle_write (uint8_t *b,
               size_t   len,
               void    *arg)
{
        assert(!arg);
        int rval = bufferevent_write(Ev_TTY_Write, b, len);
        if (rval == -1)
                err(1, "bufferevent_write");
}

@ @.TODO@> @c
static void
_turtle_xevent (trix   *tctx,
                XEvent *xev)
{
        assert(tctx == Tctx);
        switch (xev->type) {
        default:
                fcomplain("unhandled event %d", xev->type);
                break;
        case 0: /* TODO: not the cleanest exit... */
                signal_del(&Ev_SIGCHLD);
                event_del(&Ev_TTY_Read);
                _turtle_stop_clock();
                break;
        case ConfigureNotify:
                Window_Width = xev->xconfigure.width;
                Window_Height = xev->xconfigure.height;
                if (1) { /* resize-or-stretch (TODO) */
                        tt_font *dfont = tt_panel_get_font(Panel, 0, 0);
                        assert(dfont);
                        int tw = tt_panel_font_get_cell_width(dfont);
                        int th = tt_panel_font_get_cell_height(dfont);
                        int w = Window_Width / tw;
                        int h = Window_Height / th;
                        int pw = tt_panel_get_width(Panel);
                        int ph = tt_panel_get_height(Panel);
                        if (w != pw || h != ph)
                                tt_panel_resize(Panel, w, h, true);
                }
                /* fallthrough */
        case Expose:
                tgl_drawgl(NULL);
                break;
        case KeyPress:
                user_keypress(xev);
                break;
        case KeyRelease:
                user_keyrelease(xev);
                break;
        case ButtonPress:
                user_pointer_press(xev);
                break;
        case MotionNotify:
                user_pointer_motion(xev);
                break;
        case ButtonRelease:
                user_pointer_release(xev);
                break;
        case SelectionNotify: /* Something is sending its selection. */
                _turtle_selection_notify(xev);
                break;
        case SelectionRequest: /* Something has asked for the selection. */
                _turtle_selection_request(xev);
                break;
        }
}

@ @c
static void
_turtle_ioerr (struct bufferevent *ev,
               short               e,
               void               *arg)
{
        assert(arg == NULL);
        fbug("ioerr %u", e);
        abort();
}

@ @.TODO@> @c
static void
_turtle_selection_notify (XEvent *xev)
{
        unsigned long nitems, ofs, rem;
        int format;
        unsigned char *data, *last, *repl;
        Atom type, incratom, property = None;

        incratom = XInternAtom(X_Display, "INCR", 0);

        ofs = 0;
        if (xev->type == SelectionNotify)
                property = xev->xselection.property;
        else if (xev->type == PropertyNotify)
                property = xev->xproperty.atom;

        if (property == None)
                return;

        do {
                if (XGetWindowProperty(X_Display, Main_Window, property, ofs,
                                        BUFSIZ/4, False, AnyPropertyType,
                                        &type, &format, &nitems, &rem,
                                        &data)) {
                        fprintf(stderr, "Clipboard allocation failed\n");
                        return;
                }

                if (xev->type == PropertyNotify && nitems == 0 && rem == 0) {
                        /*
                         * If there is some PropertyNotify with no data, then
                         * this is the signal of the selection owner that all
                         * data has been transferred. We won't need to receive
                         * PropertyNotify events anymore.
                         */
                        X_Events &= ~PropertyChangeMask;
                        XSelectInput(X_Display, Main_Window, X_Events);
                }

                if (type == incratom) {
                        /*
                         * Activate the PropertyNotify events so we receive
                         * when the selection owner does send us the next
                         * chunk of data.
                         */
                        X_Events |= PropertyChangeMask;
                        XSelectInput(X_Display, Main_Window, X_Events);

                        /*
                         * Deleting the property is the transfer start signal.
                         */
                        XDeleteProperty(X_Display, Main_Window, (int)property);
                        continue;
                }

                term_paste(Term, (char *) data, nitems * format / 8); // TODO: SRM mode (DEC 5-135)
                XFree(data);
                /* number of 32-bit chunks returned */
                ofs += nitems * format / 32;
        } while (rem > 0);

        /*
         * Deleting the property again tells the selection owner to send the
         * next data chunk in the property.
         */
        XDeleteProperty(X_Display, Main_Window, (int)property);
}


@ @c
static void
_turtle_selection_request (XEvent *xev)
{

        XSelectionRequestEvent *xsre = (XSelectionRequestEvent *) xev;
        XSelectionEvent repl = {0};
        repl.type = SelectionNotify;
        repl.requestor = xsre->requestor;
        repl.selection = xsre->selection;
        repl.target = xsre->target;
        repl.time = xsre->time;
        if (xsre->property == None)
                xsre->property = xsre->target;

        char *seltext;
        Atom xa_targets = XInternAtom(X_Display, "TARGETS", 0);
        Atom string = XInternAtom(X_Display, "UTF8_STRING", 0);
        if (xsre->target == xa_targets) {
                /* respond with the supported type */
                XChangeProperty(xsre->display, xsre->requestor, xsre->property,
                                XA_ATOM, 32, PropModeReplace,
                                (unsigned char *) &string, 1);
                repl.property = xsre->property;
        } else if (xsre->target == string || xsre->target == XA_STRING) {
                /*
                 * xith XA_STRING non ascii characters may be incorrect in the
                 * requestor. It is not our problem, use utf8.
                 */
                if (xsre->selection == XA_PRIMARY) {
                        seltext = tt_panel_get_selection(Panel, 1);
                } else {
                        fbug("Unhandled clipboard selection 0x%lx",
                                xsre->selection);
                        return;
                }
                if (seltext != NULL) {
                        XChangeProperty(xsre->display, xsre->requestor,
                                        xsre->property, xsre->target,
                                        8, PropModeReplace,
                                        (unsigned char *)seltext, strlen(seltext));
                        repl.property = xsre->property;
                }
        }

        /* all done, send a notification to the listener */
        if (!XSendEvent(xsre->display, xsre->requestor, 1, 0, (XEvent *) &repl))
                fbug("XSendEvent");
}

@* Clock.

@<Global...@>=
struct timespec     Clock_n; /* for calculating */
struct timeval      Clock_m; /* for libevent */
bool                Clock_Ticking;
struct event        Clock_Event;

@ @<Fun...@>=
static void _turtle_tick (int, short, void *);
static void _turtle_init_clock (void);
static void _turtle_stop_clock (void);

@ @c
static void
_turtle_init_clock (void)
{
        assert(Main_Loop);
        Clock_n.tv_sec = 0;
        Clock_n.tv_nsec = 1000000000 / 12;
        TIMESPEC_TO_TIMEVAL(&Clock_m, &Clock_n);
        evtimer_set(&Clock_Event, _turtle_tick, NULL);
        event_base_set(Main_Loop, &Clock_Event);
        evtimer_add(&Clock_Event, &Clock_m);
}

@ @c
static void
_turtle_stop_clock (void)
{
        evtimer_del(&Clock_Event);
}

@ @c
static void
_turtle_tick (int    fd,
              short  events,
              void  *arg)
{
        struct timespec next, now;
        struct timeval vnext;
        clock_gettime(CLOCK_MONOTONIC, &now);
        next.tv_nsec = now.tv_nsec / Clock_n.tv_nsec;
        next.tv_nsec++;
        next.tv_nsec *= Clock_n.tv_nsec;
        next.tv_nsec -= now.tv_nsec;
        assert(next.tv_nsec < 1000000000);
        next.tv_sec = 0;
        TIMESPEC_TO_TIMEVAL(&vnext, &next);
        evtimer_add(&Clock_Event, &vnext);
        if (Clock_Ticking)
                fcomplain("tick tock!");
        if (Clock_Ticking)
                return;
        Clock_Ticking = true;
        if (tt_panel_get_dirty(Panel)) {
                tgl_panel_to_vertex();
                tt_panel_clean(Panel);
        }
        tgl_drawgl(&now);
        Clock_Ticking = false;
}
