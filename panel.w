@i format.w
@i types.w

@** Turtle Panel.

``\.{panel.h}'' is used by curses.

@(tpanel.h@>=
#ifndef TURTLE_PANEL_H
#define TURTLE_PANEL_H
#include <stdbool.h>
#include <stdint.h>
#include <X11/Xlib.h>
#include <X11/Xft/Xft.h>
@#
#include "utf8.h"
#ifndef TTAPI
#define TTAPI __attribute__((__visibility__("default")))
#endif
@<Public API (\.{panel.o})@>@;
#endif /* |TURTLE_PANEL_H| */

@ @c
#include "tpanel.h"
@#
#include <assert.h>
#include <err.h>
#include <math.h>
@#
#include <sys/tree.h>
@#
#include "hrfont.h"
#include "hrfont-xft.h"
#include "log.h"
#include "atlas.h"
#include "buf.h"
#undef LOG_PREFIX
#define LOG_PREFIX "panel"
@h
#include "panel-internal.h"

@<Global variables (\.{panel.o})@> @;

@ For testing.

@(panel-internal.h@>=
#ifndef TT_PANEL_INTERNAL
#define TT_PANEL_INTERNAL
@h
@<Type definitions (\.{panel.o})@> @;
@<Panel type definitions (\.{panel.o})@> @;
@<Function declarations (\.{panel.o})@> @;
#endif /* |TT_PANEL_INTERNAL| */

@ @<Pub...@>=
typedef struct tt_cell tt_cell;
typedef struct tt_font tt_font;
typedef struct tt_glyph tt_glyph;
typedef struct tt_panel tt_panel;

typedef struct tt_rgba {
        uint8_t r, g, b, a;
} tt_rgba;

#define TT_CELL_LENGTH ((4 * 4) + sizeof (tt_glyph *))

@ @<Type def...@>=
typedef union tt_irgba {
        struct {
                uint8_t r, g, b, a;
        };
        tt_rgba rgba;
        struct {
#if 1 /* stupid-endian */
                uint32_t index:24;
                uint8_t  is_indexed:1;
                uint8_t  _cant_count:7;
#else
                uint32_t index:24;
                uint8_t  _on_intel:7;
                uint8_t  is_indexed:1;
#endif
        };
} tt_irgba;

// DEC 5.3/5-15.
typedef struct tt_cell {
        union {
                struct {
                        uint32_t  _pad:5;
                        uint32_t  invoked:4; /* 0--11 */
                        uint32_t  alt:2; /* 0--3 */
                        uint32_t  cp:21;
                };
                uint32_t mask;
        };
        uint32_t  flags;
        tt_irgba  bg, fg;
        tt_glyph *drawn;
} tt_cell;

@ @<Panel type...@>=
typedef struct tt_panel {
        uint32_t   flags;
        int16_t   _pad0;
        int16_t    htab_period;
        int16_t    width, height;
        int16_t    top, right, bottom, left;
        int16_t    cursor_col, cursor_row;
        tt_irgba   cursor_colour;
        float      cursor_period;
        void     (*resize_fun)(int16_t, int16_t, void *);
        void      *resize_arg;
        //
        tt_selection select[TTP_MOUSE_BUTTONS];
        //
        tt_rgba    palette[256];
        tt_cell    blank, pen;
        tt_cell   *content[2];
        uint8_t   *htab[2];
        tt_font   *invoked_font[11][4];
        tp_atlas  *type_case;
        RB_HEAD(tt_glyph_index, tt_glyph) index;
} tt_panel;

@ The unusual order of the first 11 flags is to have them correspond
to the SGR operation which toggles them, pointlessly.

@d TTP_RENDITION_MASK ((1lu << TTP_RENDITION_COUNT) - 1)
@<Pub...@>=
enum tt_rendition_flags { @/
@t\iI@> TTP_OVERLINE,    /* SGR 53 */
@t\iI@> TTP_BOLD,        /* SGR 1 */
@t\iI@> TTP_FAINT,       /* SGR 2 */
@t\iI@> TTP_ITALIC,      /* SGR 3 */
@t\iI@> TTP_UNDERLINE_1, /* SGR 4 */
@t\iI@> TTP_BLINK_SLOW,  /* SGR 5 */
@t\iI@> TTP_BLINK_FAST,  /* SGR 6 */
@t\iI@> TTP_INVERSE,     /* SGR 7 */
@t\iI@> TTP_HIDDEN,      /* SGR 8 */
@t\iI@> TTP_STRIKEOUT,   /* SGR 9 */
@t\iI@> TTP_UNDERLINE_2, /* SGR 21 */
@t\iI@> TTP_RENDITION_COUNT
};

@ @<Type def...@>=
enum tt_panel_flags { @/
@t\iI@> TTP_ALT_CONTENT, @/
@t\iI@> TTP_BLINKING_CURSOR, @/
@t\iI@> TTP_DIRTY, @/
@t\iI@> TTP_INTERM, @/
@t\iI@> TTP_INUSE, @/
@t\iI@> TTP_INVERSE_PANEL, @/
@t\iI@> TTP_VISIBLE_CURSOR, @/
@t\iI@> TTP_BOLD_USES_FONT, @/
@t\iI@> TTP_DISPLAY_COLOUR, @/
@t\iI@> TTP_COUNT
};

@ @<Pub...@>=
#define declare_panel_flag_accessors(N) @;    \
TTAPI void tt_panel_set_ ##N (tt_panel *);    \
TTAPI void tt_panel_clear_ ##N (tt_panel *);  \
TTAPI bool tt_panel_get_ ##N (tt_panel *);
@#
#define declare_panel_readonly_flag(N) @;     \
TTAPI bool tt_panel_get_ ##N (tt_panel *);

@ @<Fun...@>=
#define define_panel_flag_accessors(N,V,F) @; \
void                                          \
tt_panel_set_ ##N (tt_panel *pp)              \
{                                             \
        SET_FLAG((pp->V), (F));               \
}                                             \
                                              \
void                                          \
tt_panel_clear_ ##N (tt_panel *pp)            \
{                                             \
        CLEAR_FLAG((pp->V), (F));             \
}                                             \
                                              \
bool                                          \
tt_panel_get_ ##N (tt_panel *pp)              \
{                                             \
        return !!IS_FLAG((pp->V), (F));       \
}

#define define_panel_readonly_flag(N,V,F) @;  \
bool                                          \
tt_panel_get_ ##N (tt_panel *pp)              \
{                                             \
        return !!IS_FLAG((pp->V), (F));       \
}

@ @<Pub...@>=
TTAPI int tt_panel_attach (tt_panel *, tt_panel **);
TTAPI void tt_panel_detach (tt_panel *);
TTAPI void tt_panel_destroy (tt_panel *);
TTAPI tt_panel *tt_panel_new (int16_t, int16_t);
TTAPI void tt_panel_set_resize_fun (tt_panel *,
        void (*)(int16_t, int16_t, void *), void *);
declare_panel_readonly_flag(dirty) @;
declare_panel_readonly_flag(alt) @;
TTAPI void tt_panel_clean(tt_panel *);

@ @d _tt_panel_clear_dirty(P) CLEAR_FLAG((P)->flags, TTP_DIRTY)
@d _tt_panel_set_dirty(P) SET_FLAG((P)->flags, TTP_DIRTY)
@c
static void _tt_panel_destroy (tt_panel *);
define_panel_readonly_flag(dirty, flags, TTP_DIRTY) @;
define_panel_readonly_flag(alt, flags, TTP_ALT_CONTENT) @;

@ @c
void
tt_panel_clean (tt_panel *pp)
{
        _tt_panel_clear_dirty(pp);
}

@ @c
tt_panel *
tt_panel_new (int16_t width,
              int16_t height)
{
        assert(TTP_COUNT <= 64);
        if (width < 8 || height < 1)
                return NULL;
        tt_panel *ret = calloc(1, sizeof (*ret));

        ret->blank.bg.is_indexed = true;
        ret->blank.bg.index = 0;
        ret->blank.fg.is_indexed = true;
        ret->blank.fg.index = 7;
        ret->pen = ret->blank;
        SET_FLAG(ret->flags, TTP_DISPLAY_COLOUR);
        SET_FLAG(ret->flags, TTP_BLINKING_CURSOR);
        SET_FLAG(ret->flags, TTP_VISIBLE_CURSOR);
        ret->cursor_colour.rgba = (tt_rgba) { 0, 255, 0, 0.8 * 255 };
        assert(!ret->cursor_colour.is_indexed);
        ret->cursor_period = 1;
        ret->cursor_row = ret->cursor_col = 1;

        _tt_panel_init_palette(ret);

        RB_INIT(&ret->index);
        ret->type_case = atlas_alloc(4096, 4096, 4, 1, 0, false);
        if (!ret->type_case)
                goto fail;

        if (!Builtin_Font.cell_width || !Builtin_Font.cell_height) {
                assert(!Builtin_Font.cell_width && !Builtin_Font.cell_height);
                Builtin_Font.face = hrf_builtin_font();
                Builtin_Font.strike = hrf_alloc_stroker(Builtin_Font.face, 0);
                assert(Builtin_Font.strike);
                if (_tt_panel_font_init(ret, &Builtin_Font) == -1) {
                        warnx("cannot initialise builtin font");
                        memset(&Builtin_Font, 0, sizeof (Builtin_Font));
                        goto fail;
                }
        }

        ret->htab_period = 8;
        _tt_panel_resize(ret, width, height, false);
        if (!ret->content[0])
                goto fail;

        SET_FLAG(ret->flags, TTP_INUSE);
        return ret;

fail:
        if (ret && ret->type_case)
                atlas_destroy(ret->type_case);
        free(ret);
        return NULL;
}

@ @c
int
tt_panel_attach (tt_panel  *pp,
                 tt_panel **dst)
{
        if (IS_FLAG(pp->flags, TTP_INTERM)) {
                warnx("tt_panel_attach: attached");
                return -1;
        }
        SET_FLAG(pp->flags, TTP_INTERM);
        *dst = pp;
        return 0;
}

@ @c
void
tt_panel_detach (tt_panel *pp)
{
        assert(IS_FLAG(pp->flags, TTP_INTERM));
        CLEAR_FLAG(pp->flags, TTP_INTERM);
        _tt_panel_destroy(pp);
}

@ @c
void
tt_panel_destroy (tt_panel *pp)
{
        assert(IS_FLAG(pp->flags, TTP_INUSE)); // permit?
        CLEAR_FLAG(pp->flags, TTP_INUSE);
        _tt_panel_destroy(pp);
}

@ @c
static void
_tt_panel_destroy (tt_panel *pp)
{
        if (IS_FLAG(pp->flags, TTP_INTERM) || IS_FLAG(pp->flags, TTP_INUSE))
                return;
        free(pp->htab[0]);
        free(pp->content[0]);
        atlas_destroy(pp->type_case);
        free(pp);
}

@ @c
void
tt_panel_set_resize_fun (tt_panel  *pp,
                         void     (*fun)(int16_t, int16_t, void *),
                         void      *arg)
{
        pp->resize_fun = fun;
        pp->resize_arg = arg;
}












@* Cells.

@d _panel_refp(P,W,X,Y) ((P) + ((Y) * (W)) + (X))
@d _panel_ref(P,A,X,Y) ((P)->content[A] + ((Y) * (P)->width) + (X))
@d CALT(P) IS_FLAG((P)->flags, TTP_ALT_CONTENT)
@<Pub...@>=
TTAPI void tt_panel_cell_get_colour (tt_panel *, int16_t, int16_t,
        int32_t *, tt_rgba *, tt_rgba *, bool);
TTAPI tt_glyph *tt_panel_cell_get_glyph (tt_panel *, int16_t, int16_t);
TTAPI void tt_panel_garble (tt_panel *);
TTAPI void tt_panel_put_at (tt_panel *, u_cp, int16_t, int16_t);
TTAPI void tt_panel_put_here (tt_panel *, u_cp);
TTAPI void tt_panel_test_pattern (tt_panel *, bool);

@ @<Fun...@>=
static void _tt_panel_apply_colour (tt_panel *, tt_irgba *, tt_irgba *,
        uint32_t *);
static tt_glyph *_tt_panel_locate_glyph (tt_panel *, tt_font *, u_cp);
static tt_glyph *_tt_panel_locate_or_draw_glyph (tt_panel *, tt_font *, u_cp);
static void _tt_panel_put_char (tt_panel *, int16_t, int16_t, u_cp);

@ @c
void
tt_panel_cell_get_colour (tt_panel *pp,
                          int16_t   col,
                          int16_t   row,
                          int32_t  *flags,
                          tt_rgba  *retbg,
                          tt_rgba  *retfg,
                          bool      style)
{
        tt_cell *c = _panel_ref(pp, CALT(pp), col - 1, row - 1);
        *flags = c->flags;
        if (IS_FLAG(pp->flags, TTP_DISPLAY_COLOUR)) {
                *retbg = c->bg.rgba;
                *retfg = c->fg.rgba;
        } else {
                *retbg = pp->blank.bg.rgba;
                *retfg = pp->blank.fg.rgba;
        }
        if (style)
                _tt_panel_apply_colour(pp, (tt_irgba *) retbg,
                        (tt_irgba *) retfg, flags);
        *flags &= TTP_RENDITION_MASK;
}

@ @c
static void
_tt_panel_apply_colour (tt_panel *pp,
                        tt_irgba *bg,
                        tt_irgba *fg,
                        uint32_t *flags)
{
        if (bg->is_indexed)
                assert(bg->index < 256);
        if (bg->is_indexed)
                bg->rgba = pp->palette[bg->index];
        else if (bg->a > 0x7f)
                bg->a |= 1;
        if (fg->is_indexed)
                assert(fg->index < 256);
        if (fg->is_indexed && fg->index < 8
                        && IS_FLAG(*flags, TTP_BOLD)
                        && !IS_FLAG(pp->flags, TTP_BOLD_USES_FONT))
                fg->rgba = pp->palette[fg->index + 8];
        else if (fg->is_indexed)
                fg->rgba = pp->palette[fg->index];
        else if (fg->a > 0x7f)
                fg->a |= 1;

        tt_irgba t;
        if (tt_panel_get_inverse(pp) ^ IS_FLAG(*flags, TTP_INVERSE))
                t = *fg, *fg = *bg, *bg = t;

        if (IS_FLAG(*flags, TTP_HIDDEN))
                *fg = *bg;
}

@ @c
tt_glyph *
tt_panel_cell_get_glyph (tt_panel *pp,
                         int16_t   col,
                         int16_t   row)
{
        tt_cell *c = _panel_ref(pp, CALT(pp), col - 1, row - 1);
        return c->drawn;
}

@ @c
void
tt_panel_put_at (tt_panel *pp,
                 u_cp      cp,
                 int16_t   col,
                 int16_t   row)
{
        if (row < 1 || col < 1 || row > pp->height || col > pp->width)
                return;
        _tt_panel_put_char(pp, col, row, cp);
        assert(tt_panel_get_dirty(pp));
}

@ @c
void
tt_panel_put_here (tt_panel *pp,
                   u_cp      cp)
{
        _tt_panel_put_char(pp, pp->cursor_col, pp->cursor_row, cp);
        assert(tt_panel_get_dirty(pp));
}

@ @c
static void
_tt_panel_put_char (tt_panel *pp,
                    int16_t   col,
                    int16_t   row,
                    u_cp      cp)
{
        tt_glyph *g;

        assert(col >= 1 && col <= pp->width);
        assert(row >= 1 && row <= pp->height);
        tt_cell c = pp->pen;
        c.cp = cp;
        tt_font *font = _tt_panel_get_alt_font(pp, c.invoked, c.alt, cp);
        c.drawn = _tt_panel_locate_or_draw_glyph(pp, font, cp);
        assert(c.drawn);
#if 0
        if (0 && c.drawn->advance_rl > pp->cell_width + 1) {
                fcomplain("too wide! %f > %u", c.drawn->advance_rl, pp->cell_width);
        }
#endif
        *_panel_ref(pp, CALT(pp), col - 1, row - 1) = c;
        _tt_panel_set_dirty(pp);
}

@ @c
static tt_glyph *
_tt_panel_locate_or_draw_glyph (tt_panel *pp,
                                tt_font  *font,
                                u_cp      cp)
{
        tt_glyph *glyph = _tt_panel_search_index(pp, font, cp);
        if (glyph)
                return glyph;
        switch (_tt_panel_font_draw_glyph(pp, font, cp)) {
        case -2: /* not found */
                /*...*/
                return _tt_panel_search_index(pp, font, 0);
                break;
        case -1:
                goto fail;
        case 0:
                break;
        }
        assert(!glyph);
        glyph = calloc(1, sizeof (*glyph));
        glyph->font = font;
        glyph->cp = cp;
        if (_tt_panel_save_to_atlas(pp, glyph) == -1)
                goto fail;
        if (_tt_panel_save_to_index(pp, glyph) == -1)
                goto fail;
        return glyph;
fail:
        free(glyph);
        return _tt_panel_search_index(pp, font, 0);
}

@ @c
static tt_glyph *
_tt_panel_locate_glyph (tt_panel *pp,
                        tt_font  *font,
                        u_cp      cp)
{
        tt_glyph *glyph = _tt_panel_search_index(pp, font, cp);
        if (glyph)
                return glyph;
        return _tt_panel_search_index(pp, font, 0);
}

@ @c
void
tt_panel_garble (tt_panel *pp)
{
        for (int row = 1; row <= pp->height; row++)
                for (int col = 1; col <= pp->width; col++)
                        _tt_panel_put_char(pp, col, row,
                                arc4random_uniform('~' - ' ') + ' ');
}

@ @d CPINC(P,A) if ((P) == 0x7e && (A))
        (P) = 32;
else if ((P) == 0x7e)
        (P) = 0xa1;
else if ((P) == 0xac)
        (P) = 0xae;
else if ((P) == 0x2ff)
        (P) = 33; /* not space */
else
        (P)++ /* no semicolon */
@c
void
tt_panel_test_pattern (tt_panel *pp,
                       bool      ascii)
{
        for (int row = 1; row <= pp->height; row++) {
                u_cp cp = 31;
                for (int i = 0; i < row; i++)
                        CPINC(cp, ascii);
                for (int col = 1; col <= pp->width; col++) {
                        _tt_panel_put_char(pp, col, row, cp);
                        CPINC(cp, ascii);
                }
        }
        assert(tt_panel_get_dirty(pp));
}







@* Panel Size \AM\ Margins.

@<Pub...@>=
#define TTP_MIN_WIDTH  1
#define TTP_MAX_WIDTH  INT16_MAX
#define TTP_MIN_HEIGHT 1
#define TTP_MAX_HEIGHT INT16_MAX
@#
TTAPI int16_t tt_panel_get_bottom (tt_panel *);
TTAPI int16_t tt_panel_get_height (tt_panel *);
TTAPI int16_t tt_panel_get_left (tt_panel *);
TTAPI int16_t tt_panel_get_right (tt_panel *);
TTAPI void tt_panel_get_size (tt_panel *, int16_t *, int16_t *, int16_t *,
        int16_t *, int16_t *, int16_t *);
TTAPI int16_t tt_panel_get_top (tt_panel *);
TTAPI int16_t tt_panel_get_width (tt_panel *);
TTAPI void tt_panel_set_margins (tt_panel *, int16_t, int16_t, int16_t, int16_t);
TTAPI int tt_panel_resize (tt_panel *, int16_t, int16_t, bool);

@ @<Fun...@>=
static void _tt_panel_resize (tt_panel *, int16_t, int16_t, bool);

@ @c
void
tt_panel_get_size (tt_panel *pp,
                   int16_t  *w,
                   int16_t  *h,
                   int16_t  *t,
                   int16_t  *r,
                   int16_t  *b,
                   int16_t  *l)
{
        *w = pp->width;
        *h = pp->height;
        if (t)
                *t = pp->top;
        if (r)
                *r = pp->right;
        if (b)
                *b = pp->bottom;
        if (l)
                *l = pp->left;
}

@ @c
int16_t
tt_panel_get_width (tt_panel *pp)
{
        return pp->width;
}

int16_t
tt_panel_get_height (tt_panel *pp)
{
        return pp->height;
}

int16_t
tt_panel_get_top (tt_panel *pp)
{
        return pp->top;
}

int16_t
tt_panel_get_right (tt_panel *pp)
{
        return pp->right;
}

int16_t
tt_panel_get_bottom (tt_panel *pp)
{
        return pp->bottom;
}

int16_t
tt_panel_get_left (tt_panel *pp)
{
        return pp->left;
}

@ @c
void
tt_panel_set_margins (tt_panel *pp,
                      int16_t   t,
                      int16_t   r,
                      int16_t   b,
                      int16_t   l)
{
        if (t < 0 || r < 0 || b < 0 || l < 0)
                return;
        if (t == 0)
                t = pp->top;
        if (r == 0)
                r = pp->right;
        if (b == 0)
                b = pp->bottom;
        if (l == 0)
                l = pp->left;
        if (l > pp->width + 1 || r > pp->width + 1 || l >= r)
                return;
        if (t > pp->height + 1 || b > pp->height + 1 || t >= b)
                return;
        pp->top = t;
        pp->right = r;
        pp->bottom = b;
        pp->left = l;
}

@ @c
int
tt_panel_resize (tt_panel *pp,
                 int16_t   width,
                 int16_t   height,
                 bool      copy)
{
        if (width == 0)
                width = pp->width;
        if (height == 0)
                height = pp->height;
        if (width < 1 || height < 1)
                return -1;
        _tt_panel_resize(pp, width, height, copy);
        if (pp->width != width || pp->height != height) {
                fcomplain("panel_resize");
                return -1;
        }
        assert(tt_panel_get_dirty(pp));
        if (pp->resize_fun)
                (pp->resize_fun)(width, height, pp->resize_arg);
        return 0;
}

@ Cannot use |_tt_panel_clear_region| as it needs pp's width.

@c
static void
_tt_panel_resize (tt_panel *pp,
                  int16_t   width,
                  int16_t   height,
                  bool      copy)
{
        tt_cell *new[2], *old[2];
        char *htnew[2], *htold[2];

        assert(width >= 1);
        assert(height >= 1);
        int dim = width * height;
        new[0] = calloc(2 * dim, sizeof (tt_cell));
        htnew[0] = calloc(2 * _tabstrip_bytes(width), 1);
        if (!new[0] || !htnew[0]) {
                free(new[0]);
                free(htnew[0]);
                return;
        }
        new[1] = new[0] + dim;
        htnew[1] = htnew[0] + _tabstrip_bytes(width);

        int srcw, srch;
        if (pp->width || pp->height)
                assert(pp->content[0] && pp->content[1]);
        if (!copy)
                srcw = 0;
        else if (width > pp->width)
                srcw = pp->width;
        else
                srcw = width;
        if (!copy)
                srch = 0;
        else if (height > pp->height)
                srch = pp->height;
        else
                srch = height;

        int x, y;
        tt_cell blank = pp->pen;
        for (y = 0; y < srch; y++) {
                for (x = 0; x < srcw; x++) {
                        *_panel_refp(new[0], width, x, y)
                                = *_panel_ref(pp, 0, x, y);
                        *_panel_refp(new[1], width, x, y)
                                = *_panel_ref(pp, 1, x, y);
                }
                for (; x < width; x++) {
                        *_panel_refp(new[0], width, x, y) = blank;
                        *_panel_refp(new[1], width, x, y) = blank;
                }
        }
        for (; y < height; y++) {
                for (x = 0; x < width; x++) {
                        *_panel_refp(new[0], width, x, y) = blank;
                        *_panel_refp(new[1], width, x, y) = blank;
                }
        }

        for (x = 0; x < srcw; x++) {
                if (_tt_panel_tabstrip_stop_at(pp->htab[0], x))
                        _tt_panel_tabstrip_set_at(htnew[0], x);
                if (_tt_panel_tabstrip_stop_at(pp->htab[1], x))
                        _tt_panel_tabstrip_set_at(htnew[1], x);
        }
        if (x % pp->htab_period)
                x += pp->htab_period - (x % pp->htab_period);
        for (; x < width; x += pp->htab_period) {
                _tt_panel_tabstrip_set_at(htnew[0], x);
                _tt_panel_tabstrip_set_at(htnew[1], x);
        }

        free(pp->content[0]);
        pp->content[0] = new[0];
        pp->content[1] = new[1];
        free(pp->htab[0]);
        pp->htab[0] = htnew[0];
        pp->htab[1] = htnew[1];
        pp->top = pp->left = 1;
        pp->right = pp->width = width;
        pp->bottom = pp->height = height;
        _tt_panel_set_dirty(pp);
}









@* Fonts.

@<Type def...@>=
typedef struct tt_font {
        SLIST_ENTRY(tt_font) entry;
        FcPattern   *pattern;
        hrf_font    *face;
        hrf_stroker *strike;
        int16_t      cell_width, cell_height, cell_inset;
        int16_t      ref;
} tt_font;

@ @<Pub...@>=
TTAPI tt_font *tt_panel_font_new_from_builtin (tt_panel *);
TTAPI tt_font *tt_panel_font_new_from_xft_name (tt_panel *, Display *,
        int, char *, double);
TTAPI tt_font *tt_panel_font_new_from_xft_pattern (tt_panel *, Display *,
        int, FcPattern *, double);
TTAPI void tt_panel_font_destroy (tt_font *);
TTAPI void tt_panel_invoke_font (tt_panel *, int, tt_font *, bool);
TTAPI tt_font *tt_panel_get_font (tt_panel *, int, int);
TTAPI int16_t tt_panel_font_get_cell_width (tt_font *);
TTAPI int16_t tt_panel_font_get_cell_height (tt_font *);
TTAPI int16_t tt_panel_font_get_cell_inset (tt_font *);

@ @<Fun...@>=
static int _tt_panel_font_draw_glyph (tt_panel *, tt_font *, u_cp);
static tt_font *_tt_panel_get_alt_font (tt_panel *, int, int, u_cp);
static int _tt_panel_font_init (tt_panel *, tt_font *);

@ @c
int16_t
tt_panel_font_get_cell_width (tt_font *f)
{
        return f->cell_width;
}

@ @c
int16_t
tt_panel_font_get_cell_height (tt_font *f)
{
        return f->cell_height;
}

@ @c
int16_t
tt_panel_font_get_cell_inset (tt_font *f)
{
        return f->cell_inset;
}

@ @<Global...@>=
tt_font Builtin_Font;

@ @c
tt_font *
tt_panel_font_new_from_builtin (tt_panel *pp)
{
        Builtin_Font.ref++;
        return &Builtin_Font;
}

@ @c
tt_font *
tt_panel_font_new_from_xft_name (tt_panel *pp,
                                 Display  *xdisplay,
                                 int       xscreen,
                                 char     *name,
                                 double    size)
{
        FcPattern *pattern = hrf_prepare_xft_pattern(name, size);
        if (!pattern)
                return NULL;
        tt_font *ret = tt_panel_font_new_from_xft_pattern(pp, xdisplay,
                xscreen, pattern, size);
        if (!ret)
                FcPatternDestroy(pattern);
        return ret;
}

@ @c
tt_font *
tt_panel_font_new_from_xft_pattern (tt_panel  *pp,
                                    Display   *xdisplay,
                                    int        xscreen,
                                    FcPattern *pattern,
                                    double     size)
{
        tt_font *ret = calloc(1, sizeof (tt_font));
        if (!ret)
                return NULL;
        ret->face = hrf_load_xft_pattern(xdisplay, xscreen, NULL, 0,
                pattern, true);
        if (!ret->face)
                goto fail;

        ret->strike = hrf_alloc_stroker(ret->face, size);
        if (!ret->strike)
                goto fail;
        ret->pattern = pattern;
        if (_tt_panel_font_init(pp, ret) == -1)
                goto fail;
#if 0 /* need accessors in hrf */
        finform("found font %s %s %.0fpt (wanted %.0fpt)",
                ret->face->face->family_name,
                ret->face->face->style_name,
                ret->strike->size_pt,
                size);
#endif
        return ret;

fail:
        fcomplain("unable to create new font %p @@ %.0fpt", pattern, size);
        tt_panel_font_destroy(ret);
        return NULL;
}

@ ``the glyph index returned by [|FT_Get_Char_Index|] doesn't always
correspond to the internal indices used within the file. This is
done to ensure that value 0 always corresponds to the `missing
glyph'.''

@c
static int
_tt_panel_font_init (tt_panel *pp,
                     tt_font  *font)
{
        int width = 0, height = 0, descent = 0;
        int offset, nheight, ndescent, noffset;
        for (int i = 0; i <= 0x7f; i++) {
                int found = _tt_panel_font_draw_glyph(pp, font, i);
                if (found == -1 || (found == -2 && !i))
                        goto fail;
                else if (found == -2)
                        continue;
                tt_glyph *g = calloc(1, sizeof (*g));
                g->font = font;
                g->cp = i;
                if (_tt_panel_save_to_atlas(pp, g) == -1)
                        goto fail;
                if (_tt_panel_save_to_index(pp, g) == -1)
                        goto fail;

                if (g->width > width)
                        width = g->width;
                if (g->offset_y < 0) { /* Descends only. */
                        nheight = ndescent = -g->offset_y
                                + g->height;
                        noffset = 0;
                } else if (g->height < g->offset_y) {
                                                /* Ascends only. */
                        nheight = noffset = g->offset_y;
                        ndescent = 0;
                } else {
                        nheight = noffset = g->offset_y;
                        ndescent = g->height - noffset;
                }
                if (ndescent > descent)
                        descent = ndescent;
                if (noffset > offset)
                        offset = noffset;
                if (nheight > height)
                        height = nheight;
        }

        font->cell_width = sp6tof(hrf_size_get_max_advance(font->strike));
        if ((width - 2) * 2 < font->cell_width) {
                fbug("assuming font includes double-width glyphs");
                font->cell_width /= 2; /* need a better heuristic (or data) */
        }
        if (width > font->cell_width)
                fcomplain("glyphs are too wide: %u > %u", width, font->cell_width);

        font->cell_height = sp6tof(hrf_size_get_height(font->strike));
        if (height + descent > font->cell_height)
                fcomplain("glyphs are too tall: %u > %u", height + descent,
                        font->cell_height);

        font->cell_inset = font->cell_height - (height + descent);
        font->cell_inset /= 2;
        font->cell_inset += descent;

        font->ref++;
        return 0;
fail:
        abort();
}

@ @c
void
tt_panel_font_destroy (tt_font *font)
{
        if (--font->ref)
                return;
        if (font == &Builtin_Font)
                return;
        hrf_destroy_stroker(font->strike);
        hrf_destroy_font(font->face);
        if (font->pattern)
                FcPatternDestroy(font->pattern);
        free(font);
}

@ @c
static int
_tt_panel_font_draw_glyph (tt_panel *pp,
                           tt_font  *font,
                           u_cp      cp)
{
        int rval = hrf_start_glyph(font->face, font->strike, cp, false);
        if (rval == -1) {
                fcomplain("failed to start glyph U+%04X", cp);
                return -1;
        } else if (rval == -2)
                return -2;
        return hrf_stroke(font->face, font->strike);
}

@ @c
void
tt_panel_invoke_font (tt_panel *pp,
                      int       fid,
                      tt_font  *font,
                      bool      keep)
{
        if (fid < 0 || fid > 11)
                abort();
        for (int i = 3; i >= 0; i--)
                if (pp->invoked_font[fid][i])
                        tt_panel_font_destroy(pp->invoked_font[fid][i]);
                else
                        pp->invoked_font[fid][i] = NULL;
        pp->invoked_font[fid][0] = font;
        if (keep)
                font->ref++;
        finform("invoke font %u", fid);
}

@ @c
tt_font *
tt_panel_get_font (tt_panel *pp,
                   int       fid,
                   int       alt)
{
        if (fid < 0 || fid > 11)
                abort();
        if (alt < 0 || alt > 3)
                abort();
        return _tt_panel_get_alt_font(pp, fid, alt, 0);
}

@ @c
static tt_font *
_tt_panel_get_alt_font (tt_panel *pp,
                        int       invoked,
                        int       alt,
                        u_cp      cp)
{
        assert(invoked >= 0 && invoked <= 11);
        assert(alt >= 0 && alt <= 3);
        if (!pp->invoked_font[invoked][0])
                invoked = 0;
        if (!pp->invoked_font[invoked][alt]) {
                // locate \& load
                alt = 0;
        }
        tt_font *ret = pp->invoked_font[invoked][alt];
        return ret ? ret : &Builtin_Font;
}









@* Font Atlas.

@<Pub...@>=
TTAPI bool tt_panel_type_case_get_dirty (tt_panel *);
TTAPI int32_t tt_panel_type_case_get_size (tt_panel *);
TTAPI void *tt_panel_type_case_get_data (tt_panel *);
TTAPI void tt_panel_type_case_clear_dirty (tt_panel *);

@ @<Fun...@>=
static void _tt_panel_hrf_to_atlas (int, void *, int, void *);
static int _tt_panel_save_to_atlas (tt_panel *, tt_glyph *);

@ @c
bool
tt_panel_type_case_get_dirty (tt_panel *pp)
{
        return atlas_get_dirty(pp->type_case);
}

@ @c
void
tt_panel_type_case_clear_dirty (tt_panel *pp)
{
        return atlas_clear_dirty(pp->type_case);
}

@ @c
static int
_tt_panel_save_to_atlas (tt_panel *pp,
                         tt_glyph *dst)
{
        tt_font *font = dst->font;
        assert(font);
        hrf_stroker *strike = font->strike;

        tt_glyph acp = *dst;
        acp.width = hrf_stroker_get_width(strike);
        acp.height = hrf_stroker_get_height(strike);
        bool copy = acp.width > 0;
        if (!copy) {
                assert(acp.height == 0);
                acp.width = acp.height = 1;
        }
        if (atlas_find_region(pp->type_case, acp.width, acp.height,
                        acp.region) == -1) /* no space */
                return -1;

        acp.offset_x = hrf_stroker_get_offset_x(strike);
        acp.offset_y = hrf_stroker_get_offset_y(strike);
        acp.advance_rl = hrf_stroker_get_advance_rl(strike);
        acp.advance_du = hrf_stroker_get_advance_du(strike);

        if (copy) {
                struct { tp_atlas *a; int32_t r[4]; } ctx;
                ctx.a = pp->type_case;
                assert(sizeof (ctx.r) == sizeof (int32_t) * 4);
                memmove(ctx.r, acp.region, sizeof (ctx.r));
                hrf_copy_glyph_out(font->face, font->strike,
                        _tt_panel_hrf_to_atlas, &ctx);
        } else { /* fill it in with the border colour */
                uint32_t bg = *((uint32_t *) atlas_ref_border(pp->type_case));
                atlas_set_region(pp->type_case, acp.region,
                        (uint8_t *) &bg, 0, 0, 1); // TODO: doesn't do what it says on the tin?
        }

        *dst = acp;
        return 0;
}

@ @c
static void
_tt_panel_hrf_to_atlas (int   y,
                        void *line,
                        int   width,
                        void *ctxp)
{
        struct { tp_atlas *a; int32_t r[4]; } *ctx = ctxp;
        assert(atlas_get_depth(ctx->a) == 4);
        assert(ctx->r[2] == width);
        atlas_set_region(ctx->a, ctx->r, line, 0, y, y + 1);
}

@ @c
int32_t
tt_panel_type_case_get_size (tt_panel *pp)
{
        assert(atlas_get_width(pp->type_case)
                == atlas_get_height(pp->type_case));
        return atlas_get_width(pp->type_case);
}

@ @c
void *
tt_panel_type_case_get_data (tt_panel *pp)
{
        return atlas_ref(pp->type_case, 0, 0);
}








@* Glyph Index.

Why a red-black tree and not a hashtable? \.{sys/tree.h} is in
libbsd but \.{ohash.h} is not.

@<Pub...@>=
TTAPI u_cp tt_panel_glyph_get_cp (tt_glyph *);
TTAPI tt_font *tt_panel_glyph_get_font (tt_glyph *);
TTAPI void tt_panel_glyph_get_region (tt_glyph *, int32_t[4]);
TTAPI int16_t tt_panel_glyph_get_offset_x (tt_glyph *);
TTAPI int16_t tt_panel_glyph_get_offset_y (tt_glyph *);

@ @<Type def...@>=
typedef struct tt_glyph {
        RB_ENTRY(tt_glyph) index;
        tt_font  *font;
        u_cp      cp;
        int16_t   width, height;
        int16_t   offset_x, offset_y;
        float     advance_rl, advance_du;
        int32_t   region[4]; /* atlas */
} tt_glyph;

@ @<Fun...@>=
RB_PROTOTYPE(tt_glyph_index, tt_glyph, index, _tt_glyph_id_cmp) @;
static int _tt_glyph_id_cmp (tt_glyph *, tt_glyph *);
static int _tt_panel_save_to_index (tt_panel *, tt_glyph *);
static tt_glyph *_tt_panel_search_index (tt_panel *, tt_font *, u_cp);

@ @c
RB_GENERATE(tt_glyph_index, tt_glyph, index, _tt_glyph_id_cmp) @;

@ @c
u_cp
tt_panel_glyph_get_cp (tt_glyph *g)
{
        return g->cp;
}

@ @c
tt_font *
tt_panel_glyph_get_font (tt_glyph *g)
{
        return g->font;
}

@ @c
void
tt_panel_glyph_get_region (tt_glyph *g,
                           int32_t   r[4])
{
        int32_t Blank_Cell[4] = { 0, 0, 0, 0 };
        memmove(r, g ? g->region : Blank_Cell, sizeof (int32_t) * 4);
}

@ @c
int16_t
tt_panel_glyph_get_offset_x (tt_glyph *g)
{
        return g->offset_x;
}

@ @c
int16_t
tt_panel_glyph_get_offset_y (tt_glyph *g)
{
        return g->offset_y;
}

@ @c
static int
_tt_glyph_id_cmp (tt_glyph *n1,
                  tt_glyph *n2)
{
        if (n1->font == n2->font)
                return n1->cp < n2->cp ? -1 : n1->cp > n2->cp;
        return n1->font < n2->font ? -1 : 1;
}

@ @c
static int
_tt_panel_save_to_index (tt_panel *pp,
                         tt_glyph *glyph)
{
        assert(glyph->font);
        if (!RB_INSERT(tt_glyph_index, &pp->index, glyph))
                return 0;
        return -1;
}

@ @c
static tt_glyph *
_tt_panel_search_index (tt_panel *pp,
                        tt_font  *font,
                        u_cp      cp)
{
        tt_glyph fake = {0};
        fake.font = font;
        fake.cp = cp;
        return RB_FIND(tt_glyph_index, &pp->index, &fake);
}






@* Colour.

Colour is stored in the |tt_rgba| object as 3 8-bit RGB values and a 7-bit alpha value.

A cell can store an RGB triplet or an [[ index thingie ]]. If the
8th bit in the colour's alpha channel is set then the |tt_rgba|
object instead contains a value to look up in the current ANSI
colour index. Is it ECMA if it's colour and not color? Couleur?

@<Fun...@>=
static void _tt_panel_init_palette (tt_panel *);

@ @<Global...@>=
#include "rgbcube.h"
tt_rgba Hexacol_VGA[16] = { /* VGA {\it colours\/} not VGA {\it order} */
@t\iII@>{ 0x00, 0x00, 0x00, 0xff }, @|
        { 0xaa, 0x00, 0x00, 0xff }, @|
        { 0x00, 0xaa, 0x00, 0xff }, @|
        { 0xaa, 0x55, 0x00, 0xff }, /* brown */
@t\iII@>{ 0x00, 0x00, 0xaa, 0xff }, @|
        { 0xaa, 0x00, 0xaa, 0xff }, @|
        { 0x00, 0xaa, 0xaa, 0xff }, @|
        { 0xaa, 0xaa, 0xaa, 0xff }, @|
        { 0x55, 0x55, 0x55, 0xff }, @|
        { 0xff, 0x55, 0x55, 0xff }, @|
        { 0x55, 0xff, 0x55, 0xff }, @|
        { 0xff, 0xff, 0x55, 0xff }, @|
        { 0x55, 0x55, 0xff, 0xff }, @|
        { 0xff, 0x55, 0xff, 0xff }, @|
        { 0x55, 0xff, 0xff, 0xff }, @|
        { 0xff, 0xff, 0xff, 0xff }, @/
};

tt_rgba Hexacol_Xterm[16] = { @|
        { 0x00, 0x00, 0x00, 0xff }, @|
        { 0xcd, 0x00, 0x00, 0xff }, @|
        { 0x00, 0xcd, 0x00, 0xff }, @|
        { 0xcd, 0xcd, 0x00, 0xff }, @|
        { 0x00, 0x00, 0xee, 0xff }, @|
        { 0xcd, 0x00, 0xcd, 0xff }, @|
        { 0x00, 0xcd, 0xcd, 0xff }, @|
        { 0xe5, 0xe5, 0xe5, 0xff }, @|
        { 0x7f, 0x7f, 0x7f, 0xff }, @|
        { 0xff, 0x00, 0x00, 0xff }, @|
        { 0x00, 0xff, 0x00, 0xff }, @|
        { 0xff, 0xff, 0x00, 0xff }, @|
        { 0x5c, 0x5c, 0xff, 0xff }, /* note discussion in \.{XTerm-col.ad} */
@t\iII@>{ 0xff, 0x00, 0xff, 0xff }, @|
        { 0x00, 0xff, 0xff, 0xff }, @|
        { 0xff, 0xff, 0xff, 0xff }, @/
};


@ @c
static void
_tt_panel_init_palette (tt_panel *pp)
{
        assert(sizeof (Hexacol_VGA) == sizeof (tt_rgba) * 16);
        assert(sizeof (RGBCube) == sizeof (tt_rgba) * (256 - 16));
        memmove(pp->palette, Hexacol_VGA, sizeof (Hexacol_VGA));
        memmove(&pp->palette[16], RGBCube, sizeof (RGBCube));
}













@* Appearance \AM\ Rendition.

@ @<Pub...@>=
TTAPI void tt_panel_mod_colour_index (tt_panel *, bool, int);
TTAPI void tt_panel_mod_colour_rgba (tt_panel *, bool, uint8_t, uint8_t,
        uint8_t, uint8_t);
TTAPI void tt_panel_reset_colour (tt_panel *, bool);
TTAPI void tt_panel_reset_rendition (tt_panel *);
TTAPI void tt_panel_restore_rendition (tt_panel *, tt_cell *);
TTAPI void tt_panel_save_rendition (tt_panel *, uint32_t, tt_cell *);
TTAPI void tt_panel_set_font (tt_panel *, int);
TTAPI declare_panel_flag_accessors(inverse) @;
TTAPI declare_panel_flag_accessors(pen_blink_fast) @;
TTAPI declare_panel_flag_accessors(pen_blink_slow) @;
TTAPI declare_panel_flag_accessors(pen_bold) @;
TTAPI declare_panel_flag_accessors(pen_faint) @;
TTAPI declare_panel_flag_accessors(pen_hidden) @;
TTAPI declare_panel_flag_accessors(pen_inverse) @;
TTAPI declare_panel_flag_accessors(pen_italic) @;
TTAPI declare_panel_flag_accessors(pen_overline) @;
TTAPI declare_panel_flag_accessors(pen_strikeout) @;
TTAPI declare_panel_flag_accessors(pen_underline_1) @;
TTAPI declare_panel_flag_accessors(pen_underline_2) @;

@ @c
define_panel_flag_accessors(inverse, flags, TTP_INVERSE_PANEL) @;
define_panel_flag_accessors(pen_hidden, pen.flags, TTP_HIDDEN) @;
define_panel_flag_accessors(pen_inverse, pen.flags, TTP_INVERSE) @;
define_panel_flag_accessors(pen_italic, pen.flags, TTP_ITALIC) @;
define_panel_flag_accessors(pen_overline, pen.flags, TTP_OVERLINE) @;
define_panel_flag_accessors(pen_strikeout, pen.flags, TTP_STRIKEOUT) @;

@ @c
void
tt_panel_save_rendition (tt_panel *pp,
                         uint32_t  mask,
                         tt_cell  *save)
{
        *save = pp->pen;
        save->mask = mask;
        save->flags &= mask;
}

@ @c
void
tt_panel_reset_rendition (tt_panel *pp)
{
        pp->pen = pp->blank;
}

@ @c
void
tt_panel_restore_rendition (tt_panel *pp,
                            tt_cell  *load)
{
        uint32_t f = (pp->pen.flags & ~load->mask) | (load->flags & load->mask);
        pp->pen = *load;
        pp->pen.mask = 0;
        pp->pen.flags = f;
}

@ Mutually-exclusive flags: Blink speed.

@c
define_panel_readonly_flag(pen_blink_fast, pen.flags, TTP_BLINK_FAST) @;
define_panel_readonly_flag(pen_blink_slow, pen.flags, TTP_BLINK_SLOW) @;

void
tt_panel_clear_pen_blink_fast (tt_panel *pp)
{
        CLEAR_FLAG(pp->pen.flags, TTP_BLINK_FAST);
}

void
tt_panel_clear_pen_blink_slow (tt_panel *pp)
{
        CLEAR_FLAG(pp->pen.flags, TTP_BLINK_SLOW);
}

void
tt_panel_set_pen_blink_fast (tt_panel *pp)
{
        tt_panel_clear_pen_blink_slow(pp);
        SET_FLAG(pp->pen.flags, TTP_BLINK_FAST);
}

void
tt_panel_set_pen_blink_slow (tt_panel *pp)
{
        tt_panel_clear_pen_blink_fast(pp);
        SET_FLAG(pp->pen.flags, TTP_BLINK_SLOW);
}

@ Bold \AM\ faint.

@c
define_panel_readonly_flag(pen_bold, pen.flags, TTP_BOLD) @;
define_panel_readonly_flag(pen_faint, pen.flags, TTP_FAINT) @;

void
tt_panel_clear_pen_bold (tt_panel *pp)
{
        CLEAR_FLAG(pp->pen.flags, TTP_BOLD);
}

void
tt_panel_clear_pen_faint (tt_panel *pp)
{
        CLEAR_FLAG(pp->pen.flags, TTP_FAINT);
}

void
tt_panel_set_pen_bold (tt_panel *pp)
{
        tt_panel_clear_pen_faint(pp);
        SET_FLAG(pp->pen.flags, TTP_BOLD);
}

void
tt_panel_set_pen_faint (tt_panel *pp)
{
        tt_panel_clear_pen_bold(pp);
        SET_FLAG(pp->pen.flags, TTP_FAINT);
}

@ Underlines.

@c
define_panel_readonly_flag(pen_underline_1, pen.flags, TTP_UNDERLINE_1) @;
define_panel_readonly_flag(pen_underline_2, pen.flags, TTP_UNDERLINE_2) @;

void
tt_panel_clear_pen_underline_1 (tt_panel *pp)
{
        CLEAR_FLAG(pp->pen.flags, TTP_UNDERLINE_1);
}

void
tt_panel_clear_pen_underline_2 (tt_panel *pp)
{
        CLEAR_FLAG(pp->pen.flags, TTP_UNDERLINE_2);
}

void
tt_panel_set_pen_underline_1 (tt_panel *pp)
{
        tt_panel_clear_pen_underline_2(pp);
        SET_FLAG(pp->pen.flags, TTP_UNDERLINE_1);
}

void
tt_panel_set_pen_underline_2 (tt_panel *pp)
{
        tt_panel_clear_pen_underline_1(pp);
        SET_FLAG(pp->pen.flags, TTP_UNDERLINE_2);
}

@ @c
void
tt_panel_mod_colour_index (tt_panel *pp,
                           bool      isbg,
                           int       index)
{
        if (index < 0 || index > 255)
                return;
        tt_irgba c = {0};
        c.is_indexed = true;
        c.index = index;
        if (isbg)
                pp->pen.bg = c;
        else
                pp->pen.fg = c;
}

@ @c
void
tt_panel_mod_colour_rgba (tt_panel *pp,
                          bool      isbg,
                          uint8_t   red,
                          uint8_t   green,
                          uint8_t   blue,
                          uint8_t   alpha)
{
        tt_irgba *c;
        if (isbg)
                c = &pp->pen.bg;
        else
                c = &pp->pen.fg;
        c->r = red;
        c->g = green;
        c->b = blue;
        c->a = alpha;
        c->is_indexed = false;
        assert(c->a == (alpha & 0xfe));
}

@ @c
void
tt_panel_reset_colour (tt_panel *pp,
                       bool      isbg)
{
        if (isbg)
                pp->pen.bg = pp->blank.bg;
        else
                pp->pen.fg = pp->blank.fg;
}

@ @c
void
tt_panel_set_font (tt_panel *pp,
                   int       fid)
{
        if (fid < 0 || fid > 11)
                return;
        abort();
}







@* Cursor.

Consists of two distinct things. The current location and the style
(rendition) that will be applied.

@<Pub...@>=
TTAPI tt_rgba tt_panel_cursor_get_colour (tt_panel *);
TTAPI void tt_panel_cursor_get_pos (tt_panel *, int16_t *, int16_t *);
TTAPI int16_t tt_panel_cursor_get_row (tt_panel *);
TTAPI int16_t tt_panel_cursor_get_col (tt_panel *);
TTAPI void tt_panel_cursor_set_pos (tt_panel *, int16_t, int16_t);
TTAPI declare_panel_flag_accessors(cursor_visible) @;
TTAPI declare_panel_flag_accessors(cursor_blinking) @;
TTAPI double tt_panel_get_cursor_period (tt_panel *);

@ @c
int16_t
tt_panel_cursor_get_row (tt_panel *pp)
{
        return pp->cursor_row;
}

int16_t
tt_panel_cursor_get_col (tt_panel *pp)
{
        return pp->cursor_col;
}

void
tt_panel_cursor_get_pos (tt_panel *pp,
                         int16_t  *col,
                         int16_t  *row)
{
        *col = pp->cursor_col;
        *row = pp->cursor_row;
}

@ @c
void
tt_panel_cursor_set_pos (tt_panel *pp,
                         int16_t   col,
                         int16_t   row)
{
        if (col < 0)
                return;
        else if (col == 0)
                col = pp->cursor_col;
        if (row < 0)
                return;
        else if (row == 0)
                row = pp->cursor_row;
        col--;
        row--;
        col %= pp->width;
        row %= pp->height;
        pp->cursor_col = col + 1;
        pp->cursor_row = row + 1;
}

@ @c
tt_rgba
tt_panel_cursor_get_colour (tt_panel *pp)
{
        tt_irgba _ = {0}, ret = pp->cursor_colour; /* ignore |_| */
        _tt_panel_apply_colour(pp, &_, &ret, (int32_t *) &_);
        return ret.rgba;
}

@ @c
define_panel_flag_accessors(cursor_blinking, flags, TTP_BLINKING_CURSOR) @;
define_panel_flag_accessors(cursor_visible, flags, TTP_VISIBLE_CURSOR) @;

@ @c
double
tt_panel_get_cursor_period (tt_panel *pp)
{
        return pp->cursor_period;
}











@* Tabulation.

Originally a tabulation bar was a physical strip inserted into a
typewriter which consisted marks at the positions which the print
head would advance to when the typist pressed the TAB key.

In most cases every eighth position is a tab stop.

Some uses set tab stops at any position.

Tab stops are stored as a linked list of positions. If the position
is negative then it represents the period at which further tab stops
are located.

@d _tabstrip_bytes(W) (((W) / 8) + !!((W) % 8))
@<Pub...@>=
TTAPI void tt_panel_reset_tabs (tt_panel *, int16_t);
TTAPI int16_t tt_panel_find_tabstop (tt_panel *, int16_t);
TTAPI void tt_panel_insert_tabstop (tt_panel *, int16_t);
TTAPI void tt_panel_remove_tabstop (tt_panel *, int16_t);

@ @<Fun...@>=
static void _tt_panel_clear_tabs (tt_panel *, int16_t);
static void _tt_panel_tabstrip_clear_at (uint8_t *, int16_t);
static void _tt_panel_tabstrip_set_at (uint8_t *, int16_t);

@ @d _tt_panel_tabstrip_stop_at(S,X) ((S) && ((S)[(X) / 8] & (1 << ((X) % 8))))
@c
static void
_tt_panel_tabstrip_clear_at (uint8_t *strip,
                             int16_t  x)
{
        assert(strip);
        uint8_t bit = 1 << (x % 8);
        strip[x / 8] = strip[x / 8] & ~bit;
}

@ @c
static void
_tt_panel_tabstrip_set_at (uint8_t *strip,
                           int16_t  x)
{
        assert(strip);
        uint8_t bit = 1 << (x % 8);
        strip[x / 8] = strip[x / 8] | bit;
}

@ @c
void
tt_panel_reset_tabs (tt_panel *pp,
                     int16_t   period)
{
        if (period < 1 || period > TTP_MAX_WIDTH)
                return;
        _tt_panel_clear_tabs(pp, period);
}

@ @c
static void
_tt_panel_clear_tabs (tt_panel *pp,
                      int16_t   period)
{
        assert(period >= 0);
        memset(pp->htab[CALT(pp)], 0, _tabstrip_bytes(pp->width));
        if (!period)
                return;
        pp->htab_period = period;
        for (int col = period; col <= pp->width; col += period)
                _tt_panel_tabstrip_set_at(pp->htab[CALT(pp)], col - 1);
}

@ @c
int16_t
tt_panel_find_tabstop (tt_panel *pp,
                       int16_t   from)
{
        if (from >= pp->width)
                return pp->width;
        else if (!from)
                from = pp->cursor_col;
        else if (from < 0)
                from = 1;
        for (int col = from + 1; col <= pp->width; col++)
                if (_tt_panel_tabstrip_stop_at(pp->htab[CALT(pp)], col - 1))
                        return col;
        return pp->width;
}

@ @c
void
tt_panel_insert_tabstop (tt_panel *pp,
                         int16_t   col)
{
        if (col < 1 || col > pp->width)
                return;
        _tt_panel_tabstrip_set_at(pp->htab[CALT(pp)], col - 1);
}

@ This is inefficient but that shouldn't matter.

@.TODO@>
@c
void
tt_panel_remove_tabstop (tt_panel *pp,
                         int16_t   col)
{
        if (col < 0 || col > pp->width)
                return;
        else if (col == 0)
                _tt_panel_clear_tabs(pp, 0);
        else
                _tt_panel_tabstrip_clear_at(pp->htab[CALT(pp)], col - 1);
}













@* Blit.

@<Pub...@>=
TTAPI void tt_panel_blit (tt_panel *, int16_t, int16_t, int16_t, int16_t,
        int16_t, int16_t);
TTAPI void tt_panel_clear_region (tt_panel *, int16_t, int16_t, int16_t,
        int16_t, u_cp);
TTAPI void tt_panel_clear (tt_panel *, u_cp);
TTAPI void tt_panel_hscroll (tt_panel *, int16_t, bool);
TTAPI void tt_panel_vscroll (tt_panel *, int16_t, bool);
TTAPI void tt_panel_delete_line_here (tt_panel *, int16_t, bool);
TTAPI void tt_panel_insert_line_here (tt_panel *, int16_t, bool);

@ @<Fun...@>=
static void _tt_panel_clear_region (tt_panel *, int, int16_t, int16_t,
        int16_t, int16_t, u_cp);
static void _tt_panel_blit (tt_panel *, int16_t, int16_t, int16_t,
        int16_t, int16_t, int16_t);

@ @c
void
tt_panel_clear (tt_panel *pp,
                u_cp      cp)
{
        _tt_panel_clear_region(pp, 0, 0, 0, pp->width, pp->height, cp);
        assert(tt_panel_get_dirty(pp));
}

@ @c
void
tt_panel_clear_region (tt_panel *pp,
                       int16_t   col,
                       int16_t   row,
                       int16_t   cols,
                       int16_t   lines, @|
                       u_cp      cp)
{
        if (row < 1 || col < 1)
                return;
        if (row > pp->height || col > pp->width)
                return;
        row--;
        col--;
        if (lines < 1 || cols < 0)
                return;
        if (cols == 0)
                cols = pp->width - col;
        if ((int32_t) row + lines > pp->height
                        || (int32_t) col + cols > pp->width)
                return;
        _tt_panel_clear_region(pp, CALT(pp), col, row, cols, lines, cp);
        assert(tt_panel_get_dirty(pp));
}

@ @c
static void
_tt_panel_clear_region (tt_panel *pp,
                        int       alt,
                        int16_t   x,
                        int16_t   y,
                        int16_t   w, @|
                        int16_t   h,
                        u_cp      cp)
{
        tt_cell d = pp->pen;
        d.cp = cp;

        assert(x >= 0 && x < pp->width);
        assert(y >= 0 && y < pp->height);
        assert(w >= 1);
        assert(h >= 1);
        assert((int32_t) x + w <= pp->width);
        assert((int32_t) y + h <= pp->height);
        for (int yi = 0; yi < h; yi++)
                for (int xi = 0; xi < w; xi++)
                        *_panel_ref(pp, alt, x + xi, y + yi) = d;
        _tt_panel_set_dirty(pp);
}

@ @c
void
tt_panel_blit (tt_panel *pp,
               int16_t   col0,
               int16_t   row0,
               int16_t   col1,
               int16_t   row1, @|
               int16_t   cols,
               int16_t   lines)
{
        if (row0 < 1 || col0 < 1)
                return;
        if (row1 < 1 || col1 < 1)
                return;
        row0--;
        row1--;
        col0--;
        col1--;
        if (lines < 1 || cols < 1)
                return;
        if ((int32_t) row0 + lines > pp->height
                        || (int32_t) col0 + cols > pp->width)
                return;
        if ((int32_t) row1 + lines > pp->height
                        || (int32_t) col1 + cols > pp->width)
                return;
        _tt_panel_blit(pp, col0, row0, col1, row1, cols, lines);
        assert(tt_panel_get_dirty(pp));
}

@ @c
static void
_tt_panel_blit (tt_panel *pp,
                int16_t   x0,
                int16_t   y0,
                int16_t   x1,
                int16_t   y1, @|
                int16_t   w,
                int16_t   h)
{
        assert(x0 >= 0 && x0 < pp->width);
        assert(y0 >= 0 && y0 < pp->height);
        assert(x1 >= 0 && x1 < pp->width);
        assert(y1 >= 0 && y1 < pp->height);
        assert((int32_t) x0 + w <= pp->width);
        assert((int32_t) x1 + w <= pp->width);
        assert((int32_t) y0 + h <= pp->height);
        assert((int32_t) y1 + h <= pp->height);

        int alt = CALT(pp);
        if (x0 == x1 && y0 == y1)
                return;
        else if (x0 <= x1 && y0 <= y1) {
                for (int32_t dy = h - 1; dy >= 0; dy--)
                        for (int32_t dx = w - 1; dx >= 0; dx--)
                                *_panel_ref(pp, alt, x1 + dx, y1 + dy)
                                        = *_panel_ref(pp, alt, x0 + dx, y0 + dy);
        } else if (x0 <= x1 && y0 >= y1) {
                for (int32_t dy = 0; dy < h; dy++)
                        for (int32_t dx = w - 1; dx >= 0; dx--)
                                *_panel_ref(pp, alt, x1 + dx, y1 + dy)
                                        = *_panel_ref(pp, alt, x0 + dx, y0 + dy);
        } else if (x0 >= x1 && y0 >= y1) {
                for (int32_t dy = 0; dy < h; dy++)
                        for (int32_t dx = 0; dx < w; dx++)
                                *_panel_ref(pp, alt, x1 + dx, y1 + dy)
                                        = *_panel_ref(pp, alt, x0 + dx, y0 + dy);
        } else {
                assert(x0 >= x1 && y0 <= y1);
                for (int32_t dy = h - 1; dy >= 0; dy--)
                        for (int32_t dx = 0; dx < w; dx++)
                                *_panel_ref(pp, alt, x1 + dx, y1 + dy)
                                        = *_panel_ref(pp, alt, x0 + dx, y0 + dy);
        }
        _tt_panel_set_dirty(pp);
}

@ Top/bottom margins are always obeyed.

@c
void
tt_panel_hscroll (tt_panel *pp,
                  int16_t   n,
                  bool      margin)
{
        int16_t left = margin ? pp->left - 1 : 0;
        int16_t right = margin ? pp->right - 1 : pp->width - 1;
        int16_t height = pp->bottom - pp->top + 1;
        int16_t max = pp->right - pp->left - 1;
        if (n > max)
                n = max;
        else if (n < -max)
                n = -max;
        if (!n)
                return;
        else if (n > 0) {
                _tt_panel_blit(pp, left + n - 1, pp->top, left - 1, pp->top,
                        max - n, height);
                _tt_panel_clear_region(pp, 0, left, pp->top, n, height, 0);
        } else {
                _tt_panel_blit(pp, left - 1, pp->top, left - n - 1, pp->top,
                        max + n, height);
                _tt_panel_clear_region(pp, 0, right - n, pp->top, -n, height, 0);
        }
        assert(tt_panel_get_dirty(pp));
}

@ Top/bottom margins are always obeyed.

@c
void
tt_panel_vscroll (tt_panel *pp,
                  int16_t   n,
                  bool      margin)
{
        int16_t left = margin ? pp->left - 1 : 0;
        int16_t right = margin ? pp->right - 1 : pp->width - 1;
        int16_t width = right - left + 1;
        int16_t max = pp->bottom - (pp->top - 1);
        if (n > max)
                n = max;
        else if (n < -max)
                n = -max;
        if (!n)
                return;
        else if (n > 0) {
                _tt_panel_blit(pp, left, pp->top + n - 1, left, pp->top - 1,
                        width, max - n);
                _tt_panel_clear_region(pp, 0, left, pp->bottom - n, width, n, 0);
        } else {
                _tt_panel_blit(pp, left, pp->top - 1, left, pp->top - n - 1,
                        width, max + n);
                _tt_panel_clear_region(pp, 0, left, pp->top - 1, width, -n, 0);
        }
        assert(tt_panel_get_dirty(pp));
}

@ @c
void
tt_panel_insert_line_here (tt_panel *pp,
                           int16_t   n,
                           bool      margin)
{
        int rem, row, right, bottom, left;

        if (margin) {
                if (pp->cursor_row < pp->top || pp->cursor_row > pp->bottom)
                        return;
                if (pp->cursor_col < pp->left || pp->cursor_col > pp->right)
                        return;
                right = pp->right - 1;
                bottom = pp->bottom - 1;
                left = pp->left - 1;
        } else {
                right = pp->width - 1;
                bottom = pp->height - 1;
                left = 0;
        }
        row = pp->cursor_row - 1;
        rem = bottom + 1 - row;
        if (n < 0)
                n = 1;
        else if (n > rem)
                n = rem;
        _tt_panel_blit(pp, left, row, left, row + n, right - left + 1, rem - n);
        _tt_panel_clear_region(pp, 0, left, row, right - left + 1, n, 0);
        assert(tt_panel_get_dirty(pp));
}

@ @c
void
tt_panel_delete_line_here (tt_panel *pp,
                           int16_t   n,
                           bool      margin)
{
        int rem, row, right, bottom, left;

        if (margin) {
                if (pp->cursor_row < pp->top || pp->cursor_row > pp->bottom)
                        return;
                if (pp->cursor_col < pp->left || pp->cursor_col > pp->right)
                        return;
                right = pp->right - 1;
                bottom = pp->bottom - 1;
                left = pp->left - 1;
        } else {
                right = pp->width - 1;
                bottom = pp->height - 1;
                left = 0;
        }
        row = pp->cursor_row - 1;
        rem = bottom + 1 - row;
        if (n < 0)
                n = 1;
        else if (n > rem)
                n = rem;
        _tt_panel_blit(pp, left, row + n, left, row, right - left + 1, rem - n);
        _tt_panel_clear_region(pp, 0, left, row + (rem - n),
                right - left + 1, n, 0);
        assert(tt_panel_get_dirty(pp));
}

@ Under no circumstances shall backspace wrap around to the previous
iine (DEC STD 070 somewhere).

@c
void
tt_panel_backspace_cursor (tt_panel *pp)
{
        if (pp->cursor_col > pp->left)
                tt_panel_cursor_set_pos(pp, pp->cursor_col - 1, pp->cursor_row);
}

@* Selection.

TODO: clear and scroll selection after is has been made.

@.TODO@>

@ @d TTP_MOUSE_BUTTONS 5
@d TTP_SELECT_DRAGGING 0
@d TTP_SELECT_HOVER    1
@d TTP_SELECT_VISIBLE  2
@d TTP_SELECT_RECT     3
@<Type def...@>=
typedef struct tt_selection {
        uint32_t flags;
        int16_t  start_col, start_row;
        int16_t  extent_col, extent_row;
        int16_t  normal_fcol, normal_frow;
        int16_t  normal_tcol, normal_trow;
        char    *content;
} tt_selection;

@ @<Pub...@>=
TTAPI char *tt_panel_get_selection (tt_panel *, int);
TTAPI bool tt_panel_get_selection_range (tt_panel *, int, int16_t[4],
        uint32_t *);
TTAPI bool tt_panel_is_anything_selected (tt_panel *, int);
TTAPI bool tt_panel_is_cell_selected (tt_panel *, int, int16_t, int16_t);
TTAPI void tt_panel_select_clear (tt_panel *, int);
TTAPI void tt_panel_select_end (tt_panel *, int);
TTAPI void tt_panel_select_move (tt_panel *, int, int16_t, int16_t);
TTAPI void tt_panel_select_start (tt_panel *, int, int, int16_t, int16_t);

@ @<Fun...@>=
static void _tt_panel_select_normalise (tt_panel *, int);
static size_t _tt_panel_save_selection (tt_panel *, int, char *, size_t);

@ @c
char *
tt_panel_get_selection (tt_panel *pp,
                        int       button1)
{
        return pp->select[button1 - 1].content;
}

@ @c
bool
tt_panel_get_selection_range (tt_panel *pp,
                              int       button1,
                              int16_t  *area,
                              uint32_t *flags)
{
        int button0 = button1 - 1;
        if (!IS_FLAG(pp->select[button0].flags, TTP_SELECT_VISIBLE))
                return false;
        area[0] = pp->select[button0].start_col;
        area[1] = pp->select[button0].start_row;
        area[2] = pp->select[button0].extent_col;
        area[3] = pp->select[button0].extent_row;
        *flags = pp->select[button0].flags;
        return true;
}

@ @c
void
tt_panel_select_start (tt_panel *pp,
                       int       button1,
                       int       clicks,
                       int16_t   cx,
                       int16_t   cy)
{
        if (button1 < 1 || button1 > TTP_MOUSE_BUTTONS)
                return;
        if (!(cx >= 1 && cx <= pp->width && cy >= 1 && cy <= pp->height))
                return;
        int button0 = button1 - 1;
        pp->select[button0].flags = 0;
        SET_FLAG(pp->select[button0].flags, TTP_SELECT_VISIBLE);
        pp->select[button0].start_col = cx;
        pp->select[button0].start_row = cy;
        SET_FLAG(pp->select[button0].flags, TTP_SELECT_DRAGGING);
        tt_panel_select_move(pp, button1, cx, cy);
        free(pp->select[button0].content);
        pp->select[button0].content = NULL;
}

@ @c
void
tt_panel_select_move (tt_panel *pp,
                      int       button1,
                      int16_t   cx,
                      int16_t   cy)
{
        if (button1 < 1 || button1 > TTP_MOUSE_BUTTONS)
                return;
        if (!(cx >= 1 && cx <= pp->width && cy >= 1 && cy <= pp->height))
                return;
        int button0 = button1 - 1;
        if (!IS_FLAG(pp->select[button0].flags, TTP_SELECT_DRAGGING))
                return;
        if (cx >= 1 && cx <= pp->width && cy >= 1 && cy <= pp->height) {
                SET_FLAG(pp->select[button0].flags, TTP_SELECT_HOVER);
                pp->select[button0].extent_col = cx;
                pp->select[button0].extent_row = cy;
        } else
                CLEAR_FLAG(pp->select[button0].flags, TTP_SELECT_HOVER);
        _tt_panel_select_normalise(pp, button0);
        _tt_panel_set_dirty(pp);
}

@ @c
void
tt_panel_select_clear (tt_panel *pp,
                       int       button1)
{
        if (button1 < 1 || button1 > TTP_MOUSE_BUTTONS)
                return;
        CLEAR_FLAG(pp->select[button1 - 1].flags, TTP_SELECT_DRAGGING);
        CLEAR_FLAG(pp->select[button1 - 1].flags, TTP_SELECT_VISIBLE);
        abort();
        assert(tt_panel_get_dirty(pp));
}

@ @c
void
tt_panel_select_end (tt_panel *pp,
                     int       button1)
{
        if (button1 < 1 || button1 > TTP_MOUSE_BUTTONS)
                return;
        CLEAR_FLAG(pp->select[button1 - 1].flags, TTP_SELECT_DRAGGING);
        assert(!pp->select[button1 - 1].content);
        size_t want = _tt_panel_save_selection(pp, button1, NULL, 0);
        pp->select[button1 - 1].content = malloc(want);
        _tt_panel_save_selection(pp, 1, pp->select[button1 - 1].content, want);
}

@ @c
static void
_tt_panel_select_normalise (tt_panel *pp,
                            int       button0)
{
        int scol = pp->select[button0].start_col;
        int srow = pp->select[button0].start_row;
        int ecol = pp->select[button0].extent_col;
        int erow = pp->select[button0].extent_row;
        if (srow < erow) {
                pp->select[button0].normal_fcol = scol;
                pp->select[button0].normal_frow = srow;
                pp->select[button0].normal_tcol = ecol;
                pp->select[button0].normal_trow = erow;
        } else if (srow > erow) {
                pp->select[button0].normal_fcol = ecol;
                pp->select[button0].normal_frow = erow;
                pp->select[button0].normal_tcol = scol;
                pp->select[button0].normal_trow = srow;
        } else if (scol < ecol) {
                pp->select[button0].normal_fcol = scol;
                pp->select[button0].normal_tcol = ecol;
                pp->select[button0].normal_frow
                        = pp->select[button0].normal_trow = srow;
        } else {
                pp->select[button0].normal_fcol = ecol;
                pp->select[button0].normal_tcol = scol;
                pp->select[button0].normal_frow
                        = pp->select[button0].normal_trow = srow;
        }
}

@ Fill the buffer at |*buf| with the panel contents selected by
|button1|. |len| is the size of |buf| in bytes. The number of bytes
that were needed including the |NULL| terminator is returned. If
the return value is greater than the |len| argument then the buffer
is incomplete but {\it is\/} |NULL|-terminated (unless |len| is
zero).

TODO: Better end-of-line detection.

@.TODO@>
@c
static size_t
_tt_panel_save_selection (tt_panel *pp,
                          int       button1,
                          char     *buf,
                          size_t    len)
{
        size_t want = 1;
        bool full = len == 0;
        if (!full)
                buf[0] = 0;
        if (button1 < 1 || button1 > TTP_MOUSE_BUTTONS)
                return 1;
        tt_selection *sel = &pp->select[button1 - 1];
        if (!IS_FLAG(sel->flags, TTP_SELECT_VISIBLE))
                return 1;
        char *bp = buf;

        bool rect = IS_FLAG(sel->flags, TTP_SELECT_RECT);
        int from = sel->normal_fcol;
        for (int row = sel->normal_frow; row <= sel->normal_trow; row++) {
                int col = from;
                if (!rect)
                        from = 1;
                int to;
                if (row == sel->normal_trow || rect)
                        to = sel->normal_tcol;
                else
                        to = pp->width;
                for (; col <= to; col++) {
                        tt_cell *c = _panel_ref(pp, CALT(pp), col - 1, row - 1);
                        utf8_io u = utfio_write(c->cp ? c->cp : '\n');
                        if (full || want + u.length > len)
                                full = true;
                        else {
                                memmove(bp, u.buf, u.length);
                                bp += u.length;
                                *bp = 0;
                        }
                        want += u.length;
                        if (!c->cp)
                                break;
                }
        }

        return want;
}



















@* Something Else.

@ @c
#if 0
static void
_tt_panel_render_ascii (tt_panel *pp,
                        int       fid,
                        int16_t  *width,
                        int16_t  *height,
                        int16_t  *descent)
{
        tt_glyph *ucpp;
        int offset, nheight, ndescent, noffset;

        assert(fid >= 0 && fid <= 10);
        *width = *height = *descent = offset = 0;
        for (u_cp cp = ' '; cp <= '~'; cp++) {
                if (_tt_panel_find_glyph(pp, fid, 0, cp, &ucpp) < 0) {
                        fcomplain("cannot find ASCII '%c' (0x%02x) in font %u",
                                cp, cp, fid);
                        continue;
                }

                if (ucpp->width > *width)
                        *width = ucpp->width; /* Widest glyph. */

                /* Calculate height above and below the base line */
                if (ucpp->offset_y < 0) { /* Descends only. */
                        nheight = ndescent = -ucpp->offset_y
                                + ucpp->height;
                        noffset = 0;
                } else if (ucpp->height < ucpp->offset_y) {
                                                /* Ascends only. */
                        nheight = noffset = ucpp->offset_y;
                        ndescent = 0;
                } else {
                        nheight = noffset = ucpp->offset_y;
                        ndescent = ucpp->height - noffset;
                }
                if (ndescent > *descent)
                        *descent = ndescent;
                if (noffset > offset)
                        offset = noffset;
                if (nheight > *height)
                        *height = nheight;
        }
}

static void
_tt_panel_set_cell_metrics (tt_panel *pp)
{
        int fid = 0; /* use primary font */
        tt_font *pf = pp->font[fid].alt[0];
        int16_t descent, height, width;

        _tt_panel_render_ascii(pp, fid, &width, &height, &descent);

        // width may be up to two pixels more than it should be
        pp->cell_width = sp6tof(hrf_size_get_max_advance(pf->strike));
        if ( (width-2) * 2 < pp->cell_width ) {
                fbug("assuming font includes double-width glyphs");
                pp->cell_width /= 2;
                /* need a better heuristic (or data) */
        }
        if (width > pp->cell_width)
                fcomplain("glyphs are too wide: %u > %u", width, pp->cell_width);

        pp->cell_height = sp6tof(hrf_size_get_height(pf->strike));
        if (height + descent > pp->cell_height)
                fcomplain("glyphs are too tall: %u > %u", pp->cell_height,
                        height + descent);

        pp->cell_inset = pp->cell_height - (height + descent);
        pp->cell_inset /= 2;
        pp->cell_inset += descent;

        finform("calculated cell size %ux%u @@ %u", pp->cell_width, pp->cell_height, pp->cell_inset);
}
#endif








@** Presenting Characters.

@(actor.c@>=
#define FONT_NAME "Liberation Mono:antialias=true:autohint=true"
#define FONT_SIZE 42

#include <assert.h>
#include <err.h>
@#
#include <epoxy/gl.h>
@#
#include "tgl.h"
#include "tpanel.h"
#include "trix.h"

struct event_base  *Main_Loop;
trix               *Tctx;
Display            *X_Display;
int                 X_Screen;
Window              Main_Window;
int                 Window_Width = 640;
int                 Window_Height = 480;

tt_panel           *Panel = NULL;

static void _actor_xevent (trix *, XEvent *);

@ @.TODO@> @(actor.c@>=
int
main (int    argc,
      char **argv)
{
        Main_Loop = event_init();

        trix_init_full(Tctx, Main_Loop, _actor_xevent, NULL, NULL, NULL);
        X_Display = trix_get_xdisplay(Tctx);
        X_Screen = trix_get_xscreen(Tctx);
        Main_Window = trix_get_xwindow(Tctx);
        XMapWindow(X_Display, Main_Window);
        XSelectInput(X_Display, Main_Window,
                ExposureMask | KeyPressMask | KeyReleaseMask);
        XFlush(X_Display);

        Panel = tt_panel_new(80, 24);
        if (!Panel)
                errx(1, "panel_init");

        tt_font *font = tt_panel_font_new_from_xft_name(Panel, X_Display,
                X_Screen, FONT_NAME, FONT_SIZE);
        if (!font)
                errx(1, "tt_panel_font_new_from_xft_name");
        tt_panel_invoke_font(Panel, 0, font, false);

        tt_panel_garble(Panel);
        tgl_initgl();
        tgl_panel_to_vertex();

        trix_trigger(Tctx); /* font init swallows filedes readiness indicator */
        return event_base_dispatch(Main_Loop);
}

@ @(actor.c@>=
static void
_actor_xevent (trix   *tctx,
               XEvent *xev)
{
        assert(tctx == Tctx);

        switch (xev->type) {
        case ConfigureNotify:
                Window_Width = xev->xconfigure.width;
                Window_Height = xev->xconfigure.height;
                if (0) { /* Resize or stretch? */
                        tt_font *dfont = tt_panel_get_font(Panel, 0, 0);
                        assert(dfont);
                        int tw = tt_panel_font_get_cell_width(dfont);
                        int th = tt_panel_font_get_cell_height(dfont);
                        int w = Window_Width / tw;
                        int h = Window_Height / th;
                        int pw = tt_panel_get_width(Panel);
                        int ph = tt_panel_get_height(Panel);
                        if (w != pw || h != ph)
                                tt_panel_resize(Panel, w, h, true);
                }
                /* fallthrough */
        case Expose:
                tgl_drawgl(NULL);
                break;
        }
}

@ Key motions are CUU, CUD, etc.

@s event none
@(actor.c@>=
#if 0
static void
_act_sdl (SDL_Event *ev,
          void      *arg)
{
        int16_t left, right, bottom, top, row, col, width, height, n;
        if (ev->type == sdl_delay_event_id && Need_Draw)
                _actor_draw();
        else switch (ev->type) {
        case SDL_WINDOWEVENT:
                switch (ev->window.event) {
                case SDL_WINDOWEVENT_RESIZED:
                        Window_Width = ev->window.data1;
                        Window_Height = ev->window.data2;
                        Need_Draw = true;
                        break;
                }
                break;
        case SDL_KEYDOWN:
                if (ev->key.keysym.sym >= ' ' && isascii(ev->key.keysym.sym))
                        tt_panel_put_here(Panel, ev->key.keysym.sym);
                else switch (ev->key.keysym.sym) {
                case SDLK_UP:
                        tt_panel_cursor_get_pos(Panel, &col, &row);
                        n = row - 1;
                        if (n < 1)
                                n = 1;
                        tt_panel_cursor_set_pos(Panel, 0, n);
                        break;
                case SDLK_DOWN:
                        tt_panel_cursor_get_pos(Panel, &col, &row);
                        n = row + 1;
                        height = tt_panel_get_height(Panel);
                        if (n > height)
                                n = height;
                        tt_panel_cursor_set_pos(Panel, 0, n);
                        break;
                case SDLK_LEFT:
                        col = tt_panel_cursor_get_col(Panel);
                        n = col - 1;
                        if (n < 1)
                                n = 1;
                        tt_panel_cursor_set_pos(Panel, n, 0);
                        break;
                case SDLK_RIGHT:
                        col = tt_panel_cursor_get_col(Panel);
                        n = col + 1;
                        width = tt_panel_get_width(Panel);
                        if (n > width)
                                n = width;
                        tt_panel_cursor_set_pos(Panel, n, 0);
                        break;
                case SDLK_RETURN:
                        tt_panel_put_here(Panel, 'E');
                        break;
                }
                Need_Draw = true;
                break;
        }
}
#endif
