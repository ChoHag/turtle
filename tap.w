@i format.w
@i types.w

@** Test Anything Program. Single header file libraries are all the
rage and this library is no exception. TAP prefixes all of its
public symbols with \.{tap\_} or \.{TAP\_}. \.{\_tap\_} is also
used internally. The word TAP refers to this library unless otherwise
qualified (eg.~the TAP protocol).

\.{CWEB} produces \.{.c} files by default so this is a good opportunity
to demonstrate how to compile the TAP implementation. This section
(two lines) comprises all of \.{tap.c}.

@c
#define TAP_IMPLEMENTATION 1
#include "tap.h"

@ The header file \.{tap.h} is the main file of interest produced
from this \.{CWEB} source. If the preprocessor symbol
|TAP_IMPLEMENTATION| is not defined it will only include some
system headers and declare the functions that make up the TAP API.
With it, those functions will also be defined as will some private
global variables.

@(tap.h@>=
#ifndef TAP_H
#define TAP_H
#define TAP_VERSION "1"
#include <stdarg.h>
#include <stdbool.h>
@<Public API@>@;
#endif /* |TAP_H| */
@#
#ifdef TAP_IMPLEMENTATION
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
@<Implementation@>@;
#endif /* |TAP_IMPLEMENTATION| */

@ The API is deliberately simple and free from dependencies so that
test scripts are plain and unencumbered and TAP does not interfere
with the application under test any more than absolutely necessary.

First |tap_getargs|, which is optional, checks that there are no
command-line arguments. Then a series of |tap_ok| (or |tap_vok|)
commands emit the results of each test. A test label is optional
and is passed to |vfprintf| as-is. A string can be set globally to
prefix each label with.

Upon completion |main| should return the result of |tap_exit| which
validates that the test process has been carried out correctly and
sets the exit code.

A test plan indicating how many tests should be or should have been
carried out can be emitted by |tap_plan| once at the beginning or
end of the test sequence or not at all. If a plan has not been
emitted then |tap_exit| will do so.

Diagnostic messages can be included by using |tap_diag|/|tap_vdiag|
whose arguments will simply be printed in-line in the TAP output
stream with ``\.{\#\ }'' prepended.

No effort is made to check any printed text for newline characters
which could confuse the TAP parser.

@<Pub...@>=
int tap_exit (void);
void tap_getargs (int, char **);
void tap_ok (bool, char *, ...);
void tap_plan (long);
char *tap_prefix (char *);
void tap_vok (bool, char *, va_list);
void tap_vdiag (char *, va_list);
void tap_diag (char *, ...);

@** TAP Implementaion. There is very little state to keep track of.

@<Imp...@>=
static long  TAP_Expect = 0; /* If non-zero, how many tests were planned. */
static bool  TAP_Failed = false; /* Has any test failed or TAP been misused? */
static bool  TAP_Finished = false; /* Has |tap_exit| been called? */
static long  TAP_Next = 1; /* The identifier of the next test. */
static bool  TAP_Plan_Last = false; /* Was the plan was emitted after testing? */
static char *TAP_Prefix = NULL; /* Message prefix. */
static bool  TAP_Replan_Warned = false; /* A mis-placed plan was warned about? */

@ Checks the state~---~most of the variables above~---~to see whether
the tests all passed and TAP has been used correctly. This should
be the last TAP function called, usually by ``|return tap_exit()|''
at the end of |main|.

@<Imp...@>=
int
tap_exit (void)
{
        long last = TAP_Next - 1;
        if (TAP_Finished) {
                TAP_Failed = true;
                tap_diag("tap_exit called again");
        }
        if (!TAP_Expect) {
                if (last)
                        tap_plan(TAP_Expect - 1);
                else {
                        TAP_Failed = true;
                        tap_diag("No plan and no tests run");
                }
        } else if (!last) {
                TAP_Failed = true;
                tap_diag("No tests run");
        } else if (last != TAP_Expect) {
                TAP_Failed = true;
                tap_diag("Ran %ld tests but expected %ld", last, TAP_Expect);
        }
        TAP_Finished = true; /* Make sure this comes after |tap_plan|. */
        return TAP_Failed ? EXIT_FAILURE : EXIT_SUCCESS;
}

@ The meat of the TAP implementation. Emit a line indicating the
status of a test (``\.{ok}'' or ``\.{not ok}'') with its identifier
and optional label. This is all of |tap_ok| except that it ignores
|TAP_Finished| so that it can be used by the real |tap_ok| when
|tap_exit| has already been called.

@<Imp...@>=
static void
_tap_vok_imp (bool     pass,
              char    *label,
              va_list  args)
{
        long n;

        if (TAP_Plan_Last) {
                TAP_Failed = true;
                if (!TAP_Replan_Warned) {
                        TAP_Replan_Warned = true;
                        tap_diag("Tests after trailing plan");
                }
        }
        if (!pass)
                TAP_Failed = true;
        n = TAP_Next++;
        if (pass)
                printf("ok %ld", n);
        else
                printf("not ok %ld", n);
        if (TAP_Prefix || label) {
                printf(" - ");
                if (TAP_Prefix)
                        printf("%s", TAP_Prefix);
                if (TAP_Prefix && label)
                        putchar(' ');
                if (label)
                        vprintf(label, args);
        }
        putchar('\n');
}

static void
_tap_ok_imp (bool  pass,
             char *label, ...)
{
        va_list ap;
        va_start(ap, label);
        _tap_vok_imp(pass, label, ap);
        va_end(ap);
}

@ This is the rest of |tap_ok| which checks |TAP_Finished| and
defers to |_tap_vok_imp| above.

@<Imp...@>=
void
tap_ok (bool  pass,
        char *label, ...)
{
        va_list ap;
        va_start(ap, label);
        tap_vok(pass, label, ap);
        va_end(ap);
}

void
tap_vok (bool     pass,
         char    *label,
         va_list  args)
{
        if (TAP_Finished)
                _tap_ok_imp(false, "tap_[v]ok called after tap_exit!");
        _tap_vok_imp(pass, label, args);
}

@ @<Imp...@>=
char *
tap_prefix (char *new)
{
        char *old = TAP_Prefix;
        TAP_Prefix = new;
        return old;
}

@ Any arguments will cause the test to run as normal but fail
overall. Perhaps this should print a message instead and not run
the tests for people who like to run random binaries they find with
a ``\.{-h}'' or ``\.{--help}'' argument.

@<Imp...@>=
void
tap_getargs (int    argc,
             char **argv)
{
        if (TAP_Finished)
                _tap_ok_imp(false, "tap_getargs called after tap_exit!");
        if (argc == 1)
                return;
        TAP_Failed = true;
        tap_diag("unexpected arguments");
        assert(!argv[argc]); /* Silences \.{-Wunused}. */
}

@ Emit a TAP plan line. This should only be called once.

@<Imp...@>=
void
tap_plan (long expect)
{
        assert(expect > 0);
        if (TAP_Finished)
                _tap_ok_imp(false, "tap_plan called after tap_exit!");
        if (TAP_Expect) {
                TAP_Failed = true;
                tap_diag("Planned twice; replacing %ld with %ld",
                        TAP_Expect, expect);
        }
        if (TAP_Next != 1)
                TAP_Plan_Last = true;
        printf("1..%ld\n", expect);
        TAP_Expect = expect;
}

@ It's not an error to call these after |tap_exit| but it's probably
a bad idea.

@<Imp...@>=
void
tap_diag (char *label, ...)
{
        va_list ap;
        va_start(ap, label);
        tap_vdiag(label, ap);
        va_end(ap);
}

void
tap_vdiag (char    *label,
           va_list  arg)
{
        printf("# ");
        vprintf(label, arg);
        putchar('\n');
}

@** Example. Either every test script or one which compiles to an
object linked into every test binary should include the two lines
at the very beginning of this document, copied here:

|#define TAP_IMPLEMENTATION| |1|\C{ Anything will do. }\par
|#include "tap.h"|\C{ or \.{<tap.h>} if TAP has been installed. }\par

If the implementation will be compiled into a separate object file
then any source files that will link to it needs only the |include|
line without the definition.

@(tapdance.c@>=
#define TAP_IMPLEMENTATION 1
#include "tap.h"

int
main (int    argc,
      char **argv)
{
        tap_getargs(argc, argv); /* Optional. */
        tap_plan(2);
        tap_ok(true, "this test passes");
        tap_ok(false, "this test fails");
        tap_diag("this is a comment");
        tap_ok(true, "this test passes but breaks the plan");
        return tap_exit();
}

@ Running tapdance will produce the following output and the return
value will be 1.

\.{1..2}\par
\.{ok 1 - this test passes}\par
\.{not ok 2 - this test fails}\par
\.{\# this is a comment}\par
\.{ok 3 - this test passes but breaks the plan}\par
\.{\# Ran 3 tests but expected 2}\par

@** Index.
