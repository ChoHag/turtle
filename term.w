@i format.w
@i types.w
% preceeded by intro.w
\def\man#1#2{\pdfURL{\.{#2}(#1)}{http://man.openbsd.org/#2.#1}}

@** Terminal/Host Interchange. This module is responsible for
communicating with the host computer and interpreting the control
sequences and graphic characters it sends. It begins with the \CEE/
preamble followed by bookkeeping routines to initialise and destroy
a terminal object. Following this there is a section of unix-specific
code for managing signals and processes that ought to be moved
elsewhere.

This is followed by the process for interpreting the byte stream
from the host into control sequences, then an implementation of
each operation that a control sequence can perform.

The header file created for this module is named \.{tterm.h} to
avoid a clash with \.{term.h} from the Curses library.

@(tterm.h@>=
#ifndef TURTLE_TERM_H
#define TURTLE_TERM_H
#include <limits.h>
#include <stddef.h>
#include <stdint.h>
#include "buf.h"
#include "tpanel.h"
#ifndef TTAPI
#define TTAPI __attribute__((__visibility__("default")))
#endif
@<Public API (\.{term.o})@>@;
#endif /* |TURTLE_TERM_H| */

@ The \CEE/ source begins by including header files and declaring
globals.

@c
#include "tterm.h"
@#
#include <assert.h>
#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
@#
#include "parsnip/safe-math.h"
@#
#include "log.h"
#include "utf8.h"
#undef LOG_PREFIX
#define LOG_PREFIX "term"

#include "term-internal.h"

@<Global variables (\.{term.o})@>@;

@ @(term-internal.h@>=
#ifndef TT_TERM_INTERNAL
@h
@<Type definitions (\.{term.o})@>@;
@<Function declarations (\.{term.o})@>@;
#define TT_TERM_INTERNAL
#endif /* |TT_TERM_INTERNAL| */

@ This object references all of the run-time state of a single terminal
instance; its contents will be described as they are used. I don't
think this is the right place to introduce these flag values.

@d TERM_AUTO_WRAP         0
@d TERM_CURSOR_APP        1
@d TERM_DELIMIT_PASTE     2
@d TERM_EIGHTBIT          3 /* Host can/expects to receive 8-bit controls */
@d TERM_HOST_CAN_RESIZE   4
@d TERM_INSERT_REPLACE    5
@d TERM_KEYPAD_APP        6
@d TERM_LAST_COLUMN       7
@d TERM_LEFT_RIGHT_MARGIN 8
@d TERM_NEW_LINE_MODE     9
@d TERM_ORIGIN_MOTION     10 /* Origin may not be 1,1 if on */
@d TERM_PARSER_ACTIVE     11
@d TERM_UTF_HOST          12 /* Host sends/receives UTF-8 (overrides |TERM_EIGHTBIT|) */
@#
@d TERM_BUFSIZE 1024
@d TERM_MAXSIZE (1048576 * 16) /* 16MB */
@<Type def...@>=
@<Definition of |gset_xt| object@> @;

typedef struct tt_term {
        term_act_fun       bell;      /* alert (bell) action */
        tt_panel          *panel;     /* should be in-line */
        void              *resume;    /* parser return state */
        utf8_io            utf;       /* wastes four bytes */

        int                flags;

        uint8_t            gr96[4];   /* |bool| */
        uint8_t            use_gl, use_gr; /* 0, 1, 2, 3 (0 cannot go in gr) */
        gset_xt           *grachar[4]; /* see also |TERM_UTF_HOST| */

        size_t             at;        /* parser offset in |buf| */
        uint8_t            buf[TERM_BUFSIZE]; /* buffer consume reads from */
        tt_action          parser;    /* result of parsing a control sequence */

        struct timeval     slow_time; /* simulating a slow baud rate (badly) */
        struct event      *slow_more;
        float              slow, slow_budget, slow_tick;

        struct {
                int16_t    col, row;
                uint8_t    pen[TT_CELL_LENGTH];
                uint8_t    origin;
                uint8_t    gd[4], gl, gr; // or so.
        } save;

        void             (*write_fun)(uint8_t *, size_t, void *);
        void              *write_arg;
#ifdef TT_DEBUG
        int                trace_dir;   /* tracing communication with the host */
        size_t             trace_len;
        FILE              *trace_fh;
#endif
} tt_term;

@ Input from the host is parsed into values in this object which
is then used by the control operators. It can also be filled in by
the user to call the control operators directly so it is made public.

@<Pub...@>=
#define TERM_ARG_SIZE 16
#define TERM_ARG_LIMIT INT_MAX

typedef struct tt_term tt_term;

typedef struct tt_action {
        tp_array data;
        int16_t  flags;
        uint8_t  mode;
        uint8_t  prefix[2];
        uint8_t  reply;
        int8_t  _pad;
        int8_t   narg;
        int      arg[TERM_ARG_SIZE];
} tt_action;

@ The user can register functions that perform certain terminal
actions, eg.~to ring the \.{BEL}.

@<Pub...@>=
typedef void (*term_act_fun)(tt_term *);

@ Here is the public API for initialisation and destruction of a
terminal.

@<Pub...@>=
TTAPI tt_term *term_new (tt_panel *, void (*)(uint8_t *, size_t, void *),
        void *);
TTAPI void term_destroy (tt_term *);
TTAPI bool term_get_8bit_xmit (tt_term *);
TTAPI bool term_get_cursor_app_mode (tt_term *);
TTAPI bool term_get_keypad_app_mode (tt_term *);
TTAPI void term_set_bell_fun (tt_term *, term_act_fun);
TTAPI void term_set_reap_fun (tt_term *, term_act_fun);

@ Speed feature.
@<Pub...@>=
TTAPI void term_set_speed (tt_term *, float, float);

@ And here is some internal functions that should move elsewhere.
@<Fun...@>=
static void _term_read (int, short, void *);
static void _term_slow_consume (tt_term *, size_t);
static void _term_slow_more (int, short, void *);

@ Initialise a new terminal object.

TODO: Panel should be initialised here rather than the caller.

@.TODO@>
@c
tt_term *
term_new (tt_panel  *panel,
          void     (*write_fun)(uint8_t *, size_t, void *),
          void      *write_arg)
{
        tt_term *ret;

        ret = calloc(1, sizeof (tt_term));
        if (!ret)
                return NULL;
        if (tt_panel_attach(panel, &ret->panel) == -1) {
                free(ret);
                return NULL;
        }
        ret->write_fun = write_fun;
        ret->write_arg = write_arg;
        if (!array_init(&ret->parser.data, 1, TERM_BUFSIZE, 0, TERM_MAXSIZE)) {
                free(ret);
                return NULL;
        }
        utfio_init(&ret->utf);
        CLEAR_FLAG(ret->flags, TERM_EIGHTBIT);
        SET_FLAG(ret->flags, TERM_AUTO_WRAP);
        SET_FLAG(ret->flags, TERM_UTF_HOST);
        @<Initialise 7-bit character sets@>@;
        term_exec_DECSC(ret, &ret->parser); /* Initialise saved cursor */
        return ret;
}

@ Destruction of the terminal object is more complicated than usual
because there will usually be a process to wait for. If there is
then this function clears the |TERM_IS_LIVE| flag to indicate that
the caller no longer holds a reference to the terminal object.
Cleanup will continue when the signal handler is called after the
process exits.

@c
void
term_destroy (tt_term *tt)
{
        tt_panel_detach(tt->panel);
        _trace_byte(tt, LIO_NONE, NULL, 0);
        free(tt);
}

@ These functions set the callbacks for the two bell-ringers.

@c
void
term_set_bell_fun (tt_term      *tt,
                   term_act_fun  bell)
{
        tt->bell = bell;
}

@ Some accessors.

@c
bool
term_get_cursor_app_mode (tt_term *tt)
{
        return IS_FLAG(tt->flags, TERM_CURSOR_APP);
}

bool
term_get_keypad_app_mode (tt_term *tt)
{
        return IS_FLAG(tt->flags, TERM_KEYPAD_APP);
}

bool
term_get_8bit_xmit (tt_term *tt)
{
        return IS_FLAG(tt->flags, TERM_KEYPAD_APP);
}

@* Character Sets. Multiple character sets are not supported in
Turtle except ASCII and UTF-8-encoded Unicode but many applications
expect it so a bare framework exists which can be expanded if
necessary. Four character sets may be designated and one of these
may then be invoked into GL (codes 0--127) or GR (code 128--255).

@<Definition of |gset...@>=
typedef struct gset_xt {
        bool is_96; /* sized to avoid noise about padding */
        u_cp codepoint[96];
} gset_xt;

@ @<Global...@>=
gset_xt GTable_ASCII = { 0 };

@ \DEC{3.6.4}. Level 1 has ASCII in g0-g1, g0 in gl. Does not expect
to receive 8 bit anything, but of course we will so act like a
vt220.

Level 2 w/o 8-bit has ASCII in g0/1 on gl and DEC Supplemental in
g2/3 on gr.

Level 3 or 2 w/ has ASCII in g0/1 on gl, UPSS (DEC Supplemental or
Latin-1) in g2/3 on gr.

Latter also recognises "Announce Subset Of Code Extension Facilities"
in ISO 4873.

ASCII is replacable as a local option that is not implemented (NRCS).

@<Initialise 7-bit character sets@>=
ret->use_gr = 2; /* Depends on conformance level. */
ret->grachar[0] = &GTable_ASCII;
ret->grachar[1] = &GTable_ASCII;
ret->grachar[2] = &GTable_ASCII;
ret->grachar[3] = &GTable_ASCII;

@** Parsing Host Input. ECMA-48 describe how to extend ASCII with
control sequences. ECMA-35 also describes similar types of code
sequence for designating and invoking character sets. Collectively
these have come to be known as escape sequence because they can,
and sometimes do, always begin with the \.{ESC} code.

|term_consume_bytes| is a publicly-accessible wrapper around the
parser that breaks the input into pieces small enough to fit into
the terminal object's buffer.

@<Pub...@>=
TTAPI void term_consume_bytes (tt_term *, uint8_t *, size_t);

@ Parsing and interpreting those bytes uses these functions.

@<Fun...@>=
static void _term_collect_byte (tt_term *, uint8_t, bool);
static void _term_collect_param (tt_term *, uint8_t);
static void _term_collect_prefix (tt_term *, uint8_t);
static void _term_consume_bytes (tt_term *, size_t);
static void _term_emit_byte (tt_term *, uint8_t);
static void _term_emit_cp (tt_term *, u_cp);
static void _term_exec_SGR_38 (tt_term *, tt_action *, int, bool);
static void _term_exec_C0 (tt_term *, uint8_t);
static void _term_exec_C1 (tt_term *, uint8_t);
static void _term_exec_CSI (tt_term *, uint8_t);
static void _term_exec_DCS (tt_term *);
static void _term_exec_ESC (tt_term *, uint8_t);
static void _term_exec_OSC (tt_term *);
static void _term_reset_parse_state (tt_term *);
static void _term_utfio_reset (tt_term *);

@ @c
void
term_consume_bytes (tt_term *tt,
                    uint8_t *buf,
                    size_t   len)
{
        while (len > TERM_BUFSIZE) {
                term_consume_bytes(tt, buf, TERM_BUFSIZE);
                buf += TERM_BUFSIZE,
                len -= TERM_BUFSIZE;
        }
        memmove(tt->buf, buf, len);
        assert(len <= TERM_BUFSIZE);
        assert(!IS_FLAG(tt->flags, TERM_PARSER_ACTIVE));
        SET_FLAG(tt->flags, TERM_PARSER_ACTIVE);
        _term_consume_bytes(tt, len);
        CLEAR_FLAG(tt->flags, TERM_PARSER_ACTIVE);
}

@ ECMA-48 describes how to form control sequences but gives no
guidance for dealing with input which does not conform to the
standard. DEC made a popular line of terminals which did handle
such errors which has since become the de-facto standard for doing
so.

Paul Flo Williams has performed extensive research on real terminals
to determine, along with documentation produced by DEC, exactly
what this method is which is summarised in a nice diagram at
https://vt100.net/emu/dec\_ansi\_parser. When I originally wrote
the code to implement I remarked in these notes that it was about
as large a spaghetti monster as I'm willing to write before making
it table-driven. It has since grown a little.

The parser begins in the |flo_ground| state. If the buffer is empty
while the parser is in another state then the location is remembered
and the parser resumes from there when next called using the GCC
assigned goto extension.

Readers who don't like nests of goto statements should jump ahead
half a dozen pages to after the control sequences have been scanned.

@c
static void
_term_consume_bytes (tt_term *tt,
                     size_t   len)
{
        uint8_t bufscii; /* The current byte masked with |0x7f|. */
        void *next, *start;

        tt->at = -1; /* Overflows to 0 on first use. */
        if (tt->resume) {
                start = tt->resume;
                tt->resume = NULL; /* unnecessary safety valve */
                goto *start;
        }

        @<Ground state@>@;
        @<Ignored control strings@>@;
        @<Escape (\.{ESC}) sequences@>@;
        @<Control (\.{CSI}) sequences@>@;
        @<Device Control Strings (\.{DCS})@>@;
        @<Operating System Command (\.{OSC}) control strings@>@;
        abort(); /* UNREACHABLE */
}

@ The majority of states (except, notable, |flo_ground|) begin with
|_term_flo_transition| which checks for reaching the end of the
buffer and processes C0 and C1 control codes.

When the GROUND state is not |flo_ground| it is treated as the exit
path from the current state, used by DCS and OSC strings. TODO:
when exiting the DCS and OSC states (in situations that shouldn't
terminate it normally) the control and DCS/OSC may apply in the
wrong order.

@.TODO@>
% CWEB doesn't know about label pointers
@d DPTR(X) &@t@>&X /* work around a \.{CWEB} deficiency */
@d _term_flo_next(RETURN) do {
        if (++tt->at >= len) {
                tt->resume = DPTR(RETURN);
                return;
        }
} while (0)
@d _term_flo_exit(GROUND,DESTINATION) do {
        if (DPTR(GROUND) == DPTR(flo_ground))
                goto DESTINATION;
        next = DPTR(DESTINATION);
        goto GROUND;
} while (0)
@d _term_flo_transition(GROUND,RETURN) do {
        _term_flo_next(RETURN);
        _trace_byte(tt, LIO_IN, &tt->buf[tt->at], 1);
        next = DPTR(flo_ground);
        switch (ucp_stick(tt->buf[tt->at])) {
        case 1: /* C0 controls */
                if (tt->buf[tt->at] == 0x18 || tt->buf[tt->at] == 0x1a)
                        goto GROUND; /* \.{CAN}, \.{SUB} */
                else if (tt->buf[tt->at] == 0x1b)
                        _term_flo_exit(GROUND, flo_esc); /* \.{ESC} */
                break;
        case 8: /* C1 controls */
                /* should go via GROUND first if it's not |flo_ground| */
                _term_exec_C1(tt, tt->buf[tt->at]);
                goto GROUND;
        case 9: /* C1 controls */
                if (tt->buf[tt->at] == 0x90)
                        _term_flo_exit(GROUND, flo_dcs_entry); /* \.{DCS} */
                else if (tt->buf[tt->at] == 0x9b)
                        _term_flo_exit(GROUND, flo_csi_entry); /* \.{CSI} */
                else if (tt->buf[tt->at] == 0x9c)
                        goto GROUND; /* \.{ST} */
                else if (tt->buf[tt->at] == 0x9d)
                        _term_flo_exit(GROUND, flo_osc_string); /* \.{OSC} */
                else if (tt->buf[tt->at] == 0x98 || tt->buf[tt->at] >= 0x9e)
                        /* \.{SOS}, \.{PM}, \.{APC} */
                        _term_flo_exit(GROUND, flo_apc_string);
                else { /* |0x91|--|0x97|, |0x99|, |0x9a| */
                        _term_exec_C1(tt, tt->buf[tt->at]);
                        goto GROUND;
                }
        }
        bufscii = tt->buf[tt->at] & 0x7f;
} while (0)

@ This is the state machine's entry point. This is the only state
which doesn't immediately use |_term_flo_transition| because 8-bit
C1 controls are indistinguishable from UTF-8 continuation bytes:
if the parser is part-way through decoding a UTF-8 sequence then
the next byte is emitted and the decoder will handle it.

@<Ground state@>=
flo_ground:
        if (IS_FLAG(tt->flags, TERM_UTF_HOST)) {
                _term_flo_next(flo_ground);
                if (tt->utf.status == UTFIO_PROGRESS
                                && ucp_stick(tt->buf[tt->at]) > 1) {
                        _trace_byte(tt, LIO_IN, &tt->buf[tt->at], 1);
                        _term_emit_byte(tt, tt->buf[tt->at]);
                        goto flo_ground;
                }
                tt->at--; /* prepare to re-scan the same byte */
        }
        _term_flo_transition(flo_ground, flo_ground);
        if (ucp_stick(bufscii) <= 1)
                _term_exec_C0(tt, bufscii);
        else if (tt->buf[tt->at] == 0x7f) {
                /* some (badly-written?) applications send (or echo)
                        \.{DEL} when they mean \.{BS} */
                if (0) /* |IS_FLAG(tt->flags, TERM_HOST_DELETE_IS_BS)| */
                        _term_exec_C0(tt, 0x08); /* \.{BS} */
        } else
                _term_emit_byte(tt, tt->buf[tt->at]);
        goto flo_ground;

@ Controls which instruct the terminal to expect a string but which
are ignored all use this same routine. Amusingly ctlseqs mentions
\.{APC} and \.{PM} but not \.{SOS} but the function in Xterm that
does the ignoring is called \.{ParseSOS}.

@<Ignored control strings@>=
flo_apc_string:
        _term_flo_transition(flo_ground, flo_apc_string);
        goto flo_apc_string;

@ When the start of an escape or control sequence is detected the
parser discards any previous work and prepares to collect data about
a new sequence. The UTF-8 decoder is also reset which emits a
replacement character in case a sequence is somehow started while
UTF-8 is partly decoded.

The parser collects the arguments necessary to perform an action,
hence these poorly thought out flag names.

@d TERM_ACTION_PARSE_ERROR 0
@d TERM_ACTION_FIRST_DIGIT 1
@d TERM_ACTION_EXEC_FLAG   2 /* unused */
@d TERM_ACTION_FLAG_SET    3 /* unused */
@c
static void
_term_reset_parse_state (tt_term *tt)
{
        memset((char *) &tt->parser + sizeof (tt->parser.data), 0,
                sizeof (tt->parser) - sizeof (tt->parser.data));
        SET_FLAG(tt->parser.flags, TERM_ACTION_FIRST_DIGIT);
        array_clear(&tt->parser.data);
        if (array_get_length(&tt->parser.data) > TERM_BUFSIZE)
                array_realloc(&tt->parser.data, TERM_BUFSIZE);
        if (array_get_length(&tt->parser.data))
                *((char *) array_ref(&tt->parser.data, 0)) = 0;
        _term_utfio_reset(tt);
}

static void
_term_utfio_reset (tt_term *tt)
{
        if (tt->utf.status == UTFIO_PROGRESS)
                _term_emit_cp(tt, UCP_REPLACEMENT);
        utfio_init(&tt->utf);
}

@ No known control has more than two prefix bytes although ECMA-48
doesn't place any limits so the action object is limited to two
prefix bytes and receiving a third is an error and the control will
be parsed until it's complete but ignored.

@c
static void
_term_collect_prefix (tt_term *tt,
                      uint8_t  val)
{
        if (!tt->parser.prefix[0])
                tt->parser.prefix[0] = val;
        else if (!tt->parser.prefix[1])
                tt->parser.prefix[1] = val;
        else
                SET_FLAG(tt->parser.flags, TERM_ACTION_PARSE_ERROR);
}

@ The |flo_esc| state is entered after \.{ESC} is received in nearly
every condition. In this state the terminal will act immediately
on receipt of any C0 control (recall that any 8-bit C1 control has
been handled already in |_term_flo_transition|) and treat bytes
from sticks 4 and 5 as the corresponding 7-bit C1 control.

If the byte following \.{ESC} is not a C1 control then
|flo_esc_intermediate| continues scanning the escape sequence without
looking for a (7-bit) C1 control.

@<Escape (\.{ESC}) sequences@>=
flo_esc:
        _term_reset_parse_state(tt);
flo_esc_continue:
        _term_flo_transition(flo_ground, flo_esc_continue);
        switch (ucp_stick(bufscii)) {
        case 0:
        case 1:
                _term_exec_C0(tt, tt->buf[tt->at]);
                goto flo_esc_continue;
        case 2:
                _term_collect_prefix(tt, bufscii);
                goto flo_esc_intermediate;
        case 3:
        case 4:
                goto flo_esc_dispatch;
        case 5:
                switch (bufscii) {
                case 0x50:
                        goto flo_dcs_entry; /* \.P, \.{DCS} */
                case 0x5b:
                        goto flo_csi_entry; /* \.[, \.{CSI} */
                case 0x5d:
                        goto flo_osc_string; /* \.], \.{OSC} */
                case 0x5e: @; /* \.\^ (caret), \.{PM} */
                case 0x5f: @; /* \.\_ (underscore), \.{APC} */
                case 0x58: /* \.X, \.{SOS} */
                        goto flo_apc_string;
                default:
                        goto flo_esc_dispatch;
                }
        case 6:
                goto flo_esc_dispatch;
        case 7:
                if (bufscii == 0x7f)
                        goto flo_esc_continue; /* \.{DEL} */
                else
                        goto flo_esc_dispatch;
        }
        goto flo_esc_continue;

@#
flo_esc_intermediate:
        _term_flo_transition(flo_ground, flo_esc_intermediate);
        switch (ucp_stick(bufscii)) {
        case 0:
        case 1:
                _term_exec_C0(tt, bufscii);
                goto flo_esc_intermediate;
        case 2:
                _term_collect_prefix(tt, bufscii);
                goto flo_esc_intermediate;
        case 4:
        case 5:
        case 3:
        case 6:
                goto flo_esc_dispatch;
        case 7:
                if (bufscii == 0x7f)
                        goto flo_esc_intermediate; /* \.{DEL} */
                else
                        goto flo_esc_dispatch;
        }
        goto flo_esc_continue;

@ TODO: don't call c1 here (should not be done in esc\_intermediate)
and roll the rest into the gotos above.

@.TODO@>
@<Escape (\.{ESC}) sequences@>=
flo_esc_dispatch:
        if (!tt->parser.prefix[0]
                        && (ucp_stick(bufscii) == 4 || ucp_stick(bufscii) == 5))
                _term_exec_C1(tt, bufscii);
        else
                _term_exec_ESC(tt, bufscii);
        goto flo_ground;

@ The \.{CSI} control, |0x9b| or \.{ESC [}, is known as Control
Sequence Introducer and begins a sequence similar in spirit to an
escape sequence: an optional prefix of \.{<=>?}, zero or more numbers
separated by~\.;, an optional flag and a final operator.

The prefix and final byte determine the operation so the flag byte
is treated like a prefix.

@<Control (\.{CSI}) sequences@>=
flo_csi_entry:
        _term_reset_parse_state(tt);
flo_csi_entry_continue:
        _term_flo_transition(flo_ground, flo_csi_entry_continue);
        switch (ucp_stick(bufscii)) {
        case 0:
        case 1:
                _term_exec_C0(tt, bufscii);
                goto flo_csi_entry_continue;
        case 2:
                _term_last_arg(&tt->parser);
                _term_collect_prefix(tt, bufscii);
                goto flo_csi_intermediate;
        case 3:
                if (bufscii == 0x3a)
                        goto flo_csi_param_param_start; /* \.: */
                else if (bufscii >= 0x3c)
                        _term_collect_prefix(tt, bufscii); /* \.{<=>?} */
                else
                        _term_collect_param(tt, bufscii);
                goto flo_csi_param;
        case 7:
                if (bufscii == 0x7f)
                        goto flo_csi_entry_continue; /* \.{DEL} */
        }
        goto flo_csi_dispatch;

@#
flo_csi_ignore:
        _term_flo_transition(flo_ground, flo_csi_ignore);
        if (ucp_stick(bufscii) <= 1)
                _term_exec_C0(tt, bufscii);
        if (ucp_stick(bufscii) <= 3 || bufscii == 0x7f)
                goto flo_csi_ignore;
        goto flo_ground;

@#
flo_csi_param:
        _term_flo_transition(flo_ground, flo_csi_param);
        switch (ucp_stick(bufscii)) {
        case 0:
        case 1:
                _term_exec_C0(tt, bufscii);
                goto flo_csi_param;
        case 2:
                _term_last_arg(&tt->parser);
                _term_collect_prefix(tt, bufscii);
                goto flo_csi_intermediate;
        case 3:
                if (bufscii == 0x3a)
                        goto flo_csi_param_param_start;
                if (bufscii >= 0x3c)
                        goto flo_csi_ignore; /* \.:, \.{<=>?} */
                _term_collect_param(tt, bufscii);
                goto flo_csi_param;
        case 7:
                if (bufscii == 0x7f)
                        goto flo_csi_param; /* \.{DEL} */
        }
        _term_last_arg(&tt->parser);
        goto flo_csi_dispatch;

@#
flo_csi_intermediate:
        _term_flo_transition(flo_ground, flo_csi_intermediate);
        switch (ucp_stick(bufscii)) {
        case 0:
        case 1:
                _term_exec_C0(tt, bufscii);
                goto flo_csi_intermediate;
        case 2:
                _term_collect_prefix(tt, bufscii);
                goto flo_csi_intermediate;
        case 3:
                goto flo_csi_ignore;
        case 7:
                if (bufscii == 0x7f)
                        goto flo_csi_intermediate; /* \.{DEL} */
        } /* else fall through */

@ The main \.{CSI} section is too big to fit on a single page.

@<Control (\.{CSI}) sequences@>=
@#
flo_csi_dispatch:
        _term_exec_CSI(tt, bufscii);
        goto flo_ground;

@ This is the last and ugliest wrinkle added to the state machine
after it was first written. ECMA-48 allows control sequence parameters
to include the \.: byte giving the example of representing a decimal
point but little if anything used this feature until it was co-opted
to support a wider range of colours in the \.{SGR} control.

@<Control (\.{CSI}) sequences@>=
flo_csi_param_param_start:
        _term_last_arg(&tt->parser); /* not last; bad name */
        int n = tt->parser.narg - 1;
        array_push(&tt->parser.data, sizeof (tt->parser.arg[0]),
                &tt->parser.arg[n]); /* save the op parameter */
        tt->parser.arg[n] = -array_get_length(&tt->parser.data);
        _term_collect_byte(tt, 0, false);
        array_pop(&tt->parser.data, 1, NULL); /* push a string terminator
                                        in case there's no data to follow */
        goto flo_csi_param_param_continue;

@#
flo_csi_param_param_continue:
        _term_flo_transition(flo_ground, flo_csi_param_param_continue);
        switch (ucp_stick(bufscii)) {
        case 0:
        case 1:
                _term_exec_C0(tt, bufscii);
                goto flo_csi_param_param_continue;
        case 2:
                array_push(&tt->parser.data, 1, NULL); /* claim the extra 0 pushed in the last collect prefix */
                _term_collect_prefix(tt, bufscii);
                goto flo_csi_intermediate;
        case 3:
                if (bufscii >= 0x30 && bufscii <= 0x3a) {
                        _term_collect_byte(tt, bufscii, false);
                        goto flo_csi_param_param_continue;
                } else if (bufscii == 0x3b) {
                        array_push(&tt->parser.data, 1, NULL); /* claim the extra 0 pushed in the last collect prefix */
                        SET_FLAG(tt->parser.flags, TERM_ACTION_FIRST_DIGIT);
                        goto flo_csi_param;
                } else /* |bufscii >= 0x3c| */
                        goto flo_csi_ignore;
        case 7:
                if (bufscii == 0x7f) {
                        array_push(&tt->parser.data, 1, NULL); /* claim the extra 0 pushed in the last collect prefix */
                        goto flo_csi_intermediate; /* \.{DEL} \AM\ |0xff| */
                }
        } /* else fall through */
        array_push(&tt->parser.data, 1, NULL); /* claim the extra 0 pushed in the last collect prefix */
        goto flo_csi_dispatch;

@ This routine increases the value of the current parameter or moves
to collecting the next parameter. DEC state, and it's generally
accepted, that 14 bits is a sufficient maximum size for each parameter
however this routine parses into a larger value (|int|) and treats
overflow of that (but {\it not\/} a number larger than 16384) as
an error.

@d _term_arg(T,N,D) (((T)->narg > (N) && (T)->arg[N]) ? (T)->arg[N] : (D))
@d _term_next_arg(T) @[ do {
        if ((T)->narg == TERM_ARG_SIZE) {
                warnx("too many arguments");
                SET_FLAG((T)->flags, TERM_ACTION_PARSE_ERROR);
        } else
                (T)->narg++;
} while (0) @]
@d _term_last_arg(T) @[ do {
        if (IS_FLAG((T)->flags, TERM_ACTION_FIRST_DIGIT) && (T)->narg) @/
                _term_next_arg(T);
        CLEAR_FLAG((T)->flags, TERM_ACTION_FIRST_DIGIT);
} while (0) @]
@c
static void
_term_collect_param (tt_term *tt,
                     uint8_t  val)
{
        assert((val >= 0x30 && val <= 0x39) || val == 0x3b);

        if (IS_FLAG(tt->parser.flags, TERM_ACTION_PARSE_ERROR))
                return;
        if (IS_FLAG(tt->parser.flags, TERM_ACTION_FIRST_DIGIT)) {
                if (val != 0x3b) {
                        if (tt->parser.narg < TERM_ARG_SIZE)
                                tt->parser.arg[tt->parser.narg] = val & 0xf;
                        CLEAR_FLAG(tt->parser.flags, TERM_ACTION_FIRST_DIGIT);
                }
                _term_next_arg(&tt->parser);
        } else {
                assert(tt->parser.narg >= 1 && tt->parser.narg <= TERM_ARG_SIZE);
                int n = tt->parser.narg - 1;
                if (val == 0x3b)
                        SET_FLAG(tt->parser.flags, TERM_ACTION_FIRST_DIGIT);
                else if (!psnip_safe_mul(&tt->parser.arg[n], tt->parser.arg[n], 10)
                                || !psnip_safe_add(&tt->parser.arg[n],
                                        tt->parser.arg[n], val & 0xf)) {
                        /* |tt->parser.arg[n] * 10 + (val & 0xf)| */
                        if (!SET_FLAG(tt->parser.flags, TERM_ACTION_PARSE_ERROR))
                                warnx("argument overflow");
                        SET_FLAG(tt->parser.flags, TERM_ACTION_PARSE_ERROR);
                }
        }
}

@ Parsing \.{DCS} parameters is substantially similar to \.{CSI}
parameters except that C0 controls are ignored and it's followed
by a string. In the control string after the introducer sequence
(\DEC{3.5.4.5}), graphic characters with the high bit set (GR) are
explicitly allowed and passed on to the consumer as-is. This doesn't
include C1 controls.

@<Device Control Strings (\.{DCS})@>=
flo_dcs_entry:
        _term_reset_parse_state(tt);
flo_dcs_entry_continue:
        _term_flo_transition(flo_ground, flo_dcs_entry_continue);
        switch (ucp_stick(bufscii)) {
        case 0:
        case 1:
                goto flo_dcs_entry_continue;
        case 2:
                _term_collect_prefix(tt, bufscii);
                goto flo_dcs_entry_continue;
        case 3:
                if (bufscii == 0x3a)
                        goto flo_dcs_ignore; /* \.: */
                else if (bufscii >= 0x3c)
                        _term_collect_byte(tt, bufscii, false); /* \.{<=>?} */
                else
                        _term_collect_param(tt, bufscii);
                goto flo_dcs_param;
        case 7:
                if (bufscii == 0x7f)
                        goto flo_dcs_entry_continue;
        }
        _term_collect_byte(tt, bufscii, false);
        goto flo_dcs_passthrough;

@#
flo_dcs_ignore:
        _term_flo_transition(flo_ground, flo_dcs_ignore);
        goto flo_dcs_ignore;

@#
flo_dcs_param:
        _term_flo_transition(flo_ground, flo_dcs_param);
        switch (ucp_stick(bufscii)) {
        case 0:
        case 1:
                goto flo_dcs_param;
        case 2:
                _term_collect_prefix(tt, bufscii);
                goto flo_dcs_intermediate;
        case 3:
                if (bufscii == 0x3a || bufscii >= 0x3c)
                        goto flo_dcs_ignore; /* \.:, \.{<=>?} */
                _term_collect_param(tt, bufscii);
                goto flo_dcs_param;
        case 7:
                if (bufscii == 0x7f)
                        goto flo_dcs_param; /* \.{DEL} */
        }
        goto flo_dcs_passthrough;

@#
flo_dcs_intermediate:
        _term_flo_transition(flo_ground, flo_dcs_intermediate);
        switch (ucp_stick(bufscii)) {
        case 0:
        case 1:
                goto flo_dcs_intermediate;
        case 2:
                _term_collect_prefix(tt, bufscii);
                goto flo_dcs_intermediate;
        case 3:
                goto flo_dcs_ignore;
        case 7:
                if (bufscii == 0x7f)
                        goto flo_dcs_intermediate; /* \.{DEL} */
        }
        goto flo_dcs_passthrough;

@#
flo_dcs_passthrough:
        _term_last_arg(&tt->parser);
        _term_flo_transition(flo_dcs_passthrough_end, flo_dcs_passthrough);
        if (bufscii != 0x7f)
                _term_collect_byte(tt, bufscii, false); /* \.{DEL} \AM\ |0xff| */
        goto flo_dcs_passthrough;
flo_dcs_passthrough_end:
        if (tt->buf[tt->at] != 0x18
                        && tt->buf[tt->at] != 0x1a)
                _term_exec_DCS(tt); /* \.{CAN}, \.{SUB} */
        goto *next;

@ An \.{OSC} includes a control string like \.{DCS} which consists
of arguments separated by~\.; and it doesn't accept numeric parameters
like a \.{CSI} sequence, so the list of arguments is re-used to
contain offsets into the string buffer of the start of each string
argument. See |OPT_BROKEN_ST| in xterm's \.{charproc.c} for more
broken termination options that this terminal does not support.

@<Operating System Command (\.{OSC}) control strings@>=
flo_osc_string:
        _term_reset_parse_state(tt);
flo_osc_string_continue:
        _term_flo_transition(flo_osc_end, flo_osc_string_continue);
        if (bufscii == 7)
                goto flo_osc_end; /* \.{BEL} */
        if (ucp_stick(bufscii) > 1)
                _term_collect_byte(tt, bufscii, true);
        goto flo_osc_string_continue;
flo_osc_end: @;
        if (tt->buf[tt->at] != 0x18
                        && tt->buf[tt->at] != 0x1a)
                _term_exec_OSC(tt); /* \.{CAN}, \.{SUB} */
        goto *next;

@ This routine expects that the high bit has already been masked
off if necessary and ensures there is always a zero byte after the
saved string (which is not included in the collected length). The
\.{OSC} control which parses text arguments uses this routine to
separate the string at \.; characters, which it replaces with a
zero byte, and record the start of each argument in |tt->parser.arg|.

@c
static void
_term_collect_byte (tt_term *tt,
                    uint8_t  val,
                    bool     arg)
{
        uint8_t zeroed[2] = { val, 0 };
        if (arg && val == 0x3b)
                zeroed[0] = 0; /* \.; */
        if (array_push(&tt->parser.data, 2, zeroed))
                array_pop(&tt->parser.data, 1, NULL);
        else
                SET_FLAG(tt->parser.flags, TERM_ACTION_PARSE_ERROR);
        if (arg && val == 0x3b) { /* \.; */
                _term_next_arg(&tt->parser);
                if (tt->parser.narg < TERM_ARG_SIZE - 1)
                        tt->parser.arg[tt->parser.narg]
                                = array_get_used(&tt->parser.data);
        }
}

@** Interpreting control sequences. If a byte doesn't represent
part of a control sequence then it's emitted for display. The byte
is always passed to the UTF-8 decoder if a sequence is in progress
to detect an incomplete sequence.

If 7-bit character sets were going to be implemented it would be
in here so a simple mapping routine is employed. A 7-bit code
contains 94 or 96 characters not the full 128 so the byte value
minus 32 is looked up in a table of 96 unicode code points. If that
value is zero (or the set is really a 94-character set) then the
byte value is used, hence the ASCII lookup table is a 94-character
set of all zeros.

TODO: character sets because it's more or less complete and not
hard.

@.TODO@>
@c
static void
_term_emit_byte (tt_term *tt,
                 uint8_t  val)
{
        int use, val7f;
        u_cp cp;

        assert(ucp_stick(val) > 1);

        if (tt->utf.status == UTFIO_PROGRESS
                        || ((val & 0x80)
                                && (IS_FLAG(tt->flags, TERM_UTF_HOST)))) {
                switch (utfio_read(&tt->utf, val)) {
                case UTFIO_PROGRESS:
                        return;
                default:
                        fcomplain("utf8: discarding %x at %02x: %s",
                                tt->utf.value, val, utfio_errormsg(&tt->utf));
                        tt->utf.value = UCP_REPLACEMENT;
                case UTFIO_COMPLETE:
                        _term_emit_cp(tt, tt->utf.value);
                        utfio_init(&tt->utf);
                        return;
                }
        } else if (IS_FLAG(tt->flags, TERM_UTF_HOST)) { /* never use GL/GR
                                                        if unicode is expected */
                if (val)
                        _term_emit_cp(tt, val);
        } else {
                use = (val & 0x80) ? tt->use_gr : tt->use_gl;
                val7f = val & 0x7f;
                assert(val7f >= 0x20); /* nb.~|0x20| and |0x7f| are possible! */
                if ((val7f == 0 || val7f == 0x7f) && !tt->gr96[use])
                        cp = 0;
                else if (!(cp = tt->grachar[use]->codepoint[val7f - 0x20]))
                        cp = val7f;
                if (cp)
                        _term_emit_cp(tt, cp);
        }
}

@ When the terminal is ready to print a new code point the first
and likely simplest problem is Automatic Margins or Auto Wrap Mode.
Turtle follows the procedure outlined in \DEC{D.6} which states
that the cursor advances after a character is inserted unless it
is already in the last column. If in the last column and Auto Wrap
Mode is set then the Last Column Flag is set. If that flag is set
when entering a character the active position will ``advance to the
first column of the next line PRIOR to entering that character'',
and reset the flag. If Auto Wrap Mode is not set then the new code
point replaces the previous code point.

Other actions also reset (or save \AM\ restore) the Last Column
Flag and are also listed (non exhaustively?) in \DEC{D.6}.

Insert/Replace Mode (\.{IRM}) is described at \DECp{5-138}. It does
what it sounds like: pushes characters to the right of the cursor.

Even if |cp| looks like a control code, it's not. Control codes
have all been handled and although unicode code points can have the
same values as C1 (and C0) controls, the byte on the line that
represented this code point were not.

TODO: |panel_put_here| must return a value to indicate whether the
rendered glyph was single or double width. Also need to determine
what to do if that would fall off the edge.

@c
static void
_term_emit_cp (tt_term *tt,
               u_cp     cp)
{
        int16_t row, col, w, h, t, r, b, l;

        assert(!ucp_isnonchar(cp)); /* Is |0xfffe| or |0xffff| in its plane? */
        assert(!ucp_isnonbmp(cp)); /* Invalid region within the BMP? */
        assert(!ucp_isnonrange(cp)); /* Outside \.0--|0x10ffff|? */
        assert(!ucp_issurrogate(cp)); /* Is a surrogate pair half? */
        assert(cp >= 0x20 && cp != 0x7f);
        tt_panel_get_size(tt->panel, &w, &h, &t, &r, &b, &l);
        tt_panel_cursor_get_pos(tt->panel, &col, &row);
        if (IS_FLAG(tt->flags, TERM_AUTO_WRAP)
                        && IS_FLAG(tt->flags, TERM_LAST_COLUMN)) {
                assert(col == r);
                col = l;
                if (row < b)
                        tt_panel_cursor_set_pos(tt->panel, l, ++row);
                else {
                        tt_panel_cursor_set_pos(tt->panel, l, 0);
                        tt_panel_blit(tt->panel, l, t + 1, l, t,
                                r - (l - 1), b - t);
                        tt_panel_clear_region(tt->panel, l, row,
                                r - (l - 1), 1, 0);
                }
                CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
        }

        if (col == r) {
                tt_panel_put_here(tt->panel, cp);
                if (IS_FLAG(tt->flags, TERM_AUTO_WRAP))
                        SET_FLAG(tt->flags, TERM_LAST_COLUMN);
                // if double width glyph, insert gap and wrap, or squeeze glyph in half
        } else {
                if (IS_FLAG(tt->flags, TERM_INSERT_REPLACE)) {
                        int16_t right = (col > r) ? w : r;
                        tt_panel_blit(tt->panel, col, row, col + 1, row,
                                right - col, 1);
                }
                tt_panel_put_here(tt->panel, cp);
                tt_panel_cursor_set_pos(tt->panel, col + 1, 0);
        }
}

@ Parsing \.{ESC} sequences is mostly described in ECMA-35 with
some details from DEC. In particular although the final byte
determines what the sequence is, the choice of final byte is
restricted by the first byte following the \.{ESC}. Recall that C0
controls, |0x7f| and bytes with the high bit set have all been
handled by the parser state machine and will not reach here.

This section needs a lot of work because it's mostly concerned with
unimplemented character sets and only the controls to switch in and
out of UTF-8 mode and some others are correctly implemented.

A first byte that begins with 2 mostly concern character or code
sets (eg.~shifting) but see below.

A first byte beginning with 3 designates a private control function.

A first byte beginning with 4 or 5 is a C1 control.

If the first byte begins 6 or 7 (excluding |0x7f|) that's a final
byte and the sequence is complete.

This is the table of \.{ESC} sequences with a 2-byte with corresponding
labels from ECMA-35 table 3.b:

 0F \.{ESC SP}: Announce code structure (15.2)

 1F \.{ESC  !}: Designate C0

 2F \.{ESC  "}: Designate C1

 3F \.{ESC \#}: Private control

 4F \.{ESC \$}: Multi-byte character set

 5F \.{ESC \%}: Designate other coding system

 6F \.{ESC \&}: Identify revised registration

 7F \.{ESC  '}: Reserved

 8F \.{ESC  (}: Designate G0-94

 9F \.{ESC  )}: Designate G1-94

10F \.{ESC  *}: Designate G2-94

11F \.{ESC  +}: Designate G3-94

12F \.{ESC  ,}: Reserved

13F \.{ESC  -}: Designate G1-96

14F \.{ESC  .}: Designate G2-96

15F \.{ESC  /}: Designate G3-96

Multi-byte character set (\.{ESC \$}) does not refer to a UTF
encoding but the legacy concept of multiple sets of 94 or 96 character
Gn sets. Like Xterm this terminal specifies UTF-8 encoding with the
standard method of designating a coding system incompatible with
ECMA-35, \.{DOCS} (\.{ESC \%}).

The final byte for each of 1F, 2F, 8-11F and 13-15F (and the related
functions included in 4F) comes from a different namespace for each
so despite appearances there is no possibility of the character
sets overlapping (although of course they may be defined in terms
of each other).

The meaning of \.{ESC SP F} and \.{ESC SP G}, toggling sending of
8-bit C1 controls, comes from \ECMA{35}{15.2.2} \.{ACS}.

\ECMA{35}{15.4} Designate Other Coding System (\.{DOCS}). The other
coding system understood by this terminal is unicode in the form
of UTF-8.

Another (or ``an other'') coding system is designated by \.{ESC \%}
followed by the system designation of one or two bytes. A \./ before
the final byte indicates that the method of returning to an ECMA-35
coding system is {\it not\/} \.{ESC \% @@} (and that whatever does
won't restore the state it was in). It's not clear where \.G to
mean UTF-8 came from (because I haven't looked for it; registered
with ISO-2375?).

TODO: The escape sequence parser collects intermediate bytes as
prefix bytes of which there can be a maximum of two, making for
escape sequences up to four bytes long (including \.{ESC}). Is this
enough?

@.TODO@>

@ A C1 control only performs its action if there were no prefix bytes.

\ECMA{6}{9.2} ASCII IRV is designated by:

C0: ESC ! @ or ESC ! ~ if absent.
G0: ESC ( B

BUT this cannot be sent per DOCS.

@.TODO@>
@d _p1(O,P)   ((long) (O) | ((P) << 8))
@d _p2(O,P,S) ((long) (O) | ((P) << 8) | ((S) << 16))
@c
static void
_term_exec_ESC (tt_term *tt,
                uint8_t  final)
{
        assert(final >= 0x20);
        assert(tt->parser.prefix[0] || final < 0x40 || final > 0x5f);
        assert(final < 0x7f);
        if (IS_FLAG(tt->parser.flags, TERM_ACTION_PARSE_ERROR) || tt->parser.prefix[1])
                return;

        if (tt->parser.prefix[0])
                assert(ucp_stick(tt->parser.prefix[0]) == 2);
        switch (tt->parser.prefix[0]) {
        case 0x20: /* \.{SP}, \.{ACS}, \ECMA{35}{15.2.2} */
                if (!tt->parser.prefix[1] && (final == 'F' || final == 'G')) {
                        tt->parser.mode = (final == 'G');
                        term_exec_SnC1T(tt, &tt->parser);
                }
                return;
        case 0x23: /* \.\#, Private use */
                switch (final) {
                case '3': @; /* \.{DECDHL}, double-height (top). */
                case '4': @; /* \.{DECDHL}, double-height (bottom). */
                case '5': @; /* \.{DECSWL}, single-width. */
                case '6': @; /* \.{DECDWL}, double-width. */
                        return;
                case '8':
                        term_exec_DECALN(tt, &tt->parser);
                        return;
                default:
                        return;
                }
        case 0x25:
                tt->parser.mode = final; /* \.\% */
                term_exec_DOCS(tt, &tt->parser);
                return;
        case 0:
                break; /* no prefix byte */
        default:
                return;
        }

        switch (final) {
        case '7':
                term_exec_DECSC(tt, &tt->parser);
                return;
        case '8':
                term_exec_DECRC(tt, &tt->parser);
                return;
        case '=':
                term_exec_DECKPAM(tt, &tt->parser);
                return;
        case '>':
                term_exec_DECKPNM(tt, &tt->parser);
                return;

/* TODO: These do not have prefix bytes (from ctlseqs):
ESC 6     Back Index (DECBI), VT420 and up.
ESC 9     Forward Index (DECFI), VT420 and up.
ESC =     Application Keypad (DECKPAM).
ESC \.>   Normal Keypad (DECKPNM), VT100.
ESC F     Cursor to lower left corner of screen.  This is enabled by the hpLowerleftBugCompat resource.
ESC c     Full Reset (RIS), VT100.
ESC l     Memory Lock (per HP terminals).  Locks memory above the cursor.
ESC m     Memory Unlock (per HP terminals).
ESC n     Invoke the G2 Character Set as GL (LS2).
ESC o     Invoke the G3 Character Set as GL (LS3).
ESC \pipe/ Invoke the G3 Character Set as GR (LS3R).
ESC \.\}  Invoke the G2 Character Set as GR (LS2R).
ESC \.\~  Invoke the G1 Character Set as GR (LS1R), VT100.
*/
        default:
                return;
        }
}

@ Turtle understands these C0 controls.

@c
static void
_term_exec_C0 (tt_term *tt,
               uint8_t  val)
{
        _term_utfio_reset(tt);
        switch (val) {
        case 0x00:
                break; /* Do nothing. */
        case 0x07:
                term_exec_BEL(tt, &tt->parser); /* \.{\^G} */
                break;
        case 0x08:
                term_exec_BS(tt, &tt->parser); /* \.{\^H} */
                break;
        case 0x09:
                term_exec_HT(tt, &tt->parser); /* \.{\^I} */
                break;
        case 0x0a:
                term_exec_LF(tt, &tt->parser); /* \.{\^J} */
                break;
        case 0x0b:
                term_exec_VT(tt, &tt->parser); /* \.{\^K} */
                break;
        case 0x0c:
                term_exec_FF(tt, &tt->parser); /* \.{\^L} */
                break;
        case 0x0d:
                term_exec_CR(tt, &tt->parser); /* \.{\^M} */
                break;
        }
}

@ Turtle understands these C1 controls in addition to the controls
which affect the parser.

@c
static void
_term_exec_C1 (tt_term *tt,
               uint8_t  val)
{
        _term_utfio_reset(tt);
        switch (val) {
        case 'D':
        case 0x84:
                term_exec_IND(tt, &tt->parser);
                break;
        case 'E':
        case 0x85:
                term_exec_NEL(tt, &tt->parser);
                break;
        case 'H':
        case 0x88:
                term_exec_HTS(tt, &tt->parser);
                break;
        case 'M':
        case 0x8d:
                term_exec_RI(tt, &tt->parser);
                break;
        case 'Z':
        case 0x9a:
                term_exec_DECID(tt, &tt->parser);
                return;
        case '\\':
        case 0x9c:
                break; /* ST; handled by the lexing state machine */
        }
}

@ These are the \.{CSI} sequence final and prefix bytes.

@c
static void
_term_exec_CSI (tt_term *tt,
                uint8_t  action)
{
        int arg;
        if (IS_FLAG(tt->parser.flags, TERM_ACTION_PARSE_ERROR))
                return;
        switch (_p2(action, tt->parser.prefix[0], tt->parser.prefix[1])) {
        case '@@':
                term_exec_ICH(tt, &tt->parser); @+
                break;
        case 'A':
                term_exec_CUU(tt, &tt->parser); @+
                break;
        case 'B':
                term_exec_CUD(tt, &tt->parser); @+
                break;
        case 'C':
                term_exec_CUF(tt, &tt->parser); @+
                break;
        case 'D':
                term_exec_CUB(tt, &tt->parser); @+
                break;
        case 'H':
                term_exec_CUP(tt, &tt->parser); @+
                break;
        case 'J':
                term_exec_ED(tt, &tt->parser); @+
                break;
        case _p1('J', '?'):
                term_exec_DECSED(tt, &tt->parser); @+
                break;
        case 'K':
                term_exec_EL(tt, &tt->parser); @+
                break;
        case _p1('K', '?'):
                term_exec_DECSEL(tt, &tt->parser); @+
                break;
        case 'L':
                term_exec_IL(tt, &tt->parser); @+
                break;
        case 'M':
                term_exec_DL(tt, &tt->parser); @+
                break;
        case 'P':
                term_exec_DCH(tt, &tt->parser); @+
                break;
        case 'S':
                term_exec_SU(tt, &tt->parser); @+
                break;
        case 'T':
                term_exec_SD(tt, &tt->parser); @+
                break;
        case 'X':
                term_exec_ECH(tt, &tt->parser); @+
                break;
        case 'c':
                term_exec_DA1(tt, &tt->parser); @+
                break;
        case _p1('c', '='):
                term_exec_DA3(tt, &tt->parser); @+
                break;
        case _p1('c', '>'):
                term_exec_DA2(tt, &tt->parser); @+
                break;
        case 'd':
                term_exec_VPA(tt, &tt->parser); @+
                break;
        case 'e':
                term_exec_VPR(tt, &tt->parser); @+
                break;
        case 'f':
                term_exec_HVP(tt, &tt->parser); @+
                break;
        case 'g':
                term_exec_TBC(tt, &tt->parser); @+
                break;
        case 'h': @;
                tt->parser.mode = true;
                term_exec_SM(tt, &tt->parser);
                break;
        case _p1('h', '?'): @;
                tt->parser.mode = true;
                term_exec_DECSET(tt, &tt->parser);
                break;
        case 'k':
                term_exec_VPB(tt, &tt->parser); @+
                break;
        case 'l': @;
                tt->parser.mode = false;
                term_exec_SM(tt, &tt->parser);
                break;
        case _p1('l', '?'): @;
                tt->parser.mode = false;
                term_exec_DECSET(tt, &tt->parser);
                break;
        case 'm':
                term_exec_SGR(tt, &tt->parser); @+
                break;
        case 'n':
                term_exec_DSR(tt, &tt->parser); @+
                break;
        case _p1('n', '?'):
                term_exec_DECXCPR(tt, &tt->parser); @+
                break;

        case 'q':
                break; /* Blinkenlights! */
        case _p1('q', '>'): /* report xterm name \AM\ version */
                break;
        ; /* in theory prefix and suffix bytes are treated identically, so... */
        case _p1('q', ' '): @; /* set cursor style (vt520) */
        case _p1('q', '"'): @; /* select character protection attribute (DECSCA) */
        case _p1('q', '#'): /* pop video attributes from stack (XTPOPSGR), xterm */
                break;

        case 'r':
                term_exec_DECSTBM(tt, &tt->parser); @+
                break;
        case 's':
                if (IS_FLAG(tt->flags, TERM_LEFT_RIGHT_MARGIN))
                        term_exec_DECSLRM(tt, &tt->parser);
                else
                        return; /* SCOSC, Save Cursor (ANSI) */
                break;
        case 't': @;
                arg = _term_arg(&tt->parser, 0, 24);
                if (arg < 24)
                        term_exec_XTWINOPS(tt, &tt->parser);
                else
                        term_exec_DECSLPP(tt, &tt->parser);
                break;
        }
}

@ ECMA-48 doesn't define any device control strings. DEC does but
I can't find an authoritative list. Xterm's ctlseqs lists the
following:

\.{DCS Ps ; Ps \pipe/ Pt ST}: User-Defined Keys (DECUDK).

\.{DCS \$ q Pt ST}: Request Status String (DECRQSS), VT420 and up.

\.{DCS Ps \$ t Pt ST}: Restore presentation status (DECRSPS), VT320
and up. This is the response to \.{DECRQSS}, not sent from the host.

\.{DCS + Q Pt ST}: Request resource values (XTGETXRES), xterm.

\.{DCS + p Pt ST}: Set Termcap/Terminfo Data (XTSETTCAP), xterm.

\.{DCS + q Pt ST}: Request Termcap/Terminfo String (XTGETTCAP), xterm.

From the parser: Stick 2 is optional prefix bytes. Stick 3 is
optional parameters. Sticks 0-2 and 4-7 except essential C0s and
DEL are collected except that the first byte must be sticks 4-7.

@c
static void
_term_exec_DCS (tt_term *tt)
{
        uint8_t action;

        if (IS_FLAG(tt->parser.flags, TERM_ACTION_PARSE_ERROR)
                        || !array_get_length(&tt->parser.data))
                return;
        action = *((char *) array_ref(&tt->parser.data, 0));
        switch (_p2(action, tt->parser.prefix[0], tt->parser.prefix[1])) {
        case _p1('Q', '+'):
                term_exec_XTGETXRES(tt, &tt->parser); @+
                break;
        case _p1('q', '$'):
                term_exec_DECRQSS(tt, &tt->parser); @+
                break;
        case _p1('q', '+'):
                term_exec_XTGETTCAP(tt, &tt->parser); @+
                break;
        case '|': break; /* \.{DECUDK} not implemented */
        case _p1('p', '+'):
                term_exec_XTSETTCAP(tt, &tt->parser); @+
                break;
        }
}

@ |array_ref(&tt->parser.data, tt->parser.arg[n])| are |NULL|-terminated
strings which should only consist of digits which describe the
function to perform.

According to vt100.net vt520/525 terminals recognise OSC strings
for \.{DECSIN} and \.{DECSWT}. Xterm lists one:

\.{OSC Ps ; Pt ST} (also terminated by \.{BEL}) named Set Text
Parameters but notes that some controls also return information.

Xterm responds with the same terminator the host sent but note that
every C1 will terminate the control string, not just ST.

DECSIN permits up to 12 characters: \.{OSC 2 L ; Pt ST}.

DECSWT permits up to 30 characters: \.{OSC 2 1 ; Pt ST}.

The function below was hacked together with ctlseqs open on the
side. It probably interprets the controls listed there but might
not stay in this form.

Nothing is implemented.

@c
static void
_term_exec_OSC (tt_term *tt)
{
        size_t len;
        char *p;

        if (!tt->parser.narg) {
                warnx("OSC has no arguments");
                return;
        }
        p = array_ref(&tt->parser.data, tt->parser.arg[0]);
        len = strnlen(p, 4);
        if (!len || len == 4)
                goto invalid_code;
        switch(p[0]) {
        case '0':
                if (len != 1)
                        goto invalid_code;
                /* 1 arg; set icon name \AM\ window title */
                break;
        case '1':
                if (len == 1) { /* \.1 */
                        /* 1 arg; set icon name */
                } else if (len == 2) { /* \.{10}--\.{19} */
                        /* 1 arg; change a colour */
                } else if (p[1] == '1') { /* \.{110}--\.{119} */
                        /* 0 arg; reset a colour */
                } else if (p[1] == '0' && p[2] == '4') { /* \.{104} */
                        /* 1 arg; reset chosen colour */
                } else if (p[1] == '0' && p[2] == '5') { /* \.{105} */
                        /* 1 arg; reset chosen special colour */
                } else if (p[1] == '0' && p[2] == '6') { /* \.{106} */
                        /* 2 arg; enable/disable chosen special colour */
                } else
                        goto invalid_code;
                break;
        case '2':
                if (len == 3)
                        goto invalid_code;
                else if (len == 2 && p[1] != '2')
                        goto invalid_code;
                else if (len == 2) { /* \.{22} */
                        /* 1 arg; change pointer chape */
                } else { /* \.2 */
                        /* 1 arg; set window title */
                }
                break;
        case '3':
                if (len != 1)
                        goto invalid_code;
                /* 1 arg; set/clear X property */
                break;
        case '4':
                if (len == 3)
                        goto invalid_code;
                else if (len == 2 && p[1] != '6')
                        goto invalid_code;
                else if (len == 2) { /* \.{46} */
                        /* 1 arg; change log file */
                } else { /* \.4 */
                        /* x2 arg; change colour arg1 to spec arg2
                                or reply 'with a control sequence
                                of the same form which can be used
                                to set the corresponding color' if
                                arg2 is '?' */
                        /* if arg1 > MAXCOLS (88 or 256), is equivalent to \.5 with |arg1-MAXCOLS| */
                }
                break;
        case '5': /* or 50, 51, 52 */
                if (len == 3)
                        goto invalid_code;
                else if (len == 2) {
                        switch (p[1]) {
                        case '0': /* \.{50} */
                                /* 1 arg; set font */
                                break;
                        case '1': /* \.{51} */
                                /* emacs breakage */
                                break;
                        case '2': /* \.{52} */
                                /* 2 arg; selection */
                                break;
                        default:
                                goto invalid_code;
                        }
                } else { /* \.5 */
                        /* x2 arg; as \.4 but change special colour 0--4 */
                }
                break;
        case '6': /* or 60, 61 */
                if (len == 3)
                        goto invalid_code;
                else if (len == 2) {
                        switch (p[1]) {
                        case '0': /* \.{60} */
                                /* 0 arg; query allowed features */
                                break;
                        case '1': /* \.{61} */
                                /* 0 arg; query disallowed features */
                                break;
                        default:
                                goto invalid_code;
                        }
                } else { /* \.6 */
                        /* equivalent to \.{106} */
                }
                break;

        /* ctlseqs indicates that these three are ``sun shelltool, cde dtterm'': */
        case 'I': /* capital eye */
                if (len != 1)
                        goto invalid_code;
                /* set icon to file */
                break;
        case 'l': /* lower ell */
                if (len != 1)
                        goto invalid_code;
                /* set window title */
                break;
        case 'L': /* capital ell */
                if (len != 1)
                        goto invalid_code;
                /* set icon label */
                break;
        default:
invalid_code:
                warnx("invalid OSC code");
                return;
        }
}

@** Display Control Functions. This section defines the control
functions described by DEC-STD-070 sections 3--6 and D, mostly
in the order they're included in that document. Following the DEC
controls come the controls defined later (mostly Xterm).

These are the controls that this terminal can perform.

@<Pub...@>=
TTAPI void term_exec_BEL (tt_term *, tt_action *);
TTAPI void term_exec_BS (tt_term *, tt_action *);
TTAPI void term_exec_CPR (tt_term *, tt_action *);
TTAPI void term_exec_CR (tt_term *, tt_action *);
TTAPI void term_exec_CUB (tt_term *, tt_action *);
TTAPI void term_exec_CUD (tt_term *, tt_action *);
TTAPI void term_exec_CUF (tt_term *, tt_action *);
TTAPI void term_exec_CUP (tt_term *, tt_action *);
TTAPI void term_exec_CUU (tt_term *, tt_action *);
TTAPI void term_exec_DA1 (tt_term *, tt_action *);
TTAPI void term_exec_DA2 (tt_term *, tt_action *);
TTAPI void term_exec_DA3 (tt_term *, tt_action *);
TTAPI void term_exec_DCH (tt_term *, tt_action *);
TTAPI void term_exec_DECALN (tt_term *, tt_action *);
TTAPI void term_exec_DECAWM (tt_term *, tt_action *);
TTAPI void term_exec_DECBI (tt_term *, tt_action *);
TTAPI void term_exec_DECCKM (tt_term *, tt_action *);
TTAPI void term_exec_DECCOLM (tt_term *, tt_action *);
TTAPI void term_exec_DECFI (tt_term *, tt_action *);
TTAPI void term_exec_DECID (tt_term *, tt_action *);
TTAPI void term_exec_DECKPAM (tt_term *, tt_action *);
TTAPI void term_exec_DECKPNM (tt_term *, tt_action *);
TTAPI void term_exec_DECLRMM (tt_term *, tt_action *);
TTAPI void term_exec_DECOM (tt_term *, tt_action *);
TTAPI void term_exec_DECRC (tt_term *, tt_action *);
TTAPI void term_exec_DECRQSS (tt_term *, tt_action *);
TTAPI void term_exec_DECSC (tt_term *, tt_action *);
TTAPI void term_exec_DECSCNM (tt_term *, tt_action *);
TTAPI void term_exec_DECSCPP (tt_term *, tt_action *);
TTAPI void term_exec_DECSED (tt_term *, tt_action *);
TTAPI void term_exec_DECSEL (tt_term *, tt_action *);
TTAPI void term_exec_DECSET (tt_term *, tt_action *);
TTAPI void term_exec_DECSLPP (tt_term *, tt_action *);
TTAPI void term_exec_DECSLRM (tt_term *, tt_action *);
TTAPI void term_exec_DECSTBM (tt_term *, tt_action *);
TTAPI void term_exec_DECTCEM (tt_term *, tt_action *);
TTAPI void term_exec_DECXCPR (tt_term *, tt_action *);
TTAPI void term_exec_DL (tt_term *, tt_action *);
TTAPI void term_exec_DOCS (tt_term *, tt_action *);
TTAPI void term_exec_DSR (tt_term *, tt_action *);
TTAPI void term_exec_ECH (tt_term *, tt_action *);
TTAPI void term_exec_ED (tt_term *, tt_action *);
TTAPI void term_exec_EL (tt_term *, tt_action *);
TTAPI void term_exec_FF (tt_term *, tt_action *);
TTAPI void term_exec_HT (tt_term *, tt_action *);
TTAPI void term_exec_HTS (tt_term *, tt_action *);
TTAPI void term_exec_HVP (tt_term *, tt_action *);
TTAPI void term_exec_ICH (tt_term *, tt_action *);
TTAPI void term_exec_IL (tt_term *, tt_action *);
TTAPI void term_exec_IND (tt_term *, tt_action *);
TTAPI void term_exec_IRM (tt_term *, tt_action *);
TTAPI void term_exec_LF (tt_term *, tt_action *);
TTAPI void term_exec_LNM (tt_term *, tt_action *);
TTAPI void term_exec_NEL (tt_term *, tt_action *);
TTAPI void term_exec_RI (tt_term *, tt_action *);
TTAPI void term_exec_SCS (tt_term *, tt_action *);
TTAPI void term_exec_SD (tt_term *, tt_action *);
TTAPI void term_exec_SGR (tt_term *, tt_action *);
TTAPI void term_exec_SM (tt_term *, tt_action *);
TTAPI void term_exec_SU (tt_term *, tt_action *);
TTAPI void term_exec_SnC1T (tt_term *, tt_action *);
TTAPI void term_exec_TBC (tt_term *, tt_action *);
TTAPI void term_exec_VPA (tt_term *, tt_action *);
TTAPI void term_exec_VPB (tt_term *, tt_action *);
TTAPI void term_exec_VPR (tt_term *, tt_action *);
TTAPI void term_exec_VT (tt_term *, tt_action *);
TTAPI void term_exec_XTGETTCAP (tt_term *, tt_action *);
TTAPI void term_exec_XTGETXRES (tt_term *, tt_action *);
TTAPI void term_exec_XTSETTCAP (tt_term *, tt_action *);
TTAPI void term_exec_XTWINOPS (tt_term *, tt_action *);

@ \.{S7C1T}. \DECp{3-71}, \DEC{B-12}. Select 7-bit C1 Transmission.
\.{ESC SP F}.

\.{S8C1T}. \DECp{3-72}, \DEC{B-12}. Select 8-bit C1 Transmission.
\.{ESC SP G}.

This escape sequence is also described in \ECMA{35}{15.2.2} as part
of \.{ACS} Announce Code Structure.

@c
void
term_exec_SnC1T (tt_term   *tt,
                 tt_action *act)
{
        WAVE_FLAG(tt->flags, TERM_EIGHTBIT, act->mode);
}

@ \.{SI}, \.{LS0}. \DECp{3-73}. Shift In, Locking Shift Zero. C0
|0x0f|. nb. the DEC standard incorrectly says |0x1f|.

\.{SO}, \.{LS1}. \DECp{3-74}. Shift Out, Locking Shift One. C0
|0x0e|. nb. the DEC standard incorrectly says |0x1e|.

\.{LS2}. \DECp{3-75}. Locking Shift Two. \.{ESC n}.

\.{LS3}. \DECp{3-76}. Locking Shift Three. \.{ESC o}.

\.{LS1R}. \DECp{3-77}. Locking Shift One Right. \.{ESC \~}.

\.{LS2R}. \DECp{3-78}. Locking Shift Two Right. \.{ESC \}}.

\.{LS3R}. \DECp{3-79}. Locking Shift Three Right. \.{ESC \pipe/}.

\.{SS2}. \DECp{3-80}. Single Shift Two. \.{ESC N}, C1~|0x8e|.

\.{SS3}. \DECp{3-81}. Single Shift Three. \.{ESC O}, C1~|0x8f|.

None of this is implemented.

@ \.{DOCS}. \ECMA{35}{15.4}. Designate Other Coding System. \.{ESC
\% ...}. It's not clear where the choice of \.G to identify UTF-8
came from.

@c
void
term_exec_DOCS (tt_term   *tt,
                tt_action *act)
{
        assert(tt->parser.prefix[0] == '%');
        if (tt->parser.prefix[1])
                return;
        switch (act->mode) {
        case '@@': /* ECMA-35 */
                if (0) /* not implemented */
                        CLEAR_FLAG(tt->flags, TERM_UTF_HOST);
                break;
        case 'G': /* UTF-8 */
                if (0) /* not implemented */
                        SET_FLAG(tt->flags, TERM_UTF_HOST);
                break;
        }
}

@ \.{DA}/\.{DA1}. \ECMA{48}{8.3.24}, \DECp{4-17}, \DEC{B.5}. Device
Attributes (Primary). \.{CSI Ps c}. Obsoletes \.{DECID}.

\DEC{4.5} makes clear that the first parameter returned in the
response to this request indicates what the terminal is capable of
not necessarily the conformance level it's currently operating at.
The 4 in 64 indicates conformance level 4, which implies support
for \.{DECSCL}, \.{DECSR}, 11, 14 \AM\ 17. Xterm advertises 1, 2,
6, 9, 15, 18, 21 and 22 from the list at \DECp{4-19}.

Today Xterm advertises 1;2.

@c
void
term_exec_DA1 (tt_term   *tt,
               tt_action *act)
{
        if (_term_arg(act, 0, 0))
                return;
        _term_init_reply(tt, act, TERM_ANSI_CSI, "?", 'c');
        act->arg[act->narg++] = 64; /* character cell display, level 4 */
        if (IS_FLAG(tt->flags, TERM_HOST_CAN_RESIZE))
                act->arg[act->narg++] = 1; /* 132 columns */
        act->arg[act->narg++] = 21; /* horizontal scrolling */
        act->arg[act->narg++] = 22; /* colour */
        _term_reply(tt, act, 0);
}

@ \.{DECSCL}. \DECp{4-22}. Select Conformance Level. \.{CSI Ps ;
Ps " p}.

Turtle does not conform.

TODO: But should call DECSTR?

@.TODO@>

@ \.{DA2}. \DECp{4-24}. Device Attributes (Secondary). \.{CSI > Ps
c}.

The VT terminals return a list of the product identity, firmware/software
revision level and any optional extensions.

Xterm responds with 0;393;0 where 393 is its version.

@c
void
term_exec_DA2 (tt_term   *tt,
               tt_action *act)
{
        _term_init_reply(tt, act, TERM_ANSI_CSI, ">", 'c');
        act->arg[act->narg++] = 0; /* product identification */
        act->arg[act->narg++] = 0; /* firmware/software revision */
        _term_reply(tt, act, 0);
}

@ \.{DA3}. \DECp{4-26}. Device Attributes (Tertiary). \.{CSI = c}.

Returned as a device control string (\.{DCS}). The ``unique terminal
unit ID'' returned is a string of up to 31 printable characters.
The VT420 encodes the manufacturing site code and a serial number
as 8 hex digits. Xterm doesn't respond.

The VT420's unit ID can be changed when the hardware is being
configured with the \.{DECSTUI} control \.{DCS ! \{ Pt ST}.

@c
void
term_exec_DA3 (tt_term   *tt,
               tt_action *act)
{
        _term_init_reply(tt, act, TERM_ANSI_DCS, "!", '|');
        array_push(&act->data, 9, "00000000");
        _term_reply(tt, act, 0);
}

@ \.{DECID}. \DECp{4-28}. Identify Terminal. \.{ESC Z}.

Later VT models made this control obsolete. XTerm implements it as \.{DA1}.

@c
void
term_exec_DECID (tt_term   *tt,
                 tt_action *act)
{
        memset(act, 0, sizeof (*act));
        term_exec_DA1(tt, act);
}

@ \.{DECSR}. \DECp{4-29}. Secure Reset. \.{CSI Pn + p}.

Initialises the terminal to its power-up state, erasing all host
setable state information and data. If Pn is 1--16383 a \.{DECSRC}
response is emitted, If Pn is greater than 16383 the control is
ignored.

Ignored in VT52, Tektronix emulation, printer control mode (passes
control through).

Not implemented by Xterm.

@ \.{DECSTR}. \DECp{4-31}. Soft Terminal Reset. \.{CSI ! p}.

Implemented by |ReallyReset| in Xterm's charproc.c.

DEC manual resets:

Margins, Rendition, G*, cursor save buffer, IRM, origin, AWM off
(!), character set, cursor on, key modes.

Not reset:

Column (80/132), scroll, ``screen mode'', ARM, caps, tabs, click, display data, NLM, position

Xterm implements this as \.{CPR} but precedes the answer with \.?.

@ \.{DSR}.

@c
void
term_exec_DSR (tt_term   *tt,
               tt_action *act)
{
}

@ \.{DECTCEM}. \DECp{5-21}. Text Cursor Enable Mode. \.{CSI ? 25
h}, \.{CSI ? 25 l}.

DEC suggest that when re-enabled the cursor remain off for an instant
then begin its normal blinking cycle to ensure it blinks steadily.
Terminfo \.{civis}, \.{cnorm}. Also \.{cvvis} (not implemented).

@c
void
term_exec_DECTCEM (tt_term   *tt,
                   tt_action *act)
{
        if (act->mode)
                tt_panel_set_cursor_visible(tt->panel);
        else
                tt_panel_clear_cursor_visible(tt->panel);
}

@ \.{DECSTBM}. \DECp{5-25}, \DEC{D.6}. Set Top \ and Bottom Margins.
% the extra space allows TeX to stretch the line enough to fit
\.{CSI Pt ; Pb r}. Terminfo \.{smgtb}.

@c
void
term_exec_DECSTBM (tt_term   *tt,
                   tt_action *act)
{
        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
        int top = _term_arg(act, 0, 1);
        int height = tt_panel_get_height(tt->panel);
        int bottom = _term_arg(act, 1, height);
        if (top >= bottom || bottom > height)
                return;
        tt_panel_set_margins(tt->panel, top, 0, bottom, 0);
        if (IS_FLAG(tt->flags, TERM_ORIGIN_MOTION))
                tt_panel_cursor_set_pos(tt->panel,
                        tt_panel_get_left(tt->panel), top);
        else
                tt_panel_cursor_set_pos(tt->panel, 1, 1);
}

@ \.{DECSLRM}. \DECp{5-27}. Set Left and Right Margins. \.{CSI Pl
; Pr s}. Terminfo \.{smglr}.

@c
void
term_exec_DECSLRM (tt_term   *tt,
                   tt_action *act)
{
        if (!IS_FLAG(tt->flags, TERM_LEFT_RIGHT_MARGIN))
                return;
        int left = _term_arg(act, 0, 1);
        int width = tt_panel_get_width(tt->panel);
        int right = _term_arg(act, 1, width);
        if (left >= right || right > width)
                return;
        tt_panel_set_margins(tt->panel, 0, right, 0, left);
        if (IS_FLAG(tt->flags, TERM_ORIGIN_MOTION))
                tt_panel_cursor_set_pos(tt->panel,
                        tt_panel_get_left(tt->panel),
                        tt_panel_get_top(tt->panel));
        else
                tt_panel_cursor_set_pos(tt->panel, 1, 1);
}

@ \.{DECLRMM}. \DECp{5-29}. Left/Right Margin Mode. \.{CSI ? 69 h},
\.{CSI ? 69 l}. This control doesn't have its own terminfo capability
but is used as part of the \.{smglr} capability.

Although the DEC standard implies, and its example code does, reset
the margins when \.{DECLRMM} is disabled, Xterm doesn't.

@c
void
term_exec_DECLRMM (tt_term   *tt,
                   tt_action *act)
{
        if (act->mode)
                SET_FLAG(tt->flags, TERM_LEFT_RIGHT_MARGIN);
        else {
                tt_panel_set_margins(tt->panel,
                        0, tt_panel_get_width(tt->panel), 0, 1);
                CLEAR_FLAG(tt->flags, TERM_LEFT_RIGHT_MARGIN);
        }
}

@ \.{DECOM}. \DECp{5-31}, \DEC{D.6}. Origin Mode. \.{CSI ? 6 h},
\.{CSI ? 6 l}. Whether absolute cursor positioning should refer to
the corner of the display or accomodate the margins.

@c
void
term_exec_DECOM (tt_term   *tt,
                 tt_action *act)
{
        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN); /* \DEC{D.6} */
        WAVE_FLAG(tt->flags, TERM_ORIGIN_MOTION, act->mode);
}

@ \.{DECSCLM}. \DECp{5-32}. Scrolling Mode. \.{CSI ? 4 h}, \.{CSI
? 4 l}. Slows the display's scroll rate so that it's visible but
not necessarily readable (not implemented) rather than scrolling
at the maximum possible speed.

@c
void
term_exec_DECSCLM (tt_term   *tt,
                   tt_action *act)
{
        if (act->mode)
                finform("SLOW!");
}

@ \.{IND}. \DECp{5-34} \AM\ \DEC{D.6}. Index. \.{ESC D}, C1~|0x84|.
Although the \.{IND} control has been obsolete since the middle of
the 20th century it's still implemented. DEC says that the VT100
and VT125 perform this identically to \.{LF} (ie.~obey the New Line
Mode flag).

TODO: share implementation of LF/IND and maybe CUD.

@.TODO@>
@d _term_line_width(T,L) (tt_panel_get_width((T)->panel)) /* no double-wide characters */
@c
void
term_exec_IND (tt_term   *tt,
               tt_action *act)
{
        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN); /* \DEC{D.6} */
        int16_t row, col;
        tt_panel_cursor_get_pos(tt->panel, &col, &row);
        int bottom = tt_panel_get_bottom(tt->panel);
        if (row < bottom)
                tt_panel_cursor_set_pos(tt->panel, 0, ++row);
        else if (row == bottom)
                tt_panel_vscroll(tt->panel, 1,
                        IS_FLAG(tt->flags, TERM_LEFT_RIGHT_MARGIN));
        else if (row < tt_panel_get_height(tt->panel))
                tt_panel_cursor_set_pos(tt->panel, 0, ++row);
        int max = _term_line_width(tt, row);
        if (col > max)
                tt_panel_cursor_set_pos(tt->panel, max, 0);
}

@ \.{RI}. \ECMA{48}{8.3.104}, \DECp{5-36} \AM\ \DEC{D.6}. Reverse
Index. \.{ESC M}, C1~|0x8d|.

Trivia: Although it uses the acronym \.{RI} ECMA-48 names this
control Reverse Line Feed and the Index control has been removed.

@c
void
term_exec_RI (tt_term   *tt,
              tt_action *act)
{
        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN); /* \DEC{D.6} */
        int16_t row, col;
        tt_panel_cursor_get_pos(tt->panel, &col, &row);
        int top = tt_panel_get_top(tt->panel);
        if (row > top)
                tt_panel_cursor_set_pos(tt->panel, 0, --row);
        else if (row == top)
                tt_panel_vscroll(tt->panel, -1,
                        IS_FLAG(tt->flags, TERM_LEFT_RIGHT_MARGIN));
        else if (row > 1)
                tt_panel_cursor_set_pos(tt->panel, 0, --row);
        int max = _term_line_width(tt, row);
        if (col > max)
                tt_panel_cursor_set_pos(tt->panel, max, 0);
}

@ \.{DECFI}. \DECp{5-37}. Forward Index. \.{ESC 9}. Move the cursor
right, scrolling the display if necessary.

@c
void
term_exec_DECFI (tt_term   *tt,
                 tt_action *act)
{
        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN); /* \DEC{D.6} */
        int col = tt_panel_cursor_get_col(tt->panel);
        int right = tt_panel_get_right(tt->panel);
        if (col < right)
                tt_panel_cursor_set_pos(tt->panel, col + 1, 0);
        else if (col == right)
                tt_panel_hscroll(tt->panel, -1,
                        IS_FLAG(tt->flags, TERM_LEFT_RIGHT_MARGIN));
        else if (col < tt_panel_get_width(tt->panel))
                tt_panel_cursor_set_pos(tt->panel, col + 1, 0);
}

@ \.{DECBI}. \DECp{5-39}. Back Index. \.{ESC 6}. Move the cursor
left, scrolling the display if necessary.

@c
void
term_exec_DECBI (tt_term   *tt,
                 tt_action *act)
{
        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN); /* \DEC{D.6} */
        int col = tt_panel_cursor_get_col(tt->panel);
        int left = tt_panel_get_left(tt->panel);
        if (col > left)
                tt_panel_cursor_set_pos(tt->panel, col - 1, 0);
        else if (col == left)
                tt_panel_hscroll(tt->panel, 1,
                        IS_FLAG(tt->flags, TERM_LEFT_RIGHT_MARGIN));
        else if (col > 1)
                tt_panel_cursor_set_pos(tt->panel, col - 1, 0);
}

@ \.{CUU}. \ECMA{48}{8.3.22}, \DECp{5-41} \AM\ \DEC{D.6}. Cursor
Up. \.{CSI Pn A}. Terminfo \.{cuu}, \.{cuu1}.

@c
void
term_exec_CUU (tt_term   *tt,
               tt_action *act)
{
        int n = _term_arg(act, 0, 1);
        int16_t row, col;
        tt_panel_cursor_get_pos(tt->panel, &col, &row);
        n = row - n;
        int top = tt_panel_get_top(tt->panel);
        if (row >= top && n < top)
                n = top;
        else if (row < top && n < 1)
                n = 1;

        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
        tt_panel_cursor_set_pos(tt->panel, 0, n);
        int max = _term_line_width(tt, n);
        if (col > max)
                tt_panel_cursor_set_pos(tt->panel, max, 0);
}

@ \.{CUD}. \ECMA{48}{8.3.19}, \DECp{5-43} \AM\ \DEC{D.6}. Cursor
Down. \.{CSI Pn B}. Terminfo \.{cud}, \.{cud1}.

@c
void
term_exec_CUD (tt_term   *tt,
               tt_action *act)
{
        int n = _term_arg(act, 0, 1);
        int16_t row, col;
        tt_panel_cursor_get_pos(tt->panel, &col, &row);
        n += row;
        int bottom = tt_panel_get_bottom(tt->panel);
        int height = tt_panel_get_height(tt->panel);
        if (row <= bottom && n > bottom)
                n = bottom;
        else if (row > bottom && n < height)
                n = height;

        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
        tt_panel_cursor_set_pos(tt->panel, 0, n);
        int max = _term_line_width(tt, n);
        if (col > max)
                tt_panel_cursor_set_pos(tt->panel, max, 0);
}

@ \.{CUF}. \ECMA{48}{8.3.20}, \DECp{5-45} \AM\ \DEC{D.6}. Cursor
Right. \.{CSI Pn} \.C. Terminfo \.{cuf}, \.{cuf1}.

@c
void
term_exec_CUF (tt_term   *tt,
               tt_action *act)
{
        int n = _term_arg(act, 0, 1);
        int col = tt_panel_cursor_get_col(tt->panel);
        n += col;
        int right = tt_panel_get_right(tt->panel);
        int width = tt_panel_get_width(tt->panel);
        if (col <= right && n > right)
                n = right;
        else if (col > right && n > width)
                n = width;

        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
        tt_panel_cursor_set_pos(tt->panel, n, 0);
}

@ \.{CUB}. \ECMA{48}{8.3.18}, \DECp{5-47} \AM\ \DEC{D.6}. Cursor
Left. \.{CSI Pn D}. Terminfo \.{cub}, \.{cub1}.

@c
void
term_exec_CUB (tt_term   *tt,
               tt_action *act)
{
        int n = _term_arg(act, 0, 1);
        int col = tt_panel_cursor_get_col(tt->panel);
        n = col - n;
        int left = tt_panel_get_left(tt->panel);
        if (col >= left && n < left)
                n = left;
        else if (col < left && n < 1)
                n = 1;

        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
        tt_panel_cursor_set_pos(tt->panel, n, 0);
}

@ \.{CUP}. \ECMA{48}{8.3.21}, \DECp{5-49} \AM\ \DEC{D.6}. Cursor
Position. \.{CSI Pl ; Pc H}. Terminfo \.{cup}, \.{home}.

@c
void
term_exec_CUP (tt_term   *tt,
               tt_action *act)
{
        int row = _term_arg(act, 0, 1);
        int col = _term_arg(act, 1, 1);

        int16_t w, h, t, r, b, l;
        tt_panel_get_size(tt->panel, &w, &h, &t, &r, &b, &l);

        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
        if (IS_FLAG(tt->flags, TERM_ORIGIN_MOTION)) {
                row += t - 1;
                col += l - 1;
                if (row > b)
                        row = b;
                if (col > r)
                        col = r;
        } else {
                if (row > h)
                        row = h;
                if (col > w)
                        col = w;
        }
        tt_panel_cursor_set_pos(tt->panel, col, row);
}

@ \.{HVP}. \ECMA{48}{8.3.63}, \DECp{5-51}, \DEC{D.6}. Horizontal/Vertical
Position. \.{CSI Pl ; Pc f}.

@c
void
term_exec_HVP (tt_term   *tt,
               tt_action *act)
{
        term_exec_CUP(tt, act); /* clears |TERM_LAST_COLUMN| */
}

@ \.{CPR}. \ECMA{48}{8.3.14}, \DECp{5-53}. Cursor Position Report.
\.{CSI 6 n}. Xterm assigns this to the \.{u7} terminfo capability
explaining that it's ``for the \.{tack} program''.

@c
void
term_exec_CPR (tt_term   *tt,
               tt_action *act)
{
        bool origin = IS_FLAG(tt->flags, TERM_ORIGIN_MOTION);
        _term_init_reply(tt, act, TERM_ANSI_CSI, NULL, 'R');
        act->arg[act->narg++] = tt_panel_cursor_get_row(tt->panel)
                - (origin ? tt_panel_get_top(tt->panel) - 1 : 0);
        act->arg[act->narg++] = tt_panel_cursor_get_col(tt->panel)
                - (origin ? tt_panel_get_left(tt->panel) - 1 : 0);
        _term_reply(tt, act, 0);
}

@ \.{DECXCPR}. \DECp{5-55}. Extended Cursor Position Report. \.{CSI
? 6 n}.

@c
void
term_exec_DECXCPR (tt_term   *tt,
                   tt_action *act)
{
        bool origin = IS_FLAG(tt->flags, TERM_ORIGIN_MOTION);
        _term_init_reply(tt, act, TERM_ANSI_CSI, "?", 'R');
        act->arg[act->narg++] = tt_panel_cursor_get_row(tt->panel)
                - (origin ? tt_panel_get_top(tt->panel) - 1 : 0);
        act->arg[act->narg++] = tt_panel_cursor_get_col(tt->panel)
                - (origin ? tt_panel_get_left(tt->panel) - 1 : 0);
        act->arg[act->narg++] = 1;
        _term_reply(tt, act, 0);
}

@ \.{LNM}. \DECp{5-57}. New Line Mode. \.{CSI 20 h}, \.{CSI 20 l}.
On a real terminal this control also causes the return key to
transmit the \.{CR LF} sequence when set.

DEC notes that this mode should not be used in the set state.

@c
void
term_exec_LNM (tt_term   *tt,
               tt_action *act)
{
        WAVE_FLAG(tt->flags, TERM_NEW_LINE_MODE, act->mode);
}

@ \.{CR}. \ECMA{48}{8.3.15}, \DECp{5-58} \AM\ \DEC{D.6}. Carriage
Return. C0~|0x0d| (\.{\^D}). Terminfo \.{cr}.

@c
void
term_exec_CR (tt_term   *tt,
              tt_action *act)
{
        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN); /* \DEC{D.6} */
        int left = tt_panel_get_left(tt->panel);
        if (tt_panel_cursor_get_col(tt->panel) >= left)
                tt_panel_cursor_set_pos(tt->panel, left, 0);
        else
                tt_panel_cursor_set_pos(tt->panel, 1, 0);
}

@ \.{LF}. \ECMA{48}{8.3.74}, \DECp{5-59} \AM\ \DEC{D.6}. Line Feed.
C0~|0x0a| (\.{\^J}). Terminfo \.{ind}, but see the note there
(viz.~should it be?).

TODO: share implementation of LF/IND and maybe CUD.

@.TODO@>
@c
void
term_exec_LF (tt_term   *tt,
              tt_action *act)
{
        if (!IS_FLAG(tt->flags, TERM_NEW_LINE_MODE))
                term_exec_IND(tt, act);
        else {
                term_exec_CR(tt, act);
                int row = tt_panel_cursor_get_row(tt->panel);
                int bottom = tt_panel_get_bottom(tt->panel);
                if (row < bottom)
                        tt_panel_cursor_set_pos(tt->panel, 0, row + 1);
                else if (row == bottom)
                        tt_panel_vscroll(tt->panel, 1,
                                IS_FLAG(tt->flags, TERM_LEFT_RIGHT_MARGIN));
                else if (row < tt_panel_get_height(tt->panel))
                        tt_panel_cursor_set_pos(tt->panel, 0, row + 1);
        }
}

@ \.{VT}. \ECMA{48}{8.3.161}, \DECp{5-61}. Vertical Tab. C0~|0x0b|
(\.{\^K}). A printer would usually move the cursor down in a manner
similar to horizontal tab stops.

@c
void
term_exec_VT (tt_term   *tt,
              tt_action *act)
{
        term_exec_LF(tt, act);
}

@ \.{FF}. \ECMA{48}{8.3.51}, \DECp{5-62}. Form Feed. C0~|0x0c|
(\.{\^L}). A printer might move to the next page.

@c
void
term_exec_FF (tt_term   *tt,
              tt_action *act)
{
        term_exec_LF(tt, act);
}

@ \.{BS}. \ECMA{48}{8.3.5}, \DECp{5-63} \AM\ \DEC{D.6}. Back Space.
C0~|0x08| (\.{\^H}). This is Back Space which moves the cursor
backwards one space and implies nothing about what happens to the
graphic characters under the cursor.

@c
void
term_exec_BS (tt_term   *tt,
              tt_action *act)
{
        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN); /* \DEC{D.6} */
        int col = tt_panel_cursor_get_col(tt->panel);
        if (col != tt_panel_get_left(tt->panel) && col > 1)
                tt_panel_cursor_set_pos(tt->panel, col - 1, 0);
}

@ \.{NEL}. \ECMA{48}{8.3.86}, \DECp{5-64} \AM\ \DEC{D.6}. Next Line.
\.{ESC E}, C1~|0x85|.

@c
void
term_exec_NEL (tt_term   *tt,
               tt_action *act)
{
        term_exec_CR(tt, act);
        term_exec_IND(tt, act);
}

@ \.{TBC}. \ECMA{48}{8.3.154}, \DECp{5-66}. Tabulation Clear. \.{CSI
Ps g}. Terminfo \.{tbc}. Also \.{it}. DEC (\AM\ xterm) implement 0
and 3; current position and all tabs. Other parameters refer to
vertical tab stops (ECMA-48). Oddly there is no way to reset to the
terminal's default tab stops.

@c
void
term_exec_TBC (tt_term   *tt,
               tt_action *act)
{
        for (int n = 0; n < (act->narg ? act->narg : 1); n++) {
                switch (_term_arg(act, n, 0)) {
                case 0: /* clear from current column */
                        tt_panel_remove_tabstop(tt->panel,
                                tt_panel_cursor_get_col(tt->panel));
                        break;
                case 3: /* clear all */
                        tt_panel_remove_tabstop(tt->panel, 0);
                        break;
                }
        }
}

@ \.{HT}. \ECMA{48}{8.3.60}, \DECp{5-67}. Horizontal Tab. C0~|0x09|
(\.{\^I}). Terminfo \.{ht}.

TODO: What to do if there are no more tab stops?

TODO: The ANSI terminfo description uses \.{CSI I} which ctlseqs
calls \.{CHT} (with Ps).

@.TODO@>
@c
void
term_exec_HT (tt_term   *tt,
              tt_action *act)
{
        int col = tt_panel_cursor_get_col(tt->panel);
        int right = tt_panel_get_right(tt->panel);
        int to = tt_panel_find_tabstop(tt->panel, col);
        if (to <= right)
                tt_panel_cursor_set_pos(tt->panel, to, 0);
        else if (col <= right)
                tt_panel_cursor_set_pos(tt->panel, right, 0);
        else {
                assert(to > tt_panel_get_width(tt->panel)); /* TODO: No? */
                tt_panel_cursor_set_pos(tt->panel,
                        tt_panel_get_width(tt->panel), 0); /* last column flag? */
        }
}

@ \.{HTS}. \ECMA{48}{8.3.62}, \DECp{5-69}. Horizontal Tabulation Set.
\.{ESC H}, C1~|0x88|.

@c
void
term_exec_HTS (tt_term   *tt,
               tt_action *act)
{
        tt_panel_insert_tabstop(tt->panel, tt_panel_cursor_get_col(tt->panel));
}

@ \.{DECCOLM}. \DECp{5-71}, \DEC{D.6}. Column Mode. \.{CSI ? 3 h},
\.{CSI ? 3 l}. There are no capabilities in terminfo corresponding
to this feature but it's assumed to be present.

@c
void
term_exec_DECCOLM (tt_term   *tt,
                   tt_action *act)
{
        tt_action zero = {0};
        if (!IS_FLAG(tt->flags, TERM_HOST_CAN_RESIZE))
                return;
        SET_FLAG(tt->flags, TERM_LEFT_RIGHT_MARGIN);
        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
        term_exec_CUP(tt, &zero);
        int w = act->mode ? 132 : 80;
        int h = 24;
        if (!tt_panel_resize(tt->panel, w, h, false))
                term_exec_ED(tt, &zero);
}

@ \.{DECSCPP}. \DECp{5-73}. Set Columns Per Page. \.{CSI Pn \$ \pipe/}.

DEC does not mention tab stops. It's probable that as implementations
would have had some sort of maximum width that tab stops would be
set up to that width but turtle has no such maximum and if the
terminal is enlarged then tabbing past the right of the previous
width would move the cursor to the right margin.

When the width of the terminal changes the tab stops are reset to
every 8 characters.

@c
void
term_exec_DECSCPP (tt_term   *tt,
                   tt_action *act)
{
        if (!IS_FLAG(tt->flags, TERM_HOST_CAN_RESIZE))
                return;
        int cols = _term_arg(act, 0, 80);
        if (cols < TTP_MIN_WIDTH || cols > TTP_MAX_WIDTH)
                return;
        if (tt_panel_resize(tt->panel, cols, 0, true))
                tt_panel_reset_tabs(tt->panel, 8);
}

@ \.{DECSLPP}. \DECp{5-75}. Set Lines Per Page. \.{CSI Pn t} where
the (sole) argument is 24 or greater. Argument values below 24 have
been co-opted to perform \.{XTWINOPS}

@c
void
term_exec_DECSLPP (tt_term   *tt,
                   tt_action *act)
{
        if (!IS_FLAG(tt->flags, TERM_HOST_CAN_RESIZE))
                return;
        int rows = _term_arg(act, 0, 24);
        if (rows < TTP_MIN_HEIGHT || rows > TTP_MAX_HEIGHT)
                return;
        tt_panel_resize(tt->panel, 0, rows, true);
}

@ The rest of \DEC{5.4.6} and \DEC{5.4.7}, pagination, is not
implemented. \.{NP}--\.{PPB}.

@ \DEC{5.5}, windowing, is not implemented. \.{DECHCCM}--\.{SD}.

@ \.{DECSCNM}. \DECp{5-94}. Screen Mode. \.{CSI ? 5 h}, \.{CSI ? 5
l}.

@c
void
term_exec_DECSCNM (tt_term   *tt,
                   tt_action *act)
{
        if (act->mode)
                tt_panel_set_inverse(tt->panel);
        else
                tt_panel_clear_inverse(tt->panel);
}

@ \.{DECSWL}. \DECp{5-96}. Single-Width Line. \.{ESC \# 5}.

@ \.{DECDWL}. \DECp{5-97}. Double-Width Line. \.{ESC \# 6}.

The DEC definition of double-width lines does not allow characters
to start on an odd column of the physical display but in order to
display double-width characters in-line with single-width characters
the panel will need to be able to do so and then this control can
be implementedby using that feature.

@ \.{DECDHLT}. \DECp{5-99}. Double-Height Line Top. \.{ESC \# 3}.

\.{DECDHLB}. \DECp{5-99}. Double-Height Line Bottom. \.{ESC \# 4}.

@ \.{SGR}. \DECp{5-103}. Select Graphic Rendition. \.{CSI Ps ; ... ; Ps m}.

GRCM (ECMA-48) toggles between replacing and cumulative (doesn't
affect case 0).

ECMA-48 and ctlseqs(*) also list:

* 1: bold
* 2: faint or second colour
* 3: italic
* 4: underline x1
* 5: slow blink <150bpm
  6: fast blink >=150bpm
* 7: reverse
* 8: invisible
* 9: strikeout
 10-20: font
*21: underline x2
*22: Normal (neither bold nor faint), ECMA-48 3rd (ECMA-48 also mentions normal colour)
*23: Not italicized, ECMA-48 3rd (ECMA-48 also not fraktur/gothic (20) but)
*24: Not underlined, ECMA-48 3rd (x2 or x3)
*25: Steady (not blinking), ECMA-48 3rd.
 26: reserved by CCITT Rec. T.61
*27: Positive (not inverse), ECMA-48 3rd.
*28: Visible, i.e., not hidden, ECMA-48 3rd, VT300.
*29: Not crossed-out, ECMA-48 3rd.
*30-49: colour inc. 38/48 reserved for future defined in ISO 8613-3/CCITT Rec. T.416; not in ctlseqs
 50: undoes 26.

On 26 and 50: These are reserved by ECMA-48 for ``proportional
spacing'' as specified by CCITT Recommendation T.61. CCITT, now
ITU-T, has superceded T.61 with ....

E.3.2.2 p. 39 defines 26: ``proportional spacing character pitch
may be used'' with note ``at the recipient's option''. Refers to a
parameter value SHS to specify normal character pitch.

proportional support is indicated by a SGR with 26 only in handshake.

SHS on the next page with SVS (vertical spacing) too: character or
lines per mm. Also SPD for direction, GSM Graphic Size Modification
and SCO Character Orientation.

 51: framed
 52: encircled
 53: overlined
 54: undo 51,52
 55: undo 53
 56-59: reserved
 60-65: ideogram under/right-lining and stress

ECMA-48 goes on to define SHS as (CSI Ps SP K) in 8.3.118 which
defines the width of characters.

ctlseqs only: 90-97,100-107 duplicate 30-37,40-47.

ITU-T Rec. T.416 specifies \.: to separate parameters for SGRs 38
and 48 because SGR takes a list of attribute including 38 and 48.
These parameters take parameters of their own and those are separated
by \.:.


Also from T.416 ss. 8: Certain emphasis may be achieved by font
selection (8.2).

Judging by T.416 SGR 10-19 designate a font and disregard ``weight
and posture''. No way to invoke a non-designated font? Perhaps SGR
0?

T.416 deprecates 26/50.

T.412/ISO-8613-2 colour index (30-36 is 1-7, 37 is 0):

0 1,1,1 white
1 0,0,0 black
2 1,0,0 red
3 0,1,0 green
4 0,0,1 blue
5 1,1,0 yellow
6 1,0,1 mauve
7 0,1,1 cyan

Xterm OSC 4 (set-colour-index-to) matches the order in SGR.

\DEC{8.6.2.1} in discussing ReGIS lists (this matches the QL):

0 black
1 blue
2 red
3 magenta
4 green
5 cyan
6 yellow
7 white

Sixel (\DEC{9}) has colour introducer alt to Xterm's OSC 4 (but
also invokes that colour).

Conforming software will use colours 0--255.

9.12 intro ends with an ordered list of colours:

black
red
green
yellow
blue
magenta
cyan
white

Apart from swapping blue and yellow this is the same as the T.412 (416? -ed)
and SGR order.

CGA:

black
blue
green
cyan
red
magenta
yellow (brown)
white

@d _term_subarg_offset(A,I) (-(A)->arg[I])
@d _term_subarg_base(A,I)   (_term_subarg_offset((A),
        (I)) - sizeof ((A)->arg[I]))
@d _term_subarg_param(A,I)  (*(int *) array_ref(&(A)->data,
        _term_subarg_base((A), (I))))
@d _term_subarg_string(A,I) ((uint8_t *) array_ref(&(A)->data,
        _term_subarg_offset((A), (I))))
@c
void
term_exec_SGR (tt_term   *tt,
               tt_action *act)
{
        if (!act->narg) {
                act->arg[0] = 0;
                act->narg = 1;
        }
        for (int i = 0; i < act->narg; i++) {
                if (act->arg[i] < 0)
                switch(_term_subarg_param(act, i)) {
                case 38:
                        _term_exec_SGR_38(tt, act, i, false);
                        break;
                case 48:
                        _term_exec_SGR_38(tt, act, i, true);
                        break;
                }
                else
                switch(act->arg[i]) {
                case 0:
                        tt_panel_reset_rendition(tt->panel);
                        break;
                case 1:
                        tt_panel_set_pen_bold(tt->panel);
                        break;
                case 2:
                        tt_panel_set_pen_faint(tt->panel);
                        break;
                case 3:
                        tt_panel_set_pen_italic(tt->panel);
                        break;
                case 4:
                        tt_panel_set_pen_underline_1(tt->panel);
                        break;
                case 5:
                        tt_panel_set_pen_blink_slow(tt->panel);
                        break;
                case 6:
                        tt_panel_set_pen_blink_fast(tt->panel);
                        break;
                case 7:
                        tt_panel_set_pen_inverse(tt->panel);
                        break;
                case 8:
                        tt_panel_set_pen_hidden(tt->panel);
                        break;
                case 9:
                        tt_panel_set_pen_strikeout(tt->panel);
                        break;
                case 10: case 11: case 12: case 13: case 14:
                case 15: case 16: case 17: case 18: case 19:
                case 20:
                        tt_panel_set_font(tt->panel, act->arg[i] - 10);
                        break;
                case 21:
                        tt_panel_set_pen_underline_2(tt->panel);
                        break;
                case 22:
                        tt_panel_clear_pen_bold(tt->panel);
                        tt_panel_clear_pen_faint(tt->panel);
                        break;
                case 23:
                        tt_panel_clear_pen_italic(tt->panel);
                        break;
                case 24:
                        tt_panel_clear_pen_underline_1(tt->panel);
                        tt_panel_clear_pen_underline_2(tt->panel);
                        break;
                case 25:
                        tt_panel_clear_pen_blink_fast(tt->panel);
                        tt_panel_clear_pen_blink_slow(tt->panel);
                        break;
                case 27:
                        tt_panel_clear_pen_inverse(tt->panel);
                        break;
                case 28:
                        tt_panel_clear_pen_hidden(tt->panel);
                        break;
                case 29:
                        tt_panel_clear_pen_strikeout(tt->panel);
                        break;
                case 30: case 31: case 32: case 33:
                case 34: case 35: case 36: case 37:
                        tt_panel_mod_colour_index(tt->panel, false, act->arg[i] - 30);
                        break;
                case 38:
                        return;
                case 39:
                        tt_panel_reset_colour(tt->panel, false);
                        break;
                case 40: case 41: case 42: case 43:
                case 44: case 45: case 46: case 47:
                        tt_panel_mod_colour_index(tt->panel, true, act->arg[i] - 40);
                        break;
                case 48:
                        return;
                case 49:
                        tt_panel_reset_colour(tt->panel, true);
                        break;
                case 53:
                        tt_panel_set_pen_overline(tt->panel);
                        break;
                case 55:
                        tt_panel_clear_pen_overline(tt->panel);
                        break;
                case 90: case 91: case 92: case 93:
                case 94: case 95: case 96: case 97:
                        tt_panel_mod_colour_index(tt->panel, false, act->arg[i] - 82);
                        break;
                case 100: case 101: case 102: case 103:
                case 104: case 105: case 106: case 107:
                        tt_panel_mod_colour_index(tt->panel, true, act->arg[i] - 92);
                        break;
                }
        }
}

@ @c
static void
_term_exec_SGR_38 (tt_term   *tt,
                   tt_action *act,
                   int        arg,
                   bool       isbg)
{
        int n = 0, p[8] = {0};
        uint8_t *buf = _term_subarg_string(act, arg);

        while (*buf) {
                assert(*buf >= 0x30 && *buf <= 0x3a);
                if (*buf == 0x3a) {
                        if (++n >= 8) {
                                warnx("sgr38 excess");
                                return;
                        }
                        buf++;
                        continue;
                }
                if (!psnip_safe_mul(&p[n], p[n], 10)
                                || !psnip_safe_add(&p[n], p[n], *buf & 0xf)) {
                        /* |p[n] * 10 + (*buf & 0xf)| */
                        warnx("sgr38 overflow");
                        return;
                }
                buf++;
        }
        if (!n)
                return;

        switch (p[0]) { /* CSID, ignored, defines scale of RGBCMYK; no space for alpha channel */
        case 0: /* implementation-defined fg */
        case 1: /* transparent, no args */
        case 3: /* CMY: CSID,C,M,Y,-,tolerance */
        case 4: /* CMYK: CSID,C,M,Y,K */
                break;
        case 2: /* RGB: CSID,R,G,B,-,tolerance */
                if (p[1] || p[2] > 255 || p[3] > 255 || p[4] > 255) {
                        warnx("sgr38 invalid");
                        return;
                }
                tt_panel_mod_colour_rgba(tt->panel, isbg, p[2], p[3],
                        p[4], 255);
                break;
        case 5: /* indexed */
                if (n > 2 || p[1] > 255) {
                        warnx("sgr38 indexed invalid");
                        return;
                }
                tt_panel_mod_colour_index(tt->panel, isbg, p[1]);
                break;
        }
}

@ ...

\.{DECCTR}. Color Table Report.

\.{DECSTGLT}. Select Text/Graphics Look-Up Table.

@ \.{BEL}. \ECMA{48}{8.3.3}, \DECp{5-114}. Bell. C0~|0x07| (\.{\^G}).
Terminfo \.{bel}.

@c
void
term_exec_BEL (tt_term   *tt,
               tt_action *act)
{
        if (tt->bell)
                (tt->bell)(tt);
}

@ \DEC{5.8}, graphic character sets, is only minimally implemented
in |_term_emit_cp|. \.{SCS}, \.{DECAUPSS}, \.{DECRQUPSS}, \.{DECNRCM},


@ \DEC{5.9}. Mopping up remaining C0 \AM\ C1 controls.

ENQ
XON
XOFF
DECID

@ \.{SUB}.

@ \DEC{5.10}. Much of the functionality described here is obsolete.
Most of the rest isn't implemented.

@c
void
term_exec_SM (tt_term   *tt,
              tt_action *act)
{
        assert(!IS_FLAG(tt->parser.flags, TERM_ACTION_PARSE_ERROR));
        for (int i = 0; i < act->narg; i++) {
                switch(act->arg[i]) {
                case 4:
                        term_exec_IRM(tt, act);
                        break;
                case 20:
                        term_exec_LNM(tt, act);
                        break;
                /* vttest (also) toggles these when it starts: */
                case 2: /* \.{KAM} */
                case 12: /* \.{SRM} */
                        break;
                }
        }
}

@ \.{DECSET} is another name for the extension to \.{SM}/\.{RM}
described in \DEC{5.10.1.2} (\DECp{5-133}).

(h set, l clear)
tmux detach: 25h, DECTCEM (show cursor)
2004l, xterm, bracketed paste (surely delimited?)
1004l, xterm, send-focus-events
7727l, tmux sends if |flags & TERM_VT100LIKE|?
       opsd cvs 1.352 tty.c:

        mintty:

        Escape keycode

        There are two settings controlling the keycode sent by the ESC

        The first controls application escape key mode, where the
        escape key sends a keycode that allows applications such
        as vim to tell it apart from the escape character appearing
        at the start of many other keycodes, without resorting to
        a timeout mechanism.

         sequence   mode       keycode
         \.{CSI ?7727l}   normal         \.{ESC} or \.{FS}
         \.{CSI ?7727h}   application    \.{ESC O[}

        When application escape key mode is off, the escape key can
        be be configured to send \.{FS} instead of the standard \.{ESC}.
        This allows the escape key to be used as one of the special
        keys in the terminal line settings (as set with the
        stty
        utility).

         sequence   keycode
         \.{CSI ?7728l}   \.{ESC}
         \.{CSI ?7728h}   \.{FS}

69l DECLRMM

@c
void
term_exec_DECSET (tt_term   *tt,
                  tt_action *act)
{
        assert(!IS_FLAG(tt->parser.flags, TERM_ACTION_PARSE_ERROR));
        for (int i = 0; i < act->narg; i++) {
                switch(act->arg[i]) {
                case 3:
                        term_exec_DECCOLM(tt, act); @+
                        break;
                case 4:
                        term_exec_DECSCLM(tt, act); @+
                        break;
                case 5:
                        term_exec_DECSCNM(tt, act); @+
                        break;
                case 6:
                        term_exec_DECOM(tt, act); @+
                        break;
                case 7:
                        term_exec_DECAWM(tt, act); @+
                        break;
                case 25:
                        term_exec_DECTCEM(tt, act); @+
                        break;
                case 69:
                        term_exec_DECLRMM(tt, act); @+
                        break;
                /* vttest (also) toggles these when it starts: */
                case 1:
                        term_exec_DECCKM(tt, act); @+
                        break;
                case 8: /* \.{DECARM} --- Auto-Repeat Keys */
                        /* ignored */
                case 40: /* xterm --- Allow 80 to 132 mode */
                case 45: /* xterm --- Reverse-wraparound mode */
                        break; /* Ignored */
                }
        }
}

@ \.{IRM}. \DECp{5-138}. Insert/Replacement Mode. \.{CSI 4 h},
\.{CSI 4 l}. Terminfo \.{mir}, \.{rmir}, \.{smir} but \man5{terminfo}
pairs them with a 700 word caveat.

Xterm uses the rmir cap in xterm-basic to reset (replace) and smir
to set (insert) this mode.

The state of this flag affects character output in |_term_emit_cp|.

@c
void
term_exec_IRM (tt_term   *tt,
               tt_action *act)
{
        WAVE_FLAG(tt->flags, TERM_INSERT_REPLACE, act->mode);
}

@ \.{ICH}. \ECMA{48}{8.3.64}, \DECp{5-142}, \DEC{D.6}. Insert
Character. \.{CSI Pn @@}. Terminfo \.{ich}, \.{ich1}. DEC state in
\DEC{D.6} that this control clears the |TERM_LAST_COLUMN| flag
although the cursor does not move.

@c
void
term_exec_ICH (tt_term   *tt,
               tt_action *act)
{
        int16_t row, col;
        tt_panel_cursor_get_pos(tt->panel, &col, &row);
        int left = tt_panel_get_left(tt->panel);
        int right = tt_panel_get_right(tt->panel);
        if (col < left || col > right)
                return;
        int n = _term_arg(act, 0, 1);
        int max = right - col + 1;
        if (n > max)
                n = max;
        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
        tt_panel_blit(tt->panel, col, row, col + n, row, max - n, 1);
        tt_panel_clear_region(tt->panel, col, row, n, 1, 0);
}

@ \.{DCH}, \DEC{D.6}. \ECMA{48}{8.3.26}, \DECp{5-144}. Delete
Character. \.{CSI Pn P}. Terminfo \.{dch}, \.{dch1}. DEC state in
\DEC{D.6} that this control clears the |TERM_LAST_COLUMN| flag
although the cursor does not move.

@c
void
term_exec_DCH (tt_term   *tt,
               tt_action *act)
{
        int16_t row, col;
        tt_panel_cursor_get_pos(tt->panel, &col, &row);
        int left = tt_panel_get_left(tt->panel);
        int right = tt_panel_get_right(tt->panel);
        if (col < left || col > right)
                return;
        int n = _term_arg(act, 0, 1);
        int max = right - col + 1;
        if (n > max)
                n = max;
        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
        tt_panel_blit(tt->panel, col + n, row, col, row, max - n, 1);
        tt_panel_clear_region(tt->panel, col + max - n, row, n, 1, 0);
}

@ \.{IL}. \ECMA{48}{8.3.67}, \DECp{5-146}, \DEC{D.6}. Insert Line.
\.{CSI Pn L}. Terminfo \.{il}, \.{il1}. No need to test
|TERM_LEFT_RIGHT_MARGIN| --- if it's not set then left and right
will be the panel's extremes, which the cursor cannot go beyond.

@c
void
term_exec_IL (tt_term   *tt,
              tt_action *act)
{
        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
        int n = _term_arg(act, 0, 1);
        int max = tt_panel_get_bottom(tt->panel)
                - tt_panel_cursor_get_row(tt->panel) + 1;
        if (n > max)
                n = max;
        tt_panel_insert_line_here(tt->panel, n, true);
}

@ \.{DL}, \DEC{D.6}. \ECMA{48}{8.3.32}, \DECp{5-148}. Delete Line.
\.{CSI Pn M}. Terminfo \.{dl}, \.{dl1}.

@c
void
term_exec_DL (tt_term   *tt,
              tt_action *act)
{
        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
        int n = _term_arg(act, 0, 1);
        int max = tt_panel_get_bottom(tt->panel)
                - tt_panel_cursor_get_row(tt->panel) + 1;
        if (n > max)
                n = max;
        tt_panel_delete_line_here(tt->panel, n, true);
}

@ \.{DECIC}. \DECp{5-150}. Insert Column. \.{CSI Pn ' \}}.

\.{DECDC}. \DECp{5-151}. Delete Column. \.{CSI Pn ' \~}.

Curiously these are not called out for reseting last column mode
by \DEC{D.6}.

@ \.{ECH}. \ECMA{48}{8.3.38}, \DECp{5-152}, \DEC{D.6}. Erase Character.
\.{CSI Pn X}. Terminfo \.{ech}. This control is not affected by the
margins or insert/replace mode.

@c
void
term_exec_ECH (tt_term   *tt,
               tt_action *act)
{
        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
        int16_t row, col;
        tt_panel_cursor_get_pos(tt->panel, &col, &row);
        int n = _term_arg(act, 0, 1);
        int max = tt_panel_get_width(tt->panel) - col + 1;
        if (n > max)
                n = max;
        tt_panel_clear_region(tt->panel, col, row, n, 1, 0);
}

@ \.{EL}. \ECMA{48}{8.3.41}, \DECp{5-154}, \DEC{D.6}. Erase In Line.
\.{CSI Pn} \.{K}. Terminfo \.{el}, \.{el1}. This control is not
affected by the margins.

@c
void
term_exec_EL (tt_term   *tt,
              tt_action *act)
{
        int16_t row, col;
        tt_panel_cursor_get_pos(tt->panel, &col, &row);
        for (int n = 0; n < (act->narg ? act->narg : 1); n++) {
                switch (_term_arg(act, n, 0)) {
                case 0: /* cursor to end */
                        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
                        tt_panel_clear_region(tt->panel, col, row,
                                tt_panel_get_width(tt->panel) - col + 1, 1, 0);
                        break;
                case 1: /* cursor to beginning */
                        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
                        tt_panel_clear_region(tt->panel, 1, row, col, 1, 0);
                        break;
                case 2: /* whole line */
                        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
                        tt_panel_clear_region(tt->panel, 1, row,
                                tt_panel_get_width(tt->panel), 1, 0);
                        break;
                }
        }
}

@ \.{ED}. \ECMA{48}{8.3.39}, \DECp{5-156}, \DEC{D.6}. Erase in
Display. \.{CSI Pn J}. Terminfo \.{clear}, \.{ed}. This control is
not affected by the margins or origin mode.

@c
void
term_exec_ED (tt_term   *tt,
              tt_action *act)
{
        int16_t row, col, w, h;
        tt_panel_cursor_get_pos(tt->panel, &col, &row);
        for (int n = 0; n < (act->narg ? act->narg : 1); n++) {
                switch (_term_arg(act, n, 0)) {
                case 0: /* cursor to end */
                        tt_panel_get_size(tt->panel, &w, &h, NULL, NULL,
                                NULL, NULL);
                        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
                        tt_panel_clear_region(tt->panel, col, row,
                                w - col + 1, 1, 0);
                        if (row < h)
                                tt_panel_clear_region(tt->panel, 1, row + 1,
                                        w, h - row, 0);
                        break;
                case 1: /* cursor to beginning */
                        w = tt_panel_get_width(tt->panel);
                        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
                        tt_panel_clear_region(tt->panel, 1, row, col, 1, 0);
                        tt_panel_clear_region(tt->panel, 1, 1, w, row - 1, 0);
                        break;
                case 2: /* whole display */
                        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
                        tt_panel_clear(tt->panel, 0);
                        break;
                case 3:
                        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
                        break; /* Erase saved lines (xterm) */
                }
        }
}

@ \.{DECSEL}. \DECp{5-159}. Selective Erase In Line. \.{CSI ? Pn
K}.

\.{DECSED}. \DECp{5-162}. Selective Erase In Display. \.{CSI ? Pn
J}.

Without selectively erasable characters this is identical to \.{EL}
and \.{ED} except that they're ignored in level 1 operation, and
appear to not reset the rendition.

@c
void
term_exec_DECSEL (tt_term   *tt,
                  tt_action *act)
{
        term_exec_EL(tt, act);
}

void
term_exec_DECSED (tt_term   *tt,
                  tt_action *act)
{
        term_exec_ED(tt, act);
}

@ \DEC{5.12}, on line performance improvements, is not implemented.
\.{DECCRA}--\.{DECSACE}.

Possibly a useful interface to panel blitting.

@ \DEC{5.13}, saving and restoring state, is not implemented yet but will be.

\.{DECSC}. \DECp{5-187}. Save Cursor. \.{ESC 7}.

Save Cursor.

Saves: position, rendition, origin, G0-3 designation, GL/R invokation, colour (as rendition)

Panel has save spot for rendition, colour

Need origin flag, 6 G bytes, position

Overwrites anything that was saved previously.

@c
void
term_exec_DECSC (tt_term   *tt,
                 tt_action *act)
{
        tt_panel_cursor_get_pos(tt->panel, &tt->save.col, &tt->save.row);
        tt_panel_save_rendition(tt->panel, ~0, (tt_cell *) tt->save.pen);
        tt->save.origin = IS_FLAG(tt->flags, TERM_ORIGIN_MOTION);
}

@ \.{DECRC}. \DECp{5-189}. Restore Cursor. \.{ESC 8}.

Restore all the values saved by \.{DECSC}.

Discards active values.

TODO: Cursor Save Buffer.

Cursor is moved to within margins if origin mode is (now) set.

@c
void
term_exec_DECRC (tt_term   *tt,
                 tt_action *act)
{
        int16_t w, h, t, r, b, l, col, row;
        tt_panel_get_size(tt->panel, &w, &h, &t, &r, &b, &l);
        if (tt->save.col < l)
                col = l;
        else if (tt->save.col > r)
                col = r;
        else
                col = tt->save.col;
        if (tt->save.row < t)
                row = t;
        else if (tt->save.row > b)
                row = b;
        else
                row = tt->save.row;
        tt_panel_cursor_set_pos(tt->panel, col, row);
        tt_panel_restore_rendition(tt->panel, (tt_cell *) tt->save.pen);
        WAVE_FLAG(tt->flags, TERM_ORIGIN_MOTION, tt->save.origin);
}


@ \.{DECRQM}. Request Mode.

\.{DECRPM}. Report Mode.

@ \.{DECRQSS}. Request Selection or Setting. \.{DCS \$ q Pt ST}.

Responds with \.{DECRPSS}.

This has been implemented hackily based on the Xterm implementation
because I couldn't find the definition in DEC-STD-070.

Response is \.{DECRSPS}: \.{DCS Ps \$ t Pt ST}. Ps is 0 (valid) or
1 (invalid) request. Pt depends on the request, but is the CSI
sequence without the CSI leader which will re-apply the state queried
in the \.{DECRQSS}' Pt:

            m        SGR
            " p      DECSCL
            SP q     DECSCUSR
            " q      DECSCA
            r        DECSTBM
            s        DECSLRM
            t        DECSLPP
           \$ \pipe/ DECSCPP
           \$ \.\}   DECSASD
           \$ \.~    DECSSDT
            * \pipe/ DECSNLS

This terminal only recognises \.{DECSTBM} and \.{DECSLRM} (it's not
clear if the SLRM setting should include \.{DECLRMM}).

``The terminal does not send a data string (D...D) [Pt] to the host
when the terminal receives an invalid request''

Xterm says something completely different about Ps than the programming
manual does. It responds with a 1 which the manual says should mean
the request was invalid.

@c
void
term_exec_DECRQSS (tt_term   *tt,
                   tt_action *act)
{
        char p[2];
        if (tt->parser.narg)
                return;
        memmove(p, array_ref(&tt->parser.data, 1), 2);
        if (array_get_length(&tt->parser.data) > 3) {
missing:
                _term_init_reply(tt, act, TERM_ANSI_DCS, "1$", 't');
                array_clear(&tt->parser.data);
                /* no return */
        } else {
                _term_init_reply(tt, act, TERM_ANSI_DCS, "0$", 't');
                switch (_p1(p[0], p[1])) {
                default:
                case 'm': /* \.{SGR} */
                case _p1('"', 'p'): /* \.{DECSCL} */
                case _p1('.', 'q'): /* \.{DECSCUSR} */
                case _p1('"', 'q'): /* \.{DECSCA} */
                case 't': /* \.{DECSLPP} */
                case _p1('$', '|'): /* \.{DECSCPP} */
                case _p1('$', '}'): /* \.{DECSASD} */
                case _p1('$', '~'): /* \.{DECSSDT} */
                case _p1('*', '|'): /* \.{DECSNLS} */
                        goto missing;
                case 'r': /* \.{DECSTBM} */
                        fcomplain("ignore DECSTBM");
                        break;
                case 's': /* \.{DECSLRM} */
                        fcomplain("ignore DECSLRM");
                        break;
                }
        }
        _term_reply(tt, act, 0);
}

@
\.{DECRQPSR}. Request Presentation State Report.

\.{DECPSR}. Presentation State Report.

\.{DECCIR}. Cursor Information Report.

\.{DECTABSR}. Tabulation Stop Report.

\.{DECRSPS}. Restore Presentation State.

\.{DECRQTSR}. Request Terminal State Report.

\.{DECTSR}. Terminal State Report.

\.{DECRSTS}. Restore Terminal State.

@ \.{KAM}. \DEC{6-39}. Keyboard Action Mode. \.{CSI 2 h}, \.{CSI 2
l}.

@ \.{DECARM}. \DEC{6-40}. Auto Repeat Mode. \.{CSI ? 8 h}, \.{CSI
? 8 l}.

Ignored. Keyboard repeat is handled by X.

@ \.{DECNRCM}. \DEC{6-51}. Character Set Mode. \.{CSI ? 4 2 h},
\.{CSI ? 4 2 l}. (Whether keys are encoded as 8-bit)

@ \.{DECKBUM}. \DEC{6-54}. Keyboard Usage Mode. \.{CSI ? 6 8 h},
\.{CSI ? 6 8 l}.

@ \.{DECCKM}. \DEC{6-60}. Cursor Key Mode. \.{CSI ? 1 h}, \.{CSI ?
1 l}. Terminfo \.{rmkx}, \.{smkx}.

@c
void
term_exec_DECCKM (tt_term   *tt,
                  tt_action *act)
{
        WAVE_FLAG(tt->flags, TERM_CURSOR_APP, act->mode);
}

@ \.{DECKPAM}, \.{DECKPNM}. \DEC{6-63}. Keypad Application Mode,
Keypad Numeric Mode. \.{ESC =}, \.{ESC >}. Terminfo \.{rmkx},
\.{smkx}.

@c
void
term_exec_DECKPAM (tt_term   *tt,
                   tt_action *act)
{
        SET_FLAG(tt->flags, TERM_KEYPAD_APP);
}

void
term_exec_DECKPNM (tt_term   *tt,
                   tt_action *act)
{
        CLEAR_FLAG(tt->flags, TERM_KEYPAD_APP);
}














@ \.{DECAWM}. \DEC{D.6}. Auto Wrap Mode. \.{CSI ? 7 h}, \.{CSI ? 7
l}. Terminfo \.{rmam}, \.{smam}.

@c
void
term_exec_DECAWM (tt_term   *tt,
                  tt_action *act)
{
        CLEAR_FLAG(tt->flags, TERM_LAST_COLUMN);
        WAVE_FLAG(tt->flags, TERM_AUTO_WRAP, act->mode);
}

@ \.{DECALN}. \DECp{D-19}. Screen Alignment. \.{ESC \# 8}.

Although it's not mentioned in \DEC{D.8} (which says \.{DECALN} is
not for general use), xterm fills the display with \.E characters
which is presumably what real terminals did. DEC include an example
for how to implement the algorithm which merely empties each cell
(among others, in short it clears the screen).

It seems (from use, not reading code) that Xterm puts the characters
down using the default rendition but does not reset the rendition
state, but the \.{DECALN} example algorithm only clears non-colour
rendition. Also top/bottom margins, last column flag. However it
also says the result is implementation defined and the control is
not intended for use.

The user guide gives us the E's:

This command fills the entire screen area with uppercase Es for
screen focus and alignment. This command is used by DEC manufacturing
and Field Service personnel.

@.TODO@>
@c
void
term_exec_DECALN (tt_term   *tt,
                  tt_action *act)
{
        tt_action zero = {0};
        term_exec_CUP(tt, &zero);
        term_exec_ED(tt, &zero);
        uint8_t rendition[TT_CELL_LENGTH];
        tt_panel_save_rendition(tt->panel, ~0, (tt_cell *) rendition);
        term_exec_SGR(tt, &zero);
        // TODO: AWM?
        for (int col = 1; col <= tt_panel_get_width(tt->panel); col++)
                for (int row = 1; row <= tt_panel_get_height(tt->panel); row++)
                        tt_panel_put_at(tt->panel, 'E', col, row);
        tt_panel_restore_rendition(tt->panel, (tt_cell *) rendition);
}

@ \.{SD}. \ECMA{48}{8.3.113}. Scroll Down. \.{CSI Ps T}.

@c
void
term_exec_SD (tt_term   *tt,
              tt_action *act)
{
        tt_panel_vscroll(tt->panel, -_term_arg(act, 0, 1), false);
}

@ \.{SU}. \ECMA{48}{8.3.147}. \DECp{5-91}. Scroll Up. \.{CSI Ps S}.

@c
void
term_exec_SU (tt_term   *tt,
              tt_action *act)
{
        tt_panel_vscroll(tt->panel, _term_arg(act, 0, 1), false);
}

@ \.{VPA}. \ECMA{48}{8.3.158}. Line Position Absolute. \.{CSI Ps d}.

@c
void
term_exec_VPA (tt_term   *tt,
               tt_action *act)
{
        act->arg[0] = _term_arg(act, 0, 1);
        act->arg[1] = tt_panel_cursor_get_col(tt->panel);
        act->narg = 2;
        term_exec_CUP(tt, act);
}

@ \.{VPB}. \ECMA{48}{8.3.159}. Line Position Backward. \.{CSI Ps k}.

@c
void
term_exec_VPB (tt_term   *tt,
               tt_action *act)
{
        term_exec_CUU(tt, act);
}

@ \.{VPR}. \ECMA{48}{8.3.160}. Line Position Forward. \.{CSI Ps e}.

@c
void
term_exec_VPR (tt_term   *tt,
               tt_action *act)
{
        term_exec_CUD(tt, act);
}

@ \.{XTGETXRES}. \.{DCS + Q Pt ST}. Xterm uses this to expose its
internal state.

@c
void
term_exec_XTGETXRES (tt_term   *tt,
                     tt_action *act)
{
        if (tt->parser.narg)
                return;
        fcomplain("XTGETRES not implemented");
}

@ \.{XTGETTCAP}. \.{DCS + q Pt ST}.

@c
void
term_exec_XTGETTCAP (tt_term   *tt,
                     tt_action *act)
{
        if (tt->parser.narg)
                return;
        fcomplain("XTGETTCAP not implemented");
}

@ \.{XTSETTCAP}. \.{DCS + p Pt ST}.

@c
void
term_exec_XTSETTCAP (tt_term   *tt,
                     tt_action *act)
{
        if (tt->parser.narg)
                return;
        fcomplain("XTSETTCAP not implemented");
}

@ \.{XTWINOPS}. X-Terminal Window Operations. \.{CSI Ps ; Ps ; Ps
t} where the first/only argument is less than 24. An initial argument
of 24 or above is \.{DECSLPP}. Treating the list of operations in
ctlseqs as authoritative.

@c
void
term_exec_XTWINOPS (tt_term   *tt,
                    tt_action *act)
{
        if (tt->parser.narg > 3)
                return;
        switch (_term_arg(act, 0, 0)) {
        case 1: /* deiconify */
                if (tt->parser.narg > 1)
                        return;
                break;
        case 2: /* iconify */
                if (tt->parser.narg > 1)
                        return;
                break;
        case 3: /* move */
                if (tt->parser.narg > 3)
                        return;
                break;
#if 0
        case 4: /* resize (pixels) */
                /* Omitted parameters reuse the current height or
                        width. Zero parameters use the display's
                        height or width. */
                if (tt->parser.narg > 3)
                        return;
                int h = _term_arg(act, 2, DISPLAY_HEIGHT);
                if (tt->parser.narg < 3)
                        h = PANEL_HEIGHT; /* window border? */
                int w = _term_arg(act, 1, DISPLAY_WIDTH);
                if (tt->parser.narg < 2)
                        h = PANEL_WIDTH; /* window border? */
                break;
#endif
        case 5: /* raise */
                if (tt->parser.narg > 1)
                        return;
                break;
        case 6: /* lower */
                if (tt->parser.narg > 1)
                        return;
                break;
        case 7: /* refresh */
                if (tt->parser.narg > 1)
                        return;
                break;
        case 8: /* resize (cells) */
                /* Omitted parameters reuse the current height or
                        width. Zero parameters use the display's
                        height or width. */
                if (tt->parser.narg > 3)
                        return;
                break;
        }
}

@** Writing to the host.

@<Pub...@>=
TTAPI int term_key (tt_term *, bool, uint8_t *, size_t); // deprecate
TTAPI int term_write (tt_term *, uint8_t *, size_t); // deprecate
TTAPI int term_paste (tt_term *, uint8_t *, size_t);
TTAPI int term_sequence (tt_term *, uint8_t *, size_t);
TTAPI int term_utf8 (tt_term *, uint8_t *, size_t);

@ @<Fun...@>=
static void _term_init_reply (tt_term *, tt_action *, uint8_t, char *, int8_t);
static void _term_print_reply (tt_term *, tt_action *, int8_t);
static int _term_print_terminated_string (tt_term *, char *);
static int _term_printf (tt_term *, char *, ...);
static int _term_putc (tt_term *, uint8_t);
static void _term_reply (tt_term *, tt_action *, int8_t);
static int _term_vprintf (tt_term *, char *, va_list);

@ @<Pub...@>=
#define TERM_ANSI_CSI 0x9b
#define TERM_ANSI_DCS 0x90
#define TERM_ANSI_ESC 0x1b
#define TERM_ANSI_OSC 0x9d
#define TERM_ANSI_ST  0x9c

@ TODO: Sending 8-bit C1 controls and an 8-bit channel are not the
same (eg. utf-8).

@.TODO@>
@c
int
term_key (tt_term *tt,
          bool     meta,
          uint8_t *buf,
          size_t   len)
{
        if (!len)
                return 0;
        assert(buf);
        assert(!buf[len]);
        uint8_t b = buf[0], eb[2] = { 033, b };
        if ((b & 0xc0) == 0x80) { /* C1 (eg. sequence) */
                if (IS_FLAG(tt->flags, TERM_EIGHTBIT))
                        return term_write(tt, buf, len);
                eb[1] = (b | 0x40) & 0x7f;
                b = term_write(tt, eb, 2);
                assert(b == 2);
                return 1 + term_write(tt, buf + 1, len - 1);
        } else if (len == 1 && meta) {
                assert((b & 0x7f) == buf[0]);
                b |= 0x80;
                if (IS_FLAG(tt->flags, TERM_EIGHTBIT))
                        return term_write(tt, &b, 1);
                else
                        return term_write(tt, eb, 2);
        } else
                return term_write(tt, buf, len);
}

@ @c
int
term_write (tt_term *tt,
            uint8_t *buf,
            size_t   len)
{
        _trace_byte(tt, LIO_OUT, buf, len);
        (tt->write_fun)(buf, len, tt->write_arg);
        return len;
}

@ Change LF in the selection to CR when sending it down the wire
(TODO: except when delimited? Or another ``literal'' flag?)

TODO: don't change the incoming buffer.

@.TODO@>
@c
int
term_paste (tt_term *tt,
            uint8_t *buf,
            size_t   len)
{
        if (IS_FLAG(tt->flags, TERM_DELIMIT_PASTE)) {
                term_write(tt, "\033[200~", 6);
                term_write(tt, buf, len);
                term_write(tt, "\033[201~", 6);
        } else {
                uint8_t *b = buf;
                size_t rlen = len;
                while ((buf = memchr(b, '\n', rlen))) {
                        size_t d = buf - b;
                        term_write(tt, b, d);
                        term_write(tt, "\n", 1);
                        rlen -= d + 1;
                        b = buf + 1;
                }
                term_write(tt, b, rlen);
        }
        return len;
}

@ @c
static void
_term_init_reply (tt_term   *tt,
                  tt_action *act,
                  uint8_t    mode,
                  char      *prefix,
                  int8_t     op)
{
        if (array_get_length(&act->data))
                array_clear(&act->data);
        memset((char *) act + sizeof (act->data), 0,
                sizeof (*act) - sizeof (act->data));
        if (prefix) {
                assert(!prefix[1] || !prefix[2]);
                act->prefix[0] = prefix[0];
                act->prefix[1] = prefix[1];
        }
        act->mode = mode;
        act->reply = op;
}

@ @c
static void
_term_reply (tt_term   *tt,
             tt_action *act,
             int8_t     op)
{
        if (op)
                act->reply = op;
        _term_print_reply(tt, act, act->reply);
}

@ Never UTF-8!

@c
static void
_term_print_reply (tt_term   *tt,
                   tt_action *act,
                   int8_t     op)
{
        uint8_t mode;
        assert(op);
        mode = act->mode;
        if ((mode & 0x80) && !IS_FLAG(tt->flags, TERM_EIGHTBIT)) {
                _term_putc(tt, TERM_ANSI_ESC);
                mode = (mode & 0x7f) | 0x40;
        }
        _term_putc(tt, mode);
        if (act->prefix[0])
                _term_putc(tt, act->prefix[0]);
        if (act->prefix[1])
                _term_putc(tt, act->prefix[1]);
        for (int i = 0; i < act->narg; i++) {
                _term_printf(tt, "%u", act->arg[i]);
                if (i < act->narg - 1)
                        _term_putc(tt, ';');
        }
        _term_putc(tt, op);
        if (array_get_length(&act->data))
                _term_print_terminated_string(tt, array_base(&act->data));
}

@ @c
static int
_term_print_terminated_string (tt_term *tt,
                               char    *str)
{
        int r = -1;
        size_t len;

        len = strlen(str);
        _trace_byte(tt, LIO_OUT, (uint8_t *) str, len);
        (tt->write_fun)(str, len, tt->write_arg);
        if (!IS_FLAG(tt->flags, TERM_EIGHTBIT)) {
                _term_putc(tt, TERM_ANSI_ESC);
                _term_putc(tt, (TERM_ANSI_ST & 0x7f) | 0x40);
        } else
                _term_putc(tt, TERM_ANSI_ST);
        return len;
}

@ @c
static int
_term_printf (tt_term *tt,
              char    *fmt, ...)
{
        int ret;
        va_list ap;

        va_start(ap, fmt);
        ret = _term_vprintf(tt, fmt, ap);
        va_end(ap);
        return (ret);
}

static int
_term_vprintf (tt_term *tt,
               char    *fmt,
               va_list  ap)
{
        char lame[10240];
        size_t len;
        _trace_byte(tt, LIO_OUT, (uint8_t *) lame,
                (len = vsnprintf(lame, 10240, fmt, ap)));
        (tt->write_fun)(lame, len, tt->write_arg);
        return -1;
}

static int
_term_putc (tt_term *tt,
            uint8_t  byte)
{
        _trace_byte(tt, LIO_OUT, &byte, 1);
        (tt->write_fun)(&byte, 1, tt->write_arg);
        return 1;
}

@** Tracing I/O.

This is dangerous, keyboard input is logged.

@<Type def...@>=
enum { LIO_NONE, LIO_IN, LIO_OUT };

@ @<Pub...@>=
TTAPI void term_trace_start (tt_term *, char *);
TTAPI void term_trace_stop (tt_term *);

@ @<Fun...@>=
static void _trace_byte (tt_term *, int, uint8_t *, size_t);

@ @c
void
term_trace_start (tt_term *tt,
                  char    *filename)
{
#ifdef TT_DEBUG
        char defaultfn[PATH_MAX + 1];
        FILE *newfs;

        if (tt->trace_fh)
                return;
        if (filename == NULL)
                snprintf((filename = defaultfn), PATH_MAX,
                        "turtle.%u.%u.log", getuid(), getpid());
        newfs = fopen(filename, "a");
        if (!newfs)
                return;
        fprintf(newfs, "\r\nTracing started\n");
        fflush(newfs);
        term_trace_stop(tt);
        tt->trace_fh = newfs;
#endif
}

@ @c
void
term_trace_stop (tt_term *tt)
{
#ifdef TT_DEBUG
        if (!tt->trace_fh)
                return;
        fprintf(tt->trace_fh, "\r\nTracing stopped\n");
        if (fclose(tt->trace_fh) == -1)
                warn("fclose");
        tt->trace_fh = NULL;
#endif
}

@ @c
static void
_trace_byte (tt_term *tt,
             int      dir,
             uint8_t *buf,
             size_t   len)
{
#ifdef TT_DEBUG
        if (!tt->trace_fh)
                return;
        if (dir != tt->trace_dir) {
                assert(LIO_NONE == 0);
                if (tt->trace_dir < 0)
                        tt->trace_dir = -tt->trace_dir;
                if (tt->trace_dir != LIO_NONE)
                        fprintf(tt->trace_fh, " |||\n");
                if (tt->trace_dir != dir)
                        fprintf(tt->trace_fh, (dir == LIO_IN) ? "IN> " : "<<< ");
                else
                        fprintf(tt->trace_fh, (dir == LIO_IN) ? "  > " : "  < ");
                tt->trace_dir = dir;
                tt->trace_len = 4;
        }
        for (; len; len--, buf++) {
                if (*buf < 0x20 || *buf >= 0x7f)
                        fprintf(tt->trace_fh, " %02x", *buf);
                else
                        fprintf(tt->trace_fh, "  %c", *buf);
                tt->trace_len += 3;
                if (tt->trace_len >= 68)
                        tt->trace_dir = -tt->trace_dir;
        }
        fflush(tt->trace_fh);
#endif
}

@** Test Terminal.

@(pseudo.c@>=
#include <assert.h>
#include <err.h>
#include <locale.h>
#include <stdio.h>
@#
#include <event2/event.h>
#include <X11/keysym.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
@#
#include "atlas.h"
#include "buf.h"
#include "site.h"
#include "tri.h"
#include "ucpdb.h"
#include "panel.h"
#include "tterm.h"
#include "matrix.c"

#define FONT_NAME "Liberation Mono:antialias=true:autohint=true"
#define FONT_SIZE 42

struct event_base *Loop;
tt_panel          *Panel;
mat4_t             Projection;
trix_xt           *Trix;
tt_term           *Term;

static void _pseudo_bell (tt_term *);
static bool _pseudo_dirty (trix_xt *);
static bool _pseudo_draw_cell (uint32_t, bool, vertex_xt *, size_t,
        index_xt *, size_t);
static void _pseudo_gone (tt_term *);
static void _pseudo_key (XEvent *);
static void _pseudo_refresh_display (trix_xt *, struct timespec *, bool);
static void _pseudo_xevent (trix_xt *, XEvent *);

@ @(pseudo.c@>=
int
main (int    argc,
      char **argv)
{
	setlocale(LC_CTYPE, "");

        Loop = event_base_new();
        if (!Loop)
                errx(1, "event_base_new");

        do_tri_init(Trix, Loop, _pseudo_xevent, 640, 480);
        if (!Trix)
                errx(1, "tri_init");
        else if (Trix == (trix_xt *) -1)
                return 0;
        tri_set_draw_fun(Trix, _pseudo_refresh_display);
        tri_set_xevent_mask(Trix, KeyPressMask);

        Panel = new_panel(80, 24, _pseudo_draw_cell);
        if (!Panel)
                errx(1, "new_panel");
        panelfont_xt *font = new_panel_font_name(FONT_NAME, FONT_SIZE,
                tri_xdisplay(Trix), tri_xscreen(Trix));
        if (!font)
                errx(1, "new_panel_font_name");
        tt_panel_invoke_font(Panel, font, 0);
        tt_panel_set_cursor_blink(Panel);
        tri_set_query_dirty_fun(Trix, _pseudo_dirty);

        Term = term_new(Loop, Panel);
        if (!Term)
                errx(1, "term_new");
        if (!term_attach_pty_exec(Term, "/usr/local/bin/vttest", NULL))
                errx(1, "term_attach_pty_exec");
        term_install_signal_handler(Term);
        term_set_reap_fun(Term, _pseudo_gone);
        term_set_bell_fun(Term, _pseudo_bell);
        term_set_speed(Term, 115200, 0.1);

        XMapWindow(tri_xdisplay(Trix), tri_xwindow(Trix));
        tri_go(Trix);
        term_destroy(Term);
}

@ @(pseudo.c@>=
static bool
_pseudo_dirty (trix_xt *t)
{
        assert(t == Trix);
        return 1 || tt_panel_get_dirty(Panel); // cursor moving not taken into account
}

@ @(pseudo.c@>=
static void
_pseudo_gone (tt_term *tt)
{
        assert(tt == Term);
        assert(!term_has_pty(Term));
        tri_destroy(Trix);
}

@ @(pseudo.c@>=
static void
_pseudo_bell (tt_term *tt)
{
        assert(tt == Term);
        finform("Ding dong!");
}

@ @(pseudo.c@>=
static void
_pseudo_xevent (trix_xt *t,
                XEvent  *ep)
{
        assert(t == Trix);
        switch (ep->type) {
        default:
                fcomplain("unhandled event %d", ep->type);
        case ConfigureNotify:
        case Expose:
        case MapNotify:
        case UnmapNotify:
        case ReparentNotify:
                break;
        case 0:
                if (term_has_pty(Term))
                        term_close_pty(Term);
                break;
        case KeyPress:
                _pseudo_key(ep);
                break;
        }
}

@ @(pseudo.c@>=
static void
_pseudo_refresh_display (trix_xt         *t,
                         struct timespec *at,
                         bool             filthy)
{
        size_t s[2];
        assert(t == Trix);
        tt_panel_get_pixel_size(Panel, s); /* Changes with font */
        Projection = m4_ortho(0, s[0], 0, s[1], -1, 1);
        if (!tt_panel_redraw(Panel, at, filthy))
                fcomplain("draw error");
}

@ @(pseudo.c@>=
static bool
_pseudo_draw_cell (uint32_t   t,
                   bool       strip,
                   vertex_xt *v,
                   size_t     vl,
                   index_xt  *i,
                   size_t     il)
{
        return tri_draw_vertices(Trix, strip ? GL_TRIANGLE_STRIP : GL_TRIANGLES,
                @| (float *) &Projection, t, v, vl, i, il);
}

@ The X.Org implementation of |XLookupString| in \.{KeyBind.c}
either copies the string a keysym has been rebound to into |buf|
or makes a stab at guessing which character is represented and puts
that in the first byte of |buf|. In neither case is any effort made
to ensure the string is terminated with a zero.

However, the only strings longer than one character (the low 7 or
8 bits of the keysym) that could be returned by |XLookupString| are
those already provided by this client to |XRebindKeysym|.

|Xutf8LookupString| boils away to |_XimLookupUTF8Text| in \.{imConv.c}
which {\it doesn't\/} correctly check that |nbytes| is one less
than its |BUF_SIZE| (20).

In fact the |XLookupString| from |XKBBind.c| is used. This ends up
in |XkbTranslateKeySym| where |NULL| is replaced with its own 4-byte
buffer and it's the length of what's written in there that's returned.

Why not 5? Maximum encoded length is 4 + terminator?

@(pseudo.c@>=
static void
_pseudo_key (XEvent *ep)
{
        KeySym k;
        int len;
        char buf[8];

        len = XLookupString(&ep->xkey, buf, 8, &k, NULL);
        finform("%06lx -- %u bytes%s", k, len, len ? ":" : "");
        if (len == 1 && k == XK_BackSpace)
                term_write(Term, (uint8_t *) "\177", 1);
        else if (len)
                term_write(Term, (uint8_t *) buf, len);
        else switch (k) {
        case XK_Left:
                finform("left");
                break;
        case XK_Up:
                finform("up");
                break;
        case XK_Right:
                finform("right");
                break;
        case XK_Down:
                finform("down");
                break;
        default:
                fcomplain("unexpected key 0x%lx", k);
        }
}

@** Standards Notes.

Terminals have been in development for a long time.

Turtle implements some of ECMA-48 and DEC STD 070.

Terminfo describes the capabilities that a terminal has in a
machine-accessible form. A lot of functionality described by ECMA
and DEC is assumed by terminal-using applications. Some of the
things they standardise were in flux at the time and terminfo
describes their implementation as well as extensions to the standard.

ISO-646 is ECMA-6: 7 bit character set (ie. ASCII)
ISO-2022 is ECMA-35, ANSI-X3.41: 7 \AM\ 8 bit code extension techniques
ISO-4873 is ECMA-43: 8 bit companion of ISO-646
ISO-6429 in ECMA-48, ANSI-X3.64: control functions
ISO-7498 is the OSI network model
ISO-10646 is unicode

@* ECMA-48. Section 8.3 defines the control codes. Many of these
are obsolete in modern terminals. Controls which are obsolete or
not implemented are not listed unless they are notable.

Implementation is mostly complete except tabulation and some cursor
movement commands.

ECMA-48 has 4 (?) ways to move the cursor down a line: CNL, CUD, NEL, LF.

TODO: tabulate.

\.{APC} Application Program Command. Parsed and ignored.

8.3.3 \.{BEL}. Implemented.

\.{BPH} Break Permitted Here. Obsolete.

        Superceded by unicode.

8.3.5 \.{BS} Backspace.

8.3.6 \.{CAN} Cancel.

\.{CNL} Cursor Next Line. \.{NEL} in DEC STD 070.

8.3.14 \.{CPR} Active Position Report.

8.3.15 \.{CR} Carriage Return.

        Mentions features which have been replaced with windowing,
        itself now largely obsolete.

\.{CSI} Control Sequence Introducer.

8.3.18 \.{CUB} Cursor Left.

8.3.19 \.{CUD} Cursor Down.

8.3.20 \.{CUF} Cursor Right.

8.3.21 \.{CUP} Cursor Position.

8.3.22 \.{CUU} Cursor Up.

8.3.24 \.{DA} Device Attributes. Parsed. Partially implemented.

\.{DCS} Device Control String. Parsed and ignored.

8.3.39 \.{ED} Erase in Page.

\.{ESC} Escape. Parsed.

        Many sequences unimplemented.

8.3.26 \.{DCH} Delete Character.

8.3.32 \.{DL} Delete Line.

8.3.38 \.{ECH} Erase Character.

8.3.51 \.{FF} Form Feed.

8.3.60 \.{HT} Character Tabulation. Not implemented.

8.3.62 \.{HTS} Character Tablulation Set. Not implemented.

8.3.63 \.{HVP} Character and Line Position.

8.3.64 \.{ICH} Insert Character.

8.3.67 \.{IL} Insert Line.

8.3.74 \.{LF} Line Feed.

        Mentions features which have been replaced with windowing,
        itself now largely obsolete.

8.3.86 \.{NEL} Next Line.

8.3.88 \.{NUL} Null. Implemented and ignored.

\.{OSC} Operating System Command. Parsed and ignored.

8.3.104 \.{RI} Reverse Line Feed.

\.{SD} Scroll Down. Not implemented.

        Originally moved (scrolled) the displayed data. Soon replaced
        by Index which moves the cursor down ``without moving
        horizontally''.

\.{SGR} Select Graphic Rendition. Not implemented.

\.{SM} Set Mode.

        Not implemented even to modern standards. Only four modes
        mentioned by ECMA are listed in ctlseqs.txt, including
        \.{20} (``mode two-zero'', not twenty modes) which ECMA
        says ``shall not be used''.

8.3.156 \.{TBC} Tabulation Clear.

8.3.161 \.{VT} Line Tabulation.

@* DEC. DEC-STD-070 is not a public standard but a corporate document
describing how Digital's VT line of terminals should work.

1.5 Conformance.

A level is a superset of the previous level and new functionality.

Each level consists of functions which must be included (required).

Conformance to level n:

    SHALL implement ALL functions 1..n.

    MAY NOT implement functions of level > n.

Some vt100/vt125 (vt52?) functions MAY NOT be included.

Level 1: vt102, vt125.

        Not planned: Shifting, Character sets.

        Not planned yet: SGR, double-size.

        Not discovered in docs: DECSC, DECRC.

Level 1 extensions:

        What to do about Insert/Replace Mode?

        Not planned: 132-column mode, printer port, katakana.

        Not planned yet: ReGIS, Sixel.

Level 1 preferences:

        scrolling, reverse-video, auto-repeat.

Level 2.

        Not planned: shifting, character sets.

Level 2 extensions:

        Not planned: dynamic character sets, user defined keys.


3.1.2 NRCS National Replacement Character Set.

A set of 7 bit extensions to the vt100 introduced with the vt200.
By 1989 these have been deprecated in favour of 8 bit multinational
character sets which they have subsequently been superceced by
unicode.

3.1.3 8-bit Interface Architecture.

This is also largely obsolete in favour of unicode but not quite
as much as the 7 bit raplacement character sets.

3.1.4 Seems to describe how the 8-bit interface interfaces with the
Terminal Interface Architecture, described briefly in appendix B,
which seems to more or less describe the curses library.

    B.3 GL is ASCII, GR is DEC Supplemental. "That assumption should
    be adhered to by all conforming software unless some very
    explicit, well-documented additional mechanisms are used".
    Locking shifts are explicitly called out as being for exceptional
    cases.

    B.4.1.1 Let a lower level do argument compression on the line.

    B.4.1.2 16 parameters (recommended maximum) with values up to
    at least (minimum maximum) 255 are supported universally. Larger
    bounds may not be.

    B.4.2 The line is not in a known state when it's initialised.

    This should be irrelevant for turtle even if put on a physical
    line but here is the summary for completion:

    B.4.2.1 Restore communication with XON.

    For some reason this was not included in the full sequence below.

    B.4.2.2 Terminate any existing control string with ST.

    B.4.2.2.1 Printer Control Mode. Obsolete form of directing
    output to the attached printer instead.

    B.4.2.2.2 Select Conformance Level specifices the "compliance
    level" to DEC's specification. How (or if) this implementation
    will map to DEC conformance levels is not clear.

    B.4.2.3 Full sequence:
        7-bit  ESC \ ESC [ 4 i ESC [ 6 2 " p
        8-bit     ST   CSI 4 i   CSI 6 2 " p

    B.4.2.4 Device Control String. States that control strings
    should be accepted but not acted upon in a DCS (in fact it
    states only BS, CR and LF need be "recognised and interpreted"
    within the control string and calls out some implementations
    which may act on other control characters differently. This
    implementation does not make such a distinction and copies the
    8-bit device control string as-is.

    APC, OSC and PM strings are also mentioned but as not implemented
    by any DEC product and "should not be used".

    B.4.2.5 Select 7-bit and 8-bit C1 Transmission. Described in
    detail in the "Code Extension Layer" chapter, this should have
    been specified by the conformance layer (CSI 6 2 " p means level
    2: vt200).

    Turtle will implement this feature and allow 8-bit C1 control
    sequences but it should probably not be used. This only governs
    how C1 controls are sent to the host: the terminal will always
    accept 7-bit and 8-bit C1 controls.

    B.5.2.1 "The first terminal function to be performed by [the
    operating system] is the Primary Device Attributes request".

    Ironically: "because devices are identified to the operating
    system according to functional, rather than product, characteristics".
    DA1 focuses almost entirely on product.

    B.5.2.2 DA2. Device (product) identification. For backwards
    compatibility.

    B.5.2.3 DECID deprecated. Only implemented at level 1 or 2 conformance?

    B.5.2.4 DECSCL allows the application to set terminal conformance
    level. The recommendation is to leave the terminal at its higher
    conformance level and emulate the lower level in the operating
    system.

    B.5.3 Device test is "strictly for hardware diagnostic".

    B.5.4 For some reason this is the place to mention that the
    cursor graphic can be turned off.

    B.6 Local functions (and their emulation) not revelant. This
    feature has eventually become the cooked/cbreak/raw distinction
    performed by the kernel pty driver.

    B.7.1 Auto Repeat. Recommends to handle it as-is and not disable
    it, which is convenient because X.

    B.7.2 Typing Ahead. No longer relevant.

    B.8 ReGIS. TODO much later.

3.3 describes ASCII, how to fit ASCII into 8 bits, and how to handle
control codes. This is realised as the parser taken from vt100.net.

3.4 How to treat space, delete and their GR equivalents. Delete
refers to DEC STD 070-6.

3.5 General description of escape sequences, control sequences and
control strings. This is, again, embodied in the FSM from vt100.net.

3.5.1.2.1 CAN terminates immediately "without execution", any
sequence in progress.

3.5.1.2.2 SUB can be seen as a historical analog of the unicode
replacement character but the effect now is identical to CAN.

3.5.1.2.3 ESC cancels any escape or control sequence and starts a
new one.

3.5.1.2.4 C1 controls. Implied by 3.5.1.2.3 because C1 controls
start with ESC in a 7-bit environment.

3.5.1.2.5 ST declared universal. Terminates:

    a Escape sequences in progress (implied by 3.5.1.2.3).
    b Control sequences in progress (\ditto).
    c Control string in progress. May be normal string termination
      or a variant of 3.5.1.2.5.b.
    d Nothing (ie. ignored as usual if nothing is in progress).

A note adds: products may implement non-ANSI modes that do not
recognise ST as documented exceptions.

3.5.1.3 Unimplemented functions ignored as if not received. Applies
to controls and parameters.

    Turtle is currently too strict (but only complains on its stdout).

3.5.1.4 Output routines need more control over the data (using
write(2) is too crude) to properly care for 7/8-bit C1/G2/G3 controls.

0xff explicitly called out to be ignored in sequences and other
8-bit codes are treated as equivalent 7-bit code.

SI and SO can occur in 7-bit environment: within a CSI, ST-terminated
string or between SS and its character. Will terminal implement shifts?

3.5.2 ESC sequence is ESC, 0 or more intermediates from 0x2x, a
final from 0x3x to 0x7x excluding 0x7f.

"At least three" intermediates should be accepted. More should be
detected and the sequence ignored.

3.5.3 CSI. Prefix characters are not mentioned here but intermediates
after the parameters are.

"At least three" intermediates should be accepted. More should be
detected and the sequence ignored.

3.5.3.1 Recommends minimum of 14 bits (16384) for parameter values.
Larger values SHOULD BE mapped to the largest.

Up to 16 parameters is required for compatibility. More MAY BE
ignored.

Semicolon to separate paremeters. Colon reserved for future
standardisation but if received the sequence should be accepted and
ignored. 0x3a--0x3f described in 3.5.3.4. Leading 0's ignored. 0
or absent represents a default.

ISO 6429 stamps all over that an allows 0 to be a value with knobs
on for compatibility. DEC decided bugger that but did it anyway on
a few things (see DEC STD 138-0 but I'm going to gamble that it
doesn't matter).

3.5.3.2 States again that 0 or an absence is the default value to
"one parameter or to the entire parameter string" when the paremeters
are numeric.

3.5.3.4 0x3c-0x3f in the first position means the whole string is
subject to "special interpretation" not defined in any standard.
0x3c-0x3f anywhere else causes the sequence to be ignored.

3.5.4 Control {\it Strings\/}. As opposed to control sequences.
Introduced by APC, DCS, OSC or PM. How C0 control are interpreted
is up to each mode but they are not acted upon.

Thanks to Linux controls other than ST must be used to detect string
termination.

3.5.4.3 Adds SOS to the list of control strings to understand and
ignore.

Also notes that ISO may define escape and control sequences inside
Character Strings (to the best of my knowlege it didn't).

3.5.4.4 ST but also "0x08--0x0d are valid within control strings".

Other controls than ST will terminate strings mostly for safety
reasons but "conforming software should not depend on this practice"
and notes that other control characters "may be defined to have
other meanings in the future". The recommendation is to ignore all
other controls.

The linux people did not get that memo.

3.5.4.5 Ignore the 8th bit in control strings that are going to be
ignored anyway (APC, OSC, PM). Let the consumer decide what to do
with them in DCS strings {\it except in the DCS introducer sequence\/}
where the 8th bit should also be ignored.

    Turtle does not do this correctly because I had to guess (TODO: fix).

3.5.4.6 Parse but ignore control strings.

3.5.5 An example of all of the above in VAX-11 C, dated 1984--1989.

3.6 Character sets. 8-bit has two graphic regions, GL and GR.
Terminal has four sets of graphic characters, G0, G1, G2 and G3.
Character sets may be designated to any of the four sets and each
set may be invoked onto GL or GR, by {\it shifting\/}.

This functionality has been largely or entirely superceded by unicode.

LS0--LS3 lock the corresponding G-table into GL (LS0 and LS1 were
previously Shift In and Shift Out but the meaning has not changed).

LS1R--LS2R lock the corresponding G-table into GR.

Unaffected by shifting:

    C0 and C1 controls.
    SP and DEL or 0xa0/0xff with GL or GR has a 94-character set.
    Contents of an escape sequence.
    The byte following SS2 or SS3.

Speaking of, SS2 and SS3 (Single Shift) invoke G2 or G3 into GL.

G0 cannot have a 96-character set (SP and DEL always SP and DEL).

3.6.3 User Preference Supplemental Set (UPSS).

Superceded by Private Use Areas in unicode. Support is required for
conformance level 3 at least to switch between ISO Latin-1 and DEC
Supplemental. Refers to DEC STD 070-6.

3.6.4.1 Level 1, G0-G3 are all ASCII. G0 into GL. 7 bits only.

3.6.4.2 Level 2 (without 8-bit extension). ASCII in G0, G1. DEC
Supplemental in G2, G3. G0 in GL, G2 in GR.

3.6.4.3 Level 3 (or level 2 with 8-bit). ASCII in G0, G1. UPSS in
G2, G3. G0 in GL, G2 in GR.

Level 3 also recognises a reset from ISO-4873.

3.6.4.4 NRCS (national replacement). Shifting may be necessary/useful
for graphics but this one's definitely taken over by unicode.

3.8 Specifies how to indicate conformance to ISO-4873 (ECMA-43)
which extends the Gn system in ways we probably don't need. Note
that ISO-4873 levels (1--3) are different from DEC conformance
levels (1--3).

3.8.2 S7C1T/S8C1T Select [78]-bit C1 Transmission. Needed. Defaults
to 7-bit.

3.8.3 Shifting. SI/LS0, SO/LS1, LS[23], LS[123]R, SS[23]

4.3 State Descriptions.

4.4 Device Initialisation.

4.5 Control Functions (DA).

Has list of extensions to return in DA1 (CSI Ps c) on pp. 4-19.

4.5.X DECSCL Select Conformance Level.

4.5.X DA2 Request/report device identification.

4.5.X DA3 Request terminal unit identification. Xterm sends zeros.

4.5.DECID Request device identification code.

4.5.DECSR Secure Reset. May be useful after password input. Not in
ctlseqs (thus not in Xterm?).

4.5.DECSTR Soft Terminal Reset.

5.4.1 Logical Display. 80x24 or 132x24. Starts at 1x1 in top left.

No windows extensions.

Glyph location corresponds to lower-left corner of co-ordinates
(this matters for double width/height characters).

5.4.2 DECTCM Text Cursor Mode (terminfo civis, cvvis, cnorm).

5.4.3 Margins/Scrolling.

5.4.3.DECSTBM, Set Top and Bottom Margins. Terminfo csr.

5.4.3.DECSLRM, Set Left and Right Margins. Terminfo smglp, smglr, smgrp.

5.4.3.DECLRMM, Left/Right Margin Mode. Terminfo mgc.

5.4.3.DECOM, Origin Mode. Not used by Xterm terminfo but part of DECSTR.

5.4.3.ECSCLM, Scrolling Mode.

5.4.3.IND. Move down with scrolling. Ignores New Line Mode unlike LF (except on vt100 and vt125, where they're identical).

5.4.3.RI. Reverse index. Unavailable on vt100 or vt125 or non-quirky?

5.4.3.DECFI. Forward (right) Index.

5.4.3.DECBI. Backward (left) Index.

5.4.4 Cursor Movement.

CUB/CUD/CUF/CUP/CUU, CR, LF from ECMA-48.

5.4.4.CPR, Cursor Position Report. Part of u6/u7/u8/u9. According
to Xterm terminfo for tack and/or ANSI ENQ.

5.4.4.DECXCPR, Extended Cursor Position Report.

5.4.4.LNM, New Line Mode.

Whether receipr of LF, VT and FF do their own thing (not set) or
also do CR (set). Also return key will {\it send\/} CR LF.

Deprecated (should not be used == off?) for conforming software.

Not in Xterm terminfo.

5.4.4.VT, Vertical Tab. Useful on line printers. LF on screen.

5.4.4.FF, Form Feed. Likewise.

5.4.4.BS, Back Space. Unaffected by Auto Wrap Mode. Cursor never
advances to previous line.

Erasure, if done, is not done here.

5.4.4.NEL, Next Line. Explicitly LF in New Line Mode or CR LF.

5.4.5 Tabs. TBD.

@* Terminfo.

The terminfo file describes which features or bugs a terminal has
and how to activate certain features. Applications use the terminfo
database and also make assumptions that the terminal is some degree
of ECMA-48, DEC VT and xterm compatible.

The description of the terminfo file that accompanies this terminal
continues in \.{turtle.info}.

@* ctlseqs.

Documentation on controls comes largely from
https://invisible-island.net/xterm/ctlseqs/ctlseqs.txt

@* Unicode.

( Summary of 5000 years of writing and all the damage it's caused )

@* Keyboard.

This is exclusively from \DEC{6}. Most is obsoleted by X and/or unicode.

Two modes: typewriter, data processing (switches between keycaps).

See also DEC-STD-107 (level 1).
DEC-STD-169 level 2 adds character sets, keypad and function keys.
Level 3 doesn't add a great deal.

Level 1 conformance:

All 1 keys, no other, 2 or 3 keys.
DECARM, DECCKM, DECKPAM/DECKPNM, Line Feed/New Line Mode.
KAM (Keyboard Action Mode)
Caps lock, maybe shift lock.
Enable 8 bit transmission.

Level 2 conformance:

All 2 keys, no others.
Optional 8 bit transmission.
Optional NRCS.
L1 controls
KAM + DECSCL
Keyclick, caps/shift lock (caps lock by factory default!), optional bell, etc.

Level 3 conformance:

Level 2 plus:
8 bit required.
DECKBUM
DECSCL again
Optional composing keys

Level 1 op:

No 8th bit, or 8th bit unset.
No E1-E6.
No F6-F10.
F11 => ESC 0x1b
F12 => BS
F13 => LF
No F14-F20.
Really no 8th bit (beep if the user tries!)

Level 2/3 op:

All keys.
F11-13 transmit as usual.
All valid compose sequences OK
"No additional Compose sequences"

But in a 7-bit environment:
No composing or transmission with 8th bit.

"Delete" where PC has Backspace (but with the same long left arrow)
Caps lock is two keys: ctrl and lock.
20 F-keys (not called F).
Keypad has four function keys, normal height [-,] in place of +.
F1 == Hold
F2 == Print
F3 == Setup
F4
F5 == Break
F11 == ESC
F12 == BS
F13 == LF

Modes: KAM, Repeat, Click, Margin bell, Warning bell.

9 keystroke (not byte) ``silo'' (buffer).

\DEC{6.6.2}, \DECp{6-38} Keyboard Action Mode locks the keyboard on the SM control
or a full silo.

Locked keyboard:
    wait indicator
    All but local keys (F1-F5) disabled
    F5/break may be disabled elsewise
    Unlocks for set-up (unless unlocked by the user)

Unlocks for:
    silo space (if not KAM)
    RM KAM
    Select conformance level (DECSCL)
    Reset (RIS)
    Self test
    Enter Set-Up in level 1 (doesn't clear in other levels)
    In Set-Up: Clear Comm or Reset Terminal
    Soft reset (DECSTR)

KAM Keyboard Action Mode CSI 2 [hl].

\DEC{6.6.3}, \DECp{6-40}. DECARM Auto Repeat Mode. CSI ? 8 [hl].

Non-repeating keys:
    Return, Escape, F1-5, Ctrl, Shift, Lock, Compose, diacrits, 'in a ... compose sequence'.

Repeats different keys at different rates and depending on connection speed (baud rate).

6.6.4.1 Visual indicator of HOLD SCREEN (XON/XOFF?).

6.6.4.2 \ditto\ lock key

6.6.4.3 \ditto\ compose key

6.6.4.4 Keyboard Lock (silo full). This {\it does\/} mention XOFF.
        Locks if mid-compose.

6.6.5 Bells. Warning, Margin.

Warning max 4-5 per second. Bells queued until buffer (10) is full then discarded.

The margin bell rings when moving to within 8 characters of the
right margin. This might be an interesting anachronism but useless
today.

6.6.5.3 Keyclick. Once per repeat. Warning bell takes precedence.

6.7 Affecting modes:

Emulation (of vt52, vt100, vt300).
C1 transmission
Character set (keyboard encoding)
UPSS user graphic character set
Keyboard Dialect
Keyboard Usage (data/typewriter)
Host (line) environment.
Key lock
BackArrow key mode (not sure when this is from --- 1988? Already disagreements about BS/DEL).
Disable break.
Disable compose key.
Various other things replaced by xkeymap.
Cursor Key Mode: ANSI vs. Application Function Codes.
Numeric Keypad Mode: Numeric vs. Application Mode.

\.{S7C1T}, \.{S8C1T}. \DEC{6.7.3}, \DECp{6-50}. Select [78]-bit C1
Transmission. \.{ESC SP F} (7), \.{ESC SP G} (8).

TODO: Why bytes 6 and 7 (F, G) not 7 and 8?

6.7.4 Character Set Mode selects between 7- and permitted to send
8-bit characters. May be where we put the unicode toggle, if it's
toggleable. CSI ? 4 2 [hl]. h=7, l=8.

6.7.5 Keyboard Usage Mode. ie. Local language. Another good choice
for the unicode toggle.

DECKBUM (data/typewriter) described on \DECp{6-57}.

6.7.6 Keyboard Dialect. Another local language control. Another
choice for unicode.

6.8.1 Cursor Key Mode (how to transmit cursor keys). DECCKM.

6.9.1 \ditto\ keypad. DECKPNM. DECKPAM. DECNKM.

6.11 Application Function Keys. Lists codes to send for function
keys (here defines codes ending \.~). Only lists F6-F20.

6.12 Local Function Keys. See \DEC{D} now that there's no such thing.

6.12.1 Hold Screen Key. Different from XON/XOFF. See \DEC{12}.

6.12.2 Print Screen Key. Unimplemented for now. See \DEC{7}.

6.12.3 Set-Up Key. Unimplemented. Covered by configuration, CLI and
a GUI/API.

6.12.4 More fancy keys not useful in X.

6.12.5 Break key. Very magic. Refers to \DEC{3}, \DEC{4} and \DEC{12}.
Probably useless in X.

6.13 Main key array --- specials. X.

6.13.4 Ctrl-space will send a NUL.

6.13.5 Return sends CRLF if NLM. Much detail here for emulation.

6.13.7 Delete key usually causes 0x7f, also aborts compose. VT300
permits Set-Up to toggle \.{<X]} between 0x08 and 0x7f.

Trivia: This was a settable option on the VT320 released in 1987.
The product was obviously in development long before that and the
argument itself must have been fairly old hat by then. Linux was
released in 1991. It did not originate the argument.

6.14 Maaaany pages of keyboard layouts.

6.15 Maaaany pages of compose sequences.

6.16 Specifies control keys to sequences, alphabet plus:
    \.{\^SP}: 00
    \.{\^2}: 00
    \.{\^3}: 1b (after z: 1a)
    \.{\^[}: 1b
    \.{\^4}: 1c
    \.{\^\\}: 1c
    \.{\^5}: 1d
    \.{\^]}: 1d
    \.{\^6}: 1e
    \.{\^~}: 1e
    \.{\^7}: 1f
    \.{\^?}: 1f
    \.{\^8}: 7f
    \.{\^TAB}: 09
    \.{\^DET}: 18

    \.{\^Q}, \.{\^S} only if XOFF handling is disabled.

Shift ignored.

6.17 Summary.

More from xterm faq
    F1--F4 \AM\ PF1--PF4, ESC O P--S
    No break key (sends "break")
    All functions are ESC O x for any x or CSI Pn ~.

@** Index.
